﻿# jdm7dv's Research Environment

The research environment contains Microsoft's Research Singularity RDK now Verve.
And my courses and sandboxed environment.

# Courses
### M.I.T.

1. Advanced Calculus for Engineers
2. AI
3. Computational Science
4. DSP
5. Effective Programming in C and C++
6. Human Origins and Evolution
7. Linear Algebra
8. Microscale Engineering for the Life Sciences
9. Music and Technology
10. Patents, Copyrights, and the Law of Intellectual Property
11. Practical Programming in C
12. Psychology
13. Quantum Physics I
14. Regenerative Medicine
15. Software Engineering
16. String Theory
17. Systems and Synthetic Biology
18. Systems Biology

My personal operating systems engineering course (changing all the time)

### Stanford

1. Quantum Computing for Scientists and Engineers 

## The jdm7dv directory 

Which contains various useful forks and projects.   

I am only one person and try to study everyday. I believe in a life dedicated to learning. 

