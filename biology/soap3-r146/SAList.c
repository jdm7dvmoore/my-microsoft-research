/*
 *
 *    SAList.c
 *    Soap3(gpu)
 *
 *    Copyright (C) 2011, HKU
 *
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU General Public License
 *    as published by the Free Software Foundation; either version 2
 *    of the License, or (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include "SAList.h"

SAList* SAListConstruct() {
      SAList* sa_list;
      sa_list = (SAList *) malloc(sizeof(SAList));
      sa_list->sa = (PESRAAlignmentResult*) malloc(sizeof(PESRAAlignmentResult) * INITIAL_SIZE);
      sa_list->curr_size = 0;
      sa_list->available_size = INITIAL_SIZE;
      return sa_list;
}

void SAListReset(SAList* sa_list) {
      sa_list->curr_size = 0;
}

void SAListFree(SAList* sa_list) {
      free(sa_list->sa);
      free(sa_list);
}

void addSAToSAList(SAList* sa_list, unsigned int l_value, unsigned int r_value, unsigned char strand, unsigned char mismatchCount) {
      if (sa_list->curr_size >= sa_list->available_size) {
            // increase the size of the array by double
            unsigned int new_size = sa_list->available_size * 2;
            PESRAAlignmentResult* new_sa = (PESRAAlignmentResult*) malloc(sizeof(PESRAAlignmentResult) * new_size);
            memcpy(new_sa, sa_list->sa, sizeof(PESRAAlignmentResult)*sa_list->available_size);
            free(sa_list->sa);
            sa_list->sa = new_sa;
            sa_list->available_size = new_size;
      }
      // add the values to the array
      sa_list->sa[sa_list->curr_size].saIndexLeft = l_value;
      sa_list->sa[sa_list->curr_size].saIndexRight = r_value;
      sa_list->sa[sa_list->curr_size].strand = strand;
      sa_list->sa[sa_list->curr_size].mismatchCount = mismatchCount;
      sa_list->curr_size++;
}


OCCList* OCCListConstruct() {
      OCCList* occ_list;
      occ_list = (OCCList *) malloc (sizeof(OCCList));
      occ_list->occ = (SRAOccurrence*) malloc (sizeof(SRAOccurrence) * INITIAL_SIZE);
      occ_list->curr_size = 0;
      occ_list->available_size = INITIAL_SIZE;
      return occ_list;
}

void OCCListReset(OCCList* occ_list) {
      occ_list->curr_size = 0;
}

void addToOCCList(OCCList* occ_list, unsigned int pos, char strand, char mismatchCount) {
      if (occ_list->curr_size >= occ_list->available_size) {
            // increase the size of the array by double
            unsigned int new_size = occ_list->available_size * 2;
            SRAOccurrence* new_occ = (SRAOccurrence*) malloc (sizeof(SRAOccurrence) * new_size);
            memcpy(new_occ, occ_list->occ, sizeof(SRAOccurrence)*occ_list->available_size);
            free(occ_list->occ);
            occ_list->occ = new_occ;
            occ_list->available_size = new_size;
      }
      // add the values to the array
      occ_list->occ[occ_list->curr_size].ambPosition = pos;
      occ_list->occ[occ_list->curr_size].strand = strand;
      occ_list->occ[occ_list->curr_size].mismatchCount = mismatchCount;
      occ_list->curr_size++;
}

void OCCListFree(OCCList* occ_list) {
      free(occ_list->occ);
      free(occ_list);
}
