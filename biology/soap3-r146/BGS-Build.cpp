/*
 *
 *    BGS-Build.cpp
 *    Soap3(gpu)
 *
 *    Copyright (C) 2011, HKU
 *
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU General Public License
 *    as published by the Free Software Foundation; either version 2
 *    of the License, or (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "BWT.h"

void printUsage() {

  printf("\n");
  printf("  BGS-Build -- 2BWT Index Enhancer -- beta -- Usage Guide\n");
  printf("\n");
  printf("  ./BGS-Build <2bwtIndex>\n");
  printf("\n");
  printf("      2bwtIndex : The bwt index built by 2bwt-builder.\n");
  printf("\n");
  
}

int main(int argc, char **argv) {

	if (argc!=2) {
		fprintf(stderr,"Invalid number of command-line arguments\n");
		printUsage();
		return 1;
	}

	char *bwtCodeFileName = (char*)malloc(strlen(argv[1])+5);
	char *occValueFileName = (char*)malloc(strlen(argv[1])+5);
	char *revBwtCodeFileName = (char*)malloc(strlen(argv[1])+15);
	char *revOccValueFileName = (char*)malloc(strlen(argv[1])+15);

	char *gpuOccValueFileName = (char*)malloc(strlen(argv[1])+25);
	char *revGpuOccValueFileName = (char*)malloc(strlen(argv[1])+35);

	strcpy(bwtCodeFileName, argv[1]);
	strcpy(bwtCodeFileName + strlen(argv[1]), ".bwt");
	strcpy(occValueFileName, argv[1]);
	strcpy(occValueFileName + strlen(argv[1]), ".fmv");
	strcpy(revBwtCodeFileName, argv[1]);
	strcpy(revBwtCodeFileName + strlen(argv[1]), ".rev.bwt");
	strcpy(revOccValueFileName, argv[1]);
	strcpy(revOccValueFileName + strlen(argv[1]), ".rev.fmv");
	
	strcpy(gpuOccValueFileName, argv[1]);
	strcpy(gpuOccValueFileName + strlen(argv[1]), ".fmv.gpu");
	strcpy(revGpuOccValueFileName, argv[1]);
	strcpy(revGpuOccValueFileName + strlen(argv[1]), ".rev.fmv.gpu");

	BWT * bwt = BWTLoad(bwtCodeFileName, occValueFileName, NULL, NULL, NULL, NULL);
	free(bwtCodeFileName);
	free(occValueFileName);

	BWT * revBwt = BWTLoad(revBwtCodeFileName, revOccValueFileName, NULL, NULL, NULL, NULL);
	free(revBwtCodeFileName);
	free(revOccValueFileName);

	// Start building the BGS-Index
	
	printf("[BGS-Build] Writing GPU occurrence table to %s..\n",gpuOccValueFileName);
	FILE * fout = (FILE*)fopen64(gpuOccValueFileName, "wb");
	if (fout == NULL) {
		fprintf(stderr, "[BGS-Build] Cannot open output file!\n");
		exit(1);
	}
	fwrite(&bwt->inverseSa0, sizeof(unsigned int), 1,fout);
	fwrite(bwt->cumulativeFreq + 1, sizeof(unsigned int), ALPHABET_SIZE, fout);
	unsigned int numOfOccValue = (bwt->textLength + 128 - 1) / 128 + 1;
	
	int i,j;
	unsigned int x;
	int adj = 0;
	for (x=0;x<numOfOccValue;x++) {
		unsigned int sumTmp = 0;
		for (j=0;j<ALPHABET_SIZE;j++) {
			if (x*128 > bwt->inverseSa0) {adj=1;}
			unsigned int tmp = BWTOccValue(bwt, x*128+adj, j);
			
			sumTmp+=tmp;
			tmp+=bwt->cumulativeFreq[j];
			fwrite(&tmp,sizeof(unsigned int),1,fout);
		}
		if (sumTmp % 128 !=0 ) {fprintf(stderr,"[BGS-Build] ERROR %u %u\n",x,sumTmp);return 0;}
	}
	fclose(fout);
	
	printf("[BGS-Build] Writing reversed GPU occurrence table to %s..\n",revGpuOccValueFileName);
	fout = (FILE*)fopen64(revGpuOccValueFileName, "wb");
	if (fout == NULL) {
		fprintf(stderr, "[BGS-Build] Cannot open output file!\n");
		exit(1);
	}
	fwrite(&revBwt->inverseSa0, sizeof(unsigned int), 1,fout);
	fwrite(revBwt->cumulativeFreq + 1, sizeof(unsigned int), ALPHABET_SIZE, fout);
	adj = 0;
	for (x=0;x<numOfOccValue;x++) {
		unsigned int sumTmp = 0;
		for (j=0;j<ALPHABET_SIZE;j++) {
			if (x*128 > revBwt->inverseSa0) {adj=1;}
			unsigned int tmp = BWTOccValue(revBwt, x*128+adj, j);
			
			sumTmp+=tmp;
			tmp+=revBwt->cumulativeFreq[j];
			fwrite(&tmp,sizeof(unsigned int),1,fout);
		}
		if (sumTmp % 128 !=0 ) {fprintf(stderr,"[BGS-Build] ERROR %u %u\n",x,sumTmp);return 0;}
	}
	fclose(fout);
	printf("[BGS-Build] Building of BGS-Index has completed.\n");
	
	BWTFree(bwt);
	BWTFree(revBwt);

	printf("[BGS-Build] GPUBWT terminated.\n");

	return 0;
}
