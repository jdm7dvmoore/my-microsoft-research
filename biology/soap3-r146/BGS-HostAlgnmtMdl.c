/*
 *
 *    BGS-HostAlgnmtMdl.c
 *    Soap3(gpu)
 *
 *    Copyright (C) 2011, HKU
 *
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU General Public License
 *    as published by the Free Software Foundation; either version 2
 *    of the License, or (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include "BGS-HostAlgnmtMdl.h"

void DebugPrintModel(SRAModel * SRAMismatchModel) {
	int i,j,k;
	printf("Debug printing alignment model:\n");
	for (j=0;j<MAX_NUM_OF_SRA_CASES;j++) {
		if (SRAMismatchModel->cases[j].id==0) continue;
		printf("    Case %u id=%u:\n", j, SRAMismatchModel->cases[j].id);
		for (k=0;k<MAX_NUM_OF_SRA_STEPS;k++) {
			int flag = 0;
			switch (SRAMismatchModel->cases[j].steps[k].type) {
				case SRA_STEP_TYPE_COMPLETE:
					flag = 1;
					break;
				case SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP:
					printf("        SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP\n");
					break;
				case SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP:
					printf("        SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP\n");
					break;
				case SRA_STEP_TYPE_BACKWARD_ONLY_BWT:
					printf("        SRA_STEP_TYPE_BACKWARD_ONLY_BWT\n");
					break;
				case SRA_STEP_TYPE_BI_DIRECTIONAL_BWT:
					printf("        SRA_STEP_TYPE_BI_DIRECTIONAL_BWT\n");
					break;

			}
			if (flag == 1) break;
			printf("        From %d to %d allowing mismatch %d-%d\n", 
			SRAMismatchModel->cases[j].steps[k].start,
			SRAMismatchModel->cases[j].steps[k].end,
			SRAMismatchModel->cases[j].steps[k].MinError,
			SRAMismatchModel->cases[j].steps[k].MaxError,
			SRAMismatchModel->cases[j].steps[k].ceThreshold);
		}
		
		// Print left aligned for CE
		//for (k=0;k<SRAMismatchModel->ModelReadLength;k++) {
		//	printf("%d ",SRAMismatchModel->cases[j].leftMostAligned[k]);
		//}
		//printf("\n");
                    
		
	}
}


void SRAModelFree(SRAModel * sraModel) {
	if (sraModel==NULL) return;
	free(sraModel);
}


int SRAEditCasesPopulate(SRAModel * SRAMismatchModel, int nextCase, 
							 unsigned int ReadLength, int MaxError, int smallErrorFirst, 
							  BWT * bwt, BWT * rev_bwt) {
	
	int firstCase = nextCase;
	int j,k;
	unsigned int region_m1a_start = 0;
	unsigned int region_m1a_end = ReadLength - (ReadLength * 0.5f);
	unsigned int region_m1b_start = region_m1a_end + 1;
	unsigned int region_m1b_end = ReadLength - 1;

	unsigned int region_m2a_start = 0;
	unsigned int region_m2a_end = (ReadLength * 0.3f);
	unsigned int region_m2b_start = region_m2a_end + 1;
	unsigned int region_m2b_end = region_m2b_start + (ReadLength * 0.3f) -1;
	unsigned int region_m2c_start = region_m2b_end + 1;
	unsigned int region_m2c_end = ReadLength - 1;
	
	unsigned int region_m3a_start = 0;
	unsigned int region_m3a_end = (ReadLength * 0.25f);
	unsigned int region_m3b_start = region_m3a_end + 1;
	unsigned int region_m3b_end = region_m3b_start + (ReadLength * 0.25f) -1;
	unsigned int region_m3c_start = region_m3b_end + 1;
	unsigned int region_m3c_end = region_m3c_start + (ReadLength * 0.25f) -1;
	unsigned int region_m3d_start = region_m3c_end + 1;
	unsigned int region_m3d_end = ReadLength - 1;

	unsigned int region_m4a_start = 0;
	unsigned int region_m4a_end = (ReadLength * 0.2f);
	unsigned int region_m4b_start = region_m4a_end + 1;
	unsigned int region_m4b_end = region_m4b_start + (ReadLength * 0.2f) -1;
	unsigned int region_m4c_start = region_m4b_end + 1;
	unsigned int region_m4c_end = region_m4c_start + (ReadLength * 0.2f) -1;
	unsigned int region_m4d_start = region_m4c_end + 1;
	unsigned int region_m4d_end = region_m4d_start + (ReadLength * 0.2f) -1;
	unsigned int region_m4e_start = region_m4d_end + 1;
	unsigned int region_m4e_end = ReadLength - 1;

	unsigned int region_m5a_start = 0;
	unsigned int region_m5a_end = (ReadLength * 0.16f);
	unsigned int region_m5b_start = region_m5a_end + 1;
	unsigned int region_m5b_end = region_m5b_start + (ReadLength * 0.16f) -1;
	unsigned int region_m5c_start = region_m5b_end + 1;
	unsigned int region_m5c_end = region_m5c_start + (ReadLength * 0.16f) -1;
	unsigned int region_m5d_start = region_m5c_end + 1;
	unsigned int region_m5d_end = region_m5d_start + (ReadLength * 0.16f) -1;
	unsigned int region_m5e_start = region_m5d_end + 1;
	unsigned int region_m5e_end = region_m5e_start + (ReadLength * 0.16f) -1;
	unsigned int region_m5f_start = region_m5e_end + 1;
	unsigned int region_m5f_end = ReadLength - 1;

	if ((MaxError>=0 && smallErrorFirst==1) || MaxError==0) {
		SRAMismatchModel->cases[nextCase].id=1;
		SRAMismatchModel->cases[nextCase].MaxError=0;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = ReadLength - 1;
		SRAMismatchModel->cases[nextCase].steps[0].end   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].ceThreshold   = 1;
		SRAMismatchModel->cases[nextCase].steps[0].ceStart       = 10;
		SRAMismatchModel->cases[nextCase].steps[0].ceEnd         = 34;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;
		
		SRAMismatchModel->cases[nextCase].id=1;
		SRAMismatchModel->cases[nextCase].type=SRA_CASE_TYPE_NEXT_STAGE;
		nextCase++;
	}

	if ((MaxError>=1 && smallErrorFirst==1) || MaxError==1) {
		SRAMismatchModel->cases[nextCase].id=11;
		SRAMismatchModel->cases[nextCase].MaxError=1;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m1b_end;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m1b_start;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;

		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE_BOUNDARY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m1a_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m1a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		//SRAMismatchModel->cases[nextCase].steps[1].ceStart       = region_m1a_end-2;
		//SRAMismatchModel->cases[nextCase].steps[1].ceEnd         = region_m1a_end;
		//SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=12;
		SRAMismatchModel->cases[nextCase].MaxError=1;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m1a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m1a_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m1b_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m1b_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		//SRAMismatchModel->cases[nextCase].steps[1].ceStart       = region_m1b_start;
		//SRAMismatchModel->cases[nextCase].steps[1].ceEnd         = region_m1b_start+2;
		//SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 3;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;//*/
		nextCase++;
		
		SRAMismatchModel->cases[nextCase].id=1;
		SRAMismatchModel->cases[nextCase].type=SRA_CASE_TYPE_NEXT_STAGE;
		nextCase++;
	}
	if ((MaxError>=2 && smallErrorFirst==1) || MaxError==2) {
		SRAMismatchModel->cases[nextCase].id=21;
		SRAMismatchModel->cases[nextCase].MaxError=2;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m2c_end;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m2c_start;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE_BOUNDARY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m2b_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m2a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].ceStart       = region_m2b_end-1;
		SRAMismatchModel->cases[nextCase].steps[1].ceEnd         = region_m2b_end;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 5;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=22;
		SRAMismatchModel->cases[nextCase].MaxError=2;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = 0;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m2a_end;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m2b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m2c_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m2c_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].ceStart       = region_m2c_start;
		SRAMismatchModel->cases[nextCase].steps[1].ceEnd         = region_m2c_start+1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 5;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=23;
		SRAMismatchModel->cases[nextCase].MaxError=2;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = 0;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m2a_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m2b_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m2b_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m2c_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m2c_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=24;
		SRAMismatchModel->cases[nextCase].MaxError=2;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m2b_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m2b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE_BOUNDARY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m2c_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m2c_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m2a_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m2a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;
		
		SRAMismatchModel->cases[nextCase].id=1;
		SRAMismatchModel->cases[nextCase].type=SRA_CASE_TYPE_NEXT_STAGE;
		nextCase++;
	}
	if ((MaxError>=3 && smallErrorFirst==1) || MaxError==3) {
		SRAMismatchModel->cases[nextCase].id=31;
		SRAMismatchModel->cases[nextCase].MaxError=3;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m3d_end;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m3c_start;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE_BOUNDARY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m3b_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m3a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=32;
		SRAMismatchModel->cases[nextCase].MaxError=3;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m3a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m3b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m3c_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m3d_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=33;
		SRAMismatchModel->cases[nextCase].MaxError=3;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m3a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m3a_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE_BOUNDARY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m3b_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m3b_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m3c_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m3d_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=34;
		SRAMismatchModel->cases[nextCase].MaxError=3;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m3b_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m3b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m3a_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m3a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m3c_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m3d_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=35;
		SRAMismatchModel->cases[nextCase].MaxError=3;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m3c_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m3c_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m3d_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m3d_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m3b_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m3a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=36;
		SRAMismatchModel->cases[nextCase].MaxError=3;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m3d_end;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m3d_start;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE_BOUNDARY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m3c_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m3c_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m3b_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m3a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;
		
		SRAMismatchModel->cases[nextCase].id=1;
		SRAMismatchModel->cases[nextCase].type=SRA_CASE_TYPE_NEXT_STAGE;
		nextCase++;
	}
	if ((MaxError>=4 && smallErrorFirst==1) || MaxError==4) {
		SRAMismatchModel->cases[nextCase].id=41;
		SRAMismatchModel->cases[nextCase].MaxError=4;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE_BOUNDARY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 4;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=42;
		SRAMismatchModel->cases[nextCase].MaxError=4;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 4;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=43;
		SRAMismatchModel->cases[nextCase].MaxError=4;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4a_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE_BOUNDARY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4b_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;


		nextCase++;
		SRAMismatchModel->cases[nextCase].id=44;
		SRAMismatchModel->cases[nextCase].MaxError=4;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4b_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4a_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;


		nextCase++;
		SRAMismatchModel->cases[nextCase].id=45;
		SRAMismatchModel->cases[nextCase].MaxError=4;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4a_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4b_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;


		nextCase++;
		SRAMismatchModel->cases[nextCase].id=46;
		SRAMismatchModel->cases[nextCase].MaxError=4;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4b_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4a_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=47;
		SRAMismatchModel->cases[nextCase].MaxError=4;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4b_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4a_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE_BOUNDARY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m4c_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[3].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[3].start = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[3].end   = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[3].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[3].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[4].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=48;
		SRAMismatchModel->cases[nextCase].MaxError=4;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4c_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4b_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4b_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m4a_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[3].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[3].start = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[3].end   = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[3].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[3].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[4].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=49;
		SRAMismatchModel->cases[nextCase].MaxError=4;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4d_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4e_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 3;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;


		nextCase++;
		SRAMismatchModel->cases[nextCase].id=410;
		SRAMismatchModel->cases[nextCase].MaxError=4;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4e_start;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE_BOUNDARY;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4d_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 3;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;

		
		SRAMismatchModel->cases[nextCase].id=1;
		SRAMismatchModel->cases[nextCase].type=SRA_CASE_TYPE_NEXT_STAGE;
		nextCase++;
	}
	if ((MaxError>=5 && smallErrorFirst==1) || MaxError==5) {
		// ___________________________________________
		// |_____________5_____________|______0______|
		// |      |      |      |      |      |      |	
		SRAMismatchModel->cases[nextCase].id=51;
		SRAMismatchModel->cases[nextCase].MaxError=5;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE_BOUNDARY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 5;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;

		// ___________________________________________
		// |_____________0_____________|______5______|
		// |      |      |      |      |      |      |
		nextCase++;
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].MaxError=5;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 5;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;

		// ___________________________________________
		// |______0______|______1______|______4______|
		// |      |      |      |      |      |      |
		nextCase++;
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].MaxError=5;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE_BOUNDARY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 4;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;

		// ___________________________________________
		// |______1______|______0______|______4______|
		// |      |      |      |      |      |      |
		nextCase++;
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].MaxError=5;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 4;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;










		// ___________________________________________
		// |______ ______4______ ______|__0___|__1___|
		// |      |      |      |      |      |      |
		nextCase++;
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].MaxError=5;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5e_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5f_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 4;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;
		
		// ___________________________________________
		// |______ ______4______ ______|__1___|__0___|
		// |      |      |      |      |      |      |
		nextCase++;
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].MaxError=5;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5f_start;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE_BOUNDARY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5e_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 4;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;












		// ___________________________________________
		// |______0______|______2______|______3______|
		// |      |      |      |      |      |      |
		nextCase++;
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].MaxError=5;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;

		// ___________________________________________
		// |______2______|______0______|______3______|
		// |      |      |      |      |      |      |
		nextCase++;
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].MaxError=5;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;

		// ___________________________________________
		// |___0__|__1___|______1______|______3______|
		// |      |      |      |      |      |      |
		nextCase++;
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].MaxError=5;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5a_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE_BOUNDARY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5b_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[3].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[3].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[3].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[3].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[3].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[4].type  = SRA_STEP_TYPE_COMPLETE;

		// ___________________________________________
		// |___1__|__0___|______1______|______3______|
		// |      |      |      |      |      |      |
		nextCase++;
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].MaxError=5;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5b_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5a_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[3].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[3].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[3].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[3].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[3].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[4].type  = SRA_STEP_TYPE_COMPLETE;














		// ___________________________________________
		// |______0______|______3______|______2______|
		// |      |      |      |      |      |      |
		nextCase++;
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].MaxError=5;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 3;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;
		// ___________________________________________
		// |______3______|______0______|______2______|
		// |      |      |      |      |      |      |
		nextCase++;
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].MaxError=5;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 3;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;

		// ___________________________________________
		// |__0___|__1___|______2______|______2______|
		// |      |      |      |      |      |      |
		nextCase++;
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].MaxError=5;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5a_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5b_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[3].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[3].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[3].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[3].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[4].type  = SRA_STEP_TYPE_COMPLETE;
		
		// ___________________________________________
		// |__1___|__0___|______2______|______2______|
		// |      |      |      |      |      |      |
		nextCase++;
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].MaxError=5;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5b_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5a_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[3].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[3].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[3].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[3].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[4].type  = SRA_STEP_TYPE_COMPLETE;

		// ___________________________________________
		// |______2______|__0___|__1___|______2______|
		// |      |      |      |      |      |      |
		nextCase++;
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].MaxError=5;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5c_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE_BOUNDARY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5d_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[3].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[3].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[3].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[3].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[4].type  = SRA_STEP_TYPE_COMPLETE;

		// ___________________________________________
		// |______2______|__1___|__0___|______2______|
		// |      |      |      |      |      |      |
		nextCase++;
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].MaxError=5;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5d_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5c_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[3].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[3].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[3].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[3].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[4].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;
		
		SRAMismatchModel->cases[nextCase].id=1;
		SRAMismatchModel->cases[nextCase].type=SRA_CASE_TYPE_NEXT_STAGE;
		nextCase++;
	}

	return nextCase;
}

int SRAMismatchCasesPopulate(SRAModel * SRAMismatchModel, int nextCase, 
							 unsigned int ReadLength, int MaxError, int smallErrorFirst, 
							  BWT * bwt, BWT * rev_bwt) {
	
	int firstCase = nextCase;
	int j,k;
	unsigned int region_m1a_start = 0;
	unsigned int region_m1a_end = (ReadLength * 0.5f) - 1;
	unsigned int region_m1b_start = region_m1a_end + 1;
	unsigned int region_m1b_end = ReadLength - 1;

	unsigned int region_m2a_start = 0;
	unsigned int region_m2a_end = (ReadLength * 0.3f) - 1;
	unsigned int region_m2b_start = region_m2a_end + 1;
	unsigned int region_m2b_end = region_m2b_start + (ReadLength * 0.3f) -1;
	unsigned int region_m2c_start = region_m2b_end + 1;
	unsigned int region_m2c_end = ReadLength - 1;
	
	unsigned int region_m3a_start = 0;
	unsigned int region_m3a_end = (ReadLength * 0.25f) - 1;
	unsigned int region_m3b_start = region_m3a_end + 1;
	unsigned int region_m3b_end = region_m3b_start + (ReadLength * 0.25f) -1;
	unsigned int region_m3c_start = region_m3b_end + 1;
	unsigned int region_m3c_end = region_m3c_start + (ReadLength * 0.25f) -1;
	unsigned int region_m3d_start = region_m3c_end + 1;
	unsigned int region_m3d_end = ReadLength - 1;

	unsigned int region_m4a_start = 0;
	unsigned int region_m4a_end = (ReadLength * 0.2f) - 1;
	unsigned int region_m4b_start = region_m4a_end + 1;
	unsigned int region_m4b_end = region_m4b_start + (ReadLength * 0.2f) -1;
	unsigned int region_m4c_start = region_m4b_end + 1;
	unsigned int region_m4c_end = region_m4c_start + (ReadLength * 0.2f) -1;
	unsigned int region_m4d_start = region_m4c_end + 1;
	unsigned int region_m4d_end = region_m4d_start + (ReadLength * 0.2f) -1;
	unsigned int region_m4e_start = region_m4d_end + 1;
	unsigned int region_m4e_end = ReadLength - 1;

	unsigned int region_m5a_start = 0;
	unsigned int region_m5a_end = (ReadLength * 0.16f);
	unsigned int region_m5b_start = region_m5a_end + 1;
	unsigned int region_m5b_end = region_m5b_start + (ReadLength * 0.16f) -1;
	unsigned int region_m5c_start = region_m5b_end + 1;
	unsigned int region_m5c_end = region_m5c_start + (ReadLength * 0.16f) -1;
	unsigned int region_m5d_start = region_m5c_end + 1;
	unsigned int region_m5d_end = region_m5d_start + (ReadLength * 0.16f) -1;
	unsigned int region_m5e_start = region_m5d_end + 1;
	unsigned int region_m5e_end = region_m5e_start + (ReadLength * 0.16f) -1;
	unsigned int region_m5f_start = region_m5e_end + 1;
	unsigned int region_m5f_end = ReadLength - 1;

	if ((MaxError>=0 && smallErrorFirst==1) || MaxError==0) {
		SRAMismatchModel->cases[nextCase].id=1;
		SRAMismatchModel->cases[nextCase].MaxError=0;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = ReadLength - 1;
		SRAMismatchModel->cases[nextCase].steps[0].end   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].ceThreshold   = 5;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;
		
		SRAMismatchModel->cases[nextCase].id=1;
		SRAMismatchModel->cases[nextCase].type=SRA_CASE_TYPE_NEXT_STAGE;
		nextCase++;
	}

	if ((MaxError>=1 && smallErrorFirst==1) || MaxError==1) {
		SRAMismatchModel->cases[nextCase].id=11;
		SRAMismatchModel->cases[nextCase].MaxError=1;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m1b_end;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m1b_start;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m1a_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m1a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;

		SRAMismatchModel->cases[nextCase].id=12;
		SRAMismatchModel->cases[nextCase].MaxError=1;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m1a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m1a_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m1b_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m1b_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;
		
		SRAMismatchModel->cases[nextCase].id=1;
		SRAMismatchModel->cases[nextCase].type=SRA_CASE_TYPE_NEXT_STAGE;
		nextCase++;
	}

	
	if ((MaxError>=2 && smallErrorFirst==1) || MaxError==2) {
		// | 0.3 | 0.3 | 0.4 |
		SRAMismatchModel->cases[nextCase].id=21;
		SRAMismatchModel->cases[nextCase].MaxError=2;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m2c_end;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m2c_start;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m2b_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m2a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[1].ceStart   = region_m2b_end-9;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;

		SRAMismatchModel->cases[nextCase].id=22;
		SRAMismatchModel->cases[nextCase].MaxError=2;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = 0;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m2a_end;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m2b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m2c_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m2c_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[1].ceEnd   = region_m2c_start+9;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;

		SRAMismatchModel->cases[nextCase].id=23;
		SRAMismatchModel->cases[nextCase].MaxError=2;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = 0;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m2a_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m2b_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m2b_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceStart   = (region_m2a_end>=17)?SRAMismatchModel->cases[nextCase].steps[1].ceStart:17;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 10;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m2c_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m2c_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 10;
		SRAMismatchModel->cases[nextCase].steps[2].ceEnd   = region_m2c_start + 9;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;

		SRAMismatchModel->cases[nextCase].id=24;
		SRAMismatchModel->cases[nextCase].MaxError=2;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m2b_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m2b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m2c_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m2c_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 10;
		SRAMismatchModel->cases[nextCase].steps[1].ceEnd   = (region_m2b_end-region_m2b_start+1>=17)?SRAMismatchModel->cases[nextCase].steps[1].ceEnd:region_m2b_end-18;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m2a_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m2a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 10;
		SRAMismatchModel->cases[nextCase].steps[2].ceEnd   = region_m2c_start + 9;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;
		
		SRAMismatchModel->cases[nextCase].id=1;
		SRAMismatchModel->cases[nextCase].type=SRA_CASE_TYPE_NEXT_STAGE;
		nextCase++;
	}
	
	if ((MaxError>=3 && smallErrorFirst==1) || MaxError==3) {
		// | 0.25 | 0.25 | 0.25 | 0.25 |
		SRAMismatchModel->cases[nextCase].id=31;
		SRAMismatchModel->cases[nextCase].MaxError=3;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m3d_end;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m3c_start;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m3b_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m3a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=32;
		SRAMismatchModel->cases[nextCase].MaxError=3;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m3a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m3b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m3c_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m3d_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=33;
		SRAMismatchModel->cases[nextCase].MaxError=3;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m3a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m3a_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m3b_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m3b_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m3c_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m3d_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=35;
		SRAMismatchModel->cases[nextCase].MaxError=3;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m3c_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m3c_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m3d_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m3d_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m3b_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m3a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=36;
		SRAMismatchModel->cases[nextCase].MaxError=3;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m3d_end;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m3d_start;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m3c_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m3c_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m3b_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m3a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;
        
		nextCase++;
		SRAMismatchModel->cases[nextCase].id=34;
		SRAMismatchModel->cases[nextCase].MaxError=3;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m3b_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m3b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m3a_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m3a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m3c_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m3d_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		
		SRAMismatchModel->cases[nextCase].id=1;
		SRAMismatchModel->cases[nextCase].type=SRA_CASE_TYPE_NEXT_STAGE;
		nextCase++;
	}
	
	if ((MaxError>=4 && smallErrorFirst==1) || MaxError==4) {
		// | 0.2 | 0.2 | 0.2 | 0.2 | 0.2 |
		SRAMismatchModel->cases[nextCase].id=41;
		SRAMismatchModel->cases[nextCase].MaxError=4;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 4;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=42;
		SRAMismatchModel->cases[nextCase].MaxError=4;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 4;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=43;
		SRAMismatchModel->cases[nextCase].MaxError=4;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4a_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4b_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;


		nextCase++;
		SRAMismatchModel->cases[nextCase].id=44;
		SRAMismatchModel->cases[nextCase].MaxError=4;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4b_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4a_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;


		nextCase++;
		SRAMismatchModel->cases[nextCase].id=45;
		SRAMismatchModel->cases[nextCase].MaxError=4;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4a_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4b_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;


		nextCase++;
		SRAMismatchModel->cases[nextCase].id=46;
		SRAMismatchModel->cases[nextCase].MaxError=4;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4b_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4a_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=47;
		SRAMismatchModel->cases[nextCase].MaxError=4;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4b_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4a_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m4c_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[3].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[3].start = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[3].end   = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[3].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[3].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[4].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=48;
		SRAMismatchModel->cases[nextCase].MaxError=4;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4c_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4b_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4b_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m4a_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[3].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[3].start = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[3].end   = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[3].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[3].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[4].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=49;
		SRAMismatchModel->cases[nextCase].MaxError=4;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4d_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4e_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 3;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;


		nextCase++;
		SRAMismatchModel->cases[nextCase].id=410;
		SRAMismatchModel->cases[nextCase].MaxError=4;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4e_start;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4d_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 3;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;
		
		SRAMismatchModel->cases[nextCase].id=1;
		SRAMismatchModel->cases[nextCase].type=SRA_CASE_TYPE_NEXT_STAGE;
		nextCase++;
	}

	if ((MaxError>=5 && smallErrorFirst==1) || MaxError==5) {

		// | 0.16 | 0.16 | 0.16 | 0.16 | 0.16 | 0.16 |

		// ___________________________________________
		// |_____________5_____________|______0______|
		// |      |      |      |      |      |      |
		SRAMismatchModel->cases[nextCase].id=51;
		SRAMismatchModel->cases[nextCase].MaxError=5;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 5;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;

		// ___________________________________________
		// |_____________0_____________|______5______|
		// |      |      |      |      |      |      |
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].MaxError=5;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 5;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;

		// ___________________________________________
		// |______0______|______1______|______4______|
		// |      |      |      |      |      |      |
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].MaxError=5;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 4;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;

		// ___________________________________________
		// |______1______|______0______|______4______|
		// |      |      |      |      |      |      |
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].MaxError=5;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 4;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;










		// ___________________________________________
		// |______ ______4______ ______|__0___|__1___|
		// |      |      |      |      |      |      |
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].MaxError=5;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5e_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5f_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 4;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;
		
		// ___________________________________________
		// |______ ______4______ ______|__1___|__0___|
		// |      |      |      |      |      |      |
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].MaxError=5;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5f_start;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5e_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 4;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;












		// ___________________________________________
		// |______0______|______2______|______3______|
		// |      |      |      |      |      |      |
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].MaxError=5;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;

		// ___________________________________________
		// |______2______|______0______|______3______|
		// |      |      |      |      |      |      |
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].MaxError=5;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;

		// ___________________________________________
		// |___0__|__1___|______1______|______3______|
		// |      |      |      |      |      |      |
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].MaxError=5;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5a_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5b_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[3].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[3].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[3].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[3].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[3].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[4].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;

		// ___________________________________________
		// |___1__|__0___|______1______|______3______|
		// |      |      |      |      |      |      |
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].MaxError=5;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5b_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5a_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[3].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[3].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[3].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[3].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[3].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[4].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;














		// ___________________________________________
		// |______0______|______3______|______2______|
		// |      |      |      |      |      |      |
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].MaxError=5;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 3;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;
		// ___________________________________________
		// |______3______|______0______|______2______|
		// |      |      |      |      |      |      |
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].MaxError=5;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 3;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;

		// ___________________________________________
		// |__0___|__1___|______2______|______2______|
		// |      |      |      |      |      |      |
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].MaxError=5;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5a_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5b_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[3].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[3].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[3].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[3].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[4].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;
		
		// ___________________________________________
		// |__1___|__0___|______2______|______2______|
		// |      |      |      |      |      |      |
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].MaxError=5;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5b_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5a_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[3].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[3].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[3].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[3].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[4].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;

		// ___________________________________________
		// |______2______|__0___|__1___|______2______|
		// |      |      |      |      |      |      |
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].MaxError=5;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5c_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5d_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[3].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[3].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[3].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[3].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[4].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;

		// ___________________________________________
		// |______2______|__1___|__0___|______2______|
		// |      |      |      |      |      |      |
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].MaxError=5;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5d_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5c_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[3].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[3].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[3].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[3].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[4].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;
		
		SRAMismatchModel->cases[nextCase].id=1;
		SRAMismatchModel->cases[nextCase].type=SRA_CASE_TYPE_NEXT_STAGE;
		nextCase++;
	}

	return nextCase;
}


int SRAEditCasesPopulate8G(SRAModel * SRAMismatchModel, int nextCase, 
							 unsigned int ReadLength, int MaxError, int smallErrorFirst, 
							  BWT * bwt, BWT * rev_bwt) {
	
	int firstCase = nextCase;
	int j,k;
	unsigned int region_m1a_start = 0;
	unsigned int region_m1a_end = ReadLength - (ReadLength * 0.5f);
	unsigned int region_m1b_start = region_m1a_end + 1;
	unsigned int region_m1b_end = ReadLength - 1;

	unsigned int region_m2a_start = 0;
	unsigned int region_m2a_end = (ReadLength * 0.3f);
	unsigned int region_m2b_start = region_m2a_end + 1;
	unsigned int region_m2b_end = region_m2b_start + (ReadLength * 0.3f) -1;
	unsigned int region_m2c_start = region_m2b_end + 1;
	unsigned int region_m2c_end = ReadLength - 1;
	
	unsigned int region_m3a_start = 0;
	unsigned int region_m3a_end = (ReadLength * 0.25f);
	unsigned int region_m3b_start = region_m3a_end + 1;
	unsigned int region_m3b_end = region_m3b_start + (ReadLength * 0.25f) -1;
	unsigned int region_m3c_start = region_m3b_end + 1;
	unsigned int region_m3c_end = region_m3c_start + (ReadLength * 0.25f) -1;
	unsigned int region_m3d_start = region_m3c_end + 1;
	unsigned int region_m3d_end = ReadLength - 1;

	unsigned int region_m4a_start = 0;
	unsigned int region_m4a_end = (ReadLength * 0.2f);
	unsigned int region_m4b_start = region_m4a_end + 1;
	unsigned int region_m4b_end = region_m4b_start + (ReadLength * 0.2f) -1;
	unsigned int region_m4c_start = region_m4b_end + 1;
	unsigned int region_m4c_end = region_m4c_start + (ReadLength * 0.2f) -1;
	unsigned int region_m4d_start = region_m4c_end + 1;
	unsigned int region_m4d_end = region_m4d_start + (ReadLength * 0.2f) -1;
	unsigned int region_m4e_start = region_m4d_end + 1;
	unsigned int region_m4e_end = ReadLength - 1;

	unsigned int region_m5a_start = 0;
	unsigned int region_m5a_end = (ReadLength * 0.16f);
	unsigned int region_m5b_start = region_m5a_end + 1;
	unsigned int region_m5b_end = region_m5b_start + (ReadLength * 0.16f) -1;
	unsigned int region_m5c_start = region_m5b_end + 1;
	unsigned int region_m5c_end = region_m5c_start + (ReadLength * 0.16f) -1;
	unsigned int region_m5d_start = region_m5c_end + 1;
	unsigned int region_m5d_end = region_m5d_start + (ReadLength * 0.16f) -1;
	unsigned int region_m5e_start = region_m5d_end + 1;
	unsigned int region_m5e_end = region_m5e_start + (ReadLength * 0.16f) -1;
	unsigned int region_m5f_start = region_m5e_end + 1;
	unsigned int region_m5f_end = ReadLength - 1;

	if ((MaxError>=0 && smallErrorFirst==1) || MaxError==0) {
		SRAMismatchModel->cases[nextCase].id=1;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = ReadLength - 1;
		SRAMismatchModel->cases[nextCase].steps[0].end   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].ceThreshold   = 1;
		SRAMismatchModel->cases[nextCase].steps[0].ceStart       = 10;
		SRAMismatchModel->cases[nextCase].steps[0].ceEnd         = 34;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;
		
		SRAMismatchModel->cases[nextCase].id=1;
		SRAMismatchModel->cases[nextCase].type=SRA_CASE_TYPE_NEXT_STAGE;
		nextCase++;
	}

	if ((MaxError>=1 && smallErrorFirst==1) || MaxError==1) {
		SRAMismatchModel->cases[nextCase].id=11;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m1b_end;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m1b_start;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;

		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE_BOUNDARY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m1a_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m1a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		//SRAMismatchModel->cases[nextCase].steps[1].ceStart       = region_m1a_end-2;
		//SRAMismatchModel->cases[nextCase].steps[1].ceEnd         = region_m1a_end;
		//SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=12;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m1a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m1a_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m1b_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m1b_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		//SRAMismatchModel->cases[nextCase].steps[1].ceStart       = region_m1b_start;
		//SRAMismatchModel->cases[nextCase].steps[1].ceEnd         = region_m1b_start+2;
		//SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 3;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;//*/
		nextCase++;
		
		SRAMismatchModel->cases[nextCase].id=1;
		SRAMismatchModel->cases[nextCase].type=SRA_CASE_TYPE_NEXT_STAGE;
		nextCase++;
	}
	if ((MaxError>=2 && smallErrorFirst==1) || MaxError==2) {
		SRAMismatchModel->cases[nextCase].id=21;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m2c_end;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m2c_start;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE_BOUNDARY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m2b_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m2a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].ceStart       = region_m2b_end-1;
		SRAMismatchModel->cases[nextCase].steps[1].ceEnd         = region_m2b_end;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 5;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=22;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = 0;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m2a_end;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m2b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m2c_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m2c_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].ceStart       = region_m2c_start;
		SRAMismatchModel->cases[nextCase].steps[1].ceEnd         = region_m2c_start+1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 5;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=23;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = 0;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m2a_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m2b_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m2b_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m2c_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m2c_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=24;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m2b_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m2b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE_BOUNDARY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m2c_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m2c_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m2a_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m2a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;
		
		SRAMismatchModel->cases[nextCase].id=1;
		SRAMismatchModel->cases[nextCase].type=SRA_CASE_TYPE_NEXT_STAGE;
		nextCase++;
	}
	if ((MaxError>=3 && smallErrorFirst==1) || MaxError==3) {
		SRAMismatchModel->cases[nextCase].id=31;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m3d_end;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m3c_start;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE_BOUNDARY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m3b_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m3a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=32;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m3a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m3b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m3c_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m3d_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=33;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m3a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m3a_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE_BOUNDARY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m3b_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m3b_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m3c_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m3d_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=34;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m3b_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m3b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m3a_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m3a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m3c_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m3d_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=35;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m3c_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m3c_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m3d_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m3d_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m3b_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m3a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=36;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m3d_end;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m3d_start;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE_BOUNDARY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m3c_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m3c_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m3b_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m3a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;
		
		SRAMismatchModel->cases[nextCase].id=1;
		SRAMismatchModel->cases[nextCase].type=SRA_CASE_TYPE_NEXT_STAGE;
		nextCase++;
	}
	if ((MaxError>=4 && smallErrorFirst==1) || MaxError==4) {
		SRAMismatchModel->cases[nextCase].id=41;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE_BOUNDARY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 4;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=42;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 4;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=43;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4a_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE_BOUNDARY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4b_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;


		nextCase++;
		SRAMismatchModel->cases[nextCase].id=44;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4b_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4a_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;


		nextCase++;
		SRAMismatchModel->cases[nextCase].id=45;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4a_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4b_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;


		nextCase++;
		SRAMismatchModel->cases[nextCase].id=46;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4b_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4a_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=47;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4b_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4a_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE_BOUNDARY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m4c_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[3].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[3].start = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[3].end   = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[3].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[3].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[4].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=48;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4c_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4b_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4b_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m4a_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[3].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[3].start = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[3].end   = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[3].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[3].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[4].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=49;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4d_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4e_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 3;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;


		nextCase++;
		SRAMismatchModel->cases[nextCase].id=410;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4e_start;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE_BOUNDARY;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4d_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 3;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;

		
		SRAMismatchModel->cases[nextCase].id=1;
		SRAMismatchModel->cases[nextCase].type=SRA_CASE_TYPE_NEXT_STAGE;
		nextCase++;
	}
	if ((MaxError>=5 && smallErrorFirst==1) || MaxError==5) {
		// ___________________________________________
		// |_____________5_____________|______0______|
		// |      |      |      |      |      |      |	
		SRAMismatchModel->cases[nextCase].id=51;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE_BOUNDARY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 5;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;

		// ___________________________________________
		// |_____________0_____________|______5______|
		// |      |      |      |      |      |      |
		nextCase++;
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 5;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;

		// ___________________________________________
		// |______0______|______1______|______4______|
		// |      |      |      |      |      |      |
		nextCase++;
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE_BOUNDARY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 4;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;

		// ___________________________________________
		// |______1______|______0______|______4______|
		// |      |      |      |      |      |      |
		nextCase++;
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 4;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;










		// ___________________________________________
		// |______ ______4______ ______|__0___|__1___|
		// |      |      |      |      |      |      |
		nextCase++;
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5e_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5f_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 4;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;
		
		// ___________________________________________
		// |______ ______4______ ______|__1___|__0___|
		// |      |      |      |      |      |      |
		nextCase++;
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5f_start;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE_BOUNDARY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5e_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 4;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;












		// ___________________________________________
		// |______0______|______2______|______3______|
		// |      |      |      |      |      |      |
		nextCase++;
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;

		// ___________________________________________
		// |______2______|______0______|______3______|
		// |      |      |      |      |      |      |
		nextCase++;
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;

		// ___________________________________________
		// |___0__|__1___|______1______|______3______|
		// |      |      |      |      |      |      |
		nextCase++;
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5a_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE_BOUNDARY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5b_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[3].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[3].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[3].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[3].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[3].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[4].type  = SRA_STEP_TYPE_COMPLETE;

		// ___________________________________________
		// |___1__|__0___|______1______|______3______|
		// |      |      |      |      |      |      |
		nextCase++;
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5b_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5a_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[3].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[3].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[3].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[3].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[3].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[4].type  = SRA_STEP_TYPE_COMPLETE;














		// ___________________________________________
		// |______0______|______3______|______2______|
		// |      |      |      |      |      |      |
		nextCase++;
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 3;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;
		// ___________________________________________
		// |______3______|______0______|______2______|
		// |      |      |      |      |      |      |
		nextCase++;
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 3;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;

		// ___________________________________________
		// |__0___|__1___|______2______|______2______|
		// |      |      |      |      |      |      |
		nextCase++;
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5a_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5b_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[3].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[3].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[3].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[3].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[4].type  = SRA_STEP_TYPE_COMPLETE;
		
		// ___________________________________________
		// |__1___|__0___|______2______|______2______|
		// |      |      |      |      |      |      |
		nextCase++;
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5b_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5a_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[3].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[3].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[3].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[3].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[4].type  = SRA_STEP_TYPE_COMPLETE;

		// ___________________________________________
		// |______2______|__0___|__1___|______2______|
		// |      |      |      |      |      |      |
		nextCase++;
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5c_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE_BOUNDARY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5d_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[3].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[3].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[3].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[3].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[4].type  = SRA_STEP_TYPE_COMPLETE;

		// ___________________________________________
		// |______2______|__1___|__0___|______2______|
		// |      |      |      |      |      |      |
		nextCase++;
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5d_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5c_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[3].ErrorType  = SRA_STEP_ERROR_TYPE_EDIT_DISTANCE;
		SRAMismatchModel->cases[nextCase].steps[3].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[3].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[3].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].ceThreshold   = 20;
		SRAMismatchModel->cases[nextCase].steps[4].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;
		
		SRAMismatchModel->cases[nextCase].id=1;
		SRAMismatchModel->cases[nextCase].type=SRA_CASE_TYPE_NEXT_STAGE;
		nextCase++;
	}

	return nextCase;
}

int SRAMismatchCasesPopulate8G(SRAModel * SRAMismatchModel, int nextCase, 
							 unsigned int ReadLength, int MaxError, int smallErrorFirst, 
							  BWT * bwt, BWT * rev_bwt) {
	
	int firstCase = nextCase;
	int j,k;
	unsigned int region_m1a_start = 0;
	unsigned int region_m1a_end = (ReadLength * 0.5f) - 1;
	unsigned int region_m1b_start = region_m1a_end + 1;
	unsigned int region_m1b_end = ReadLength - 1;

	unsigned int region_m2a_start = 0;
	unsigned int region_m2a_end = (ReadLength * 0.3f) - 1;
	unsigned int region_m2b_start = region_m2a_end + 1;
	unsigned int region_m2b_end = region_m2b_start + (ReadLength * 0.3f) -1;
	unsigned int region_m2c_start = region_m2b_end + 1;
	unsigned int region_m2c_end = ReadLength - 1;
	
	unsigned int region_m3a_start = 0;
	unsigned int region_m3a_end = (ReadLength * 0.25f) - 1;
	unsigned int region_m3b_start = region_m3a_end + 1;
	unsigned int region_m3b_end = region_m3b_start + (ReadLength * 0.25f) -1;
	unsigned int region_m3c_start = region_m3b_end + 1;
	unsigned int region_m3c_end = region_m3c_start + (ReadLength * 0.25f) -1;
	unsigned int region_m3d_start = region_m3c_end + 1;
	unsigned int region_m3d_end = ReadLength - 1;

	unsigned int region_m4a_start = 0;
	unsigned int region_m4a_end = (ReadLength * 0.2f) - 1;
	unsigned int region_m4b_start = region_m4a_end + 1;
	unsigned int region_m4b_end = region_m4b_start + (ReadLength * 0.2f) -1;
	unsigned int region_m4c_start = region_m4b_end + 1;
	unsigned int region_m4c_end = region_m4c_start + (ReadLength * 0.2f) -1;
	unsigned int region_m4d_start = region_m4c_end + 1;
	unsigned int region_m4d_end = region_m4d_start + (ReadLength * 0.2f) -1;
	unsigned int region_m4e_start = region_m4d_end + 1;
	unsigned int region_m4e_end = ReadLength - 1;

	unsigned int region_m5a_start = 0;
	unsigned int region_m5a_end = (ReadLength * 0.16f);
	unsigned int region_m5b_start = region_m5a_end + 1;
	unsigned int region_m5b_end = region_m5b_start + (ReadLength * 0.16f) -1;
	unsigned int region_m5c_start = region_m5b_end + 1;
	unsigned int region_m5c_end = region_m5c_start + (ReadLength * 0.16f) -1;
	unsigned int region_m5d_start = region_m5c_end + 1;
	unsigned int region_m5d_end = region_m5d_start + (ReadLength * 0.16f) -1;
	unsigned int region_m5e_start = region_m5d_end + 1;
	unsigned int region_m5e_end = region_m5e_start + (ReadLength * 0.16f) -1;
	unsigned int region_m5f_start = region_m5e_end + 1;
	unsigned int region_m5f_end = ReadLength - 1;

	if ((MaxError>=0 && smallErrorFirst==1) || MaxError==0) {
		SRAMismatchModel->cases[nextCase].id=1;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = ReadLength - 1;
		SRAMismatchModel->cases[nextCase].steps[0].end   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].ceStart    = 9;
		SRAMismatchModel->cases[nextCase].steps[0].ceThreshold= 1;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;
		
		SRAMismatchModel->cases[nextCase].id=1;
		SRAMismatchModel->cases[nextCase].type=SRA_CASE_TYPE_NEXT_STAGE;
		nextCase++;//*/
	}

	if ((MaxError>=1 && smallErrorFirst==1) || MaxError==1) {
		SRAMismatchModel->cases[nextCase].id=11;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m1b_end;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m1b_start;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m1a_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m1a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceStart       = region_m1a_end-2;
		SRAMismatchModel->cases[nextCase].steps[1].ceEnd         = region_m1a_end;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 3;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;

		SRAMismatchModel->cases[nextCase].id=12;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m1a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m1a_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m1b_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m1b_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceStart   = region_m1b_start;
		SRAMismatchModel->cases[nextCase].steps[1].ceEnd   = region_m1b_start+2;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 3;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;
		
		SRAMismatchModel->cases[nextCase].id=1;
		SRAMismatchModel->cases[nextCase].type=SRA_CASE_TYPE_NEXT_STAGE;
		nextCase++;//*/
	}

	
	if ((MaxError>=2 && smallErrorFirst==1) || MaxError==2) {
		// | 0.3 | 0.3 | 0.4 |
		SRAMismatchModel->cases[nextCase].id=21;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m2c_end;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m2c_start;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m2b_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m2a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 5;
		SRAMismatchModel->cases[nextCase].steps[1].ceStart   = region_m2b_end-4;
		SRAMismatchModel->cases[nextCase].steps[1].ceEnd     = region_m2b_end;//-1;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;//*/

		SRAMismatchModel->cases[nextCase].id=22;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m2a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m2b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m2c_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m2c_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 5;
		SRAMismatchModel->cases[nextCase].steps[1].ceStart       = region_m2c_start;
		SRAMismatchModel->cases[nextCase].steps[1].ceEnd         = region_m2c_start+4;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;//*/

		SRAMismatchModel->cases[nextCase].id=23;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m2a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m2a_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m2b_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m2b_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceStart   = (region_m2a_end>=17)?SRAMismatchModel->cases[nextCase].steps[1].ceStart:17;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m2c_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m2c_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[2].ceEnd   = region_m2c_start + 9;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;//*/

		SRAMismatchModel->cases[nextCase].id=24;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m2b_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m2b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m2c_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m2c_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].ceEnd   = (region_m2b_end-region_m2b_start+1>=17)?SRAMismatchModel->cases[nextCase].steps[1].ceEnd:region_m2b_end-18;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m2a_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m2a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[2].ceEnd   = region_m2c_start + 9;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;//*/
		
		SRAMismatchModel->cases[nextCase].id=1;
		SRAMismatchModel->cases[nextCase].type=SRA_CASE_TYPE_NEXT_STAGE;
		nextCase++;
	}
	
	if ((MaxError>=3 && smallErrorFirst==1) || MaxError==3) {
		// | 0.25 | 0.25 | 0.25 | 0.25 |
		SRAMismatchModel->cases[nextCase].id=31;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m3d_end;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m3c_start;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m3b_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m3a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 10;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=32;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m3a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m3b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m3c_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m3d_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 10;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=33;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m3a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m3a_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m3b_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m3b_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 10;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m3c_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m3d_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;

        
		nextCase++;
		SRAMismatchModel->cases[nextCase].id=35;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m3c_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m3c_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m3d_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m3d_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 10;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m3b_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m3a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;
        
		nextCase++;
		SRAMismatchModel->cases[nextCase].id=36;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m3d_end;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m3d_start;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m3c_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m3c_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 10;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m3b_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m3a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;
        
		nextCase++;
		SRAMismatchModel->cases[nextCase].id=34;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m3b_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m3b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m3a_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m3a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 10;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m3c_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m3d_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;


		nextCase++;
		
		SRAMismatchModel->cases[nextCase].id=1;
		SRAMismatchModel->cases[nextCase].type=SRA_CASE_TYPE_NEXT_STAGE;
		nextCase++;
	}
	
	if ((MaxError>=4 && smallErrorFirst==1) || MaxError==4) {
		// | 0.2 | 0.2 | 0.2 | 0.2 | 0.2 |
		SRAMismatchModel->cases[nextCase].id=41;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 4;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 10;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=42;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 4;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 10;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=43;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4a_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4b_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 10;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;


		nextCase++;
		SRAMismatchModel->cases[nextCase].id=44;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4b_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4a_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 10;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;


		nextCase++;
		SRAMismatchModel->cases[nextCase].id=45;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4a_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4b_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 10;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;


		nextCase++;
		SRAMismatchModel->cases[nextCase].id=46;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4b_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4a_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 10;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=47;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4b_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4a_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 10;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m4c_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[3].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[3].start = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[3].end   = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[3].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[3].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[4].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=48;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4c_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4b_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4b_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 10;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m4a_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[3].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[3].start = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[3].end   = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[3].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[3].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[4].type  = SRA_STEP_TYPE_COMPLETE;

		nextCase++;
		SRAMismatchModel->cases[nextCase].id=49;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4d_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4e_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 10;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 3;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;


		nextCase++;
		SRAMismatchModel->cases[nextCase].id=410;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m4e_end;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m4e_start;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m4d_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m4d_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 10;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m4c_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m4a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 3;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;
		
		SRAMismatchModel->cases[nextCase].id=1;
		SRAMismatchModel->cases[nextCase].type=SRA_CASE_TYPE_NEXT_STAGE;
		nextCase++;
	}

	if ((MaxError>=5 && smallErrorFirst==1) || MaxError==5) {

		// | 0.16 | 0.16 | 0.16 | 0.16 | 0.16 | 0.16 |

		// ___________________________________________
		// |_____________5_____________|______0______|
		// |      |      |      |      |      |      |
		SRAMismatchModel->cases[nextCase].id=51;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 5;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 10;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;

		// ___________________________________________
		// |_____________0_____________|______5______|
		// |      |      |      |      |      |      |
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 5;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 10;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;

		// ___________________________________________
		// |______0______|______1______|______4______|
		// |      |      |      |      |      |      |
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 10;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 4;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;

		// ___________________________________________
		// |______1______|______0______|______4______|
		// |      |      |      |      |      |      |
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 10;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 4;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;










		// ___________________________________________
		// |______ ______4______ ______|__0___|__1___|
		// |      |      |      |      |      |      |
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5e_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5f_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 10;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 4;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;
		
		// ___________________________________________
		// |______ ______4______ ______|__1___|__0___|
		// |      |      |      |      |      |      |
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5f_start;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5e_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 10;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BACKWARD_ONLY_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 4;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;












		// ___________________________________________
		// |______0______|______2______|______3______|
		// |      |      |      |      |      |      |
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 10;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;

		// ___________________________________________
		// |______2______|______0______|______3______|
		// |      |      |      |      |      |      |
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 10;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;

		// ___________________________________________
		// |___0__|__1___|______1______|______3______|
		// |      |      |      |      |      |      |
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5a_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5b_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 10;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[3].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[3].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[3].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[3].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[3].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[4].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;

		// ___________________________________________
		// |___1__|__0___|______1______|______3______|
		// |      |      |      |      |      |      |
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5b_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5a_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 10;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[3].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[3].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[3].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[3].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[3].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[4].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;














		// ___________________________________________
		// |______0______|______3______|______2______|
		// |      |      |      |      |      |      |
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 3;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 10;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;
		// ___________________________________________
		// |______3______|______0______|______2______|
		// |      |      |      |      |      |      |
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 3;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 3;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 10;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;

		// ___________________________________________
		// |__0___|__1___|______2______|______2______|
		// |      |      |      |      |      |      |
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5a_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5b_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 10;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[3].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[3].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[3].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[3].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[4].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;
		
		// ___________________________________________
		// |__1___|__0___|______2______|______2______|
		// |      |      |      |      |      |      |
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5b_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5a_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 10;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[3].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[3].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[3].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[3].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[4].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;

		// ___________________________________________
		// |______2______|__0___|__1___|______2______|
		// |      |      |      |      |      |      |
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5c_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5d_start;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 10;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[3].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[3].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[3].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[3].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[4].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;

		// ___________________________________________
		// |______2______|__1___|__0___|______2______|
		// |      |      |      |      |      |      |
		SRAMismatchModel->cases[nextCase].id=52;
		SRAMismatchModel->cases[nextCase].steps[0].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP;
		SRAMismatchModel->cases[nextCase].steps[0].start = region_m5d_start;
		SRAMismatchModel->cases[nextCase].steps[0].end   = region_m5d_end;
		SRAMismatchModel->cases[nextCase].steps[0].MinError   = 0;
		SRAMismatchModel->cases[nextCase].steps[0].MaxError   = 0;
		SRAMismatchModel->cases[nextCase].steps[1].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[1].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[1].start = region_m5c_end;
		SRAMismatchModel->cases[nextCase].steps[1].end   = region_m5c_start;
		SRAMismatchModel->cases[nextCase].steps[1].MinError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].MaxError   = 1;
		SRAMismatchModel->cases[nextCase].steps[1].ceThreshold   = 10;
		SRAMismatchModel->cases[nextCase].steps[2].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[2].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[2].start = region_m5b_end;
		SRAMismatchModel->cases[nextCase].steps[2].end   = region_m5a_start;
		SRAMismatchModel->cases[nextCase].steps[2].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[2].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[3].type  = SRA_STEP_TYPE_BI_DIRECTIONAL_BWT;
		SRAMismatchModel->cases[nextCase].steps[3].ErrorType  = SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
		SRAMismatchModel->cases[nextCase].steps[3].start = region_m5e_start;
		SRAMismatchModel->cases[nextCase].steps[3].end   = region_m5f_end;
		SRAMismatchModel->cases[nextCase].steps[3].MinError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].MaxError   = 2;
		SRAMismatchModel->cases[nextCase].steps[3].ceThreshold   = 0;
		SRAMismatchModel->cases[nextCase].steps[4].type  = SRA_STEP_TYPE_COMPLETE;
		nextCase++;
		
		SRAMismatchModel->cases[nextCase].id=1;
		SRAMismatchModel->cases[nextCase].type=SRA_CASE_TYPE_NEXT_STAGE;
		nextCase++;
	}

	return nextCase;
}

 
SRAModel *  SRAModelConstruct(unsigned int ReadLength, 
							  SRAQuerySetting * qSettings, int modelId,
							  BWT * bwt, BWT * rev_bwt) {

#ifdef DEBUG_2BWT
	printf("\n[SRAModelConstruct] Size of SRAModel = %u bytes (%9.4f M)\n", sizeof(SRAModel), (float) sizeof(SRAModel)/1024.0/1024.0);
#endif
	SRAModel * SRAMismatchModel = (SRAModel*) malloc(sizeof(SRAModel));

	int ErrorType = qSettings->ErrorType;
	int MaxError = qSettings->MaxError;
	int SmallErrorFirst = 0;
	// the following codes are commented due to the bug found for 
	// REPORT_ALL_BEST
	/*  
	if ( qSettings->OutputType == REPORT_UNIQUE_BEST ||
		qSettings->OutputType == REPORT_RANDOM_BEST ||
		qSettings->OutputType == REPORT_ALL_BEST) {
		SmallErrorFirst = 1;
	}*/

	//DIU
	//SmallErrorFirst=1;
	//EODIU

	//_____________________________________________________________________________________________________
	//Initialize it
	int i,j,k,l;
    SRAMismatchModel->ModelReadLength = ReadLength;
	for (j=0;j<MAX_NUM_OF_SRA_CASES;j++) {
		SRAMismatchModel->cases[j].id=0;
		SRAMismatchModel->cases[j].type=SRA_CASE_TYPE_ALIGNMENT;
		SRAMismatchModel->cases[j].ErrorType = ErrorType;
		for (k=0;k<MAX_NUM_OF_SRA_STEPS;k++) {
			SRAMismatchModel->cases[j].steps[k].type = SRA_STEP_TYPE_COMPLETE;
			SRAMismatchModel->cases[j].steps[k].start = 0;
			SRAMismatchModel->cases[j].steps[k].end = 0;
			SRAMismatchModel->cases[j].steps[k].ceThreshold = 0;
			SRAMismatchModel->cases[j].steps[k].ceStart = 0;
			SRAMismatchModel->cases[j].steps[k].ceEnd = ReadLength-1;
			
			SRAMismatchModel->cases[j].steps[k].ErrorType = SRA_STEP_ERROR_TYPE_NO_ERROR;
			SRAMismatchModel->cases[j].steps[k].MinError = 0;
			SRAMismatchModel->cases[j].steps[k].MaxError = 0;
		}
	}
	j=0;
	if ( ErrorType == SRA_STEP_ERROR_TYPE_NO_ERROR || ErrorType == SRA_STEP_ERROR_TYPE_MISMATCH_ONLY )  {
		if (modelId == SRA_MODEL_8G) {

#ifdef DEBUG_2BWT
			printf("[SRAModelConstruct] Constructed SRA_MODEL_8G\n",j);
#endif 
			j=SRAMismatchCasesPopulate8G(SRAMismatchModel,j,ReadLength,MaxError,SmallErrorFirst,bwt,rev_bwt);
		} else {

#ifdef DEBUG_2BWT
			printf("[SRAModelConstruct] Constructed SRA_MODEL_16G\n",j);
#endif 
			j=SRAMismatchCasesPopulate(SRAMismatchModel,j,ReadLength,MaxError,SmallErrorFirst,bwt,rev_bwt);
		}
	} else if ( ErrorType == SRA_STEP_ERROR_TYPE_EDIT_DISTANCE ) {
		if (modelId == SRA_MODEL_8G) {

#ifdef DEBUG_2BWT
			printf("[SRAModelConstruct] Constructed SRA_MODEL_8G\n",j);
#endif 
			j=SRAEditCasesPopulate8G(SRAMismatchModel,j,ReadLength,MaxError,SmallErrorFirst,bwt,rev_bwt);
		} else {

#ifdef DEBUG_2BWT
			printf("[SRAModelConstruct] Constructed SRA_MODEL_16G\n",j);
#endif 
			j=SRAEditCasesPopulate(SRAMismatchModel,j,ReadLength,MaxError,SmallErrorFirst,bwt,rev_bwt);
		}
	}

#ifdef DEBUG_2BWT
	printf("[SRAModelConstruct] Constructed a %d cases model\n",j);
#endif 

	//_____________________________________________________________________________________________________
	//Post-compute
	for (j=0;j<MAX_NUM_OF_SRA_CASES;j++) {
		SRAMismatchModel->cases[j].leftMostAligned = (int*) malloc(sizeof(int)*ReadLength);
		int leftMost = ReadLength;
		for (k=0;k<MAX_NUM_OF_SRA_STEPS;k++) {
			if (SRAMismatchModel->cases[j].steps[k].type != SRA_STEP_TYPE_COMPLETE) {
				int start = SRAMismatchModel->cases[j].steps[k].start;
				int end = SRAMismatchModel->cases[j].steps[k].end;
				int step = -2 * (start>end) + 1;
				SRAMismatchModel->cases[j].steps[k].step = step;
				SRAMismatchModel->cases[j].steps[k].len = (end - start) * step + 1;
				if (step > 0) {
					//Forward alignment
					SRAMismatchModel->cases[j].steps[k].bwt = rev_bwt;
					if (start<leftMost) {leftMost = start;}
                    for (l=start;l<=end;l+=step) {
						SRAMismatchModel->cases[j].leftMostAligned[l]=leftMost;
					}
				} else {
					//Backward alignment
					SRAMismatchModel->cases[j].steps[k].bwt = bwt;
                    for (l=start;l>=end;l+=step) {
						SRAMismatchModel->cases[j].leftMostAligned[l]=leftMost--;
                    }
				}
				if (SmallErrorFirst) {
					SRAMismatchModel->cases[j].steps[k].MinError = SRAMismatchModel->cases[j].steps[k].MaxError;
				}
				if (SRAMismatchModel->cases[j].steps[k].ceThreshold>MAX_CE_THRESHOLD) {
					SRAMismatchModel->cases[j].steps[k].ceThreshold=MAX_CE_THRESHOLD;
				}
				if (SRAMismatchModel->cases[j].steps[k].ceStart>SRAMismatchModel->cases[j].steps[k].ceEnd) {
					int swap = SRAMismatchModel->cases[j].steps[k].ceEnd;
					SRAMismatchModel->cases[j].steps[k].ceEnd=SRAMismatchModel->cases[j].steps[k].ceStart;
					SRAMismatchModel->cases[j].steps[k].ceStart=swap;
				}

				//DIU
				//SRAMismatchModel->cases[j].steps[k].ceThreshold=0;
				//EODIU
					
			}

		}
	}

	return SRAMismatchModel;
}
