/*
 *
 *    PE.c
 *    Soap3(gpu)
 *
 *    Copyright (C) 2011, HKU
 *
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU General Public License
 *    as published by the Free Software Foundation; either version 2
 *    of the License, or (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include "PE.h"

void createQueryPackedDNA(unsigned char* query, unsigned int query_length, unsigned int* outPackedDNA) {
      unsigned int numOfWord = (query_length+15)/16;
      // initialize outPackedDNA
      unsigned int i;
      for (i=0; i<numOfWord; i++) {
            outPackedDNA[i]=0;
      }
      for (i=0; i<query_length; i++) {
            outPackedDNA[i/16] |= (((unsigned int)query[i]) << ((15 - i%16)*2));
      }
}

void createRevQueryPackedDNA(unsigned char* query, unsigned int query_length, unsigned int* outPackedDNA) {
      unsigned int numOfWord = (query_length+15)/16;
      // initialize outPackedDNA
      unsigned int i;
      for (i=0; i<numOfWord; i++) {
            outPackedDNA[i]=0;
      }
      for (i=0; i<query_length; i++) {
            outPackedDNA[i/16] |= ((3-(unsigned int)query[query_length-1-i]) << ((15 - i%16)*2));
      }
}


char numMismatch(unsigned int* query, unsigned int* target, unsigned int query_length, unsigned int max_mismatch) {
      // 2 bit per char
      unsigned int i;
      unsigned int j;
      unsigned int numOfWord = (query_length+15)/16;
      /*
      printf("Query:\n");
      for (i = 0; i < numOfWord * 16; ++i) {
            j = (query[i / 16] >> ((15 - i%16)*2)) & 3;
            printf("%u ", j);
      }
      printf("\n");
      printf("Target:\n");
      for (i = 0; i < numOfWord * 16; ++i) {
            j = (target[i / 16] >> ((15 - i%16)*2)) & 3;
            printf("%u ", j);
      }
      printf("\n");
       */
      unsigned int pattern;
      char mismatches = 0;
      for (i=0; i<numOfWord && mismatches<=max_mismatch; i++) {
            pattern = query[i] ^ target[i];
            pattern = (((pattern & 2863311530) >> 1) | (pattern & 1431655765));
            // 2863311530 = 101010....10
            // 1431655765 = 010101....01
            mismatches += __builtin_popcount(pattern);
      }
      
      // printf("Number of mismatches: %u\n", mismatches);
      return mismatches;
}

char numMismatchAll(unsigned int* query, unsigned int* target, unsigned int query_length) {
      // 2 bit per char
      unsigned int i;
      unsigned int j;
      unsigned int numOfWord = (query_length+15)/16;
      /*
       printf("Query:\n");
       for (i = 0; i < numOfWord * 16; ++i) {
       j = (query[i / 16] >> ((15 - i%16)*2)) & 3;
       printf("%u ", j);
       }
       printf("\n");
       printf("Target:\n");
       for (i = 0; i < numOfWord * 16; ++i) {
       j = (target[i / 16] >> ((15 - i%16)*2)) & 3;
       printf("%u ", j);
       }
       printf("\n");
       */
      unsigned int pattern;
      char mismatches = 0;
      for (i=0; i<numOfWord; i++) {
            pattern = query[i] ^ target[i];
            pattern = (((pattern & 2863311530) >> 1) | (pattern & 1431655765));
            // 2863311530 = 101010....10
            // 1431655765 = 010101....01
            mismatches += __builtin_popcount(pattern);
      }
      
      // printf("Number of mismatches: %u\n", mismatches);
      return mismatches;
}


void createTargetPackedDNA(unsigned int* target, unsigned int pos, unsigned int target_length, 
                           unsigned int* outPackedDNA) {
      
      
      unsigned int i;
      /*
      unsigned int j;
      printf("Target (before packed):\n");
      printf("pos = %u; pos mod 16 = %u\n", pos, pos%16);
      for (i=0; i<target_length; i++) {
            j = (target[(pos+i)/16] >> (15-(pos+i)%16)*2) & 3;
            printf("%u ", j);
      }
      printf("\n");
      */
      unsigned int numOfWord = (target_length+15)/16;
      // initialize targetPackedDNA
      for (i=0; i<numOfWord; i++) {
            outPackedDNA[i]=0;
      }
      // get the position from l  (hsp->packedDNA)
      // unsigned int s = BWTSaValue(bwt,l);
      for (i=0; i<numOfWord; i++) {
            outPackedDNA[i] |= (target[(pos+i*16)/16] << ((pos+i*16)%16*2));
            if ((pos%16) > 0)
                  outPackedDNA[i] |= (target[(pos+i*16)/16 + 1] >> ((16 - (pos+i*16)%16)*2));
      }
      // the length of the last word
      unsigned int lastWordLen = (target_length-1)%16+1; // range ~ [1,16]
      if (lastWordLen < 16)
            // 4294967295 = 11111...1
            outPackedDNA[numOfWord-1] &= (4294967295 << ((16 - lastWordLen)*2));
}

void SRAEnrichSARanges(HSP* hsp, BWT* bwt, unsigned int* query, unsigned int* rev_query, unsigned int query_length,
                       unsigned int l, unsigned int r, char max_mismatch, char* num_mis, char* strand) {
      // SRAEnrichSARanges takes a SA range as input and return the mismatch count and the strand of the SA range.
      
      // create the packedDNA for the target sequence;
      unsigned int targetPackedDNA[(MAX_READ_LENGTH+15)/16];
      createTargetPackedDNA(hsp->packedDNA, BWTSaValue(bwt,l), query_length, targetPackedDNA);
      
      (*num_mis) = numMismatch(query, targetPackedDNA, query_length, max_mismatch);
      (*strand)=1;
      
      if ((*num_mis) > max_mismatch) {
            (*num_mis) = numMismatch(rev_query, targetPackedDNA, query_length, max_mismatch);
            (*strand) = 2;
      }
}

char SRAEnrichSARangeswithStrand(HSP* hsp, BWT* bwt, unsigned int* query, unsigned int* rev_query, unsigned int query_length,
                       unsigned int l, unsigned int r, char strand) {
      // It takes a SA range [l,r] as well as the strand as input and return the number of mismatches of the SA range.
      // strand = 1: forward; strand = 2: reverse
      
      // create the packedDNA for the target sequence;
      unsigned int targetPackedDNA[(MAX_READ_LENGTH+15)/16];
      createTargetPackedDNA(hsp->packedDNA, BWTSaValue(bwt,l), query_length, targetPackedDNA);
      if (strand==1)
            return numMismatchAll(query, targetPackedDNA, query_length);
      else
            return numMismatchAll(rev_query, targetPackedDNA, query_length);
}


char MismatchInPos(HSP* hsp, BWT* bwt, unsigned int* query, unsigned int* rev_query, unsigned int query_length, unsigned int pos, char strand) {
      // MismatchInPos takes a position and the strand as input and return the mismatch count

      // create the packedDNA for the target sequence;
      unsigned int targetPackedDNA[(MAX_READ_LENGTH+15)/16];
      createTargetPackedDNA(hsp->packedDNA, pos, query_length, targetPackedDNA);
      if (strand==1)
            return numMismatchAll(query, targetPackedDNA, query_length);
      else
            return numMismatchAll(rev_query, targetPackedDNA, query_length);
}
