/*
 *
 *    HOCC.c
 *    Soap3(gpu)
 *
 *    Copyright (C) 2011, HKU
 *
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU General Public License
 *    as published by the Free Software Foundation; either version 2
 *    of the License, or (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */


#include "HOCC.h"

void HOCCGetCachedSARange(HOCC* highOcc, unsigned long long index,unsigned long long * l,unsigned long long * r) {
	unsigned int hOccCachedL = highOcc->table[index*2];
	unsigned int hOccCachedR;
	if (index<(highOcc->ttlPattern)-1) {
		hOccCachedR = hOccCachedL+ ((highOcc->table[index*2+3])-(highOcc->table[index*2+1]))-1;
	} else { 
		hOccCachedR = hOccCachedL+ ((highOcc->ttlOcc)-(highOcc->table[index*2+1]))-1;
	}
	(*l)=hOccCachedL;
	(*r)=hOccCachedR;
}


unsigned int HOCCApproximateRank(HOCC* highOcc, unsigned long long l) {
	unsigned int * bitPattern=highOcc->packed;
	unsigned int * occMajorValue=highOcc->majorValue;
	unsigned short * occMinorValue=highOcc->minorValue;
	
	int BIT_PER_WORD = 32;
	int OCC_MAJOR_INTERVAL = 65536;
	int OCC_MINOR_INTERVAL = 256;
	unsigned int SAMPLE_FACTOR=4;
	
	unsigned int sampledIndex = l/SAMPLE_FACTOR;
	
	unsigned int majorIndex = sampledIndex / OCC_MAJOR_INTERVAL;
	unsigned int minorIndex = sampledIndex / OCC_MINOR_INTERVAL;
	
	int offset = sampledIndex % OCC_MINOR_INTERVAL;	
	unsigned int i = minorIndex * OCC_MINOR_INTERVAL;
	int wordToCount = offset / BIT_PER_WORD;
	wordToCount += (offset % BIT_PER_WORD > 0);
	
	unsigned int popCount = 0;
	int j;
	for (j=0;j<wordToCount-1;j++) {
		popCount+=__builtin_popcount(bitPattern[i/BIT_PER_WORD+j]);
		//printBinary(bitPattern[i/BIT_PER_WORD+j],BIT_PER_WORD);
	}
	unsigned int offsetBitPattern = bitPattern[i/BIT_PER_WORD+j] >> (BIT_PER_WORD - (offset % BIT_PER_WORD));
	//printBinary(bitPattern[i/BIT_PER_WORD+j],BIT_PER_WORD);
	popCount+=__builtin_popcount(offsetBitPattern);
	
	unsigned int adsCount = 0;
	if (majorIndex>0) adsCount+=occMajorValue[majorIndex-1];
	if (minorIndex>0 && minorIndex%(OCC_MAJOR_INTERVAL/OCC_MINOR_INTERVAL)>0) adsCount+=occMinorValue[minorIndex-1];
	//printf("ADS count = %u\n",adsCount);
	
	
	return adsCount+popCount;
}