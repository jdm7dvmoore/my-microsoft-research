/*
 *
 *    definitions.h
 *    Soap3(gpu)
 *
 *    Copyright (C) 2011, HKU
 *
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU General Public License
 *    as published by the Free Software Foundation; either version 2
 *    of the License, or (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#ifndef _DEFINITIONS_H_
#define _DEFINITIONS_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "OCC.h"
#include "BGS-HostAlgnmtMdl.h"

#define ALPHABET_SIZE 4
#define MAX_READ_LENGTH 1024
#define MAX_READ_NAME_LENGTH 256
#define CHAR_PER_WORD 16 // 32-bit word


// Note that when the round 1's SA Range is updated,
// the value of "WORD_PER_ANSWER" needs to update too.
// 1st round constants
// #define WORD_PER_ANSWER 4 // for all types of mismatch
// for exact-match
#define MAX_SA_RANGES_ALLOWED1_0 2
// for 1-mismatch
#define MAX_SA_RANGES_ALLOWED1_1 4
// for 2-mismatch
#define MAX_SA_RANGES_ALLOWED1_2 4
// for 3-mismatch
#define MAX_SA_RANGES_ALLOWED1_3 2
// for 4-mismatch
#define MAX_SA_RANGES_ALLOWED1_4 2


// 2nd round constants
// changing the following values may affect the memory control of the program
// for exact-match
#define MAX_SA_RANGES_ALLOWED2_0 16
// for 1-mismatch
#define MAX_SA_RANGES_ALLOWED2_1 512
// for 2-mismatch
#define MAX_SA_RANGES_ALLOWED2_2 32
// for 3-mismatch
#define MAX_SA_RANGES_ALLOWED2_3 16
// for 4-mismatch
#define MAX_SA_RANGES_ALLOWED2_4 16

// best 8192 128
#define NUM_BLOCKS 8192
#define THREADS_PER_BLOCK 128
#define QUERIES_PER_THREAD 1
#define INPUT_BUFFER_SIZE 102400
#define MAX_NUM_BATCH 15
// # of reads will be loaded into the memory each time = MAX_NUM_BATCH*NUM_BLOCKS*THREADS_PER_BLOCK

// whether the second round is skipped
// for exact-match
#define SKIP_ROUND2_0 1
// for 1-mismatch
#define SKIP_ROUND2_1 0
// for 2-mismatch
#define SKIP_ROUND2_2 0
// for 3-mismatch
#define SKIP_ROUND2_3 1
// for 4-mismatch
#define SKIP_ROUND2_4 1

#define GPU_OCC_INTERVAL 128
#define GPU_OCC_VALUE_PER_WORD 1

// #define LOOKUP_SIZE 13
// #define LOOKUP_SIZE_0 0
#define LOOKUP_LOAD_STEP 1048576

// partition for 2-mismatch
#define SIZE_X_RATIO .3
#define SIZE_Y_RATIO .3
// SIZE_Z = the remaining

// partition for 3-mismatch
#define SIZE_1_RATIO .25
#define SIZE_2_RATIO .25
#define SIZE_3_RATIO .25
// SIZE_4 = the remaining

// partition for 4-mismatch
#define SIZE_A_RATIO .2
#define SIZE_B_RATIO .2
#define SIZE_C_RATIO .2
#define SIZE_D_RATIO .2
// SIZE_E = the remaining


#define NUM_CASES_0M 1
#define NUM_CASES_1M 2
#define NUM_CASES_2M 4
#define NUM_CASES_3M 6
#define NUM_CASES_4M 10
#define MAX_NUM_CASES 10
#define MAX_FILEEXT_LEN 6
#define MAX_NUM_CPU_THREADS 6
#define MAX_NUM_MISMATCH 4

// default values of parameters
#define DEFAULT_MAX_READ_LEN 120
#define DEFAULT_NUM_MISMATCH_FOR_NORMAL_READ 3 // read length >= 50
#define DEFAULT_NUM_MISMATCH_FOR_SHORT_READ 2 // read length < 50
#define DEFAULT_TRIM_LEN 20
#define DEFAULT_NUM_MISMATCH_FOR_TRIM 2

#define OUTPUT_ALL_VALID 1
#define OUTPUT_ALL_BEST 2
#define OUTPUT_UNIQUE_BEST 3
#define OUTPUT_RANDOM_BEST 4

#define SINGLE_READ 1
#define PAIR_END_READ 2

// uncomment the below to enable the constraint 
// on the number of hits for each end
// when finding a valid pair of hit
#define NO_CONSTRAINT_SINGLE_READ_NUM_FOR_PAIRING

// DEBUG SWITCH
// ============
//
// Uncomment the below to enable BGS to output
// found SA ranges to standard out.
// Note that sm_20 is needed for this to work.
// #define BGS_OUTPUT_SA_RANGE_TO_SCREEN
//----
//
// Uncomment the below to show debug messages.
// IT IS HIGHLY RECOMMENDED that this functionality
// is used against only small amount of read input, like below 10,
//// as the amount of output generated is enormous.
//#define BGS_OUTPUT_GENERAL_DEBUG_MESSAGE
//
//#define BGS_OUTPUT_KERNEL_DEBUG_MESSAGE
//----
//
// Uncomment the below to disable the cases.
//#define BGS_DISABLE_CASE_A
//#define BGS_DISABLE_CASE_B
//#define BGS_DISABLE_CASE_C
//#define BGS_DISABLE_CASE_D
//#define BGS_DISABLE_CASE_E
//#define BGS_DISABLE_CASE_F
//#define BGS_DISABLE_CASE_G
//#define BGS_DISABLE_CASE_H
//#define BGS_DISABLE_CASE_I
//#define BGS_DISABLE_CASE_J
//----
//
// Uncomment the below to disable negative strand for both device and host.
//#define BGS_DISABLE_NEGATIVE_STRAND
//----
//
// Uncomment the below to enable extra timing figures.
//#define BGS_GPU_CASE_BREAKDOWN_TIME
//#define BGS_CPU_CASE_BREAKDOWN_TIME
//#define BGS_ROUND_BREAKDOWN_TIME
//#define BGS_CPU_JOBS_BREAKDOWN_TIME
//----
//
// Uncomment the below to enable file name output.
// #define BGS_OUTPUT_FILENAMES_TO_SCREEN
//----
//
// Uncomment the below to enable breakdown in reported occurrences.
//#define BGS_OCC_RESULT_BREAKDOWN
//----
// Uncomment the below to enable memory usage output.
//#define BGS_OUTPUT_MEMORY_USAGE
//----
// Uncomment the below to enable bad read information.
//#define BGS_BAD_READS_INFO
//----
// Uncomment the below to output the readID for second iteration (i.e. no-hit reads for trimming).
//#define BGS_OUTPUT_NO_HIT_READS
//----
// Uncomment the below to output the input parameters.
// #define BGS_OUTPUT_PARAMETERS
//----

typedef unsigned int uint;
typedef unsigned long long ullint;


typedef struct HostKernelArguements {
      unsigned char * upkdQueries;
      char * upkdQueryNames;
      unsigned int batchFirstReadId;
      unsigned int skipFirst;
      unsigned int numQueries;
      
      OCC * occ;
      
      SRAQuerySetting SRAQuerySet;
      SRAQueryResultCount SRAAccumulatedResultCount;
      SRAQueryInput SRAQueryInputPos;
      SRAQueryInput SRAQueryInputNeg;
      SRAModel ** SRAMismatchModel;
      
      char * outputFileName;
      unsigned int ** answers;
      unsigned int ** badReadIndices;
      unsigned int ** badAnswers;
      unsigned int * badStartOffset; 
      unsigned int * badCountOffset;
      unsigned int maxReadLength;
      unsigned int * readLengths;
      unsigned int * readIDs;
      unsigned int * unAlignedIDs;
      unsigned int unAlignedOcc;
      
      unsigned int alignedOcc;
      unsigned int alignedReads;
      int threadId;
      
      unsigned int numCases;
      char outputGoodReads;
      unsigned int sa_range_allowed_1;
      unsigned int sa_range_allowed_2;
      char skip_round_2;
      unsigned int accumReadNum;
      unsigned int word_per_ans;
      
      char readType; // 1: single read; 2: pair-end read
      char maxNumMismatch;
      int insert_low;
      int insert_high;
      int peStrandLeftLeg;
      int peStrandRightLeg;
      char reportType; // 1: all valid; 2: all best; 3: unique best; 4: random best
      int outputUnpairReads; // for SAM output. if 1: then output the unpaired reads alignment.
      
      unsigned int peMaxOutputPerPair;
      uint8_t isTerminalCase;
      
} HostKernelArguements;

typedef struct InputOptions {
      
      char* queryFileName;
      char* queryFileName2;
      int maxReadLength;
      int outputFormat;
      
      char readType; // 1: single; 2: pair-end
      int numMismatch; // the maximum number of mismatch allowed for the alignment of input reads
      int alignmentType; // 1: All valid alignment; 2: all best alignment
      char enableTrim; // whether the no-hit reads will be trimmed for further process
      
      int insert_low;
      int insert_high;

      // the following arguments take into effect only when trim is enable:
      int lenTrim; // the length should be trimmed from the tail part of the read
      int numMismatchForTrim; // the maximum number of mismatch allowed for the alignment of trimmed reads
} InputOptions;


typedef struct IniParams {
      char Ini_SaValueFileExt[MAX_FILEEXT_LEN];
      int  Ini_NumOfCpuThreads;
      char Ini_HostAlignmentModelStr[4];
      int  Ini_HostAlignmentModel;
      int  Ini_GPUMemory;
      int  Ini_PEStrandLeftLeg;
      int  Ini_PEStrandRightLeg;
      unsigned int Ini_MaxOutputPerRead;
      unsigned int Ini_PEMaxOutputPerPair;
} IniParams;

typedef struct IndexFileNames {
      char *iniFileName;
      char *bwtCodeFileName;
      char *occValueFileName;
      char *gpuOccValueFileName;
      char *lookupTableFileName;
      char *revBwtCodeFileName;
      char *revOccValueFileName;
      char *revGpuOccValueFileName;
      char *revLookupTableFileName;
      char *saCodeFileName;
      char *memControlFileName;
      
      //For HSP
      char *packedDnaFileName;
      char *annotationFileName;
      char *ambiguityFileName;
      char *translateFileName;
} IndexFileNames;

#endif
