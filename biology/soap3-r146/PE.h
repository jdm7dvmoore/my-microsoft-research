/*
 *
 *    PE.h
 *    Soap3(gpu)
 *
 *    Copyright (C) 2011, HKU
 *
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU General Public License
 *    as published by the Free Software Foundation; either version 2
 *    of the License, or (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#ifndef _PE_H_
#define _PE_H_

#include <stdio.h>
#include <stdlib.h>
#include "BWT.h"
#include "HSP.h"

#include "definitions.h"

// make the packed DNA for the query
void createQueryPackedDNA(unsigned char* query, unsigned int query_length, unsigned int* outPackedDNA);

// make the packed DNA for the reverse strand of the query
void createRevQueryPackedDNA(unsigned char* query, unsigned int query_length, unsigned int* outPackedDNA);


// SRAEnrichSARanges takes a SA range as input and return the mismatch count and the strand of the SA range.
void SRAEnrichSARanges(HSP* hsp, BWT* bwt, unsigned int* query, unsigned int* rev_query, unsigned int query_length, unsigned int l, unsigned int r, char max_mismatch, char* num_mis, char* strand);


// It takes a SA range [l,r] as well as the strand as input and return the number of mismatches of the SA range.
char SRAEnrichSARangeswithStrand(HSP* hsp, BWT* bwt, unsigned int* query, unsigned int* rev_query, unsigned int query_length, unsigned int l, unsigned int r, char strand);
// strand = 1: forward; strand = 2: reverse
      
// MismatchInPos takes a position and the strand as input and return the mismatch count
char MismatchInPos(HSP* hsp, BWT* bwt, unsigned int* query, unsigned int* rev_query, unsigned int query_length, unsigned int pos, char strand);

#endif
