/*
 *
 *    soap3_aligner.cu
 *    Soap3(gpu)
 *
 *    Copyright (C) 2011, HKU
 *
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU General Public License
 *    as published by the Free Software Foundation; either version 2
 *    of the License, or (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include "BWT.h"
#include "HSP.h"
#include "Timing.h"
#include "dictionary.h"
#include "iniparser.h"
#include "definitions.h"
#include "GPUfunctions.h"
#include "CPUfunctions.h"
#include "alignment.h"
#include "SAM.h"

#include "BGS-IO.h"
#include "BGS-HostAlgnmtMdl.h"
#include "BGS-HostAlgnmtOps.h"

int main(int argc, char **argv) {


      // ======================================================================================
      // | VARIABLE DECLARATION                                                               |
      // ======================================================================================

      // local variables used in main, like BWT indexes and such.
      int i;
      BWT * bwt;
      BWT * revBwt;
      HSP * hsp;
      uint numOfOccValue;
      uint *lkt, *revLkt;
      uint *revOccValue, *occValue;
      uint lookupWordSize;

      double startTime, indexLoadTime, copyTime, readLoadTime, alignmentTime;
      double lastEventTime;
      double totalReadLoadTime=0.0;
      double totalAlignmentTime=0.0;
      double totalTrimAlignmentTime=0.0;

      char *queryFileName = "";
      char *queryFileName2 = "";

      FILE *outputFile;
      FILE *queryFile;
      FILE *queryFile2;

      // user-specified maximum read length
      // and number of words per query
      uint maxReadLength; 
      uint wordPerQuery; 
      // uint numQueries;
      ullint roundUp;
      ullint totalQueryLength;
      uint *queries;
      uint *readLengths;
      uint *readIDs;
      char *upkdQueryNames;
      unsigned char *upkdQueries;
      unsigned char *noAlignment;
      unsigned int *unAlignedPair;
      char queryFileBuffer[INPUT_BUFFER_SIZE];
      char queryFileBuffer2[INPUT_BUFFER_SIZE];
      unsigned int numOfAnswer;
      unsigned int numOfAlignedRead;
      unsigned int numOfUnAlignedPairs;

      // Declare variables and set up preference for device.
      #ifdef BGS_GPU_CASE_BREAKDOWN_TIME
      cudaEvent_t start, stop;
      float time1, totalDeviceTime;
      #endif
      uint *_bwt, *_occ, *_lookupTable; 
      uint *_revBwt, *_revOcc, *_revLookupTable;


      // Accumulated number of reads aligned and alignments
      ullint totalReadsAlignedForInputReads = 0;
      ullint totalAnsForInputReads = 0;

      // Input parameters
      InputOptions input_options;




      // ======================================================================================
      // | Configuration on GPU functions                                                     |
      // ======================================================================================

      cudaFuncSetCacheConfig(kernel, cudaFuncCachePreferShared);
      cudaFuncSetCacheConfig(kernel_4mismatch_1, cudaFuncCachePreferShared);
      cudaFuncSetCacheConfig(kernel_4mismatch_2, cudaFuncCachePreferShared);




      // ======================================================================================
      // | CHECK THE INPUT ARGUMENTS                                                          |
      // ======================================================================================


      bool inputValid = parseInputArgs(argc, argv, input_options);
      if (!inputValid)
         exit(1);
         
      queryFileName = input_options.queryFileName;
      if (input_options.readType == PAIR_END_READ)
         queryFileName2 = input_options.queryFileName2;
      maxReadLength = input_options.maxReadLength;




      // ======================================================================================
      // | PARSING CONFIGURATION FILE                                                         |
      // ======================================================================================
      char* iniFileName;
      iniFileName = (char*)malloc(strlen(argv[0])+5);
      strcpy(iniFileName, argv[0]);
      strcpy(iniFileName + strlen(argv[0]), ".ini");
      IniParams ini_params;
      if (ParseIniFile(iniFileName, ini_params)!=0) {
        fprintf(stderr,"Failed to parse config file ... %s\n", iniFileName);
        return 1;
      }
      printf("[Main] Finished parsing ini file %s.\n\n", iniFileName);
      free(iniFileName);


      // restriction on the maxReadLength when using GPU card with 3G memory
      if ((ini_params.Ini_GPUMemory==3) && (maxReadLength>128)) {
         printf("For GPU card with 3G memory, the program cannot support maximum read length more than 128.\n");
         exit(1);
      }


      // restriction on the number of hits of each end for pairing
      #ifdef NO_CONSTRAINT_SINGLE_READ_NUM_FOR_PAIRING 
            if (input_options.readType == PAIR_END_READ)
                  ini_params.Ini_MaxOutputPerRead=0xFFFFFFFF;
      #endif 
      

      // ======================================================================================
      // | VARIABLES SETTING AND INITIALISATION                                               |
      // ======================================================================================

      // determine number of words per query. rounded up to power of 2
      wordPerQuery = 1;
      while (wordPerQuery < maxReadLength)
      wordPerQuery *= 2;

      wordPerQuery = wordPerQuery / CHAR_PER_WORD;

      uint maxNumQueries = MAX_NUM_BATCH*NUM_BLOCKS*THREADS_PER_BLOCK;
      // maxNumQueries has to be divible by 2
      maxNumQueries = maxNumQueries/2 * 2;


      // The filenames used by the indexes
      IndexFileNames index;
      processIndexFileName(index, argv[2], ini_params);

      // For Output filenames
      char * outputFileName[MAX_NUM_CPU_THREADS];
      for (i=0; i<MAX_NUM_CPU_THREADS; i++)
            outputFileName[i] = (char*)malloc(strlen(argv[3])+8);

      //For Output of the trimmed reads
      /*
      char * outputFileNameTrim[MAX_NUM_CPU_THREADS];
      for (i=0; i<MAX_NUM_CPU_THREADS; i++)
            outputFileNameTrim[i] = (char*)malloc(strlen(argv[3])+13);
      */      

      for (i=0; i<MAX_NUM_CPU_THREADS; i++) {
            sprintf(outputFileName[i], "%s.gout.%d", argv[3], i+1);
            // sprintf(outputFileNameTrim[i], "%s.gout.trim.%d", argv[3], i+1);
      }


      // ======================================================================================
      // | PRINT OUT THE PARAMETERS                                                           |
      // ======================================================================================
      // show the parameter values if necessary
      printParameters(input_options, ini_params);



      // ======================================================================================
      // | INDEX LOADING                                                                      |
      // ======================================================================================
      //Start measuring runtime..
      startTime = setStartTime();
      lastEventTime = startTime;
      loadIndex(index, &bwt, &revBwt, &hsp, &lkt, &revLkt,
               &revOccValue, &occValue, numOfOccValue, lookupWordSize);
        
      indexLoadTime = getElapsedTime(startTime);
      printf("[Main] Finished loading index into host.\n");
      printf("[Main] Loading time : %9.4f seconds\n", indexLoadTime);
      printf("[Main] Reference sequence length : %u\n\n", bwt->textLength);
      lastEventTime = indexLoadTime;




      // ======================================================================================
      // | COPY INDEX TO DEVICE MEMORY                                                        |
      // ======================================================================================


      copyIndexToGPU(&_bwt, &_occ, &_lookupTable,
                    &_revBwt, &_revOcc, &_revLookupTable,
                    bwt, revBwt, occValue, revOccValue,
                    lkt, revLkt, numOfOccValue, lookupWordSize);



      copyTime = getElapsedTime(startTime);
      printf("[Main] Finished copying index into device (GPU).\n");
      printf("[Main] Loading time : %9.4f seconds\n\n", copyTime-lastEventTime);
      lastEventTime = copyTime;
        







      // ======================================================================================
      // | ALLOCATE MEMORY FOR THE ARRAYS                                                     |
      // ======================================================================================

      roundUp = (maxNumQueries + 31) / 32 * 32 + 1024;
      totalQueryLength = roundUp * wordPerQuery;

      queries = (uint*) malloc(totalQueryLength * sizeof(uint)); // a large array to store all queries
      readLengths = (uint*) malloc(roundUp * sizeof(uint));
      readIDs = (uint*) malloc(roundUp * sizeof(uint));
      upkdQueries = (unsigned char*) malloc(roundUp*maxReadLength * sizeof(unsigned char)); // TODO: still need this?
      upkdQueryNames = (char*) malloc(roundUp*MAX_READ_NAME_LENGTH * sizeof(char)); 
      noAlignment = (unsigned char*) malloc(roundUp * sizeof(unsigned char));
      unAlignedPair = (unsigned int*) malloc(roundUp * sizeof(unsigned int));

        
      uint maxBatchSize = NUM_BLOCKS * THREADS_PER_BLOCK * QUERIES_PER_THREAD; // queries processed in one kernel call
      // maxBatchSize has to be divible by 2
      maxBatchSize = maxBatchSize/2 * 2;


      #ifdef BGS_GPU_CASE_BREAKDOWN_TIME
      totalDeviceTime=0;
      #endif


      // initialize the output files
      bam_header_t samOutputHeader;
      samfile_t * samOutputFilePtr[MAX_NUM_CPU_THREADS];
      // samfile_t * samOutputFilePtrTrim[MAX_NUM_CPU_THREADS];
      switch (input_options.outputFormat) {
        case SRA_OUTPUT_FORMAT_SAM_API:
          SAMOutputHeaderConstruct(&samOutputHeader,hsp);
          for (int i = 0; i < MAX_NUM_CPU_THREADS; i++) {
            samOutputFilePtr[i] = samopen(outputFileName[i],"wh",&samOutputHeader);
            // samOutputFilePtrTrim[i] = samopen(outputFileNameTrim[i],"wh",&samOutputHeader);
          }
          break;
        default:
          for (int i = 0; i < MAX_NUM_CPU_THREADS; i++) {
              outputFile = (FILE*)fopen(outputFileName[i], "w");
              if (outputFile==NULL) { fprintf(stderr,"Cannot open outputFile %s\n", outputFileName[i]); exit(1);} 
              OCCWriteOutputHeader(hsp,outputFile,maxReadLength,1,input_options.outputFormat); // will modify the number of reads later
              fclose(outputFile);
              /*
              outputFile = (FILE*)fopen(outputFileNameTrim[i], "w");
              if (outputFile==NULL) { fprintf(stderr,"Cannot open outputFile %s\n", outputFileNameTrim[i]); exit(1);} 
              OCCWriteOutputHeader(hsp,outputFile,maxReadLength,1,input_options.outputFormat); // will modify the number of reads later
              fclose(outputFile);
              */
          }
          break;
      }
        





        

      // ======================================================================================
      // | LOADING INPUT SHORT READ FILE                                                      |
      // ======================================================================================
      queryFile = (FILE*)fopen(queryFileName, "rb");
      if (queryFile==NULL) { fprintf(stderr,"Cannot open queryFile\n"); exit(1);}

      size_t bufferSize = fread(queryFileBuffer,sizeof(char),INPUT_BUFFER_SIZE,queryFile);
      uint bufferIndex = 0;
      char queryChar = queryFileBuffer[bufferIndex++];

      size_t bufferSize2;
      uint bufferIndex2;
      char queryChar2;
      if (input_options.readType==PAIR_END_READ) {
          // pair-ended reads
          queryFile2 = (FILE*)fopen(queryFileName2, "rb");
          if (queryFile2==NULL) { fprintf(stderr,"Cannot open queryFile2\n"); exit(1);}

          bufferSize2 = fread(queryFileBuffer2,sizeof(char),INPUT_BUFFER_SIZE,queryFile2);
          bufferIndex2 = 0;
          queryChar2 = queryFileBuffer2[bufferIndex2++];
      }



      uint accumReadNum = 0;

      uint numQueries;
      if (input_options.readType==SINGLE_READ) {
          numQueries = loadSingleReads(queryFile, queryFileBuffer, queries, readLengths, readIDs, upkdQueries, upkdQueryNames,
                  maxReadLength, maxNumQueries, bufferSize, queryChar, bufferIndex, accumReadNum, wordPerQuery);
      } else {
          numQueries = loadPairReads(queryFile, queryFileBuffer, queryFile2, queryFileBuffer2, 
                  queries, readLengths, readIDs, upkdQueries, upkdQueryNames,
                  maxReadLength, maxNumQueries, 
                  bufferSize, queryChar, bufferIndex, bufferSize2, queryChar2, bufferIndex2, 
                  accumReadNum, wordPerQuery);
      }

      printf("[Main] Loaded %u short reads from the query file.\n", numQueries);
      readLoadTime = getElapsedTime(startTime);
      printf("[Main] Elapsed time on host : %9.4f seconds\n\n", readLoadTime-lastEventTime);
      totalReadLoadTime += readLoadTime-lastEventTime;
      lastEventTime = readLoadTime;


      while (numQueries > 0) {


         // ==================================================================
         // | IF USER DOES NOT SPECIFY # OF MISMATCHES                       |
         // | THEN SET THE DEFAULT # OF MISMATCHES AS:                       |
         // |   - IF READ LENGTH < 50, DEFAULT_NUM_MISMATCH_FOR_SHORT_READ   |
         // |   - IF READ LENGTH >= 50, DEFAULT_NUM_MISMATCH_FOR_NORMAL_READ |
         // ==================================================================
         
         if (input_options.numMismatch==-1) {
            // user does not specify # of mismatches
            input_options.numMismatch = get_default_mismatch_num(readLengths, numQueries);
            printf("Maximum number of mismatches allowed: %i\n",  input_options.numMismatch);
         }







          // =======================================
          // | BATCH PROCESS READ (GPU/GPU/CPU)    |
          // =======================================

          

          numOfAnswer = 0;
          numOfAlignedRead = 0;
          numOfUnAlignedPairs = 0;
          
          uint origNumQueries = numQueries;


          char** currOutputFileName = outputFileName;
          samfile_t** currSamOutputFilePtr = samOutputFilePtr;
          
          /*
                if (currIteration == 1 && input_options.enableTrim==TRUE) {
                      currOutputFileName = outputFileNameTrim;
                      currSamOutputFilePtr = samOutputFilePtrTrim;
                }
          */
          

          //*******************************//
          // Perform Alignment             //
          //*******************************//
          if (input_options.readType==PAIR_END_READ && input_options.alignmentType == OUTPUT_RANDOM_BEST) {

                random_best_pair_alignment(queries, readLengths, input_options.numMismatch, wordPerQuery,
                         maxBatchSize, numQueries, accumReadNum,
                         bwt, revBwt, _bwt, _revBwt, _occ, _revOcc,
                         hsp, lkt, revLkt,
                         _lookupTable, _revLookupTable, ini_params, input_options,
                         upkdQueries, 
                         noAlignment, 
                         unAlignedPair, numOfUnAlignedPairs,
                         readIDs, upkdQueryNames,
                         currOutputFileName, currSamOutputFilePtr,
                         numOfAnswer, numOfAlignedRead);

          } else if (input_options.readType==SINGLE_READ && 
                   (input_options.alignmentType == OUTPUT_ALL_BEST || 
                    input_options.alignmentType == OUTPUT_UNIQUE_BEST ||
                    input_options.alignmentType == OUTPUT_RANDOM_BEST)) {

                best_single_alignment(queries, readLengths, input_options.numMismatch, wordPerQuery,
                         maxBatchSize, numQueries, accumReadNum,
                         bwt, revBwt, _bwt, _revBwt, _occ, _revOcc,
                         hsp, lkt, revLkt,
                         _lookupTable, _revLookupTable, ini_params, input_options,
                         upkdQueries, 
                         noAlignment, 
                         unAlignedPair, numOfUnAlignedPairs,
                         readIDs, upkdQueryNames,
                         currOutputFileName, currSamOutputFilePtr,
                         numOfAnswer, numOfAlignedRead);
         } else { 

                all_valid_alignment(queries, readLengths, input_options.numMismatch, wordPerQuery,
                         maxBatchSize, numQueries, accumReadNum,
                         bwt, revBwt, _bwt, _revBwt, _occ, _revOcc,
                         hsp, lkt, revLkt,
                         _lookupTable, _revLookupTable, ini_params, input_options,
                         upkdQueries, 
                         noAlignment, 
                         unAlignedPair, numOfUnAlignedPairs,
                         readIDs, upkdQueryNames,
                         currOutputFileName, currSamOutputFilePtr,
                         numOfAnswer, numOfAlignedRead, false, 1, 1);

          }
          
          
          printf("Finished alignment with <= %i mismatches\n", input_options.numMismatch);
          alignmentTime = getElapsedTime(startTime);
          printf("Elapsed time : %9.4f seconds\n\n", alignmentTime-lastEventTime);
          totalAlignmentTime += alignmentTime-lastEventTime;
          lastEventTime = alignmentTime;
          totalReadsAlignedForInputReads += numOfAlignedRead;
          totalAnsForInputReads += numOfAnswer;
          printf("Number of reads aligned: %u (number of alignments: %u)\n", numOfAlignedRead, numOfAnswer);
         
          
          accumReadNum += origNumQueries;

          if (input_options.readType==SINGLE_READ) {
                numQueries = loadSingleReads(queryFile, queryFileBuffer, queries, readLengths, readIDs, upkdQueries, upkdQueryNames,
                        maxReadLength, maxNumQueries, bufferSize, queryChar, bufferIndex, accumReadNum, wordPerQuery);
          } else {
                numQueries = loadPairReads(queryFile, queryFileBuffer, queryFile2, queryFileBuffer2, 
                        queries, readLengths, readIDs, upkdQueries, upkdQueryNames,
                        maxReadLength, maxNumQueries, 
                        bufferSize, queryChar, bufferIndex, bufferSize2, queryChar2, bufferIndex2, 
                        accumReadNum, wordPerQuery);
          }

          if (numQueries > 0) {
                printf("[Main] Loaded %u short reads from the query file.\n", numQueries);
                readLoadTime = getElapsedTime(startTime);
                printf("[Main] Elapsed time on host : %9.4f seconds\n\n", readLoadTime-lastEventTime);
                totalReadLoadTime += readLoadTime-lastEventTime;
                lastEventTime = readLoadTime;
          }


      }
      fclose(queryFile);


      switch (input_options.outputFormat) {
        case SRA_OUTPUT_FORMAT_DEFAULT:
            // update the header of the output files
            for (int i = 0; i < MAX_NUM_CPU_THREADS; i++) {
                outputFile = (FILE*)fopen(outputFileName[i], "r+");
                if (outputFile==NULL) { fprintf(stderr,"Cannot open outputFile %s\n", outputFileName[i]); exit(1);}
                fseek ( outputFile , 0 , SEEK_SET );
                OCCWriteOutputHeader(hsp,outputFile,maxReadLength,accumReadNum,input_options.outputFormat); // update the number of reads
                fclose(outputFile);
                /*
                outputFile = (FILE*)fopen(outputFileNameTrim[i], "r+");
                if (outputFile==NULL) { fprintf(stderr,"Cannot open outputFile %s\n", outputFileNameTrim[i]); exit(1);} 
                fseek ( outputFile , 0 , SEEK_SET );
                OCCWriteOutputHeader(hsp,outputFile,maxReadLength,accumReadNum,input_options.outputFormat); // udate the number of reads
                fclose(outputFile);
                */
            }
            break;
        case SRA_OUTPUT_FORMAT_SAM_API:
            for (int i = 0; i < MAX_NUM_CPU_THREADS; i++) {
                samclose(samOutputFilePtr[i]);
                // samclose(samOutputFilePtrTrim[i]);
            }
            SAMOutputHeaderDestruct(&samOutputHeader);
            break;
      }





      cudaThreadExit();




      // ======================================================================================
      // | SHOW THE SUMMARY                                                                   |
      // ======================================================================================


      if (input_options.readType==PAIR_END_READ) {
          printf("[Main] Overall number of pairs of reads aligned: %llu (number of alignments: %llu)\n", totalReadsAlignedForInputReads/2, totalAnsForInputReads);
      } else {
          printf("[Main] Overall number of reads aligned: %llu (number of alignments: %llu)\n", totalReadsAlignedForInputReads, totalAnsForInputReads);
          printf("[Main] Overall number of unaligned reads: %llu\n", accumReadNum-totalReadsAlignedForInputReads);
      }

      printf("[Main] Overall read load time : %9.4f seconds\n",totalReadLoadTime);
      printf("[Main] Overall alignment time (excl. read loading) : %9.4f seconds\n",totalAlignmentTime+totalTrimAlignmentTime);


      // ======================================================================================
      // | CLEAN UP                                                                           |
      // ======================================================================================


      // free device memory
      printf("[Main] Free device memory..\n");
      cudaFree(_bwt);
      cudaFree(_occ);
      // cudaFree(_lookupTable);
      cudaFree(_revBwt);
      cudaFree(_revOcc);
      // cudaFree(_revLookupTable);

      printf("[Main] Free index from host memory..\n");
      BWTFree(bwt);
      BWTFree(revBwt);
      HSPFree(hsp,1);

      printf("[Main] Free host memory..\n");
      free(queries);
      free(readLengths);
      free(readIDs);
      free(noAlignment);
      free(unAlignedPair);

      // free(isBad);
      free(upkdQueries);
      free(upkdQueryNames);
      free(lkt);
      free(revLkt);
      for (i=0; i<MAX_NUM_CPU_THREADS; i++)
        free(outputFileName[i]);

      printf("[Main] soap3_aligner terminated.\n");

      // ======================================================================================
      // | SHOW THE COMMAND FOR MERGING THE OUTPUT FILES INTO ONE                             |
      // ======================================================================================

      show_merge_file_command(input_options, queryFileName, queryFileName2);


      return 0;
}
