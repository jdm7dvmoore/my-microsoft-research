/*
 *
 *    BGS-HostAlgnmtMdl.h
 *    Soap3(gpu)
 *
 *    Copyright (C) 2011, HKU
 *
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU General Public License
 *    as published by the Free Software Foundation; either version 2
 *    of the License, or (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */


#ifndef __BGS_HOSTALIGNMENT_MODEL_H__
#define __BGS_HOSTALIGNMENT_MODEL_H__

#include <stdio.h>
#include <stdlib.h>
#include "BWT.h"
#include "HSP.h"
#include "HOCC.h"
#include "OCC.h"
#include "samtools-0.1.18/sam.h"

//Define the debug flag to turn on extra output.
//#define DEBUG_2BWT

#define MAX_NUM_OF_SRA_STEPS                        16
#define MAX_NUM_OF_SRA_CASES                        64

#define SRA_MAX_READ_LENGTH                          1024
#define MAX_NUM_OF_MISMATCH                          5
#define MAX_NUM_OF_INDEL                             5

#define MAX_CE_THRESHOLD                            32   // from 32 changed to 0 so that the CE function will not be invoked
#define MAX_CE_BUFFER_SIZE                          SRA_MAX_READ_LENGTH / CHAR_PER_64 + 1

#define SRA_MODEL_8G                                 0
#define SRA_MODEL_16G                                1

#define SRA_CASE_TYPE_ALIGNMENT                      0
#define SRA_CASE_TYPE_NEXT_STAGE                     1

#define SRA_STEP_TYPE_COMPLETE                       0
#define SRA_STEP_TYPE_COMPLETE_ERROR                 1
#define SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP           2
#define SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP  3
#define SRA_STEP_TYPE_BACKWARD_ONLY_BWT              4
#define SRA_STEP_TYPE_BI_DIRECTIONAL_BWT             5

#define SRA_STEP_ERROR_TYPE_NO_ERROR                             0
#define SRA_STEP_ERROR_TYPE_MISMATCH_ONLY                        1
#define SRA_STEP_ERROR_TYPE_EDIT_DISTANCE                        2
#define SRA_STEP_ERROR_TYPE_EDIT_DISTANCE_BOUNDARY               3

#define REPORT_UNIQUE_BEST                           0  
#define REPORT_RANDOM_BEST                           1
#define REPORT_ALL                                   2
#define REPORT_ALL_BEST                              3
#define REPORT_BEST_QUALITY                          4
#define REPORT_DETAIL                                5

#define SRA_OUTPUT_FORMAT_DEFAULT               0
#define SRA_OUTPUT_FORMAT_PLAIN                 1
#define SRA_OUTPUT_FORMAT_SAM                   4
#define SRA_OUTPUT_FORMAT_BAM                   3
#define SRA_OUTPUT_FORMAT_SAM_API               2

typedef struct SRAQuerySetting{
	int ReadStrand;
	int OutputType;
	int MaxError;
	int ErrorType;
	FILE * OutFilePtr;
	char * OutFileName;
	char   OutFileFormat;
    
    //Parameter for file splitting
	unsigned int OutFilePart;
	unsigned int writtenCacheCount;
    unsigned int MaxOutputPerRead;
    
	BWT * bwt;
	HSP * hsp;
	HOCC * highOcc;
	OCC * occ;
	unsigned int * lookupTable;
	unsigned int * rev_lookupTable;
    
    //Parameter for SAM
    samfile_t * SAMOutFilePtr;
    
} SRAQuerySetting;

typedef struct SRAQueryResultCount{
	unsigned long long Shared_ReadPositions[MAX_CE_THRESHOLD];
	int Shared_AccMismatch[MAX_CE_THRESHOLD];
	int Shared_AccQualities[MAX_CE_THRESHOLD];

	//IsOpened      1 : Yes
	//              0 : No
	int IsOpened;

	//IsClosed      1 : Yes
	//              0 : No
	int IsClosed;

	//IsUnique      1 : Yes
	//              0 : No
	int IsUnique;

	//Best alignment position
	//IsBest        0 : Unknown
	//              1 : Exists
	int IsBest;

	unsigned long long BestAlignmentPosition;
	int BestAlignmentStrand;
	int BestAlignmentNumOfError;
	int BestAlignmentQuality;

	//WithError stores the number of occurrences found with the corresponding number of mismatch/edits
	unsigned int WithError[(MAX_NUM_OF_MISMATCH>MAX_NUM_OF_INDEL?MAX_NUM_OF_MISMATCH:MAX_NUM_OF_INDEL)+1];
    unsigned int TotalOccurrences;

	//Position retrieved by SA, CE or HOCC
	unsigned int RetrievedBySa;
	unsigned int RetrievedByCe;
	unsigned int RetrievedByHocc;
} SRAQueryResultCount;

typedef struct SRAQueryInput{
	unsigned long long ReadId;
	unsigned long long ReadLength;
	unsigned char * ReadCode;
	unsigned char ReadStrand;
	char * ReadName;
	unsigned long long ReadPacCode[SRA_MAX_READ_LENGTH/CHAR_PER_64+1];
	int * ReadQuality;
	SRAQuerySetting * QuerySetting;
	SRAQueryResultCount * QueryOutput;
} SRAQueryInput;

typedef struct SRAStep {
	int type;
	int start;
	int end;

	int ceThreshold;
	int ceStart;
	int ceEnd;
	
	int ErrorType;
	int MinError;
	int MaxError;

	int step;          //computed
	int len;           //computed
	BWT * bwt;         //computed
} SRAStep;

typedef struct SRACase {
	int id;
	int type;
	int ErrorType;
	int MaxError;
	SRAStep steps[MAX_NUM_OF_SRA_STEPS];
	int * leftMostAligned; //computed, the left most aligned character.
} SRACase;

typedef struct SRAModel {
    unsigned int ModelReadLength;
	SRACase     cases[MAX_NUM_OF_SRA_CASES];
} SRAModel;

void DebugPrintModel(SRAModel * SRAMismatchModel);


void SRAModelFree(SRAModel * sraModel);


int SRAEditCasesPopulate(SRAModel * SRAMismatchModel, int nextCase, 
							 unsigned int ReadLength, int MaxError, int smallErrorFirst, 
							  BWT * bwt, BWT * rev_bwt);
                              
int SRAMismatchCasesPopulate(SRAModel * SRAMismatchModel, int nextCase, 
							 unsigned int ReadLength, int MaxError, int smallErrorFirst, 
							  BWT * bwt, BWT * rev_bwt);

int SRAEditCasesPopulate8G(SRAModel * SRAMismatchModel, int nextCase, 
							 unsigned int ReadLength, int MaxError, int smallErrorFirst, 
							  BWT * bwt, BWT * rev_bwt);
int SRAMismatchCasesPopulate8G(SRAModel * SRAMismatchModel, int nextCase, 
							 unsigned int ReadLength, int MaxError, int smallErrorFirst, 
							  BWT * bwt, BWT * rev_bwt);
 
SRAModel *  SRAModelConstruct(unsigned int ReadLength, 
							  SRAQuerySetting * qSettings, int modelId,
							  BWT * bwt, BWT * rev_bwt);
#endif
