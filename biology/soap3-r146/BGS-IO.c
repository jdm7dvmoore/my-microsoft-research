/*
 *
 *    BGS-IO.c
 *    Soap3(gpu)
 *
 *    Copyright (C) 2011, HKU
 *
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU General Public License
 *    as published by the Free Software Foundation; either version 2
 *    of the License, or (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include "BGS-IO.h"

//Define the below parameter to output the alignment result(text position)
// on screen instead of writing into output file
//#define DEBUG_2BWT_OUTPUT_TO_SCREEN

//Define the below parameter to output the alignment result(text position)
// with the old 2BWT-Aligner format
//#define DEBUG_2BWT_OUTPUT_32_BIT

//Define the below parameter to stop cache reported SA range and text position.
// This is turned on when we are trying to obtain alignment timing only.
//#define DEBUG_2BWT_NO_OUTPUT

//Define the below parameter to skip writing the alignment result(text position)
// into disk. This is turned on when we are trying to obtain alignment timing only.
//#define DEBUG_2BWT_NO_WRITE_FILE

//Define the below parameter to skip translating the alignment result(text position).
// This is turned on when we are trying to obtain alignment timing only.
//#define DEBUG_2BWT_NO_TRANSLATION

OCC * OCCConstruct() {
    OCC * occ = (OCC*) malloc( sizeof(OCC));
    SAMOccurrenceConstruct(occ);
    occ->occPositionCacheCount = 0;
    return occ;
}


void OCCFree(OCC * occ) {
    SAMOccurrenceDestruct(occ);
    free(occ);
}

unsigned int OCCWriteOutputHeader(HSP * hsp, FILE * outFilePtr, 
									unsigned int maxReadLength,
									unsigned int numOfReads,
                                    int outputFormat) {
	unsigned short * ambiguityMap = hsp->ambiguityMap;
	Translate * translate = hsp->translate;
	unsigned int tp, approxIndex, approxValue;
	unsigned int outputVersion = OCC_OUTPUT_FORMAT;
	unsigned int writtenByteCount = 0;
	int i;

    switch (outputFormat) {
    case SRA_OUTPUT_FORMAT_SAM_API:
        //Ad-hoc writing header is not supported by API
        break;
    case SRA_OUTPUT_FORMAT_SAM:

        fprintf(outFilePtr,"@HD\tVN:1.4\tSO:unsorted\n");
        for (i=0;i<hsp->numOfSeq;i++) {
            int j;
            for (j=0;j<255;j++) {
                if (hsp->annotation[i].text[j]=='\0' ||
                    hsp->annotation[i].text[j]==' ' ||
                    hsp->annotation[i].text[j]=='\t' ||
                    hsp->annotation[i].text[j]=='\r' ||
                    hsp->annotation[i].text[j]=='\n') {
                    break;
                }
            }
            hsp->annotation[i].text[j]='\0';
            tp = hsp->seqOffset[i].endPos;
            approxIndex = tp>>GRID_SAMPLING_FACTOR_2_POWER;
            approxValue = ambiguityMap[approxIndex];
            while (translate[approxValue].startPos>tp) {
                approxValue--;
            }
            tp-translate[approxValue].correction;
            
            fprintf(outFilePtr,"@SQ\tSN:%s\tLN:%u\n",hsp->annotation[i].text,tp);
        }
        fprintf(outFilePtr,"@PG\tID:%s\tVN:%s\n","SOAP3-GPU","v1.0.0 R106");
        break;
    
    case SRA_OUTPUT_FORMAT_PLAIN:
      #ifndef SKIP_PLAIN_HEADER
        fprintf(outFilePtr,"SOAP3 Plain Output File\n");
        fprintf(outFilePtr,"========================\n");
        fprintf(outFilePtr,"Read#  ChromId#  Offset  Strand  #Mismatch\n");
      #endif                
        break;
        
    //Implicit case SRA_OUTPUT_FORMAT_DEFAULT:
    default:
        fwrite(&(outputVersion),sizeof(int),1,outFilePtr); writtenByteCount+=sizeof(int);
        fwrite(&(maxReadLength),sizeof(int),1,outFilePtr); writtenByteCount+=sizeof(int);
        fwrite(&(numOfReads),sizeof(int),1,outFilePtr); writtenByteCount+=sizeof(int);
        fwrite(&(hsp->numOfSeq),sizeof(unsigned int),1,outFilePtr); writtenByteCount+=sizeof(unsigned int);
        for (i=0;i<hsp->numOfSeq;i++) {
            fwrite(&hsp->annotation[i].gi,sizeof(int),1,outFilePtr); writtenByteCount+=sizeof(int);
            fwrite(&hsp->annotation[i].text,sizeof(char)*(MAX_SEQ_NAME_LENGTH+1),1,outFilePtr);writtenByteCount+=sizeof(char)*(MAX_SEQ_NAME_LENGTH+1);
            
            tp = hsp->seqOffset[i].endPos;
            approxIndex = tp>>GRID_SAMPLING_FACTOR_2_POWER;
            approxValue = ambiguityMap[approxIndex];
            while (translate[approxValue].startPos>tp) {
                approxValue--;
            }
            tp-=translate[approxValue].correction;
            
            fwrite(&tp,sizeof(unsigned int),1,outFilePtr);writtenByteCount+=sizeof(unsigned int);
        }
    }
	return writtenByteCount;
}


void OCCFlushCachePlain(OCC * occ, HSP * hsp, FILE * outFilePtr) {
    char strandStr[4] = "?+-";
	Translate * occTranslate = hsp->translate;
	unsigned short * occAmbiguityMap = hsp->ambiguityMap;
    OCCPositionCache lastDelimEntry;

    if (occ->occPositionCacheCount>0){
		// ALERT: THIS PART IS NOT 64-BIT COMPATIBLE.
		unsigned long long j;
		unsigned int tp;
		unsigned int approxIndex,approxValue;

		unsigned long long k = 0; j=0;
		for (k=0;k<occ->occPositionCacheCount;k++) {
			if (occ->occPositionCache[j].ChromId==255) {
                lastDelimEntry.ChromId = occ->occPositionCache[j].ChromId;
                lastDelimEntry.occMismatch = occ->occPositionCache[j].occMismatch;
                lastDelimEntry.ReadStrand = occ->occPositionCache[j].ReadStrand;
                lastDelimEntry.tp = occ->occPositionCache[j].tp;
            } else if (occ->occPositionCache[j].ChromId==254) {
			} else {
				tp = (unsigned int) occ->occPositionCache[j].tp;
#ifndef DEBUG_2BWT_NO_TRANSLATION
        		approxIndex = tp>>GRID_SAMPLING_FACTOR_2_POWER;
        		approxValue = occAmbiguityMap[approxIndex];
        		while (occTranslate[approxValue].startPos>tp) {
        			approxValue--;
        		}
				tp-=occTranslate[approxValue].correction;
#endif
#ifdef DEBUG_2BWT_NO_TRANSLATION 
			 occ->occPositionCacheToDisk[j].cell[1]=0;
#endif

#ifdef DEBUG_2BWT_OUTPUT_TO_SCREEN
				printf("[OCCFlushCache] %d %d \n",occTranslate[approxValue].chrID,tp);
#endif
                fprintf(outFilePtr,"%llu %u %llu %c %d\n",lastDelimEntry.tp,occTranslate[approxValue].chrID,(unsigned long long)tp,strandStr[occ->occPositionCache[j].ReadStrand],occ->occPositionCache[j].occMismatch);
			}
			j++;
		}
    }
    
    occ->occPositionCache[0].ChromId =       lastDelimEntry.ChromId;
    occ->occPositionCache[0].occMismatch =   lastDelimEntry.occMismatch;
    occ->occPositionCache[0].ReadStrand =    lastDelimEntry.ReadStrand;
    occ->occPositionCache[0].tp =            lastDelimEntry.tp;
    occ->occPositionCacheCount=1;
}


void OCCFlushCacheDefault(OCC * occ, HSP * hsp, FILE * outFilePtr) {

	Translate * occTranslate = hsp->translate;
	unsigned short * occAmbiguityMap = hsp->ambiguityMap;

    if (occ->occPositionCacheCount>0){
		// ALERT: THIS PART IS NOT 64-BIT COMPATIBLE.
		unsigned long long j;
		unsigned int tp;
		unsigned int approxIndex,approxValue;

		unsigned long long k = 0; j=0;
		for (k=0;k<occ->occPositionCacheCount;k++) {
			if (occ->occPositionCache[j].ChromId==255) {
#ifdef DEBUG_2BWT_OUTPUT_32_BIT
				(*(unsigned int*)&(occ->occPositionCacheToDisk[j].cell[1]))=(unsigned int)occ->occPositionCache[j].tp;
				occ->occPositionCacheToDisk[j].cell[0]=0;
#endif
#ifndef DEBUG_2BWT_OUTPUT_32_BIT
				(*(unsigned long long*)&(occ->occPositionCacheToDisk[j].cell[2]))=(unsigned long long)occ->occPositionCache[j].tp;
				occ->occPositionCacheToDisk[j].cell[1]=0;
				occ->occPositionCacheToDisk[j].cell[0]=0;
#endif
			} else if (occ->occPositionCache[j].ChromId==254) {
            } else {
				tp = (unsigned int) occ->occPositionCache[j].tp;
#ifndef DEBUG_2BWT_NO_TRANSLATION
        		approxIndex = tp>>GRID_SAMPLING_FACTOR_2_POWER;
        		approxValue = occAmbiguityMap[approxIndex];
        		while (occTranslate[approxValue].startPos>tp) {
        			approxValue--;
        		}
				tp-=occTranslate[approxValue].correction;
#endif
#ifdef DEBUG_2BWT_NO_TRANSLATION 
#ifdef DEBUG_2BWT_OUTPUT_32_BIT
			 occ->occPositionCacheToDisk[j].cell[0]=0;
#endif
#ifndef DEBUG_2BWT_OUTPUT_32_BIT
			 occ->occPositionCacheToDisk[j].cell[1]=0;
#endif
#endif

#ifdef DEBUG_2BWT_OUTPUT_TO_SCREEN
				printf("[OCCFlushCache] %d %d \n",occTranslate[approxValue].chrID,tp);
#endif
#ifdef DEBUG_2BWT_OUTPUT_32_BIT
				(*(unsigned int*)&(occ->occPositionCacheToDisk[j].cell[1]))=(unsigned int)tp;
				occ->occPositionCacheToDisk[j].cell[0]=occTranslate[approxValue].chrID;
#endif
#ifndef DEBUG_2BWT_OUTPUT_32_BIT
				(*(unsigned long long*)&(occ->occPositionCacheToDisk[j].cell[2]))=(unsigned long long)tp;
				occ->occPositionCacheToDisk[j].cell[0]=occ->occPositionCache[j].ReadStrand;
				occ->occPositionCacheToDisk[j].cell[1]=occTranslate[approxValue].chrID;
#endif
			}
			j++;
		}

#ifndef DEBUG_2BWT_NO_WRITE_FILE
        fwrite(occ->occPositionCacheToDisk,sizeof(OCCPositionCacheToDisk)*occ->occPositionCacheCount,1,outFilePtr);
#endif
    }
    
    occ->occPositionCacheCount=0;
}


void OCCDirectWritePairUnmapSAM(SRAQueryInput * qInput, SRAOccurrence * sraOcc, int isFirst) {
    SRAQuerySetting * qSetting = qInput->QuerySetting;
    int OutFileFormat = qSetting->OutFileFormat;
    FILE * outFilePtr = qSetting->OutFilePtr;
    HSP * hsp = qSetting->hsp;
    Translate * occTranslate = hsp->translate;
    unsigned short * occAmbiguityMap = hsp->ambiguityMap;


    unsigned long long j=0, k=0;
    unsigned long long ambPosition;
    unsigned long long tp;
    unsigned int correctPosition;
    unsigned int approxIndex,approxValue;

    unsigned long long occCount = 0;

// ATTENTION: THIS PART IS NOT 64-BIT COMPATIBLE.
    {
        ambPosition = sraOcc->ambPosition;
        tp = ambPosition;
        correctPosition = ambPosition;
        approxIndex = ambPosition>>GRID_SAMPLING_FACTOR_2_POWER;
        approxValue = occAmbiguityMap[approxIndex];
        while (occTranslate[approxValue].startPos>ambPosition) {
            approxValue--;
        }

        correctPosition-=occTranslate[approxValue].correction;
        tp=correctPosition;

        //QNAME FLAG RNAME POS
        //MAPQ CIGAR RNEXT PNEXT TLEN
        //SEQ QUAL

        unsigned int samFlag = 0;
        samFlag |= SAM_FLAG_MATE_UNMAPPED;

        samFlag |= SAM_FLAG_FIRST_IN_PAIR*isFirst;
        samFlag |= SAM_FLAG_SECOND_IN_PAIR*(1-isFirst);
        
        if (sraOcc->strand==QUERY_NEG_STRAND) {
            samFlag |= SAM_FLAG_READ_ALGNMT_STRAND*isFirst;
        }

        samFlag |= isFirst * 
        fprintf(outFilePtr,"%s\t%u\t%s\t%llu\t%d\t%lluM\t%s\t%llu\t%d\t",
            qInput->ReadName,         //QNAME
            samFlag,                  //FLAG
            hsp->annotation[occTranslate[approxValue].chrID-1].text,    //RNAME
            tp,                      //POS
            SAM_MAPQ_UNAVAILABLE,    //MAPQ
            qInput->ReadLength,          //CIGAR
            "*",                      //MRNM
            0LL,                      //MPOS
            0                        //ISIZE
        );
        int i;
        for (i=0;i<qInput->ReadLength;i++) {
            fprintf(outFilePtr,"%c",dnaChar[qInput->ReadCode[i]]);      //SEQ
        }
        fprintf(outFilePtr,"\t");
        for (i=0;i<qInput->ReadLength;i++) {
            fprintf(outFilePtr,"%c",33);                                //QUAL
        }
        fprintf(outFilePtr,"\n");
        occCount++;
    }
}

void OCCDirectWritePairOccSAM(SRAQueryInput * qInput, PEPairs * pePair) {
    SRAQuerySetting * qSetting = qInput->QuerySetting;
    int OutFileFormat = qSetting->OutFileFormat;
    FILE * outFilePtr = qSetting->OutFilePtr;
    HSP * hsp = qSetting->hsp;
    Translate * occTranslate = hsp->translate;
    unsigned short * occAmbiguityMap = hsp->ambiguityMap;


    unsigned long long j=0, k=0;
    unsigned long long ambPosition;
    unsigned long long tp_1, tp_2;
    char chr_1, chr_2;
    unsigned int correctPosition;
    unsigned int approxIndex,approxValue;

    unsigned long long occCount = 0;
    char * mateChar;
    char equalChr[3] = "=";

// ATTENTION: THIS PART IS NOT 64-BIT COMPATIBLE.
    {
        ambPosition = pePair->algnmt_1;
        tp_1 = ambPosition;
        correctPosition = ambPosition;
        approxIndex = ambPosition>>GRID_SAMPLING_FACTOR_2_POWER;
        approxValue = occAmbiguityMap[approxIndex];
        while (occTranslate[approxValue].startPos>ambPosition) {
            approxValue--;
        }

        correctPosition-=occTranslate[approxValue].correction;
        tp_1=correctPosition;
        chr_1=occTranslate[approxValue].chrID-1;
    }
    {
        ambPosition = pePair->algnmt_2;
        tp_2 = ambPosition;
        correctPosition = ambPosition;
        approxIndex = ambPosition>>GRID_SAMPLING_FACTOR_2_POWER;
        approxValue = occAmbiguityMap[approxIndex];
        while (occTranslate[approxValue].startPos>ambPosition) {
            approxValue--;
        }

        correctPosition-=occTranslate[approxValue].correction;
        tp_2=correctPosition;
        chr_2=occTranslate[approxValue].chrID-1;
    }
    
    if (chr_1==chr_2) { mateChar = equalChr; } else { mateChar = hsp->annotation[chr_2].text; }

    unsigned int samFlag = 0;
    samFlag |= SAM_FLAG_PROPER_PAIR;
    samFlag |= SAM_FLAG_FIRST_IN_PAIR;
    if (pePair->strand_1==QUERY_NEG_STRAND) {
        samFlag |= SAM_FLAG_READ_ALGNMT_STRAND;
    }
    if (pePair->strand_2==QUERY_NEG_STRAND) {
        samFlag |= SAM_FLAG_MATE_ALGNMT_STRAND;
    }

    //QNAME FLAG RNAME POS
    //MAPQ CIGAR RNEXT PNEXT TLEN
    //SEQ QUAL
    fprintf(outFilePtr,"%s\t%u\t%s\t%llu\t%d\t%lluM\t%s\t%llu\t%d\t",
        qInput->ReadName,         //QNAME
        samFlag,                  //FLAG
        hsp->annotation[chr_1].text,    //RNAME
        tp_1,                       //POS
        SAM_MAPQ_UNAVAILABLE,     //MAPQ  (unavailable)
        qInput->ReadLength,       //CIGAR 
        mateChar,                 //MRNM
        tp_2,                     //MPOS
        pePair->insertion         //ISIZE (unavailable in single-end)
    );
    int i;
    for (i=0;i<qInput->ReadLength;i++) {
        fprintf(outFilePtr,"%c",dnaChar[qInput->ReadCode[i]]);      //SEQ
    }
    fprintf(outFilePtr,"\t");
    for (i=0;i<qInput->ReadLength;i++) {
        fprintf(outFilePtr,"%c",33);                                //QUAL
    }
    fprintf(outFilePtr,"\n");
}

void OCCFlushCacheSAM(SRAQueryInput * qInput) {
    SRAQuerySetting * qSetting = qInput->QuerySetting;
    int OutFileFormat = qSetting->OutFileFormat;
    FILE * outFilePtr = qSetting->OutFilePtr;
    OCC * occ = qSetting->occ;
    HSP * hsp = qSetting->hsp;
    Translate * occTranslate = hsp->translate;
    unsigned short * occAmbiguityMap = hsp->ambiguityMap;


    unsigned long long j=0, k=0;
    unsigned long long ambPosition;
    unsigned long long tp;
    unsigned int correctPosition;
    unsigned int approxIndex,approxValue;

    unsigned long long occCount = 0;
    for (k=0;k<occ->occPositionCacheCount;k++) {
        if (occ->occPositionCache[j].ChromId==255) {
        } else if (occ->occPositionCache[j].ChromId==254) {
        } else {

	    // ATTENTION: THIS PART IS NOT 64-BIT COMPATIBLE.
            ambPosition = occ->occPositionCache[j].tp;
            tp = ambPosition;
            correctPosition = ambPosition;
            approxIndex = ambPosition>>GRID_SAMPLING_FACTOR_2_POWER;
            approxValue = occAmbiguityMap[approxIndex];
            while (occTranslate[approxValue].startPos>ambPosition) {
                approxValue--;
            }

            correctPosition-=occTranslate[approxValue].correction;
            tp=correctPosition;

            unsigned int samFlag = 0;
            if (occ->occPositionCache[j].ReadStrand==QUERY_NEG_STRAND) {
                samFlag |= SAM_FLAG_READ_ALGNMT_STRAND;
            }

            //QNAME FLAG RNAME POS
            //MAPQ CIGAR RNEXT PNEXT TLEN
            //SEQ QUAL
            fprintf(outFilePtr,"%s\t%u\t%s\t%llu\t%d\t%lluM\t%s\t%llu\t%d\t",
                qInput->ReadName,         //QNAME
                samFlag,                  //FLAG
                hsp->annotation[occTranslate[approxValue].chrID-1].text,    //RNAME
                tp,                       //POS
                SAM_MAPQ_UNAVAILABLE,     //MAPQ  (unavailable)
                qInput->ReadLength,       //CIGAR 
                "*",                      //MRNM  (unavailable in single-end)
                0LL,                      //MPOS  (unavailable in single-end)
                0                         //ISIZE (unavailable in single-end)
            );
            int i;
            for (i=0;i<qInput->ReadLength;i++) {
                fprintf(outFilePtr,"%c",dnaChar[qInput->ReadCode[i]]);      //SEQ
            }
            fprintf(outFilePtr,"\t");
            for (i=0;i<qInput->ReadLength;i++) {
                fprintf(outFilePtr,"%c",33);                                //QUAL
            }
            fprintf(outFilePtr,"\n");
            occCount++;
        }
        j++;
    }

    if (occCount==0) {

        unsigned int samFlag = 0;
        samFlag |= SAM_FLAG_READ_UNMAPPED;
        fprintf(outFilePtr,"%s\t%u\t%s\t%llu\t%d\t%s\t%s\t%llu\t%d\t%s\t%s\n",
            qInput->ReadName,         //QNAME
            samFlag,                  //FLAG
            "*",                      //RNAME
            0LL,                      //POS
            0,                        //MAPQ
            "*",                      //CIGAR
            "*",                      //MRNM
            0LL,                      //MPOS
            0,                        //ISIZE
            "*",                      //SEQ
            "*"                       //QUAL
        );
    }
    
    qSetting->writtenCacheCount++;
    occ->occPositionCacheCount=0;
}


void OCCFlushCacheSAMAPI(SRAQueryInput * qInput) {
    SRAQuerySetting * qSetting = qInput->QuerySetting;
    int OutFileFormat = qSetting->OutFileFormat;
    FILE * outFilePtr = qSetting->OutFilePtr;
    OCC * occ = qSetting->occ;
    HSP * hsp = qSetting->hsp;
    Translate * occTranslate = hsp->translate;
    unsigned short * occAmbiguityMap = hsp->ambiguityMap;

    samfile_t * samFilePtr = qSetting->SAMOutFilePtr;
    bam1_t * samAlgnmt = &(occ->SAMOutBuffer);
    
    unsigned long long i=0, j=0, k=0;
    unsigned long long ambPosition;
    unsigned long long tp;
    unsigned int correctPosition;
    unsigned int approxIndex,approxValue;
    
    //Assuming the answer being handled by OCCFlushCacheSAMAPI are all answers of qInput
    // Each occurrences reported by SAM_API will share the following aux data.
    //----------------------------------------------------------------------------------------------------------------------------
        samAlgnmt->core.bin = bam_reg2bin(0,0);                     //SAM: bin calculated by bam_reg2bin()
        samAlgnmt->core.n_cigar = 1;                                //SAM: number of CIGAR operations
        samAlgnmt->core.l_qseq = qInput->ReadLength;                //SAM: length of the query sequence (read)
        samAlgnmt->core.l_qname = strlen(qInput->ReadName)+1;         //SAM: length of the query name
        
        samAlgnmt->core.mtid = -1;
        samAlgnmt->core.mpos = -1;
        samAlgnmt->core.isize = 0;
        
        samAlgnmt->l_aux = 0;
        samAlgnmt->data_len = 0;
        samAlgnmt->m_data = SAM_MDATA_SIZE;
        SAMIUint8ConcatString(samAlgnmt->data,&(samAlgnmt->data_len),qInput->ReadName,strlen(qInput->ReadName)+1);    //Name
        SAMIUint8ConcatUint32(samAlgnmt->data,&(samAlgnmt->data_len),qInput->ReadLength<<BAM_CIGAR_SHIFT);          //CIGAR
        for (i = 0; i < qInput->ReadLength/2; i++) {                                                                //Read
            unsigned char biChar = 0;
            biChar |= bam_nt16_table[dnaChar[qInput->ReadCode[i*2]]] << 4;
            biChar |= bam_nt16_table[dnaChar[qInput->ReadCode[i*2+1]]];
            SAMIUint8ConcatUint8(samAlgnmt->data,&(samAlgnmt->data_len),biChar);
        }
        if ( qInput->ReadLength % 2 == 1 ) {
            uint8_t biChar = bam_nt16_table[dnaChar[qInput->ReadCode[qInput->ReadLength-1]]] << 4;
            SAMIUint8ConcatUint8(samAlgnmt->data,&(samAlgnmt->data_len),biChar);
        }
        for (i = 0; i < qInput->ReadLength; i++) {                                                                  //Quality                
            SAMIUint8ConcatUint8(samAlgnmt->data,&(samAlgnmt->data_len),0xff);
        }
        unsigned int auxStart = samAlgnmt->data_len;
        //SAMIUint8ConcatString(samAlgnmt->data,&(samAlgnmt->data_len),"XAZTESTING",11);
        samAlgnmt->l_aux += samAlgnmt->data_len - auxStart;
    //----------------------------------------------------------------------------------------------------------------------------                      

    unsigned long long occCount = 0;
    for (k=0;k<occ->occPositionCacheCount;k++) {
        if (occ->occPositionCache[j].ChromId==255) {
        } else if (occ->occPositionCache[j].ChromId==254) {

            unsigned int samFlag = 0;
            samFlag |= SAM_FLAG_READ_UNMAPPED;

            // ---------------------->  |
                                        samAlgnmt->core.tid = -1;                                   //SAM: chromosome ID, defined by bam_header_t
                                        samAlgnmt->core.pos = -1;                                   //SAM: 0-based leftmost coordinate4
                                        samAlgnmt->core.qual = 0;                                   //SAM: mapping quality
                                        samAlgnmt->core.flag = samFlag;                             //SAM: bitwise flag
                                        
                                        samwrite(samFilePtr,samAlgnmt);
            // ---------------------->  |
        } else {

	    // ATTENTION: THIS PART IS NOT 64-BIT COMPATIBLE.
            ambPosition = occ->occPositionCache[j].tp;
            tp = ambPosition;
            correctPosition = ambPosition;
            approxIndex = ambPosition>>GRID_SAMPLING_FACTOR_2_POWER;
            approxValue = occAmbiguityMap[approxIndex];
            while (occTranslate[approxValue].startPos>ambPosition) {
                approxValue--;
            }

            correctPosition-=occTranslate[approxValue].correction;
            tp=correctPosition;

            unsigned int samFlag = 0;
            if (occ->occPositionCache[j].ReadStrand==QUERY_NEG_STRAND) {
                samFlag |= SAM_FLAG_READ_ALGNMT_STRAND;
            }
            
            // ---------------------->  |
                                        samAlgnmt->core.tid = occTranslate[approxValue].chrID-1;    //SAM: chromosome ID, defined by bam_header_t
                                        samAlgnmt->core.pos = tp-1;                                 //SAM: 0-based leftmost coordinate
                                        samAlgnmt->core.qual = SAM_MAPQ_UNAVAILABLE;                //SAM: mapping quality
                                        samAlgnmt->core.flag = samFlag;                             //SAM: bitwise flag
                                        
                                        samwrite(samFilePtr,samAlgnmt);
            // ---------------------->  |
            occCount++;
        }
        j++;
    }

    /*if (occCount==0) {

        unsigned int samFlag = 0;
        samFlag |= SAM_FLAG_READ_UNMAPPED;
    
        // ---------------------->  |
                                    samAlgnmt->core.tid = -1;                                   //SAM: chromosome ID, defined by bam_header_t
                                    samAlgnmt->core.pos = -1;                                   //SAM: 0-based leftmost coordinate4
                                    samAlgnmt->core.qual = 0;                                   //SAM: mapping quality
                                    samAlgnmt->core.flag = samFlag;                             //SAM: bitwise flag
                                    
                                    samwrite(samFilePtr,samAlgnmt);
        // ---------------------->  |
    }*/
    
    qSetting->writtenCacheCount++;
    occ->occPositionCacheCount=0;
}



void OCCDirectWritePairOccSAMAPI(SRAQueryInput * qInput, PEPairs * pePair, int isFirstUnaligned, int isSecondUnaligned, int mateLength) {
      // output the pair based on the first read  
      SRAQuerySetting * qSetting = qInput->QuerySetting;
      int OutFileFormat = qSetting->OutFileFormat;
      FILE * outFilePtr = qSetting->OutFilePtr;
      OCC * occ = qSetting->occ;
      HSP * hsp = qSetting->hsp;
      Translate * occTranslate = hsp->translate;
      unsigned short * occAmbiguityMap = hsp->ambiguityMap;
      
      samfile_t * samFilePtr = qSetting->SAMOutFilePtr;
      bam1_t * samAlgnmt = &(occ->SAMOutBuffer);
      
      unsigned long long i=0, j=0, k=0;
      unsigned long long ambPosition;
      unsigned long long tp_1, tp_2;
      char chr_1, chr_2;
      unsigned long long tp;
      unsigned int correctPosition;
      unsigned int approxIndex,approxValue;
      
      if (isFirstUnaligned==0)
      {
            ambPosition = pePair->algnmt_1;
            tp_1 = ambPosition;
            correctPosition = ambPosition;
            approxIndex = ambPosition>>GRID_SAMPLING_FACTOR_2_POWER;
            approxValue = occAmbiguityMap[approxIndex];
            while (occTranslate[approxValue].startPos>ambPosition) {
                  approxValue--;
            }
            
            correctPosition-=occTranslate[approxValue].correction;
            tp_1=correctPosition;
            chr_1=occTranslate[approxValue].chrID-1;
      }

      if (isSecondUnaligned==0)
      {
            ambPosition = pePair->algnmt_2;
            tp_2 = ambPosition;
            correctPosition = ambPosition;
            approxIndex = ambPosition>>GRID_SAMPLING_FACTOR_2_POWER;
            approxValue = occAmbiguityMap[approxIndex];
            while (occTranslate[approxValue].startPos>ambPosition) {
                  approxValue--;
            }
            
            correctPosition-=occTranslate[approxValue].correction;
            tp_2=correctPosition;
            chr_2=occTranslate[approxValue].chrID-1;
      }
      
      unsigned int samFlag = 1;
      if ((isFirstUnaligned==0) && (isSecondUnaligned==0)) {
            samFlag |= SAM_FLAG_PROPER_PAIR;
      }
      if (isFirstUnaligned==1) {
            samFlag |= SAM_FLAG_READ_UNMAPPED;
      }
      if (isSecondUnaligned==1) {
            samFlag |= SAM_FLAG_MATE_UNMAPPED;
      }
      samFlag |= SAM_FLAG_FIRST_IN_PAIR;
      if ((isFirstUnaligned==0) && (pePair->strand_1==QUERY_NEG_STRAND)) {
            samFlag |= SAM_FLAG_READ_ALGNMT_STRAND;
      }
      if ((isSecondUnaligned==0) && (pePair->strand_2==QUERY_NEG_STRAND)) {
            samFlag |= SAM_FLAG_MATE_ALGNMT_STRAND;
      }
      
      //Assuming the answer being handled by OCCFlushCacheSAMAPI are all answers of qInput
      // Each occurrences reported by SAM_API will share the following aux data.
      //----------------------------------------------------------------------------------------------------------------------------
      samAlgnmt->core.bin = bam_reg2bin(0,0);                     //SAM: bin calculated by bam_reg2bin()
      samAlgnmt->core.n_cigar = 1;                                //SAM: number of CIGAR operations
      samAlgnmt->core.l_qseq = qInput->ReadLength;                //SAM: length of the query sequence (read)
      samAlgnmt->core.l_qname = strlen(qInput->ReadName)+1;         //SAM: length of the query name
      
      samAlgnmt->l_aux = 0;
      samAlgnmt->data_len = 0;
      samAlgnmt->m_data = SAM_MDATA_SIZE;
      SAMIUint8ConcatString(samAlgnmt->data,&(samAlgnmt->data_len),qInput->ReadName,strlen(qInput->ReadName)+1);    //Name
      SAMIUint8ConcatUint32(samAlgnmt->data,&(samAlgnmt->data_len),qInput->ReadLength<<BAM_CIGAR_SHIFT);          //CIGAR
      for (i = 0; i < qInput->ReadLength/2; i++) {                                                                //Read
            unsigned char biChar = 0;
            biChar |= bam_nt16_table[dnaChar[qInput->ReadCode[i*2]]] << 4;
            biChar |= bam_nt16_table[dnaChar[qInput->ReadCode[i*2+1]]];
            SAMIUint8ConcatUint8(samAlgnmt->data,&(samAlgnmt->data_len),biChar);
      }
      if ( qInput->ReadLength % 2 == 1 ) {
            uint8_t biChar = bam_nt16_table[dnaChar[qInput->ReadCode[qInput->ReadLength-1]]] << 4;
            SAMIUint8ConcatUint8(samAlgnmt->data,&(samAlgnmt->data_len),biChar);
      }
      for (i = 0; i < qInput->ReadLength; i++) {                                                                  //Quality                
            SAMIUint8ConcatUint8(samAlgnmt->data,&(samAlgnmt->data_len),0xff);
      }
      unsigned int auxStart = samAlgnmt->data_len;
      //SAMIUint8ConcatString(samAlgnmt->data,&(samAlgnmt->data_len),"XAZTESTING",11);
      samAlgnmt->l_aux += samAlgnmt->data_len - auxStart;
      //----------------------------------------------------------------------------------------------------------------------------                      
      
      //Outputing 1 alignment
      // ----------------------
      if (isFirstUnaligned==0) {
            samAlgnmt->core.tid = chr_1;                          //SAM: chromosome ID, defined by bam_header_t
            samAlgnmt->core.pos = tp_1-1;                         //SAM: 0-based leftmost coordinate
      } else {
            samAlgnmt->core.tid = -1;                             //SAM: chromosome ID, defined by bam_header_t
            samAlgnmt->core.pos = -1;                             //SAM: 0-based leftmost coordinate
      }
      samAlgnmt->core.qual = SAM_MAPQ_UNAVAILABLE;                //SAM: mapping quality
      samAlgnmt->core.flag = samFlag;                             //SAM: bitwise flag
      
      if (isSecondUnaligned==0) {
            samAlgnmt->core.mtid = chr_2;
            samAlgnmt->core.mpos = tp_2-1;
      } else {
            samAlgnmt->core.mtid = -1;
            samAlgnmt->core.mpos = -1;
      }
      if ((isFirstUnaligned==0) && (isSecondUnaligned==0) && (chr_1 == chr_2)) {
            if (tp_2 > tp_1)
                  samAlgnmt->core.isize = tp_2 + mateLength - tp_1;
            else
                  samAlgnmt->core.isize = -(tp_1 + qInput->ReadLength - tp_2);
      } else      
            samAlgnmt->core.isize = 0;
      
      samwrite(samFilePtr,samAlgnmt);
      // ----------------------
}

void OCCDirectWritePairOccSAMAPI2(SRAQueryInput * qInput, PEPairs * pePair, int isFirstUnaligned, int isSecondUnaligned, int mateLength) {
      // output the pair based on the second read
      SRAQuerySetting * qSetting = qInput->QuerySetting;
      int OutFileFormat = qSetting->OutFileFormat;
      FILE * outFilePtr = qSetting->OutFilePtr;
      OCC * occ = qSetting->occ;
      HSP * hsp = qSetting->hsp;
      Translate * occTranslate = hsp->translate;
      unsigned short * occAmbiguityMap = hsp->ambiguityMap;
      
      samfile_t * samFilePtr = qSetting->SAMOutFilePtr;
      bam1_t * samAlgnmt = &(occ->SAMOutBuffer);
      
      unsigned long long i=0, j=0, k=0;
      unsigned long long ambPosition;
      unsigned long long tp_1, tp_2;
      char chr_1, chr_2;
      unsigned long long tp;
      unsigned int correctPosition;
      unsigned int approxIndex,approxValue;
      
      if (isSecondUnaligned==0)
      {
            ambPosition = pePair->algnmt_2;
            tp_1 = ambPosition;
            correctPosition = ambPosition;
            approxIndex = ambPosition>>GRID_SAMPLING_FACTOR_2_POWER;
            approxValue = occAmbiguityMap[approxIndex];
            while (occTranslate[approxValue].startPos>ambPosition) {
                  approxValue--;
            }
            
            correctPosition-=occTranslate[approxValue].correction;
            tp_1=correctPosition;
            chr_1=occTranslate[approxValue].chrID-1;
      }
      
      if (isFirstUnaligned==0)
      {
            ambPosition = pePair->algnmt_1;
            tp_2 = ambPosition;
            correctPosition = ambPosition;
            approxIndex = ambPosition>>GRID_SAMPLING_FACTOR_2_POWER;
            approxValue = occAmbiguityMap[approxIndex];
            while (occTranslate[approxValue].startPos>ambPosition) {
                  approxValue--;
            }
            
            correctPosition-=occTranslate[approxValue].correction;
            tp_2=correctPosition;
            chr_2=occTranslate[approxValue].chrID-1;
      }
      
      unsigned int samFlag = 1;
      if ((isFirstUnaligned==0) && (isSecondUnaligned==0)) {
            samFlag |= SAM_FLAG_PROPER_PAIR;
      }
      if (isSecondUnaligned==1) {
            samFlag |= SAM_FLAG_READ_UNMAPPED;
      }
      if (isFirstUnaligned==1) {
            samFlag |= SAM_FLAG_MATE_UNMAPPED;
      }
      samFlag |= SAM_FLAG_SECOND_IN_PAIR;
      if ((isFirstUnaligned==0) && (pePair->strand_1==QUERY_NEG_STRAND)) {
            samFlag |= SAM_FLAG_MATE_ALGNMT_STRAND;
      }
      if ((isSecondUnaligned==0) && (pePair->strand_2==QUERY_NEG_STRAND)) {
            samFlag |= SAM_FLAG_READ_ALGNMT_STRAND;
      }
      
      //Assuming the answer being handled by OCCFlushCacheSAMAPI are all answers of qInput
      // Each occurrences reported by SAM_API will share the following aux data.
      //----------------------------------------------------------------------------------------------------------------------------
      samAlgnmt->core.bin = bam_reg2bin(0,0);                     //SAM: bin calculated by bam_reg2bin()
      samAlgnmt->core.n_cigar = 1;                                //SAM: number of CIGAR operations
      samAlgnmt->core.l_qseq = qInput->ReadLength;                //SAM: length of the query sequence (read)
      samAlgnmt->core.l_qname = strlen(qInput->ReadName)+1;         //SAM: length of the query name
      
      samAlgnmt->l_aux = 0;
      samAlgnmt->data_len = 0;
      samAlgnmt->m_data = SAM_MDATA_SIZE;
      SAMIUint8ConcatString(samAlgnmt->data,&(samAlgnmt->data_len),qInput->ReadName,strlen(qInput->ReadName)+1);    //Name
      SAMIUint8ConcatUint32(samAlgnmt->data,&(samAlgnmt->data_len),qInput->ReadLength<<BAM_CIGAR_SHIFT);          //CIGAR
      for (i = 0; i < qInput->ReadLength/2; i++) {                                                                //Read
            unsigned char biChar = 0;
            biChar |= bam_nt16_table[dnaChar[qInput->ReadCode[i*2]]] << 4;
            biChar |= bam_nt16_table[dnaChar[qInput->ReadCode[i*2+1]]];
            SAMIUint8ConcatUint8(samAlgnmt->data,&(samAlgnmt->data_len),biChar);
      }
      if ( qInput->ReadLength % 2 == 1 ) {
            uint8_t biChar = bam_nt16_table[dnaChar[qInput->ReadCode[qInput->ReadLength-1]]] << 4;
            SAMIUint8ConcatUint8(samAlgnmt->data,&(samAlgnmt->data_len),biChar);
      }
      for (i = 0; i < qInput->ReadLength; i++) {                                                                  //Quality                
            SAMIUint8ConcatUint8(samAlgnmt->data,&(samAlgnmt->data_len),0xff);
      }
      unsigned int auxStart = samAlgnmt->data_len;
      //SAMIUint8ConcatString(samAlgnmt->data,&(samAlgnmt->data_len),"XAZTESTING",11);
      samAlgnmt->l_aux += samAlgnmt->data_len - auxStart;
      //----------------------------------------------------------------------------------------------------------------------------                      
      
      //Outputing 1 alignment
      // ----------------------
      if (isSecondUnaligned==0) {
            samAlgnmt->core.tid = chr_1;                          //SAM: chromosome ID, defined by bam_header_t
            samAlgnmt->core.pos = tp_1-1;                         //SAM: 0-based leftmost coordinate
      } else {
            samAlgnmt->core.tid = -1;                             //SAM: chromosome ID, defined by bam_header_t
            samAlgnmt->core.pos = -1;                             //SAM: 0-based leftmost coordinate
      }
      samAlgnmt->core.qual = SAM_MAPQ_UNAVAILABLE;                //SAM: mapping quality
      samAlgnmt->core.flag = samFlag;                             //SAM: bitwise flag
      
      if (isFirstUnaligned==0) {
            samAlgnmt->core.mtid = chr_2;
            samAlgnmt->core.mpos = tp_2-1;
      } else {
            samAlgnmt->core.mtid = -1;
            samAlgnmt->core.mpos = -1;
      }
      if ((isFirstUnaligned==0) && (isSecondUnaligned==0) && (chr_1 == chr_2)) {
            if (tp_2 > tp_1)
                  samAlgnmt->core.isize = tp_2 + mateLength - tp_1;
            else
                  samAlgnmt->core.isize = -(tp_1 + qInput->ReadLength - tp_2);
      } else      
            samAlgnmt->core.isize = 0;
      
      samwrite(samFilePtr,samAlgnmt);
      // ----------------------
}


void OCCDirectWritePairUnmapSAMAPI(SRAQueryInput * qInput, SRAOccurrence * sraOcc, int isFirst) {
      SRAQuerySetting * qSetting = qInput->QuerySetting;
      int OutFileFormat = qSetting->OutFileFormat;
      FILE * outFilePtr = qSetting->OutFilePtr;
      OCC * occ = qSetting->occ;
      HSP * hsp = qSetting->hsp;
      Translate * occTranslate = hsp->translate;
      unsigned short * occAmbiguityMap = hsp->ambiguityMap;
      
      samfile_t * samFilePtr = qSetting->SAMOutFilePtr;
      bam1_t * samAlgnmt = &(occ->SAMOutBuffer);
      
      unsigned long long i=0, j=0, k=0;
      unsigned long long ambPosition;
      unsigned long long tp;
      unsigned int correctPosition;
      unsigned int approxIndex,approxValue;
      
      //Assuming the answer being handled by OCCFlushCacheSAMAPI are all answers of qInput
      // Each occurrences reported by SAM_API will share the following aux data.
      //----------------------------------------------------------------------------------------------------------------------------
      samAlgnmt->core.bin = bam_reg2bin(0,0);                     //SAM: bin calculated by bam_reg2bin()
      samAlgnmt->core.n_cigar = 1;                                //SAM: number of CIGAR operations
      samAlgnmt->core.l_qseq = qInput->ReadLength;                //SAM: length of the query sequence (read)
      samAlgnmt->core.l_qname = strlen(qInput->ReadName)+1;         //SAM: length of the query name
      
      samAlgnmt->core.mtid = -1;
      samAlgnmt->core.mpos = -1;
      samAlgnmt->core.isize = 0;
      
      samAlgnmt->l_aux = 0;
      samAlgnmt->data_len = 0;
      samAlgnmt->m_data = SAM_MDATA_SIZE;
      SAMIUint8ConcatString(samAlgnmt->data,&(samAlgnmt->data_len),qInput->ReadName,strlen(qInput->ReadName)+1);    //Name
      SAMIUint8ConcatUint32(samAlgnmt->data,&(samAlgnmt->data_len),qInput->ReadLength<<BAM_CIGAR_SHIFT);          //CIGAR
      for (i = 0; i < qInput->ReadLength/2; i++) {                                                                //Read
            unsigned char biChar = 0;
            biChar |= bam_nt16_table[dnaChar[qInput->ReadCode[i*2]]] << 4;
            biChar |= bam_nt16_table[dnaChar[qInput->ReadCode[i*2+1]]];
            SAMIUint8ConcatUint8(samAlgnmt->data,&(samAlgnmt->data_len),biChar);
      }
      if ( qInput->ReadLength % 2 == 1 ) {
            uint8_t biChar = bam_nt16_table[dnaChar[qInput->ReadCode[qInput->ReadLength-1]]] << 4;
            SAMIUint8ConcatUint8(samAlgnmt->data,&(samAlgnmt->data_len),biChar);
      }
      for (i = 0; i < qInput->ReadLength; i++) {                                                                  //Quality                
            SAMIUint8ConcatUint8(samAlgnmt->data,&(samAlgnmt->data_len),0xff);
      }
      unsigned int auxStart = samAlgnmt->data_len;
      //SAMIUint8ConcatString(samAlgnmt->data,&(samAlgnmt->data_len),"XAZTESTING",11);
      samAlgnmt->l_aux += samAlgnmt->data_len - auxStart;
      //----------------------------------------------------------------------------------------------------------------------------                      
      
      // ATTENTION: THIS PART IS NOT 64-BIT COMPATIBLE.
      ambPosition = sraOcc->ambPosition;
      tp = ambPosition;
      correctPosition = ambPosition;
      approxIndex = ambPosition>>GRID_SAMPLING_FACTOR_2_POWER;
      approxValue = occAmbiguityMap[approxIndex];
      while (occTranslate[approxValue].startPos>ambPosition) {
            approxValue--;
      }
      
      correctPosition-=occTranslate[approxValue].correction;
      tp=correctPosition;
      
      unsigned int samFlag = 1; // it is a paired-end read
      samFlag |= SAM_FLAG_MATE_UNMAPPED;
      
      samFlag |= SAM_FLAG_FIRST_IN_PAIR*isFirst;
      samFlag |= SAM_FLAG_SECOND_IN_PAIR*(1-isFirst);
      
      if (sraOcc->strand==QUERY_NEG_STRAND) {
            samFlag |= SAM_FLAG_READ_ALGNMT_STRAND*isFirst;
      }
      
      //Outputing no alignment
      // ----------------------
      samAlgnmt->core.tid = occTranslate[approxValue].chrID-1;    //SAM: chromosome ID, defined by bam_header_t
      samAlgnmt->core.pos = tp-1;                                 //SAM: 0-based leftmost coordinate4
      samAlgnmt->core.qual = SAM_MAPQ_UNAVAILABLE;                //SAM: mapping quality
      samAlgnmt->core.flag = samFlag;                             //SAM: bitwise flag
      
      samwrite(samFilePtr,samAlgnmt);
      // ----------------------
}

void OCCFlushCache(SRAQueryInput * qInput) {    
    SRAQuerySetting * qSetting = qInput->QuerySetting;
	int outputFormat = qSetting->OutFileFormat;
    OCC * occ = qSetting->occ;
    HSP * hsp = qSetting->hsp;
    FILE * outFilePtr = qSetting->OutFilePtr;
    
    switch (outputFormat) {
        case SRA_OUTPUT_FORMAT_DEFAULT:
            OCCFlushCacheDefault(occ,hsp,outFilePtr);
            break;
        case SRA_OUTPUT_FORMAT_PLAIN:
            OCCFlushCachePlain(occ,hsp,outFilePtr);
            break;
        case SRA_OUTPUT_FORMAT_SAM:
            OCCFlushCacheSAM(qInput);
            break;
        case SRA_OUTPUT_FORMAT_SAM_API:
            OCCFlushCacheSAMAPI(qInput);
            break;
        default:
            qSetting->writtenCacheCount++;
            occ->occPositionCacheCount=0;
            break;
    }
}

void OCCReportDelimitor(SRAQueryInput * qInput) {
	SRAQuerySetting * qSetting = qInput->QuerySetting;
	OCC * occ = qSetting->occ;
	if (occ->occPositionCacheCount >= OCC_CACHE_SIZE) {OCCFlushCache(qInput);}
	occ->occPositionCache[occ->occPositionCacheCount].tp=qInput->ReadId;
	occ->occPositionCache[occ->occPositionCacheCount].ReadStrand=0;
	occ->occPositionCache[occ->occPositionCacheCount].ChromId=255;
	occ->occPositionCacheCount++;
}


void OCCReportNoAlignment(SRAQueryInput * qInput) {
	SRAQuerySetting * qSetting = qInput->QuerySetting;
	OCC * occ = qSetting->occ;
	if (occ->occPositionCacheCount >= OCC_CACHE_SIZE) {OCCFlushCache(qInput);}
	occ->occPositionCache[occ->occPositionCacheCount].tp=qInput->ReadId;
	occ->occPositionCache[occ->occPositionCacheCount].ReadStrand=0;
	occ->occPositionCache[occ->occPositionCacheCount].ChromId=254;
	occ->occPositionCacheCount++;
}

//OCCReportSARange : asumming l<=r
unsigned long long OCCReportSARange(SRAQueryInput * qInput,
									unsigned long long l, unsigned long long r, 
                                    int occMismatch) {
	SRAQuerySetting * qSetting = qInput->QuerySetting;
	SRAQueryResultCount * rOutput  = qInput->QueryOutput;
	BWT * bwt = qSetting->bwt;
	HSP * hsp = qSetting->hsp;
	HOCC * highOcc = qSetting->highOcc;
	OCC * occ = qSetting->occ;
	FILE * outFilePtr = qSetting->OutFilePtr;
    
    //Disable this check as the output file pointer does not 
    // necessary exist anymore.
	//if (outFilePtr==NULL) return 0;
	
#ifdef DEBUG_2BWT_NO_OUTPUT
	if (l>r) return 0;
	return r-l+1;
#endif
 
#ifdef DEBUG_2BWT_OUTPUT_TO_SCREEN
				printf("[BGS-IO-Read%u] Found SA Range = %u %u\n",qInput->ReadId,l,r);
#endif
	
	unsigned long long j,k;
	unsigned long long saCount = r-l+1;

	//Retrieve SA values from either SA or HOCC. Depending on the size of the SA Range.
	if (highOcc!=NULL && r-l>=4-1) {
		//By HOCC data structure.
		unsigned long long hOccCachedL,hOccCachedR,tp;
		unsigned long long approxL = HOCCApproximateRank(highOcc,l+1);
		
		HOCCGetCachedSARange(highOcc, approxL,&hOccCachedL,&hOccCachedR);
		while (hOccCachedL>l) {
			approxL--;
			HOCCGetCachedSARange(highOcc, approxL,&hOccCachedL,&hOccCachedR);
		}

		//Case 1: When the entire SA range falls within a cached range of HOOC
		if (hOccCachedL<=l && hOccCachedR>=r) {
			//Get the first index on HOCC
			unsigned long long hOccAnsIndex = highOcc->table[approxL*2+1];
			hOccAnsIndex+= (l-hOccCachedL);

			j=0; k=hOccAnsIndex;
			while (j<saCount) {
				occ->occPositionCache[occ->occPositionCacheCount].tp=highOcc->occ[k];
				occ->occPositionCache[occ->occPositionCacheCount].ReadStrand=qInput->ReadStrand;
				occ->occPositionCache[occ->occPositionCacheCount].ChromId=0;
                occ->occPositionCache[occ->occPositionCacheCount].occMismatch=occMismatch;
				occ->occPositionCacheCount++;
                rOutput->TotalOccurrences++;
				if (occ->occPositionCacheCount >= OCC_CACHE_SIZE) {OCCFlushCache(qInput);}
				k++;
				j++;
			}

		//Case 2: When the reported SA range enclosed some cached ranges of HOOC
		} else if (hOccCachedR<r) {
			unsigned long long hOccAnsIndex = highOcc->table[approxL*2+1];
			j=l; k=hOccAnsIndex;
			while (j<=r) {
				if (j>hOccCachedR && approxL<highOcc->ttlPattern) {
					approxL++;
					HOCCGetCachedSARange(highOcc, approxL,&hOccCachedL,&hOccCachedR);
					hOccAnsIndex = highOcc->table[approxL*2+1];
					k=hOccAnsIndex;
				}

				if (occ->occPositionCacheCount >= OCC_CACHE_SIZE) {OCCFlushCache(qInput);}
				if (hOccCachedL<=j && j<=hOccCachedR) {
					tp = highOcc->occ[k];
					occ->occPositionCache[occ->occPositionCacheCount].tp=tp;
					occ->occPositionCache[occ->occPositionCacheCount].ReadStrand=qInput->ReadStrand;
					occ->occPositionCache[occ->occPositionCacheCount].ChromId=0;
                    occ->occPositionCache[occ->occPositionCacheCount].occMismatch=occMismatch;
					occ->occPositionCacheCount++;
                    rOutput->TotalOccurrences++;
					k++;
				} else {
					tp = BWTSaValue(bwt,j);
					occ->occPositionCache[occ->occPositionCacheCount].tp=tp;
					occ->occPositionCache[occ->occPositionCacheCount].ReadStrand=qInput->ReadStrand;
					occ->occPositionCache[occ->occPositionCacheCount].ChromId=0;
                    occ->occPositionCache[occ->occPositionCacheCount].occMismatch=occMismatch;
					occ->occPositionCacheCount++;
                    rOutput->TotalOccurrences++;
				}
				j++;
			}
		//Case 3: Cannot find the records in HOCC (Should not happen)
		//In this case, we compute the occ by Suffix array
		} else {
			for (j=l;j<=r;j++) {
				if (occ->occPositionCacheCount >= OCC_CACHE_SIZE) {OCCFlushCache(qInput);}
				occ->occPositionCache[occ->occPositionCacheCount].tp=BWTSaValue(bwt,j);
				occ->occPositionCache[occ->occPositionCacheCount].ReadStrand=qInput->ReadStrand;
				occ->occPositionCache[occ->occPositionCacheCount].ChromId=0;
                occ->occPositionCache[occ->occPositionCacheCount].occMismatch=occMismatch;
				occ->occPositionCacheCount++;
                rOutput->TotalOccurrences++;
			}
		}
		//*/

	} else {
		//By Suffix array.
		for (j=l;j<=r;j++) {
			if (occ->occPositionCacheCount >= OCC_CACHE_SIZE) {OCCFlushCache(qInput);}
			occ->occPositionCache[occ->occPositionCacheCount].tp=BWTSaValue(bwt,j);
			occ->occPositionCache[occ->occPositionCacheCount].ReadStrand=qInput->ReadStrand;
			occ->occPositionCache[occ->occPositionCacheCount].ChromId=0;
            occ->occPositionCache[occ->occPositionCacheCount].occMismatch=occMismatch;
			occ->occPositionCacheCount++;
            rOutput->TotalOccurrences++;
		}
	}
	return saCount;

}


unsigned long long OCCReportTextPositions(SRAQueryInput * qInput, int posCount) {
	
	//DEBUG : OUTPUT TO SCREEN
	/*printf("[ReportTextPositions] %d \n",posCount);//*/

#ifdef DEBUG_2BWT_NO_OUTPUT
	int i;
	for (i=0;i<posCount;i++) {
		qInput->QueryOutput->WithError[qInput->QueryOutput->Shared_AccMismatch[i]]++;
	}
	return posCount;
#endif

	//END OF DEBUG


	if (posCount<=0) return 0;
	SRAQuerySetting * qSetting = qInput->QuerySetting;
	SRAQueryResultCount * rOutput  = qInput->QueryOutput;
	unsigned long long * saPositions = rOutput->Shared_ReadPositions;
	int * occMismatches = rOutput->Shared_AccMismatch;
	int * occQualities = rOutput->Shared_AccQualities;
	int OutputType = qSetting->OutputType;
	int outputFormat = qSetting->OutFileFormat;
	FILE * outFilePtr = qSetting->OutFilePtr;
	HSP * hsp = qSetting->hsp;
	OCC * occ = qSetting->occ;
	int k;

    //
    for (k=0;k<posCount;k++) {
        if (occ->occPositionCacheCount >= OCC_CACHE_SIZE) {OCCFlushCache(qInput);}
        occ->occPositionCache[occ->occPositionCacheCount].tp=saPositions[k];
        occ->occPositionCache[occ->occPositionCacheCount].ReadStrand=qInput->ReadStrand;
        occ->occPositionCache[occ->occPositionCacheCount].ChromId=0;
        occ->occPositionCache[occ->occPositionCacheCount].occMismatch=occMismatches[k];
        occ->occPositionCacheCount++;
        rOutput->WithError[occMismatches[k]]++;
        rOutput->TotalOccurrences++;
    }
    rOutput->RetrievedByCe+=posCount;
    return posCount;
}
