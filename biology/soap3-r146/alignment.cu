/*
 *
 *    alignment.cu
 *    Soap3(gpu)
 *
 *    Copyright (C) 2011, HKU
 *
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU General Public License
 *    as published by the Free Software Foundation; either version 2
 *    of the License, or (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include "alignment.h"

// COPY INDEX TO DEVICE MEMORY
void copyIndexToGPU(uint **_bwt, uint **_occ, uint **_lookupTable,
                    uint **_revBwt, uint **_revOcc, uint **_revLookupTable,
                    BWT *bwt, BWT *revBwt, uint *occValue, uint *revOccValue,
                    uint *lkt, uint *revLkt, uint numOfOccValue, uint lookupWordSize) {
                    
      cudaError_t gpuErr;
      gpuErr = cudaMalloc((void**)_bwt, bwt->bwtSizeInWord * sizeof(uint));
      if (gpuErr!=cudaSuccess) {
        printf("CUDA MALLOC FAILED .. %s(%d)\n",cudaGetErrorString(gpuErr),gpuErr);
        exit(1);
      }
      gpuErr = cudaMalloc((void**)_occ, numOfOccValue * ALPHABET_SIZE * sizeof(uint));
      if (gpuErr!=cudaSuccess) {
        printf("CUDA MALLOC FAILED .. %s(%d)\n",cudaGetErrorString(gpuErr),gpuErr);
        exit(1);
      }

      gpuErr = cudaMemcpy((*_bwt), bwt->bwtCode, bwt->bwtSizeInWord * sizeof(uint), cudaMemcpyHostToDevice);
      if (gpuErr!=cudaSuccess) {
        printf("CUDA MEMCPY FAILED .. %s(%d)\n",cudaGetErrorString(gpuErr),gpuErr);
        exit(1);
      }
      gpuErr = cudaMemcpy((*_occ), occValue, numOfOccValue * ALPHABET_SIZE * sizeof(uint), cudaMemcpyHostToDevice);
      if (gpuErr!=cudaSuccess) {
        printf("CUDA MEMCPY FAILED .. %s(%d)\n",cudaGetErrorString(gpuErr),gpuErr);
        exit(1);
      }
      gpuErr = cudaMalloc((void**)_revBwt, revBwt->bwtSizeInWord * sizeof(uint));
      if (gpuErr!=cudaSuccess) {
        printf("CUDA MALLOC FAILED .. %s(%d)\n",cudaGetErrorString(gpuErr),gpuErr);
        exit(1);
      }
      gpuErr = cudaMalloc((void**)_revOcc, numOfOccValue * ALPHABET_SIZE * sizeof(uint));
      if (gpuErr!=cudaSuccess) {
        printf("CUDA MALLOC FAILED .. %s(%d)\n",cudaGetErrorString(gpuErr),gpuErr);
        exit(1);
      }
      gpuErr = cudaMemcpy((*_revBwt), revBwt->bwtCode, revBwt->bwtSizeInWord * sizeof(uint), cudaMemcpyHostToDevice);
      if (gpuErr!=cudaSuccess) {
        printf("CUDA MEMCPY FAILED .. %s(%d)\n",cudaGetErrorString(gpuErr),gpuErr);
        exit(1);
      }
      gpuErr = cudaMemcpy((*_revOcc), revOccValue, numOfOccValue * ALPHABET_SIZE * sizeof(uint), cudaMemcpyHostToDevice);
      if (gpuErr!=cudaSuccess) {
        printf("CUDA MEMCPY FAILED .. %s(%d)\n",cudaGetErrorString(gpuErr),gpuErr);
        exit(1);
      }
      
      /*
      gpuErr = cudaMalloc((void**)_lookupTable, lookupWordSize * sizeof(uint));
      if (gpuErr!=cudaSuccess) {
        printf("CUDA MALLOC FAILED .. %s(%d)\n",cudaGetErrorString(gpuErr),gpuErr);
        exit(1);
      }
      gpuErr = cudaMalloc((void**)_revLookupTable, lookupWordSize * sizeof(uint));
      if (gpuErr!=cudaSuccess) {
        printf("CUDA MALLOC FAILED .. %s(%d)\n",cudaGetErrorString(gpuErr),gpuErr);
        exit(1);
      }
      gpuErr = cudaMemcpy((*_lookupTable), lkt, lookupWordSize * sizeof(uint), cudaMemcpyHostToDevice);
      if (gpuErr!=cudaSuccess) {
        printf("CUDA MEMCPY FAILED .. %s(%d)\n",cudaGetErrorString(gpuErr),gpuErr);
        exit(1);
      }
      gpuErr = cudaMemcpy((*_revLookupTable), revLkt, lookupWordSize * sizeof(uint), cudaMemcpyHostToDevice);
      if (gpuErr!=cudaSuccess) {
        printf("CUDA MEMCPY FAILED .. %s(%d)\n",cudaGetErrorString(gpuErr),gpuErr);
        exit(1);
      }
      */

      free(occValue);
      free(revOccValue);

}

// perform round1 alignment in GPU
void perform_round1_alignment(uint* nextQuery, uint* nextReadLength, uint* answers[][MAX_NUM_CASES], 
       uint numMismatch, uint numCases, uint sa_range_allowed, uint wordPerQuery, uint word_per_ans,
       bool isExactNumMismatch, int doubleBufferIdx, uint blocksNeeded, ullint batchSize, 
       BWT* bwt, BWT* revBwt, uint* _bwt, uint* _revBwt, uint* _occ, uint* _revOcc, uint* _lookupTable,
       uint* _revLookupTable) {

      #ifdef BGS_GPU_CASE_BREAKDOWN_TIME
            float gpuPreCaseTime=0.0;
            float gpuCaseTime;
      #endif

      cudaError_t gpuErr;
      uint *_queries, *_readLengths, *_answers;
      bool *_isBad;

      ullint roundUp = (batchSize + 31) / 32 * 32;


      // allocated device memory for bad read indicator
      bool *isBad;
      isBad = (bool*) malloc(roundUp * sizeof(bool)); // an array to store bad read indicator

      
      memset(isBad,0, roundUp);
      

      gpuErr = cudaMalloc((void**)&_isBad, roundUp * sizeof(bool));
      if (gpuErr!=cudaSuccess) {
            printf("CUDA MALLOC FAILED .. %s(%d)\n",cudaGetErrorString(gpuErr),gpuErr);
            exit(1);
      }
      // to initialize the array _isBad
      gpuErr = cudaMemcpy(_isBad, isBad,
                      roundUp * sizeof(bool),
                      cudaMemcpyHostToDevice);
      if (gpuErr!=cudaSuccess) {
            printf("CUDA MEMCOPY FAILED .. %s(%d)\n",cudaGetErrorString(gpuErr),gpuErr);
            exit(1);
      }
      
      // to initialize the array answers
      for (uint caseno=0; caseno < numCases; caseno++) {
            memset(answers[doubleBufferIdx][caseno], 0, roundUp * word_per_ans * sizeof(uint));
      }

      // printf("[perform_round1_alignment] sa_range_allowed = %u; word_per_ans = %u\n", sa_range_allowed, word_per_ans);

      // allocate device memory for queries and answers
      cudaMalloc((void**)&_queries, roundUp * wordPerQuery * sizeof(uint));
      cudaMalloc((void**)&_readLengths, roundUp * sizeof(uint));
      cudaMalloc((void**)&_answers, roundUp * word_per_ans * sizeof(uint));

      gpuErr = cudaMemcpy(_queries, nextQuery, roundUp * wordPerQuery * sizeof(uint), cudaMemcpyHostToDevice);
      if (gpuErr!=cudaSuccess) {
            printf("CUDA MEMCOPY FAILED .. %s(%d)\n",cudaGetErrorString(gpuErr),gpuErr);
            exit(1);
      }
      gpuErr = cudaMemcpy(_readLengths, nextReadLength,roundUp * sizeof(uint), cudaMemcpyHostToDevice);
      if (gpuErr!=cudaSuccess) {
            printf("CUDA MEMCOPY FAILED .. %s(%d)\n",cudaGetErrorString(gpuErr),gpuErr);
            exit(1);
      }

      for (uint caseno=0; caseno < numCases; caseno++) {

               // to reset the values inside the array _answers
               gpuErr = cudaMemcpy(_answers, answers[doubleBufferIdx][caseno],
                            roundUp * word_per_ans * sizeof(uint),
                            cudaMemcpyHostToDevice);
               if (gpuErr!=cudaSuccess) {
                    printf("CUDA MEMCOPY FAILED .. %s(%d)\n",cudaGetErrorString(gpuErr),gpuErr);
                    exit(1);
               }

              // =======================================
              // | GPU-1: FOR EACH CASE                |
              // =======================================
              if (numMismatch <= 3)
                    kernel<<<blocksNeeded, THREADS_PER_BLOCK>>>
                                            (caseno, _queries, _readLengths, batchSize, wordPerQuery,
                                            _bwt, _occ, bwt->inverseSa0, _lookupTable,
                                            _revBwt, _revOcc, revBwt->inverseSa0, _revLookupTable, bwt->textLength,
                                            _answers, _isBad, 0 ,numMismatch, sa_range_allowed, word_per_ans, isExactNumMismatch);
              else if (caseno < 5) // 4 mismatch and case 0 - 4
                    kernel_4mismatch_1<<<blocksNeeded, THREADS_PER_BLOCK>>>
                                            (caseno, _queries, _readLengths, batchSize, wordPerQuery,
                                            _bwt, _occ, bwt->inverseSa0, _lookupTable,
                                            _revBwt, _revOcc, revBwt->inverseSa0, _revLookupTable, bwt->textLength,
                                            _answers, _isBad, 0 ,sa_range_allowed, word_per_ans, isExactNumMismatch);
                                            
              else  // 4 mismatch and case 5 - 9
                    kernel_4mismatch_2<<<blocksNeeded, THREADS_PER_BLOCK>>>
                                            (caseno, _queries, _readLengths, batchSize, wordPerQuery,
                                            _bwt, _occ, bwt->inverseSa0, _lookupTable,
                                            _revBwt, _revOcc, revBwt->inverseSa0, _revLookupTable, bwt->textLength,
                                            _answers, _isBad, 0 ,sa_range_allowed, word_per_ans, isExactNumMismatch);
                                      
              gpuErr = cudaMemcpy(answers[doubleBufferIdx][caseno], _answers, roundUp * word_per_ans * sizeof(uint), cudaMemcpyDeviceToHost);
              if (gpuErr!=cudaSuccess) {
                  printf("CUDA MEMCOPY FAILED .. %s(%d)\n",cudaGetErrorString(gpuErr),gpuErr);
                  exit(1);
              }


      }

      // free the memories
      free(isBad);
      cudaFree(_isBad);
      cudaFree(_queries);
      cudaFree(_readLengths);
      cudaFree(_answers);
}



// perform round2 alignment in GPU
void perform_round2_alignment(uint* queries, uint* readLengths, uint* answers[][MAX_NUM_CASES], 
       uint numMismatch, uint numCases, uint sa_range_allowed_2, uint wordPerQuery, uint word_per_ans, uint word_per_ans_2,
       bool isExactNumMismatch, int doubleBufferIdx, uint blocksNeeded, ullint batchSize, 
       BWT* bwt, BWT* revBwt, uint* _bwt, uint* _revBwt, uint* _occ, uint* _revOcc, uint* _lookupTable,
       uint* _revLookupTable,  uint processedQuery, uint* badReadIndices[][MAX_NUM_CASES],
       uint* badAnswers[][MAX_NUM_CASES]) {

        uint* badQueries;
        uint* badReadLengths;
                                
        for (int whichCase = 0; whichCase < numCases; ++whichCase) {

            ullint numBads = 0;
            // Count number of bad reads and
            for (ullint readId=0; readId<batchSize; readId++) {
                 ullint srcOffset = ((readId)/ 32 * 32 * word_per_ans + readId % 32);
                 numBads += (answers[doubleBufferIdx][whichCase][srcOffset] > 0xFFFFFFFD);
            }
            
            
            if (numBads == 0)
                  continue;
                  


            // Allocate memory and copy bad reads to another array
            ullint roundUp = (numBads + 31) / 32 * 32;
            badQueries = (uint*)malloc(roundUp * wordPerQuery * sizeof(uint));
            // printf("size of badQueries = %u\n", roundUp * wordPerQuery);
            badReadLengths = (uint*)malloc(roundUp * sizeof(uint));
            // printf("size of badReadLengths = %u\n", roundUp);
            numBads = 0;
            for (ullint readId = 0; readId < batchSize; ++readId) {
                ullint srcOffset = ((readId) / 32 * 32 * word_per_ans + readId % 32);
                ullint srcQueryOffset = (processedQuery+readId) / 32 * 32 * wordPerQuery + readId % 32;
                if (answers[doubleBufferIdx][whichCase][srcOffset] > 0xFFFFFFFD) { // is bad
                    badReadIndices[doubleBufferIdx][whichCase][numBads] = readId;
                    ullint targetOffset = numBads / 32 * 32 * wordPerQuery + numBads % 32;
                    for (ullint i = 0; i < wordPerQuery; ++i) { // copy each word of the read
                        badQueries[targetOffset + i * 32] = *(queries + (srcQueryOffset + i * 32));
                    }
                    badReadLengths[numBads] = readLengths[processedQuery+readId];
                    numBads++;
                }
            }
            // Copy reads to device memory, call kernel, and copy results back to host
            uint *_queries, *_readLengths, *_answers;
            
            bool *_isBad = NULL;
            // bool *isBad = (bool*) malloc((batchSize + 31) / 32 * 32 * sizeof(bool)); // an array to store bad read indicator
            // memset(isBad,0, (batchSize + 31) / 32 * 32 * sizeof(bool));
            // cudaMalloc((void**)&_isBad, (batchSize + 31) / 32 * 32 * sizeof(bool));
            // cudaMemcpy(_isBad, isBad,(batchSize + 31) / 32 * 32 * sizeof(bool), cudaMemcpyHostToDevice);
            // free(isBad);
            
            cudaMalloc((void**)&_queries, roundUp * wordPerQuery * sizeof(uint));
            cudaMemcpy(_queries, badQueries,
                roundUp * wordPerQuery * sizeof(uint), cudaMemcpyHostToDevice);
            free(badQueries);
            
            cudaMalloc((void**)&_readLengths, roundUp * sizeof(uint));
            cudaMemcpy(_readLengths, badReadLengths,
                       roundUp * sizeof(uint), cudaMemcpyHostToDevice);
            free(badReadLengths);
            // reset the values of the array badAnswers
            memset(badAnswers[doubleBufferIdx][whichCase], 0, roundUp * word_per_ans_2 * sizeof(uint));
            cudaMalloc((void**)&_answers, roundUp * word_per_ans_2 * sizeof(uint));
            // to reset the values inside the array _answers
            cudaMemcpy(_answers, badAnswers[doubleBufferIdx][whichCase],
                      roundUp * word_per_ans_2 * sizeof(uint),
                      cudaMemcpyHostToDevice);
            if (numMismatch <= 3) // 3 mismatch
                  kernel<<<blocksNeeded, THREADS_PER_BLOCK>>>
                      (whichCase, _queries, _readLengths, numBads, wordPerQuery,
                      _bwt, _occ, bwt->inverseSa0, _lookupTable,
                      _revBwt, _revOcc, revBwt->inverseSa0, _revLookupTable,
                      bwt->textLength, _answers, _isBad, 1, numMismatch, sa_range_allowed_2, word_per_ans_2, isExactNumMismatch);
            else if (whichCase < 5) // 4 mismatch and case no. 0 - 4
                  kernel_4mismatch_1<<<blocksNeeded, THREADS_PER_BLOCK>>>
                      (whichCase, _queries, _readLengths, numBads, wordPerQuery,
                      _bwt, _occ, bwt->inverseSa0, _lookupTable,
                      _revBwt, _revOcc, revBwt->inverseSa0, _revLookupTable,
                      bwt->textLength, _answers, _isBad, 1, sa_range_allowed_2, word_per_ans_2, isExactNumMismatch);
            else   // 4 mismatch and case no. 5 - 9
                  kernel_4mismatch_2<<<blocksNeeded, THREADS_PER_BLOCK>>>
                      (whichCase, _queries, _readLengths, numBads, wordPerQuery,
                      _bwt, _occ, bwt->inverseSa0, _lookupTable,
                      _revBwt, _revOcc, revBwt->inverseSa0, _revLookupTable,
                      bwt->textLength, _answers, _isBad, 1, sa_range_allowed_2, word_per_ans_2, isExactNumMismatch);
            cudaMemcpy(badAnswers[doubleBufferIdx][whichCase], _answers,
                roundUp * word_per_ans_2 * sizeof(uint), cudaMemcpyDeviceToHost);


            // free the memory in the device
            // cudaFree(_isBad);
            cudaFree(_queries);
            cudaFree(_answers);
            cudaFree(_readLengths);
            
        }
}


void all_valid_alignment(uint* queries, uint* readLengths, uint numMismatch, uint wordPerQuery,
       ullint maxBatchSize, uint numQueries, uint accumReadNum,
       BWT* bwt, BWT* revBwt, uint* _bwt, uint* _revBwt, uint* _occ, uint* _revOcc,
       HSP * hsp, uint *lkt, uint *revLkt,
       uint* _lookupTable, uint* _revLookupTable, IniParams ini_params, InputOptions input_options,
       unsigned char *upkdQueries, 
       unsigned char *noAlignment, 
       uint *unAlignedPair, uint& numOfUnPaired,
       uint *readIDs, char *upkdQueryNames,
       char** currOutputFileName, samfile_t ** currSamOutputFilePtr,
       uint& numOfAnswer, uint& numOfAlignedRead,
       bool outNoAlign, int outputUnpairReadsForSAM, uint8_t isTerminalCase) {

      // printf("start %u alignment \n", numMismatch);

      //Modified to be double buffered
      uint * answers[2][MAX_NUM_CASES];
      uint* badReadIndices[2][MAX_NUM_CASES]; // IDs of bad reads for each case
      uint* badAnswers[2][MAX_NUM_CASES]; // stores answers (and reads temporarily) for 2nd round
      uint threadBadStarts[2][MAX_NUM_CPU_THREADS][MAX_NUM_CASES];
      uint threadBadCounts[2][MAX_NUM_CPU_THREADS][MAX_NUM_CASES];
      uint* unAlignedPairId[MAX_NUM_CPU_THREADS];
      int doubleBufferIdx = 0; // expect to be either 0 or 1

      // Host alignment model
      int dummyQuality[MAX_READ_LENGTH];
      SRAModel * SRAMismatchModel[MAX_READ_LENGTH+1];
      HostKernelArguements hostKernelArguments[MAX_NUM_CPU_THREADS];

      // Host multi-threading variables
      int threadId;
      pthread_t threads[MAX_NUM_CPU_THREADS];
      uint threadBucketSize[MAX_NUM_CPU_THREADS];
      
      numOfAnswer = 0;
      numOfAlignedRead = 0;
      numOfUnPaired = 0;

      uint blocksNeeded;
      ullint batchSize;
      uint numCases;
      uint sa_range_allowed_1;
      uint sa_range_allowed_2;
      char skip_round_2;
      ullint queriesLeft = numQueries;
      uint maxReadLength = input_options.maxReadLength;
      uint word_per_ans;
      uint word_per_ans_2;
      int i;



      // initialization of arrays
      for (i=0; i<2; i++) {
        for (int j=0; j<MAX_NUM_CASES; j++) {
          badReadIndices[i][j]=NULL;
          badAnswers[i][j]=NULL;
          answers[i][j]=NULL;
          for (int k=0; k<MAX_NUM_CPU_THREADS; k++) {
               threadBadStarts[i][k][j]=0;
               threadBadCounts[i][k][j]=0;
          }
        }
      }

      // initialization of unAlignedPairId
      for (int i=0; i<ini_params.Ini_NumOfCpuThreads; i++) {
          unAlignedPairId[i]=(uint*) malloc(maxBatchSize*sizeof(uint));
      }


      // obtain the number of cases for this number of mismatch
      // and obtain the number of SA ranges allowed
      getParametersForThisMismatch(numMismatch, numCases, sa_range_allowed_1,
                sa_range_allowed_2, skip_round_2, word_per_ans, word_per_ans_2);

      
      // set the multi-threading arguments
      setHostKernelArguments(hostKernelArguments, threads, ini_params, bwt, hsp,
                lkt, revLkt, dummyQuality, maxReadLength, word_per_ans, &input_options);


      for (threadId=0;threadId<ini_params.Ini_NumOfCpuThreads;++threadId) {    
        hostKernelArguments[threadId].upkdQueries = upkdQueries;
        hostKernelArguments[threadId].upkdQueryNames = upkdQueryNames;
        hostKernelArguments[threadId].SRAMismatchModel = SRAMismatchModel;
        hostKernelArguments[threadId].outputFileName = currOutputFileName[threadId];
        hostKernelArguments[threadId].readLengths = readLengths;
        hostKernelArguments[threadId].readIDs = readIDs;
        hostKernelArguments[threadId].accumReadNum = accumReadNum;
        hostKernelArguments[threadId].unAlignedIDs = unAlignedPairId[threadId];
        hostKernelArguments[threadId].unAlignedOcc = 0;
      }

      ullint roundUp = (maxBatchSize + 31) / 32 * 32 + 1024;

      // allocate host memory for answers
      for (i=0; i<2; i++) {
        for (int j=0; j<numCases; j++) {
          answers[i][j] = (uint*) malloc(roundUp * word_per_ans * sizeof(uint));
        }
      }


      // For single-read alignment, unique best and random best requires only 1 answer
      if (input_options.readType==SINGLE_READ) {
          if ( input_options.alignmentType == OUTPUT_UNIQUE_BEST) {
                sa_range_allowed_1 = 1;
                skip_round_2 = 1;
          }
          if ( input_options.alignmentType == OUTPUT_RANDOM_BEST) {
                sa_range_allowed_1 = 2;
                skip_round_2 = 1;
          }
      }

      // if there is only 3G GPU memory, then skip round 2
      if (ini_params.Ini_GPUMemory==3)
            skip_round_2 = 1;


      // printParameters(input_options, ini_params);
      // printf("numMismatch = %u; numCases = %u; sa_range_allowed_1 = %u; sa_range_allowed_2 = %u; skip_round_2 = %i; word_per_ans = %u; word_per_ans_2 = %u \n", numMismatch, numCases, sa_range_allowed_1, sa_range_allowed_2, skip_round_2, word_per_ans, word_per_ans_2);


      // update the num of mismatch of the model for host alignment
      for (threadId=0;threadId<ini_params.Ini_NumOfCpuThreads;++threadId) {
        hostKernelArguments[threadId].SRAQuerySet.MaxError=numMismatch;
        hostKernelArguments[threadId].numCases = numCases;
        hostKernelArguments[threadId].sa_range_allowed_1 = sa_range_allowed_1;
        hostKernelArguments[threadId].sa_range_allowed_2 = sa_range_allowed_2;
        hostKernelArguments[threadId].maxNumMismatch = numMismatch;
        
        //Update the SAM output file ptr in QuertSetting
        hostKernelArguments[threadId].SRAQuerySet.SAMOutFilePtr = currSamOutputFilePtr[threadId];
        
        //idiciate whether it needs to output the unpaired reads
        hostKernelArguments[threadId].outputUnpairReads = outputUnpairReadsForSAM;
      }

      for (i=0;i<=MAX_READ_LENGTH;i++) { SRAMismatchModel[i] = NULL; }
      for (unsigned readId=0;readId<numQueries;++readId) {
        if (SRAMismatchModel[readLengths[readId]] == NULL) {
            SRAMismatchModel[readLengths[readId]] = SRAModelConstruct(readLengths[readId],&(hostKernelArguments[0].SRAQuerySet),ini_params.Ini_HostAlignmentModel,bwt,revBwt);
        }
      }

      
      // initialize the array
      memset(noAlignment, 0, numQueries * sizeof(unsigned char));
      
      uint* nextQuery = queries;
      uint* nextReadLength = readLengths;
      uint* nextUnAlignedPair = unAlignedPair;

      while (queriesLeft > 0) {





        uint processedQuery = numQueries - queriesLeft;
        if (queriesLeft > maxBatchSize) {
            blocksNeeded = NUM_BLOCKS;
            batchSize = maxBatchSize;
        } else {
            blocksNeeded = (queriesLeft + THREADS_PER_BLOCK*QUERIES_PER_THREAD - 1) /
            (THREADS_PER_BLOCK * QUERIES_PER_THREAD);
            batchSize = queriesLeft;
        }

        // allocate the reads to different CPU threads
        uint roughBucketSize = batchSize / ini_params.Ini_NumOfCpuThreads;
        // roughBucketSize has to be divible by 2
        roughBucketSize = roughBucketSize/2 * 2;
        uint batchSizeUnalloc = batchSize;
        for (threadId=0;threadId<ini_params.Ini_NumOfCpuThreads-1;threadId++) {
            threadBucketSize[threadId] = roughBucketSize;
            batchSizeUnalloc -= roughBucketSize;
        }
        threadBucketSize[ini_params.Ini_NumOfCpuThreads-1] = batchSizeUnalloc;


        
        // perform first round alignment in GPU
        perform_round1_alignment(nextQuery, nextReadLength, answers, 
             numMismatch, numCases, sa_range_allowed_1, wordPerQuery, 
             word_per_ans, false, doubleBufferIdx, blocksNeeded, batchSize,
             bwt, revBwt, _bwt, _revBwt, _occ, _revOcc, _lookupTable, _revLookupTable);

        // identify which read has no hit
        if (outNoAlign==TRUE) {
              uint *ans;
              for (ullint j=0;j<batchSize;++j) {
                  ullint batchReadId = j;
                  ullint readId = processedQuery+j;
                  char isNoAlign = 1;
                  for (int whichCase=0;whichCase<numCases && isNoAlign;whichCase++) {
                        ans = answers[doubleBufferIdx][whichCase] + ((batchReadId) / 32 * 32 * word_per_ans + (batchReadId) % 32);
                        if (*(ans) != 0xFFFFFFFD)
                              isNoAlign = 0;
                  }
                  if (isNoAlign==1) {
                        noAlignment[readId] = 1;
                  }
              }
        } 
        


        if (skip_round_2==0) {
               
              // =======================================
              // | GPU-2                               |
              // =======================================
              // perform second round alignment in GPU
              for (int whichCase = 0; whichCase < numCases; ++whichCase) {
                  ullint numBads = 0;
                  // Count number of bad reads and
                  for (ullint readId=0; readId<batchSize; readId++) {
                       ullint srcOffset = ((readId)/ 32 * 32 * word_per_ans + readId % 32);
                       numBads += (answers[doubleBufferIdx][whichCase][srcOffset] > 0xFFFFFFFD);
                  }
                  
                  if (numBads > 0) {
                        // Allocate memory and copy bad reads to another array
                        ullint roundUp = (numBads + 31) / 32 * 32 + 1024;
                        badReadIndices[doubleBufferIdx][whichCase] = (uint*)malloc(roundUp * sizeof(uint));
                        // printf("size of badReadIndices[%i][%i] = %u\n",doubleBufferIdx, whichCase, roundUp);
                        badAnswers[doubleBufferIdx][whichCase] = (uint*)malloc(roundUp * word_per_ans_2 * sizeof(uint));
                        // printf("size of badAnswers[%i][%i] = %u\n",doubleBufferIdx, whichCase, roundUp * word_per_ans_2);
                  }
                  // printf("numBads == %u in case %i of mismatch %u\n", numBads, whichCase, numMismatch);
              }
              perform_round2_alignment(queries, readLengths, answers, 
                   numMismatch, numCases, sa_range_allowed_2, wordPerQuery, word_per_ans,
                   word_per_ans_2, false, doubleBufferIdx, blocksNeeded, batchSize,
                   bwt, revBwt, _bwt, _revBwt, _occ, _revOcc, _lookupTable, _revLookupTable,
                   processedQuery, badReadIndices, badAnswers);
              for (int whichCase = 0; whichCase < numCases; ++whichCase) {
                  ullint readId=0;
                  for (threadId = 0; threadId < ini_params.Ini_NumOfCpuThreads; threadId ++ ) {
                      uint threadNumBad = 0;
                      // printf("whichCase = %i; threadId = %i; threadBucketSize[threadId] = %u\n",
                      //        whichCase, threadId, threadBucketSize[threadId]);
                      for (uint n = 0 ; n < threadBucketSize[threadId]; ++n) {
                          ullint srcOffset = (readId/ 32 * 32 * word_per_ans + readId % 32);
                          threadNumBad += (answers[doubleBufferIdx][whichCase][srcOffset] > 0xFFFFFFFD);
                          readId++;
                      }
                      threadBadCounts[doubleBufferIdx][threadId][whichCase] = threadNumBad;
                      // printf("threadBadCounts[%i][%i][%i] = %u\n", 
                      //    doubleBufferIdx, threadId, whichCase, threadBadCounts[doubleBufferIdx][threadId][whichCase]);
                      
                  }
              }
              for (int whichCase = 0; whichCase < numCases; ++whichCase) {
                  threadBadStarts[doubleBufferIdx][0][whichCase] = 0;
                  for (threadId = 1; threadId < ini_params.Ini_NumOfCpuThreads; threadId ++ ) {
                      threadBadStarts[doubleBufferIdx][threadId][whichCase] = threadBadStarts[doubleBufferIdx][threadId-1][whichCase] + threadBadCounts[doubleBufferIdx][threadId-1][whichCase];
                      // printf("threadBadStarts[%i][%i][%i] = %u\n", 
                      //  doubleBufferIdx, threadId, whichCase, threadBadStarts[doubleBufferIdx][threadId][whichCase]);
                  }
              }
        } // skip the round 2
        


        for (threadId = 0; threadId < ini_params.Ini_NumOfCpuThreads ; threadId++ ) {
            if (threads[threadId]!=0) {
                if (pthread_join(threads[threadId],NULL)) 
                    fprintf(stderr,"[Main:Thread%u] Crash!\n",threadId), exit(1);
                // pthread_detach(threads[threadId]);
                numOfAnswer+=hostKernelArguments[threadId].alignedOcc;
                numOfAlignedRead+=hostKernelArguments[threadId].alignedReads;
                threads[threadId]=0;
                
                // consolidate the unAlignedPairId;
                if (hostKernelArguments[threadId].unAlignedOcc > 0) {
                      memcpy(nextUnAlignedPair, unAlignedPairId[threadId],
                             (ullint)hostKernelArguments[threadId].unAlignedOcc * sizeof(uint));
                      nextUnAlignedPair += hostKernelArguments[threadId].unAlignedOcc;
                      numOfUnPaired += hostKernelArguments[threadId].unAlignedOcc;
                }
            }
        }
        
        for (int whichCase = 0; whichCase < numCases; ++whichCase) {
              if (badReadIndices[1-doubleBufferIdx][whichCase]!=NULL) {
                    free(badReadIndices[1-doubleBufferIdx][whichCase]);
                    badReadIndices[1-doubleBufferIdx][whichCase]=NULL;
              }
              if (badAnswers[1-doubleBufferIdx][whichCase]!=NULL) {
                    free(badAnswers[1-doubleBufferIdx][whichCase]);
                    badAnswers[1-doubleBufferIdx][whichCase]=NULL;
              }
        }
        
        

        // =======================================
        // | CPU: Thread #0,1,2,3,...            |
        // =======================================
        unsigned int threadProcessedQuery = 0;
        for (threadId = 0; threadId < ini_params.Ini_NumOfCpuThreads ; threadId++ ) {

            hostKernelArguments[threadId].batchFirstReadId = processedQuery;
            hostKernelArguments[threadId].skipFirst = threadProcessedQuery;
            hostKernelArguments[threadId].numQueries = threadBucketSize[threadId];
            
            hostKernelArguments[threadId].answers = answers[doubleBufferIdx];
            hostKernelArguments[threadId].alignedOcc = 0;
            hostKernelArguments[threadId].alignedReads = 0;
            hostKernelArguments[threadId].badReadIndices = badReadIndices[doubleBufferIdx];
            hostKernelArguments[threadId].badAnswers = badAnswers[doubleBufferIdx];
            hostKernelArguments[threadId].badStartOffset = threadBadStarts[doubleBufferIdx][threadId];
            hostKernelArguments[threadId].badCountOffset = threadBadCounts[doubleBufferIdx][threadId];
            hostKernelArguments[threadId].outputGoodReads = TRUE;
            hostKernelArguments[threadId].skip_round_2 = skip_round_2;
            hostKernelArguments[threadId].isTerminalCase = isTerminalCase;
            
            if (pthread_create(&(threads[threadId]), NULL, hostKernelThreadWrapper, (void*) &(hostKernelArguments[threadId])))
                fprintf(stderr,"[Main:Threads%u] Can't create hostKernelThreadWrapper\n",threadId), exit(1);

            threadProcessedQuery+=threadBucketSize[threadId];
        }
        
        

        // Swap the double buffer
        doubleBufferIdx = 1 - doubleBufferIdx;
        
        
        
        queriesLeft -= batchSize;
        nextQuery += batchSize * wordPerQuery;
        nextReadLength += batchSize;



      }


      for (threadId = 0; threadId < ini_params.Ini_NumOfCpuThreads ; threadId++ ) {
        if (threads[threadId]!=0) {
            if (pthread_join(threads[threadId],NULL)) 
                fprintf(stderr,"[Main:Thread%u] Crash!\n",threadId), exit(1);
            // pthread_detach(threads[threadId]);
            numOfAnswer+=hostKernelArguments[threadId].alignedOcc;
            numOfAlignedRead+=hostKernelArguments[threadId].alignedReads;
            threads[threadId]=0;
            // consolidate the unAlignedPairId;
            if (hostKernelArguments[threadId].unAlignedOcc > 0) {
                memcpy(nextUnAlignedPair, unAlignedPairId[threadId],
                       (ullint)hostKernelArguments[threadId].unAlignedOcc * sizeof(uint));
                nextUnAlignedPair += hostKernelArguments[threadId].unAlignedOcc;
                numOfUnPaired += hostKernelArguments[threadId].unAlignedOcc;
            }
        }
      }

      for (int whichCase = 0; whichCase < numCases; ++whichCase) {
              if (badReadIndices[1-doubleBufferIdx][whichCase]!=NULL) {
                    free(badReadIndices[1-doubleBufferIdx][whichCase]);
                    badReadIndices[1-doubleBufferIdx][whichCase]=NULL;
              }
              if (badAnswers[1-doubleBufferIdx][whichCase]!=NULL) {
                    free(badAnswers[1-doubleBufferIdx][whichCase]);
                    badAnswers[1-doubleBufferIdx][whichCase]=NULL;
              }
      }
      
      // CLEAN UP for each MISMATCH iteration                                                                          |
      for (i=0;i<=MAX_READ_LENGTH;i++) {
        if (SRAMismatchModel[i] != NULL) {
              SRAModelFree(SRAMismatchModel[i]);
              SRAMismatchModel[i] = NULL;
        }
      }

      for (threadId=0;threadId<ini_params.Ini_NumOfCpuThreads;++threadId) {
        OCCFree(hostKernelArguments[threadId].occ);
        free(unAlignedPairId[threadId]);
      }

      for (i=0; i<2; i++) {
        for (int j=0; j<numCases; j++) {
          free(answers[i][j]);
        }
      }


}

// pair-end alignment: for random-best
// 4-phases [0,1,2,4]
void random_best_pair_alignment(uint* queries, uint* readLengths, uint numMismatch, uint wordPerQuery,
       ullint maxBatchSize, uint numQueries, uint accumReadNum,
       BWT* bwt, BWT* revBwt, uint* _bwt, uint* _revBwt, uint* _occ, uint* _revOcc,
       HSP * hsp, uint *lkt, uint *revLkt,
       uint* _lookupTable, uint* _revLookupTable, IniParams ini_params, InputOptions input_options,
       unsigned char *upkdQueries, 
       unsigned char *noAlignment, 
       uint *unAlignedPair, uint& numOfUnPaired,
       uint *readIDs, char *upkdQueryNames,
       char** currOutputFileName, samfile_t ** currSamOutputFilePtr,
       uint& numOfAnswer, uint& numOfAlignedRead) {
       
            // for performing pair-end alignment with random-best output
            uint numOfAnswer0 = 0;
            uint numOfAlignedRead0 = 0;
            uint numOfAnswer1 = 0;
            uint numOfAlignedRead1 = 0;
            uint numOfAnswer2 = 0;
            uint numOfAlignedRead2 = 0;
            uint numOfAnswer3 = 0;
            uint numOfAlignedRead3 = 0;

            //********************************//
            // Phase 0: Perform 0 Alignment //
            //********************************//
            uint numMismatch_phase0 = 0;
            all_valid_alignment(queries, readLengths, numMismatch_phase0, wordPerQuery,
                   maxBatchSize, numQueries, accumReadNum,
                   bwt, revBwt, _bwt, _revBwt, _occ, _revOcc,
                   hsp, lkt, revLkt,
                   _lookupTable, _revLookupTable, ini_params, input_options,
                   upkdQueries,
                   noAlignment,
                   unAlignedPair, numOfUnPaired,
                   readIDs, upkdQueryNames,
                   currOutputFileName,  currSamOutputFilePtr,
                   numOfAnswer0, numOfAlignedRead0, false, numMismatch==0, numMismatch==0);
                   
            if (numMismatch > 0 && numOfAlignedRead0 < numQueries) {

       
                  //********************************//
                  // Phase 1: Perform 0/1 Alignment //
                  //********************************//
                  uint numMismatch_phase1 = 1;

                  // pack the reads which are not paired
                  packUnPairedReads(queries, readIDs, readLengths, unAlignedPair,
                              wordPerQuery, numOfUnPaired, maxBatchSize);

                  numQueries = numOfUnPaired;

                  all_valid_alignment(queries, readLengths, numMismatch_phase1, wordPerQuery,
                         maxBatchSize, numQueries, accumReadNum,
                         bwt, revBwt, _bwt, _revBwt, _occ, _revOcc,
                         hsp, lkt, revLkt,
                         _lookupTable, _revLookupTable, ini_params, input_options,
                         upkdQueries,
                         noAlignment,
                         unAlignedPair, numOfUnPaired,
                         readIDs, upkdQueryNames,
                         currOutputFileName,  currSamOutputFilePtr,
                         numOfAnswer1, numOfAlignedRead1, false, numMismatch==1, numMismatch==1);

                  // printf("numOfAlignedRead1 = %u\n", numOfAlignedRead1);
                  // printf("numOfUnPaired = %u\n", numOfUnPaired);

                  if (numMismatch > 1 && numOfAlignedRead1 < numQueries) {
                        //****************************************//
                        // Phase 2: Perform 0/1/2 alignment     //
                        //****************************************//
                        uint numMismatch_phase2 = 2;
                        
                        
                        // pack the reads which are not paired
                        packUnPairedReads(queries, readIDs, readLengths, unAlignedPair,
                                    wordPerQuery, numOfUnPaired, maxBatchSize);

                        numQueries = numOfUnPaired;
                        all_valid_alignment(queries, readLengths, numMismatch_phase2, wordPerQuery,
                               maxBatchSize, numQueries, accumReadNum,
                               // maxBatchSize, numOfUnPaired, accumReadNum,
                               bwt, revBwt, _bwt, _revBwt, _occ, _revOcc,
                               hsp, lkt, revLkt,
                               _lookupTable, _revLookupTable, ini_params, input_options,
                               upkdQueries,
                               noAlignment, 
                               unAlignedPair, numOfUnPaired,
                               readIDs, upkdQueryNames,
                               currOutputFileName, currSamOutputFilePtr,
                               numOfAnswer2, numOfAlignedRead2, false, numMismatch==2, numMismatch==2);
                         
                        // printf("numOfAlignedRead2 = %u\n", numOfAlignedRead2);
                        // printf("numOfUnPaired = %u\n", numOfUnPaired, 0);

                        if (numMismatch > 2 && numOfAlignedRead2 < numQueries) {
                              //******************************************//
                              // Phase 3: Perform 0/1/2/3/4 alignment     //
                              //******************************************//
                              
                              
                              // pack the reads which are not paired
                              packUnPairedReads(queries, readIDs, readLengths, unAlignedPair,
                                          wordPerQuery, numOfUnPaired, maxBatchSize);

                              numQueries = numOfUnPaired;
                              all_valid_alignment(queries, readLengths, numMismatch, wordPerQuery,
                                     maxBatchSize, numQueries, accumReadNum,
                                     // maxBatchSize, numOfUnPaired, accumReadNum,
                                     bwt, revBwt, _bwt, _revBwt, _occ, _revOcc,
                                     hsp, lkt, revLkt,
                                     _lookupTable, _revLookupTable, ini_params, input_options,
                                     upkdQueries,
                                     noAlignment, 
                                     unAlignedPair, numOfUnPaired,
                                     readIDs, upkdQueryNames,
                                     currOutputFileName, currSamOutputFilePtr,
                                     numOfAnswer3, numOfAlignedRead3, false, 1, 1);
                               
                              // printf("numOfAlignedRead3 = %u\n", numOfAlignedRead3);
                              // printf("numOfUnPaired = %u\n", numOfUnPaired);

                        }
                  }
            }
            
            numOfAnswer = numOfAnswer0 + numOfAnswer1 + numOfAnswer2 + numOfAnswer3;
            numOfAlignedRead = numOfAlignedRead0 + numOfAlignedRead1 + numOfAlignedRead2 + numOfAlignedRead3;
}         



void best_single_alignment(uint* queries, uint* readLengths, uint numMismatch, uint wordPerQuery,
       ullint maxBatchSize, uint numQueries, uint accumReadNum,
       BWT* bwt, BWT* revBwt, uint* _bwt, uint* _revBwt, uint* _occ, uint* _revOcc,
       HSP * hsp, uint *lkt, uint *revLkt,
       uint* _lookupTable, uint* _revLookupTable, IniParams ini_params, InputOptions input_options,
       unsigned char *upkdQueries, 
       unsigned char *noAlignment, 
       uint *unAlignedPair, uint& numOfUnPaired,
       uint *readIDs, char *upkdQueryNames,
       char** currOutputFileName, samfile_t ** currSamOutputFilePtr,
       uint& numOfAnswer, uint& numOfAlignedRead) {
       
      // for performing single-end alignment
      // all-best, random-best, unique-best
      numOfAnswer = 0;
      numOfAlignedRead = 0;
      
      for (uint currNumMismatch = 0; currNumMismatch<=numMismatch; currNumMismatch++) {
             uint currNumOfAnswer = 0;
             uint currNumOfAlignedRead = 0;

 
            all_valid_alignment(queries, readLengths, currNumMismatch, wordPerQuery,
                   maxBatchSize, numQueries, accumReadNum,
                   bwt, revBwt, _bwt, _revBwt, _occ, _revOcc,
                   hsp, lkt, revLkt,
                   _lookupTable, _revLookupTable, ini_params, input_options,
                   upkdQueries,
                   noAlignment, 
                   unAlignedPair, numOfUnPaired,
                   readIDs, upkdQueryNames,
                   currOutputFileName,  currSamOutputFilePtr,
                   currNumOfAnswer, currNumOfAlignedRead, true, 0, currNumMismatch==numMismatch);
                   
            
            // pack the reads which have no hits
            uint numOfUnPairedReads = packReads(queries, readIDs, readLengths, noAlignment,
                           wordPerQuery, numQueries);
                           
            // printf("numOfUnPairedReads = %u\n", numOfUnPairedReads);
            // printf("currNumOfAnswer = %u\n", currNumOfAnswer);
            // printf("currNumOfAlignedRead = %u\n", currNumOfAlignedRead);
            numOfAnswer += currNumOfAnswer;
            numOfAlignedRead += currNumOfAlignedRead;
            numQueries = numOfUnPairedReads;

      }
}         
 
