/*
 *
 *    CPUfunctions.cpp
 *    Soap3(gpu)
 *
 *    Copyright (C) 2011, HKU
 *
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU General Public License
 *    as published by the Free Software Foundation; either version 2
 *    of the License, or (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include "CPUfunctions.h"


// This function is to print out the help on the usage of the program
void printUsage(char* program_name) {
      fprintf(stderr, "\n");
      fprintf(stderr, "  GPU Short Read Aligner -- beta -- Usage Guide  \n");
      fprintf(stderr, "\n");
      fprintf(stderr, "For alignment of single-end reads:\n");
      fprintf(stderr, "  %s single <bwtCodeIndex> <queryFileName> [options]\n", program_name);
      fprintf(stderr, "\n");
      fprintf(stderr, "      bwtCodeIndex  : The bwt index built by 2bwt-builder with\n");
      fprintf(stderr, "                      newly introduced 1-level bwt occ table.\n");
      fprintf(stderr, "      queryFileName : The FASTA query file.\n\n");
      fprintf(stderr, "\n");
      fprintf(stderr, "For alignment of pair-end reads:\n");
      fprintf(stderr, "  %s pair <bwtCodeIndex> <queryFileName1> <queryFileName2> -u <maxInsertSize> -v <minInsertSize> [options]\n", program_name);
      fprintf(stderr, "\n");
      fprintf(stderr, "      bwtCodeIndex  : The bwt index built by 2bwt-builder with\n");
      fprintf(stderr, "                      newly introduced 1-level bwt occ table.\n");
      fprintf(stderr, "      queryFileName1: The FASTA file of the first reads of the pairs.\n");
      fprintf(stderr, "      queryFileName2: The FASTA file of the second reads of the pairs.\n\n");
      fprintf(stderr, "      maxInsertSize:  Maximum value of insert size.\n");
      fprintf(stderr, "      minInsertSize:  Minimum value of insert size.\n");
      fprintf(stderr, "\n");
      fprintf(stderr, "      options:\n");
      fprintf(stderr, "               -m <max # of mismatch> (<=4 default:3)\n");
      fprintf(stderr, "               -L <length of the longest read in the input> (default: 120)\n");
      fprintf(stderr, "               -h <alignent type> (1: all valid alignments; \n");
      fprintf(stderr, "                                   2: all best alignments (for single reads only); \n");
      fprintf(stderr, "                                   3: unique best alignments (for single reads only); \n");
      fprintf(stderr, "                                   4: random best alignments (default))\n");
      fprintf(stderr, "               -b <output format> (0: Succinct [Binary]; \n");
      fprintf(stderr, "                                   1: Simple [Plain] (default); \n");
      fprintf(stderr, "                                   2: SAM v1.4 [Plain]) \n");
      fprintf(stderr, "\n");
}

// This function is to parse the ini file and collect the paramaters of
// 1. The file extension of SA file
// 2. The number of CPU threads
// 3. The alignment model
// 4. The memory size in the GPU card
int ParseIniFile(char *iniFileName, IniParams &ini_params) {
      dictionary *ini;
      char tmp[10];
      
      ini = iniparser_load(iniFileName, FALSE);
      if (ini == NULL) {
            printf("[ParseIniFile] File not found!\n");
            return -1;
      }
      
      // Database:SaValueFileExt parameter
      iniparser_copystring(ini, "Database:SaValueFileExt", ini_params.Ini_SaValueFileExt, ini_params.Ini_SaValueFileExt, MAX_FILEEXT_LEN);
      
      // Alignment:NumOfCpuThreads parameter
      ini_params.Ini_NumOfCpuThreads = iniparser_getuint(ini, "Alignment:NumOfCpuThreads", ini_params.Ini_NumOfCpuThreads );
      if (ini_params.Ini_NumOfCpuThreads<=0 || ini_params.Ini_NumOfCpuThreads>MAX_NUM_CPU_THREADS) {
            printf("[ParseIniFile] Invalid valid for Ini_NumOfCpuThreads(%u)!\n",ini_params.Ini_NumOfCpuThreads);
            return -1;
      }
      
      // Alignment:HostAlignmentModel parameter
      iniparser_copystring(ini, "Alignment:HostAlignmentModel", ini_params.Ini_HostAlignmentModelStr, ini_params.Ini_HostAlignmentModelStr, 3);
      if (strcmp(ini_params.Ini_HostAlignmentModelStr,"8G")==0) {
            ini_params.Ini_HostAlignmentModel = SRA_MODEL_8G;
      } else if (strcmp(ini_params.Ini_HostAlignmentModelStr,"16G")==0) {
            ini_params.Ini_HostAlignmentModel = SRA_MODEL_16G;
      } else {
            printf("[ParseIniFile] Invalid valid for HostAlignmentModel(%s))!\n",ini_params.Ini_HostAlignmentModelStr);
            return -1;
      }
      
      ini_params.Ini_MaxOutputPerRead = iniparser_getuint(ini, "Alignment:MaxOutputPerRead", ini_params.Ini_MaxOutputPerRead );
      
      // Device:GPUMemory parameter
      ini_params.Ini_GPUMemory = iniparser_getint(ini, "Device:GPUMemory", 0);
      if (ini_params.Ini_GPUMemory < 3) {
            printf("[ParseIniFile] The program only supports GPU with at least 3G memory.\n");
            return -1;
      }
      
      iniparser_copystring(ini, "PairEnd:StrandArrangement", tmp, tmp, 10);
      if (strcmp(tmp,"+/+")==0) {
        ini_params.Ini_PEStrandLeftLeg = QUERY_POS_STRAND;
        ini_params.Ini_PEStrandRightLeg = QUERY_POS_STRAND;
      } else if (strcmp(tmp,"-/+")==0) {
        ini_params.Ini_PEStrandLeftLeg = QUERY_NEG_STRAND;
        ini_params.Ini_PEStrandRightLeg = QUERY_POS_STRAND;
      } else if (strcmp(tmp,"-/-")==0) {
        ini_params.Ini_PEStrandLeftLeg = QUERY_NEG_STRAND;
        ini_params.Ini_PEStrandRightLeg = QUERY_NEG_STRAND;
      } else {
        ini_params.Ini_PEStrandLeftLeg = QUERY_POS_STRAND;
        ini_params.Ini_PEStrandRightLeg = QUERY_NEG_STRAND;
      }
      
      ini_params.Ini_PEMaxOutputPerPair = iniparser_getuint(ini, "PairEnd:MaxOutputPerPair", ini_params.Ini_PEMaxOutputPerPair );
      
      iniparser_freedict(ini);
      return 0;
}

// This function is to load the pair-end reads for at most "maxNumQueries/2" # of pairs of reads
int loadPairReads(FILE* queryFile, char* queryFileBuffer, FILE* queryFile2, char* queryFileBuffer2,
                  uint* queries, uint* readLengths, uint* readIDs, unsigned char* upkdQueries, char* upkdQueryNames, uint maxReadLength, uint maxNumQueries,
                  size_t& bufferSize, char& queryChar, uint& bufferIndex, size_t& bufferSize2, char& queryChar2, uint& bufferIndex2, 
                  uint accumReadNum, uint wordPerQuery) {
      
      // for pair-ended reads
      
      // load reads from the read file
      // return how many reads are loaded
      
      
      // Convert ACGT to 0123
      uint charMap[256];
      for (int i=0; i<256; i++)
            charMap[i] = 0;
      charMap['A'] = 0;
      charMap['C'] = 1;
      charMap['G'] = 2;
      charMap['T'] = 3;
      charMap['U'] = 3;
      charMap['N'] = 2; // N -> G
      charMap['a'] = 0;
      charMap['c'] = 1;
      charMap['g'] = 2;
      charMap['t'] = 3;
      charMap['u'] = 3;
      charMap['n'] = 2; // N -> G
      
      
      
      uint queriesRead = 0;
      uint currentWord = 0;
      uint offset = 0;
      uint bits = 0;
      uint * queryPtr = queries;
      int isFastq = 0;
      int isFastq2 = 0;
      int i;
      
      while ((bufferSize!=0) && (bufferSize2!=0)) {
            
            
            // FOR THE FIRST READ
            //Read everything before the entry point of a read, character ">"
            while (bufferSize!=0 && queryChar != '>' && queryChar != '@') {
                  queryChar = queryFileBuffer[bufferIndex++];
                  if (bufferIndex>=bufferSize) {
                        bufferSize = fread(queryFileBuffer,sizeof(char),INPUT_BUFFER_SIZE,queryFile);
                        bufferIndex=0;
                  }
            }
            isFastq=0;
            if (queryChar=='@') {isFastq=1;}
            
            //Read the header of a read
            if (bufferSize==0) break;
            queryChar = queryFileBuffer[bufferIndex++];
            if (bufferIndex>=bufferSize) {
                  bufferSize = fread(queryFileBuffer,sizeof(char),INPUT_BUFFER_SIZE,queryFile);
                  bufferIndex=0;
            }
            uint nameCharId=0;
            int cropped=0;
            while (bufferSize!=0  && queryChar != '\n') {
                  if (queryChar == ' ' || queryChar == '\t') {cropped=1;}
                  if (!cropped && nameCharId < MAX_READ_NAME_LENGTH-1) {
                        upkdQueryNames[queriesRead*MAX_READ_NAME_LENGTH+(nameCharId++)]=queryChar;
                  }
                  queryChar = queryFileBuffer[bufferIndex++];
                  if (bufferIndex>=bufferSize) {
                        bufferSize = fread(queryFileBuffer,sizeof(char),INPUT_BUFFER_SIZE,queryFile);
                        bufferIndex=0;
                  }
            }
            upkdQueryNames[queriesRead*MAX_READ_NAME_LENGTH+(nameCharId++)]='\0';
            
            //Read the pattern body of a read
            if (bufferSize==0) break;
            queryChar = queryFileBuffer[bufferIndex++];
            if (bufferIndex>=bufferSize) {
                  bufferSize = fread(queryFileBuffer,sizeof(char),INPUT_BUFFER_SIZE,queryFile);
                  bufferIndex=0;
            }
            uint nucleoId=0;
            while (bufferSize!=0  && queryChar != '>' && queryChar != '@' && queryChar != '+') {
                  if (queryChar != '\n') {
                        bits = charMap[queryChar];
                        if (nucleoId<maxReadLength) {
                              upkdQueries[queriesRead*maxReadLength+nucleoId]=bits;
                              currentWord |= (bits << (offset * 2));
                              offset++;
                              if (offset == 16) {
                                    *queryPtr = currentWord;
                                    queryPtr += 32;
                                    offset = 0;
                                    currentWord = 0;
                              }
                        }
                        nucleoId++;
                  }
                  queryChar = queryFileBuffer[bufferIndex++];
                  if (bufferIndex>=bufferSize) {
                        bufferSize = fread(queryFileBuffer,sizeof(char),INPUT_BUFFER_SIZE,queryFile);
                        bufferIndex=0;
                  }
            }
            if (offset > 0)
                  *queryPtr = currentWord;
            currentWord = 0;
            offset = 0;
            readLengths[queriesRead] = nucleoId;
            readIDs[queriesRead] = queriesRead+1;
            if (nucleoId > maxReadLength) {
                  // printf("[WARNING] Left read #%u is longer than %u! Read truncated.\n", (queriesRead+accumReadNum)/2+1, maxReadLength);
                  readLengths[queriesRead] = maxReadLength;
            }
            
            //Parse the quality part of the read
            if (isFastq) {
                  //Read everything before the entry point of a read, character "+"
                  while (bufferSize!=0 && queryChar != '+') {
                        queryChar = queryFileBuffer[bufferIndex++];
                        if (bufferIndex>=bufferSize) {
                              bufferSize = fread(queryFileBuffer,sizeof(char),INPUT_BUFFER_SIZE,queryFile);
                              bufferIndex=0;
                        }
                  }
                  //Read the header of a read
                  if (bufferSize==0) break;
                  queryChar = queryFileBuffer[bufferIndex++];
                  if (bufferIndex>=bufferSize) {
                        bufferSize = fread(queryFileBuffer,sizeof(char),INPUT_BUFFER_SIZE,queryFile);
                        bufferIndex=0;
                  }
                  while (bufferSize!=0 && queryChar != '\n') {
                        queryChar = queryFileBuffer[bufferIndex++];
                        if (bufferIndex>=bufferSize) {
                              bufferSize = fread(queryFileBuffer,sizeof(char),INPUT_BUFFER_SIZE,queryFile);
                              bufferIndex=0;
                        }
                  }
                  //Read the quality of a read
                  if (bufferSize==0) break;
                  queryChar = queryFileBuffer[bufferIndex++];
                  if (bufferIndex>=bufferSize) {
                        bufferSize = fread(queryFileBuffer,sizeof(char),INPUT_BUFFER_SIZE,queryFile);
                        bufferIndex=0;
                  }
                  for (i=0;i<nucleoId;i++) {
                        queryChar = queryFileBuffer[bufferIndex++];
                        if (bufferIndex>=bufferSize) {
                              bufferSize = fread(queryFileBuffer,sizeof(char),INPUT_BUFFER_SIZE,queryFile);
                              bufferIndex=0;
                        }
                        //In SOAP3-GPU/SOAP3-CPU we do not have handling for read quality.
                        //therefore the read quality is CURRENTLY read from the input file
                        //and discarded immediately.
                  }
            }
            
            queriesRead++;
            queryPtr = queries + (queriesRead / 32 * 32 * wordPerQuery + queriesRead % 32);
            
            
            
            
            // FOR THE SECOND READ
            //Read everything before the entry point of a read, character ">"
            while (bufferSize2!=0 && queryChar2 != '>' && queryChar2 != '@') {
                  queryChar2 = queryFileBuffer2[bufferIndex2++];
                  if (bufferIndex2>=bufferSize2) {
                        bufferSize2 = fread(queryFileBuffer2,sizeof(char),INPUT_BUFFER_SIZE,queryFile2);
                        bufferIndex2=0;
                  }
            }
            isFastq2=0;
            if (queryChar2=='@') {isFastq2=1;}
            
            //Read the header of a read
            if (bufferSize2==0) break;
            queryChar2 = queryFileBuffer2[bufferIndex2++];
            if (bufferIndex2>=bufferSize2) {
                  bufferSize2 = fread(queryFileBuffer2,sizeof(char),INPUT_BUFFER_SIZE,queryFile2);
                  bufferIndex2=0;
            }
            nameCharId=0;
            cropped=0;
            while (bufferSize2!=0  && queryChar2 != '\n') {
                  if (queryChar2 == ' ' || queryChar2 == '\t') {cropped=1;}
                  if (!cropped && nameCharId < MAX_READ_NAME_LENGTH-1) {
                        upkdQueryNames[queriesRead*MAX_READ_NAME_LENGTH+(nameCharId++)]=queryChar2;
                  }
                  queryChar2 = queryFileBuffer2[bufferIndex2++];
                  if (bufferIndex2>=bufferSize2) {
                        bufferSize2 = fread(queryFileBuffer2,sizeof(char),INPUT_BUFFER_SIZE,queryFile2);
                        bufferIndex2=0;
                  }
            }
            upkdQueryNames[queriesRead*MAX_READ_NAME_LENGTH+(nameCharId++)]='\0';
            
            //Read the pattern body of a read
            if (bufferSize2==0) break;
            queryChar2 = queryFileBuffer2[bufferIndex2++];
            if (bufferIndex2>=bufferSize2) {
                  bufferSize2 = fread(queryFileBuffer2,sizeof(char),INPUT_BUFFER_SIZE,queryFile2);
                  bufferIndex2=0;
            }
            nucleoId=0;
            while (bufferSize2!=0  && queryChar2 != '>'&& queryChar2 != '@' && queryChar2 != '+') {
                  if (queryChar2 != '\n') {
                        bits = charMap[queryChar2];
                        if (nucleoId<maxReadLength) {
                              upkdQueries[queriesRead*maxReadLength+nucleoId]=bits;
                              currentWord |= (bits << (offset * 2));
                              offset++;
                              if (offset == 16) {
                                    *queryPtr = currentWord;
                                    queryPtr += 32;
                                    offset = 0;
                                    currentWord = 0;
                              }
                        }
                        nucleoId++;
                  }
                  queryChar2 = queryFileBuffer2[bufferIndex2++];
                  if (bufferIndex2>=bufferSize2) {
                        bufferSize2 = fread(queryFileBuffer2,sizeof(char),INPUT_BUFFER_SIZE,queryFile2);
                        bufferIndex2=0;
                  }
            }
            if (offset > 0)
                  *queryPtr = currentWord;
            currentWord = 0;
            offset = 0;
            readLengths[queriesRead] = nucleoId;
            readIDs[queriesRead] = queriesRead+1;
            if (nucleoId > maxReadLength) {
                  // printf("[WARNING] Right read #%u is longer than %u! Read truncated.\n", (queriesRead+accumReadNum)/2+1, maxReadLength);
                  readLengths[queriesRead] = maxReadLength;
            }
            
            //Parse the quality part of the read
            if (isFastq2) {
                  //Read everything before the entry point of a read, character "+"
                  while (bufferSize2!=0 && queryChar2 != '+') {
                        queryChar2 = queryFileBuffer2[bufferIndex2++];
                        if (bufferIndex2>=bufferSize2) {
                              bufferSize2 = fread(queryFileBuffer2,sizeof(char),INPUT_BUFFER_SIZE,queryFile2);
                              bufferIndex2=0;
                        }
                  }
                  //Read the header of a read
                  if (bufferSize2==0) break;
                  queryChar2 = queryFileBuffer2[bufferIndex2++];
                  if (bufferIndex2>=bufferSize2) {
                        bufferSize2 = fread(queryFileBuffer2,sizeof(char),INPUT_BUFFER_SIZE,queryFile2);
                        bufferIndex2=0;
                  }
                  while (bufferSize2!=0 && queryChar2 != '\n') {
                        queryChar2 = queryFileBuffer2[bufferIndex2++];
                        if (bufferIndex2>=bufferSize2) {
                              bufferSize2 = fread(queryFileBuffer2,sizeof(char),INPUT_BUFFER_SIZE,queryFile2);
                              bufferIndex2=0;
                        }
                  }
                  //Read the quality of a read
                  if (bufferSize2==0) break;
                  queryChar2 = queryFileBuffer2[bufferIndex2++];
                  if (bufferIndex2>=bufferSize2) {
                        bufferSize2 = fread(queryFileBuffer2,sizeof(char),INPUT_BUFFER_SIZE,queryFile2);
                        bufferIndex2=0;
                  }
                  for (i=0;i<nucleoId;i++) {
                        queryChar2 = queryFileBuffer2[bufferIndex2++];
                        if (bufferIndex2>=bufferSize2) {
                              bufferSize2 = fread(queryFileBuffer2,sizeof(char),INPUT_BUFFER_SIZE,queryFile2);
                              bufferIndex2=0;
                        }
                        //In SOAP3-GPU/SOAP3-CPU we do not have handling for read quality.
                        //therefore the read quality is CURRENTLY read from the input file
                        //and discarded immediately.
                  }
            }
            
            queriesRead++;
            queryPtr = queries + (queriesRead / 32 * 32 * wordPerQuery + queriesRead % 32);
            
            
            
            
            if (queriesRead >= maxNumQueries)
                  break;
      }
      
      return queriesRead;
      
}

// This function is to load the single reads for at most "maxNumQueries" # of reads
int loadSingleReads(FILE* queryFile, char* queryFileBuffer, uint* queries, uint* readLengths, uint* readIDs, 
                    unsigned char* upkdQueries, char* upkdQueryNames, uint maxReadLength, uint maxNumQueries,
                    size_t& bufferSize, char& queryChar, uint& bufferIndex, uint accumReadNum, uint wordPerQuery) {
      
      // for single reads
      
      // load reads from the read file
      // return how many reads are loaded
      
      
      // Convert ACGT to 0123
      uint charMap[256];
      for (int i=0; i<256; i++)
            charMap[i] = 0;
      charMap['A'] = 0;
      charMap['C'] = 1;
      charMap['G'] = 2;
      charMap['T'] = 3;
      charMap['U'] = 3;
      charMap['N'] = 2; // N -> G
      charMap['a'] = 0;
      charMap['c'] = 1;
      charMap['g'] = 2;
      charMap['t'] = 3;
      charMap['u'] = 3;
      charMap['n'] = 2; // N -> G
      
      
      
      uint queriesRead = 0;
      uint currentWord = 0;
      uint offset = 0;
      uint bits = 0;
      uint * queryPtr = queries;
      int isFastq = 0;
      int i;
      
      while (bufferSize!=0) {
            
            //Read everything before the entry point of a read, character ">"
            while (bufferSize!=0 && queryChar != '>' && queryChar != '@') {
                  queryChar = queryFileBuffer[bufferIndex++];
                  if (bufferIndex>=bufferSize) {
                        bufferSize = fread(queryFileBuffer,sizeof(char),INPUT_BUFFER_SIZE,queryFile);
                        bufferIndex=0;
                  }
            }
            
            isFastq=0;
            if (queryChar=='@') {isFastq=1;}
            
            //Read the header of a read
            if (bufferSize==0) break;
            queryChar = queryFileBuffer[bufferIndex++];
            if (bufferIndex>=bufferSize) {
                  bufferSize = fread(queryFileBuffer,sizeof(char),INPUT_BUFFER_SIZE,queryFile);
                  bufferIndex=0;
            }
            uint nameCharId=0;
            int cropped=0;
            while (bufferSize!=0  && queryChar != '\n') {
                  if (queryChar == ' ' || queryChar == '\t') {cropped=1;}
                  if (!cropped && nameCharId < MAX_READ_NAME_LENGTH-1) {
                        upkdQueryNames[queriesRead*MAX_READ_NAME_LENGTH+(nameCharId++)]=queryChar;
                  }
                  queryChar = queryFileBuffer[bufferIndex++];
                  if (bufferIndex>=bufferSize) {
                        bufferSize = fread(queryFileBuffer,sizeof(char),INPUT_BUFFER_SIZE,queryFile);
                        bufferIndex=0;
                  }
            }
            upkdQueryNames[queriesRead*MAX_READ_NAME_LENGTH+(nameCharId++)]='\0';
            
            //Read the pattern body of a read
            if (bufferSize==0) break;
            queryChar = queryFileBuffer[bufferIndex++];
            if (bufferIndex>=bufferSize) {
                  bufferSize = fread(queryFileBuffer,sizeof(char),INPUT_BUFFER_SIZE,queryFile);
                  bufferIndex=0;
            }
            uint nucleoId=0;
            while (bufferSize!=0  && queryChar != '>' && queryChar != '@' && queryChar != '+') {
                  if (queryChar != '\n') {
                        bits = charMap[queryChar];
                        if (nucleoId<maxReadLength) {
                              upkdQueries[queriesRead*maxReadLength+nucleoId]=bits;
                              currentWord |= (bits << (offset * 2));
                              offset++;
                              if (offset == 16) {
                                    *queryPtr = currentWord;
                                    queryPtr += 32;
                                    offset = 0;
                                    currentWord = 0;
                              }
                        }
                        nucleoId++;
                  }
                  queryChar = queryFileBuffer[bufferIndex++];
                  if (bufferIndex>=bufferSize) {
                        bufferSize = fread(queryFileBuffer,sizeof(char),INPUT_BUFFER_SIZE,queryFile);
                        bufferIndex=0;
                  }
            }
            if (offset > 0)
                  *queryPtr = currentWord;
            currentWord = 0;
            offset = 0;
            readLengths[queriesRead] = nucleoId;
            readIDs[queriesRead] = queriesRead+1;
            if (nucleoId > maxReadLength) {
                  // printf("[WARNING] Read #%u is longer than %u! Read truncated.\n", queriesRead+1+accumReadNum, maxReadLength);
                  readLengths[queriesRead] = maxReadLength;
            }
            
            //Parse the quality part of the read
            if (isFastq) {
                  //Read everything before the entry point of a read, character "+"
                  while (bufferSize!=0 && queryChar != '+') {
                        queryChar = queryFileBuffer[bufferIndex++];
                        if (bufferIndex>=bufferSize) {
                              bufferSize = fread(queryFileBuffer,sizeof(char),INPUT_BUFFER_SIZE,queryFile);
                              bufferIndex=0;
                        }
                  }
                  //Read the header of a read
                  if (bufferSize==0) break;
                  queryChar = queryFileBuffer[bufferIndex++];
                  if (bufferIndex>=bufferSize) {
                        bufferSize = fread(queryFileBuffer,sizeof(char),INPUT_BUFFER_SIZE,queryFile);
                        bufferIndex=0;
                  }
                  while (bufferSize!=0 && queryChar != '\n') {
                        queryChar = queryFileBuffer[bufferIndex++];
                        if (bufferIndex>=bufferSize) {
                              bufferSize = fread(queryFileBuffer,sizeof(char),INPUT_BUFFER_SIZE,queryFile);
                              bufferIndex=0;
                        }
                  }
                  //Read the quality of a read
                  if (bufferSize==0) break;
                  queryChar = queryFileBuffer[bufferIndex++];
                  if (bufferIndex>=bufferSize) {
                        bufferSize = fread(queryFileBuffer,sizeof(char),INPUT_BUFFER_SIZE,queryFile);
                        bufferIndex=0;
                  }
                  for (i=0;i<nucleoId;i++) {
                        queryChar = queryFileBuffer[bufferIndex++];
                        if (bufferIndex>=bufferSize) {
                              bufferSize = fread(queryFileBuffer,sizeof(char),INPUT_BUFFER_SIZE,queryFile);
                              bufferIndex=0;
                        }
                        //In SOAP3-GPU/SOAP3-CPU we do not have handling for read quality.
                        //therefore the read quality is CURRENTLY read from the input file
                        //and discarded immediately.
                  }
            }
            
            queriesRead++;
            queryPtr = queries + (queriesRead / 32 * 32 * wordPerQuery + queriesRead % 32);
            if (queriesRead >= maxNumQueries)
                  break;
      }
      return queriesRead;
      
}

int get_default_mismatch_num(uint* readLengths, uint numQueries) {
      // if user does not specify # of mismatches
      // then scan the first ten reads
      // if the read length < 50, then return DEFAULT_NUM_MISMATCH_FOR_SHORT_READ
      // else return DEFAULT_NUM_MISMATCH_FOR_NORMAL_READ
      if (numQueries <= 0)
            return DEFAULT_NUM_MISMATCH_FOR_NORMAL_READ;
      uint max_read_length = readLengths[0];
      for (int i=1; i<10,i<numQueries; i++) {
            if (max_read_length < readLengths[i])
                  max_read_length = readLengths[i];
      }
      if (max_read_length > 50)
            return DEFAULT_NUM_MISMATCH_FOR_NORMAL_READ;
      else
            return DEFAULT_NUM_MISMATCH_FOR_SHORT_READ;
}


bool parseInputArgs(int argc, char** argv, InputOptions& input_options) {
      // This function is to check and parse the input arguments
      // arguments: <program> single bwtCodeIndex queryFileName numQueries maxReadLength [options]
      // OR         <program> pair bwtCodeIndex queryFileName1 queryFileName2 numQueries maxReadLength [options]
      // If the argument is not correct, then it returns FALSE. Else it returns TRUE

      if (argc < 2) {
            fprintf(stderr,"No argument has been provided.\n");
            printUsage(argv[0]);
            return false;
      }

      // check the read type
      if (strcmp(argv[1],"single")==0) {
            input_options.readType = SINGLE_READ;
      } else if (strcmp(argv[1],"pair")==0) {
            input_options.readType = PAIR_END_READ;
      } else {
            fprintf(stderr,"Miss the keyword 'single' or 'pair'.\n");
            return false;
      }
      
      // check the number of arguments for the specific read type
      int min_num_args = (input_options.readType==SINGLE_READ)?4:5;
      if (argc < min_num_args) {
            fprintf(stderr,"Invalid number of command-line arguments.\n");
            printUsage(argv[0]);
            return false;
      }

      // get the query file name(s) and the maximum read length
      input_options.queryFileName = argv[3];

      if (input_options.readType == PAIR_END_READ) {
            input_options.queryFileName2 = argv[4];
      }
      
      // set default values
      input_options.maxReadLength = DEFAULT_MAX_READ_LEN;
      // input_options.numMismatch = DEFAULT_NUM_MISMATCH;
      input_options.numMismatch = -1;
      input_options.alignmentType = OUTPUT_RANDOM_BEST;
      input_options.enableTrim = FALSE;
      input_options.lenTrim = DEFAULT_TRIM_LEN;
      input_options.numMismatchForTrim = DEFAULT_NUM_MISMATCH_FOR_TRIM;
      input_options.insert_low = -1;
      input_options.insert_high = -1;
      input_options.outputFormat = SRA_OUTPUT_FORMAT_PLAIN;
      
      // get the options
      int start_option_pos = (input_options.readType==SINGLE_READ)?4:5;
      for (int i = start_option_pos; i < argc; i++) {
            if (strcmp(argv[i],"-t")==0) {
                  // trim function is enable only for single read
                  if (input_options.readType==SINGLE_READ)
                        input_options.enableTrim = TRUE;
            } else if (strcmp(argv[i],"-m")==0) {
                  // maximum # of mismatches
                  if (i+1 >= argc) {
                        fprintf(stderr,"Please specify the maximum number of mismatches after '-m' ( from 0 to %i )\n", MAX_NUM_MISMATCH);
                        return false;
                  }
                  input_options.numMismatch = atoi(argv[++i]);
                  if (input_options.numMismatch < 0 || input_options.numMismatch > MAX_NUM_MISMATCH) {
                        fprintf(stderr,"The range of maximum number of mismatches should be from 0 to %i\n", MAX_NUM_MISMATCH);
                        return false;
                  }
            } else if (strcmp(argv[i],"-h")==0) {
                  // alignment type
                  // 1: all valid alignments
                  // 2: all best alignments
                  // 3: unique best alignments
                  // 4: random best alignments
                  if (i+1 >= argc) {
                        fprintf(stderr,"Please specify the alignment type after '-h' ( 1, 2, 3 or 4 )\n");
                        return false;
                  }
                  int input_type = atoi(argv[++i]);
                  if (input_type==1) {
                        input_options.alignmentType = OUTPUT_ALL_VALID;
                  } else if (input_type==2) {
                        input_options.alignmentType = OUTPUT_ALL_BEST;
                  } else if (input_type==3) {
                        input_options.alignmentType = OUTPUT_UNIQUE_BEST;
                  } else if (input_type==4) {
                        input_options.alignmentType = OUTPUT_RANDOM_BEST;
                  } else {
                        fprintf(stderr,"The alignment type should be 1, 2, 3 or 4\n");
                        return false;
                  }
                  if (input_options.readType==PAIR_END_READ && input_options.alignmentType==OUTPUT_ALL_BEST) {
                        fprintf(stderr,"The alignment type cannot be 2 for paired-end alignment.\n");
                        return false;
                  }
                  if (input_options.readType==PAIR_END_READ && input_options.alignmentType==OUTPUT_UNIQUE_BEST) {
                        fprintf(stderr,"The alignment type cannot be 3 for paired-end alignment.\n");
                        return false;
                  }
            } else if (strcmp(argv[i],"-L")==0) {
                  // the length of the longest read in the input
                  if (i+1 >= argc) {
                        fprintf(stderr,"Please specify the length after '-L'\n");
                        return false;
                  }
                  input_options.maxReadLength = atoi(argv[++i]);
                  if (input_options.maxReadLength < 0) {
                        fprintf(stderr,"The length should not be less than 0\n");
                        return false;
                  } else if (input_options.maxReadLength > MAX_READ_LENGTH) {
                        fprintf(stderr,"The length should not be greater than %u\n", MAX_READ_LENGTH);
                        return false;
                  }
            } else if (strcmp(argv[i],"-l")==0) {
                  // the length should be trimmed from the tail part of the read
                  if (i+1 >= argc) {
                        fprintf(stderr,"Please specify the length after '-l'\n");
                        return false;
                  }
                  input_options.lenTrim = atoi(argv[++i]);
                  if (input_options.lenTrim < 0) {
                        fprintf(stderr,"The range of the length should not be less than 0\n");
                        return false;
                  }
            } else if (strcmp(argv[i],"-n")==0) {
                  // the maximum number of mismatch allowed for the alignment of trimmed reads
                  if (i+1 >= argc) {
                        fprintf(stderr,"Please specify the maximum number of mismatches after '-n' ( from 0 to %i )\n", MAX_NUM_MISMATCH);
                        return false;
                  }
                  input_options.numMismatchForTrim = atoi(argv[++i]);
                  if (input_options.numMismatchForTrim < 0 || input_options.numMismatchForTrim > MAX_NUM_MISMATCH) {
                        fprintf(stderr,"The range of maximum number of mismatches should be from 0 to %i\n", MAX_NUM_MISMATCH);
                        return false;
                  }
            } else if (strcmp(argv[i],"-u")==0) {
                  // the upper bound of the insert size
                  if (i+1 >= argc) {
                        fprintf(stderr,"Please specify the maximum value of insert size after '-u'\n");
                        return false;
                  }
                  input_options.insert_high = atoi(argv[++i]);
            } else if (strcmp(argv[i],"-v")==0) {
                  // the lower bound of the insert size
                  if (i+1 >= argc) {
                        fprintf(stderr,"Please specify the maximum value of insert size after '-v'\n");
                        return false;
                  }
                  input_options.insert_low = atoi(argv[++i]);
            } else if (strcmp(argv[i],"-b")==0) {
                  // the output format
                  if (i+1 >= argc) {
                        fprintf(stderr,"Please specify the output file format '-b'\n");
                        return false;
                  }
                  input_options.outputFormat = atoi(argv[++i]);
                  if (input_options.outputFormat != SRA_OUTPUT_FORMAT_DEFAULT &&
                        input_options.outputFormat != SRA_OUTPUT_FORMAT_SAM_API &&
                        input_options.outputFormat != SRA_OUTPUT_FORMAT_SAM &&
                        input_options.outputFormat != SRA_OUTPUT_FORMAT_PLAIN) {
                        fprintf(stderr,"The value should either be %d:Plain, %d:Succinct or %d/%d:SAM v1.4\n",SRA_OUTPUT_FORMAT_PLAIN, SRA_OUTPUT_FORMAT_DEFAULT,SRA_OUTPUT_FORMAT_SAM_API,SRA_OUTPUT_FORMAT_SAM);
                        return false;
                  }
            }
      }
      if (input_options.readType == PAIR_END_READ) {
            if (input_options.insert_high == -1) {
                  fprintf(stderr,"Please specify the maximum value of insert size after '-u'\n");
                  return false;
            }
            if (input_options.insert_low == -1) {
                  fprintf(stderr,"Please specify the minimum value of insert size after '-v'\n");
                  return false;
            }
            if (input_options.insert_low > input_options.insert_high) {
                  fprintf(stderr,"The minimum value of insert size should not greater than the maximum value of insert size\n");
                  return false;
            }
      }
      return true;
}

// pack the reads with no alignment together
// return number of reads with no alignment
uint packReads(uint* queries, uint* readIDs, uint* readLengths, unsigned char* noAlignment,
               uint wordPerQuery, uint numQueries) {
      
      ullint total_no_alignment=0;
      for (ullint readId = 0; readId < numQueries; ++readId) {
            if (noAlignment[readId]==1) {
                  // the read has no alignment
                  if (total_no_alignment < readId) {
                        ullint srcQueryOffset = (readId) / 32 * 32 * wordPerQuery + readId % 32;
                        ullint srcNewQueryOffset = (total_no_alignment) / 32 * 32 * wordPerQuery + total_no_alignment % 32;
                        for (ullint i = 0; i < wordPerQuery; ++i) { // copy each word of the read
                              queries[srcNewQueryOffset + i * 32] = *(queries + (srcQueryOffset + i * 32));
                        }
                        readLengths[total_no_alignment] = readLengths[readId];
                        readIDs[total_no_alignment] = readIDs[readId];
                  }
                  total_no_alignment++;
            }
      }
      return total_no_alignment;
}

// pack the reads which are unpaired
void packUnPairedReads(uint* queries, uint* readIDs, uint* readLengths, uint* unAlignedPair,
                       uint wordPerQuery, uint numOfUnPaired, ullint maxBatchSize) {
      

      ullint roundUp = (numOfUnPaired + 31) / 32 * 32;
      ullint totalQueryLength = roundUp * wordPerQuery;
      uint * newqueries;
      uint * newreadLengths;
      uint * newreadIDs;

      if (numOfUnPaired > 0) {
            // copy the content to the new array
            
            newqueries = (uint*) malloc(totalQueryLength * sizeof(uint)); // a large array to store all queries
            newreadLengths = (uint*) malloc(roundUp * sizeof(uint));
            newreadIDs = (uint*) malloc(roundUp * sizeof(uint));
            
            
            for (ullint i = 0; i < numOfUnPaired; i++) {
                  ullint readId = unAlignedPair[i];
                  ullint srcQueryOffset = readId / 32 * 32 * wordPerQuery + readId % 32;
                  ullint srcNewQueryOffset = i / 32 * 32 * wordPerQuery + i % 32;
                  for (ullint j = 0; j < wordPerQuery; ++j) { // copy each word of the read
                        newqueries[srcNewQueryOffset + j * 32] = *(queries + (srcQueryOffset + j * 32));
                  }
                  newreadLengths[i] = readLengths[readId];
                  newreadIDs[i] = readIDs[readId];
            }
      }
      
      // clear the content of the old array
      ullint maxRoundUp = (maxBatchSize + 31) / 32 * 32;
      ullint maxTotalQueryLength = maxRoundUp * wordPerQuery;
      memset(queries,0,maxTotalQueryLength*sizeof(uint));
      memset(readLengths,0,maxRoundUp*sizeof(uint));
      memset(readIDs,0,maxRoundUp*sizeof(uint));
      
      if (numOfUnPaired > 0) {
            // copy the content of the new array to the old array
            memcpy(queries, newqueries, totalQueryLength * sizeof(uint));
            memcpy(readLengths, newreadLengths, roundUp * sizeof(uint));
            memcpy(readIDs, newreadIDs, roundUp * sizeof(uint));
            
            // free the memory
            free(newqueries);
            free(newreadLengths);
            free(newreadIDs);
      }
}


unsigned long long ProcessReadSingleStrand(SRAQueryInput * qInput, SRAModel * aModel, int whichCase) {
      unsigned long long saCount = 0;
      SRAQuerySetting * qSetting = qInput->QuerySetting;
      SRAQueryResultCount * rOutput  = qInput->QueryOutput;
      BWT * bwt = qSetting->bwt;
      
      SRACase * thisCase = &(aModel->cases[whichCase]);
      unsigned long long saRanges[4];
      
      rOutput->IsOpened = 0;
      rOutput->IsClosed = 0;
      rOutput->IsUnique = 0;
      rOutput->IsBest = 0;
      
      saRanges[0] = 0;
      saRanges[1] = bwt->textLength;
      saRanges[2] = 0;
      saRanges[3] = bwt->textLength;
      
      if (thisCase->steps[0].type == SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP) {
            saCount += BWTExactModelBackward_Lookup(qInput,thisCase,0,saRanges);
      } else if (thisCase->steps[0].type == SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP) {
            saCount += BWTExactModelForward_Lookup(qInput,thisCase,0,saRanges);
      } else if (thisCase->steps[0].type == SRA_STEP_TYPE_BACKWARD_ONLY_BWT) {
            saCount += BWTModelSwitchBackward(qInput,0,0,
                                              thisCase,0,
                                              saRanges, 0, 0);
      } else if (thisCase->steps[0].type == SRA_STEP_TYPE_BI_DIRECTIONAL_BWT) {
            saCount += BWTModelSwitchAnyDirection(qInput,0,0,
                                                  thisCase,0,
                                                  saRanges, 0, 0);
      }
      
      return saCount;
      
}


unsigned long long ProcessReadDoubleStrand(SRAQueryInput * qInput, SRAQueryInput * qInput_neg, SRAModel * aModel, int whichCase) {
      unsigned long long saCount = 0;
      SRAQuerySetting * qSetting = qInput->QuerySetting;
      SRAQueryResultCount * rOutput  = qInput->QueryOutput;
      BWT * bwt = qSetting->bwt;
      SRACase * thisCase =  &(aModel->cases[whichCase]);
      unsigned long long saRanges[4];
      rOutput->IsOpened = 0;
      rOutput->IsClosed = 0;
      rOutput->IsUnique = 0;
      rOutput->IsBest = 0;
      
      saRanges[0] = 0;
      saRanges[1] = bwt->textLength;
      saRanges[2] = 0;
      saRanges[3] = bwt->textLength;
      
      if (thisCase->steps[0].type == SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP) {
            saCount += BWTExactModelBackward_Lookup(qInput,thisCase,0,saRanges);
      } else if (thisCase->steps[0].type == SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP) {
            saCount += BWTExactModelForward_Lookup(qInput,thisCase,0,saRanges);
      } else if (thisCase->steps[0].type == SRA_STEP_TYPE_BACKWARD_ONLY_BWT) {
            saCount += BWTModelSwitchBackward(qInput,0,0,
                                              thisCase,0,
                                              saRanges, 0, 0);
      } else if (thisCase->steps[0].type == SRA_STEP_TYPE_BI_DIRECTIONAL_BWT) {
            saCount += BWTModelSwitchAnyDirection(qInput,0,0,
                                                  thisCase,0,
                                                  saRanges, 0, 0);
      }
      
      saRanges[0] = 0;
      saRanges[1] = bwt->textLength;
      saRanges[2] = 0;
      saRanges[3] = bwt->textLength;
      
      if (thisCase->steps[0].type == SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP) {
            saCount += BWTExactModelBackward_Lookup(qInput_neg,thisCase,0,saRanges);
      } else if (thisCase->steps[0].type == SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP) {
            saCount += BWTExactModelForward_Lookup(qInput_neg,thisCase,0,saRanges);
      } else if (thisCase->steps[0].type == SRA_STEP_TYPE_BACKWARD_ONLY_BWT) {
            saCount += BWTModelSwitchBackward(qInput_neg,0,0,
                                              thisCase,0,
                                              saRanges, 0, 0);
      } else if (thisCase->steps[0].type == SRA_STEP_TYPE_BI_DIRECTIONAL_BWT) {
            saCount += BWTModelSwitchAnyDirection(qInput_neg,0,0,
                                                  thisCase,0,
                                                  saRanges, 0, 0);
      }
      
      return saCount;
      
}

unsigned long long ProcessReadSingleStrand2(SRAQueryInput * qInput, SRAModel * aModel, int whichCase, SAList* sa_list, OCCList* occ_list) {
      unsigned long long saCount = 0;
      SRAQuerySetting * qSetting = qInput->QuerySetting;
      SRAQueryResultCount * rOutput  = qInput->QueryOutput;
      BWT * bwt = qSetting->bwt;
      
      SRACase * thisCase = &(aModel->cases[whichCase]);
      unsigned long long saRanges[4];
      
      rOutput->IsOpened = 0;
      rOutput->IsClosed = 0;
      rOutput->IsUnique = 0;
      rOutput->IsBest = 0;
      
      saRanges[0] = 0;
      saRanges[1] = bwt->textLength;
      saRanges[2] = 0;
      saRanges[3] = bwt->textLength;
      
      if (thisCase->steps[0].type == SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP) {
            saCount += BWTExactModelBackward_Lookup2(qInput,thisCase,0,saRanges,sa_list,occ_list);
      } else if (thisCase->steps[0].type == SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP) {
            saCount += BWTExactModelForward_Lookup2(qInput,thisCase,0,saRanges,sa_list,occ_list);
      } else if (thisCase->steps[0].type == SRA_STEP_TYPE_BACKWARD_ONLY_BWT) {
            saCount += BWTModelSwitchBackward2(qInput,0,0,
                                              thisCase,0,
                                              saRanges, 0, 0, sa_list,occ_list);
      } else if (thisCase->steps[0].type == SRA_STEP_TYPE_BI_DIRECTIONAL_BWT) {
            saCount += BWTModelSwitchAnyDirection2(qInput,0,0,
                                                  thisCase,0,
                                                  saRanges, 0, 0, sa_list,occ_list);
      }
      
      return saCount;
      
}


unsigned long long ProcessReadDoubleStrand2(SRAQueryInput * qInput, SRAQueryInput * qInput_neg, SRAModel * aModel, int whichCase, SAList* sa_list, OCCList* occ_list) {
      unsigned long long saCount = 0;
      SRAQuerySetting * qSetting = qInput->QuerySetting;
      SRAQueryResultCount * rOutput  = qInput->QueryOutput;
      BWT * bwt = qSetting->bwt;
      SRACase * thisCase =  &(aModel->cases[whichCase]);
      unsigned long long saRanges[4];
      rOutput->IsOpened = 0;
      rOutput->IsClosed = 0;
      rOutput->IsUnique = 0;
      rOutput->IsBest = 0;
      
      saRanges[0] = 0;
      saRanges[1] = bwt->textLength;
      saRanges[2] = 0;
      saRanges[3] = bwt->textLength;
      
      if (thisCase->steps[0].type == SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP) {
            saCount += BWTExactModelBackward_Lookup2(qInput,thisCase,0,saRanges,sa_list,occ_list);
      } else if (thisCase->steps[0].type == SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP) {
            saCount += BWTExactModelForward_Lookup2(qInput,thisCase,0,saRanges,sa_list,occ_list);
      } else if (thisCase->steps[0].type == SRA_STEP_TYPE_BACKWARD_ONLY_BWT) {
            saCount += BWTModelSwitchBackward2(qInput,0,0,
                                              thisCase,0,
                                              saRanges, 0, 0,sa_list,occ_list);
      } else if (thisCase->steps[0].type == SRA_STEP_TYPE_BI_DIRECTIONAL_BWT) {
            saCount += BWTModelSwitchAnyDirection2(qInput,0,0,
                                                  thisCase,0,
                                                  saRanges, 0, 0,sa_list,occ_list);
      }
      
      saRanges[0] = 0;
      saRanges[1] = bwt->textLength;
      saRanges[2] = 0;
      saRanges[3] = bwt->textLength;
      
      if (thisCase->steps[0].type == SRA_STEP_TYPE_BACKWARD_ONLY_LOOKUP) {
            saCount += BWTExactModelBackward_Lookup2(qInput_neg,thisCase,0,saRanges,sa_list,occ_list);
      } else if (thisCase->steps[0].type == SRA_STEP_TYPE_BI_DIRECTIONAL_FORWARD_LOOKUP) {
            saCount += BWTExactModelForward_Lookup2(qInput_neg,thisCase,0,saRanges,sa_list,occ_list);
      } else if (thisCase->steps[0].type == SRA_STEP_TYPE_BACKWARD_ONLY_BWT) {
            saCount += BWTModelSwitchBackward2(qInput_neg,0,0,
                                              thisCase,0,
                                              saRanges, 0, 0,sa_list,occ_list);
      } else if (thisCase->steps[0].type == SRA_STEP_TYPE_BI_DIRECTIONAL_BWT) {
            saCount += BWTModelSwitchAnyDirection2(qInput_neg,0,0,
                                                  thisCase,0,
                                                  saRanges, 0, 0,sa_list,occ_list);
      }
      
      return saCount;
      
}


// print out the parameters
void printParameters(InputOptions input_options, IniParams ini_params) {
#ifdef BGS_OUTPUT_PARAMETERS
      printf("\n----------Parameters------------------\n");
      printf("Number of mismatches: %i\n", input_options.numMismatch);
      if (input_options.alignmentType==1)
            printf("Output all valid alignments\n");
      else
            printf("Ouput all best alignments\n");
      if (input_options.enableTrim==TRUE) {
            printf("No-hit reads will be trimmed for further process\n");
            printf("Trim length: %i\n", input_options.lenTrim);
            printf("Number of mismatches for the trimmed reads: %i\n", input_options.numMismatchForTrim);
      }
      printf("Number of CPU threads: %u\n", ini_params.Ini_NumOfCpuThreads);
      
      if (input_options.insert_high > 0)
            printf("Upper bound of insert size: %u\n", input_options.insert_high);
      if (input_options.insert_low > 0)
            printf("Lower bound of insert size: %u\n", input_options.insert_low);
      printf("--------------------------------------\n\n");
#endif
}


void processIndexFileName(IndexFileNames& index, char* indexName, IniParams ini_params) {
      int indexNameLen = strlen(indexName);
      index.iniFileName = (char*)malloc(indexNameLen+5);
      index.bwtCodeFileName = (char*)malloc(indexNameLen+5);
      index.occValueFileName = (char*)malloc(indexNameLen+5);
      index.gpuOccValueFileName = (char*)malloc(indexNameLen+25);
      index.lookupTableFileName = (char*)malloc(indexNameLen+5);
      index.revBwtCodeFileName = (char*)malloc(indexNameLen+15);
      index.revOccValueFileName = (char*)malloc(indexNameLen+15);
      index.revGpuOccValueFileName = (char*)malloc(indexNameLen+35);
      index.revLookupTableFileName = (char*)malloc(indexNameLen+15);
      index.saCodeFileName = (char*)malloc(indexNameLen+5);
      index.memControlFileName = (char*)malloc(indexNameLen+5);
      
      //For HSP
      index.packedDnaFileName = (char*)malloc(indexNameLen+5);
      index.annotationFileName = (char*)malloc(indexNameLen+5);
      index.ambiguityFileName = (char*)malloc(indexNameLen+5);
      index.translateFileName = (char*)malloc(indexNameLen+5);

      strcpy(index.bwtCodeFileName, indexName);
      strcpy(index.bwtCodeFileName + indexNameLen, ".bwt");
      strcpy(index.occValueFileName, indexName);
      strcpy(index.occValueFileName + indexNameLen, ".fmv");
      strcpy(index.lookupTableFileName, indexName);
      strcpy(index.lookupTableFileName + indexNameLen, ".lkt");
      strcpy(index.gpuOccValueFileName, indexName);
      strcpy(index.gpuOccValueFileName + indexNameLen, ".fmv.gpu");
      
      strcpy(index.revBwtCodeFileName, indexName);
      strcpy(index.revBwtCodeFileName + indexNameLen, ".rev.bwt");
      strcpy(index.revOccValueFileName, indexName);
      strcpy(index.revOccValueFileName + indexNameLen, ".rev.fmv");
      strcpy(index.revLookupTableFileName, indexName);
      strcpy(index.revLookupTableFileName + indexNameLen, ".rev.lkt");
      strcpy(index.revGpuOccValueFileName, indexName);
      strcpy(index.revGpuOccValueFileName + indexNameLen, ".rev.fmv.gpu");
      
      strcpy(index.saCodeFileName, indexName);
      strcpy(index.saCodeFileName + indexNameLen, ini_params.Ini_SaValueFileExt);
      strcpy(index.packedDnaFileName, indexName);
      strcpy(index.packedDnaFileName + indexNameLen, ".pac");
      strcpy(index.annotationFileName, indexName);
      strcpy(index.annotationFileName + indexNameLen, ".ann");
      strcpy(index.ambiguityFileName, indexName);
      strcpy(index.ambiguityFileName + indexNameLen, ".amb");
      strcpy(index.translateFileName, indexName);
      strcpy(index.translateFileName + indexNameLen, ".tra");
      
      
#ifdef BGS_OUTPUT_FILENAMES_TO_SCREEN
      printf("[Main] Using BWT index ....                %s\n",bwtCodeFileName);
      printf("[Main] Using Occ index ....                %s\n",occValueFileName);
      printf("[Main] Using Lookup index ....             %s\n",lookupTableFileName);
      printf("[Main] Using Gpu-Occ index ....            %s\n",gpuOccValueFileName);
      
      printf("[Main] Using Rev-BWT index ....            %s\n",revBwtCodeFileName);
      printf("[Main] Using Rev-Occ index ....            %s\n",revOccValueFileName);
      printf("[Main] Using Rev-Lookup index ....         %s\n",revLookupTableFileName);
      printf("[Main] Using Rev-Gpu-Occ index ....        %s\n",revGpuOccValueFileName);
      
      printf("[Main] Using SA index ....                 %s\n",saCodeFileName);
      printf("[Main] Using PackedDna index ....          %s\n",packedDnaFileName);
      printf("[Main] Using Annotation index ....         %s\n",annotationFileName);
      printf("[Main] Using Ambiguity index ....          %s\n",ambiguityFileName);
      printf("[Main] Using Translation index ....        %s\n",translateFileName);
#endif
}


void loadIndex(IndexFileNames index, BWT **bwt, BWT **revBwt, HSP **hsp, uint **lkt, uint **revLkt,
               uint **revOccValue, uint **occValue, uint &numOfOccValue, uint &lookupWordSize) {
      (*bwt) = BWTLoad(index.bwtCodeFileName, index.occValueFileName, index.saCodeFileName, NULL, NULL, NULL);
      free(index.bwtCodeFileName);
      free(index.occValueFileName);
      free(index.saCodeFileName);
      
      (*revBwt) = BWTLoad(index.revBwtCodeFileName, index.revOccValueFileName, NULL, NULL, NULL, NULL);
      free(index.revBwtCodeFileName);
      free(index.revOccValueFileName);
      
      (*hsp) = HSPLoad(index.packedDnaFileName, index.annotationFileName, index.ambiguityFileName, index.translateFileName, 1);
      free(index.packedDnaFileName);
      free(index.annotationFileName);
      free(index.ambiguityFileName);
      free(index.translateFileName);
      
      numOfOccValue = ((*bwt)->textLength + GPU_OCC_INTERVAL - 1) / GPU_OCC_INTERVAL + 1; // Value at both end for bi-directional encoding
      lookupWordSize = 1 << (LOOKUP_SIZE * 2);
      
      FILE *gpuOccValueFile, *revGpuOccValueFile, *lookupTableFile, *revLookupTableFile;
      // GPU OCC TABLE
      gpuOccValueFile = (FILE*)fopen(index.gpuOccValueFileName, "rb");
      if (gpuOccValueFile==NULL) { fprintf(stderr,"Cannot open gpuOccValueFile %s\n",index.gpuOccValueFileName); exit(1);} 
      free(index.gpuOccValueFileName);
      // consume useless parts of OCC file
      unsigned int tmp;
      fread(&tmp, sizeof(unsigned int), 1, gpuOccValueFile);
      // read cumulative frequency
      for (uint i=1; i<=ALPHABET_SIZE; i++)
            fread(&tmp, sizeof(unsigned int), 1, gpuOccValueFile);
      // read OCC from file. copied from BWT.c
      (*occValue) = (unsigned int*) malloc(numOfOccValue * ALPHABET_SIZE * sizeof(unsigned int));
      fread((*occValue), sizeof(unsigned int), numOfOccValue * ALPHABET_SIZE, gpuOccValueFile);
      fclose(gpuOccValueFile);
      
      // LOOK UP TABLE
      lookupTableFile = (FILE*)fopen(index.lookupTableFileName, "rb");
      if (lookupTableFile==NULL) { fprintf(stderr,"Cannot open lookupTableFile %s\n",index.lookupTableFileName); exit(1);}
      free(index.lookupTableFileName);
      // read lookup table
      (*lkt) = (unsigned int*) malloc(sizeof(unsigned int) * lookupWordSize);
      for (uint i = 0; i < lookupWordSize; i += LOOKUP_LOAD_STEP) {
            fread((*lkt) + i, LOOKUP_LOAD_STEP * sizeof(unsigned int), 1, lookupTableFile);
      }
      fclose(lookupTableFile);
      
      /////////////////// READ REVERSE ////////////////////////////////////
      
      // GPU OCC TABLE
      revGpuOccValueFile = (FILE*)fopen(index.revGpuOccValueFileName, "rb");
      if (revGpuOccValueFile==NULL) { fprintf(stderr,"Cannot open revGpuOccValueFile\n"); exit(1);} 
      free(index.revGpuOccValueFileName);
      // consume useless parts of OCC file
      fread(&tmp, sizeof(unsigned int), 1, revGpuOccValueFile);
      // read cumulative frequency
      for (uint i=1; i<=ALPHABET_SIZE; i++)
            fread(&tmp, sizeof(unsigned int), 1, revGpuOccValueFile);
      // read OCC from file. copied from BWT.c
      (*revOccValue) = (uint*) malloc(numOfOccValue * ALPHABET_SIZE * sizeof(uint));
      fread((*revOccValue), sizeof(uint), numOfOccValue * ALPHABET_SIZE, revGpuOccValueFile);
      fclose(revGpuOccValueFile);
      
      // LOOK UP TABLE
      revLookupTableFile = (FILE*)fopen(index.revLookupTableFileName, "rb");
      if (revLookupTableFile==NULL) { fprintf(stderr,"Cannot open revLookupTableFile\n"); exit(1);}
      free(index.revLookupTableFileName);
      // read lookup table
      (*revLkt) = (uint*) malloc(sizeof(uint) * lookupWordSize);
      for (uint i = 0; i < lookupWordSize; i += LOOKUP_LOAD_STEP) {
            fread((*revLkt) + i, LOOKUP_LOAD_STEP * sizeof(uint), 1, revLookupTableFile);
      }
      fclose(revLookupTableFile);

      // SHOW THE MEMORY USAGE ON HOST
#ifdef BGS_OUTPUT_MEMORY_USAGE
      
      // for BWT
      printf("BWT code size    : %lu bytes (%9.4f M)\n", (*bwt)->bwtSizeInWord * sizeof(unsigned int), (float) (*bwt)->bwtSizeInWord * sizeof(unsigned int)/1024.0/1024.0);
      printf("Occ value size   : %lu bytes (%9.4f M)\n", ((*bwt)->occSizeInWord + (*bwt)->occMajorSizeInWord) * sizeof(unsigned int), (float) ((*bwt)->occSizeInWord + (*bwt)->occMajorSizeInWord) * sizeof(unsigned int)/1024.0/1024.0);
      if ((*bwt)->saValueSizeInWord > 0) {
            printf("SA value size    : %lu bytes (%9.4f M)\n", (*bwt)->saValueSizeInWord * sizeof(unsigned int), (float) (*bwt)->saValueSizeInWord * sizeof(unsigned int)/1024.0/1024.0);
      }
      if ((*bwt)->inverseSaSizeInWord > 0) {
            printf("Inverse SA size  : %lu bytes (%9.4f M)\n", (*bwt)->inverseSaSizeInWord * sizeof(unsigned int), (float) (*bwt)->inverseSaSizeInWord * sizeof(unsigned int)/1024.0/1024.0);
      }
      if ((*bwt)->cachedSaIndex > 0) {
            printf("SA index range  : %lu bytes (%9.4f M)\n", (*bwt)->cachedSaIndexSizeInWord * sizeof(unsigned int), (float) (*bwt)->cachedSaIndexSizeInWord * sizeof(unsigned int)/1024.0/1024.0);
      }
      
      
      // for REVBWT
      printf("Rev BWT code size    : %lu bytes (%9.4f M)\n", (*revBwt)->bwtSizeInWord * sizeof(unsigned int), (float) (*revBwt)->bwtSizeInWord * sizeof(unsigned int)/1024.0/1024.0);
      printf("Rev Occ value size   : %lu bytes (%9.4f M)\n", ((*revBwt)->occSizeInWord + (*revBwt)->occMajorSizeInWord) * sizeof(unsigned int), (float) ((*revBwt)->occSizeInWord + (*revBwt)->occMajorSizeInWord) * sizeof(unsigned int)/1024.0/1024.0);
      if ((*revBwt)->saValueSizeInWord > 0) {
            printf("Rev SA value size    : %lu bytes (%9.4f M)\n", (*revBwt)->saValueSizeInWord * sizeof(unsigned int), (float) (*revBwt)->saValueSizeInWord * sizeof(unsigned int)/1024.0/1024.0);
      }
      if ((*revBwt)->inverseSaSizeInWord > 0) {
            printf("Rev Inverse SA size  : %lu bytes (%9.4f M)\n", (*revBwt)->inverseSaSizeInWord * sizeof(unsigned int), (float) (*revBwt)->inverseSaSizeInWord * sizeof(unsigned int)/1024.0/1024.0);
      }
      if ((*revBwt)->cachedSaIndex > 0) {
            printf("Rev SA index range  : %lu bytes (%9.4f M)\n", (*revBwt)->cachedSaIndexSizeInWord * sizeof(unsigned int), (float) (*revBwt)->cachedSaIndexSizeInWord * sizeof(unsigned int)/1024.0/1024.0);
      }
      
      
      // for HSP
      uint trailerBufferInWord = 1;
      uint trailerBufferIn128 = (trailerBufferInWord + 3) / 4 * 4;
      uint textLength = (*hsp)->dnaLength;
      
      // hsp->packedDNA
      uint hsp_mem_usage = ((textLength + 64 - 1) / 64 * 4 + trailerBufferIn128 * 4) * sizeof(unsigned int);
      // hsp->seqOffset
      hsp_mem_usage += ((*hsp)->numOfSeq+1) * sizeof(SeqOffset);
      // hsp->annotation
      hsp_mem_usage += ((*hsp)->numOfSeq+1) * sizeof(Annotation);
      // hsp->ambiguity
      hsp_mem_usage += ((*hsp)->numOfAmbiguity+2) * sizeof(Ambiguity);
      printf("[Memory usage in Host] hsp: %u bytes (%9.4f M)\n", hsp_mem_usage, (float) hsp_mem_usage / 1024.0/1024.0);
      
      printf("[Memory usage in Host] occValue: %i bytes (%9.4f M)\n", numOfOccValue * ALPHABET_SIZE * sizeof(unsigned int), (float) numOfOccValue * ALPHABET_SIZE * sizeof(unsigned int)/1024.0/1024.0);
      printf("[Memory usage in Host] lkt: %i bytes (%9.4f M)\n", sizeof(unsigned int) * lookupWordSize, (float) sizeof(unsigned int) * lookupWordSize/1024.0/1024.0);
      printf("[Memory usage in Host] revOccValue: %i bytes (%9.4f M)\n", numOfOccValue * ALPHABET_SIZE * sizeof(uint), (float) numOfOccValue * ALPHABET_SIZE * sizeof(uint)/1024.0/1024.0);
      printf("[Memory usage in Host] revLkt: %i bytes (%9.4f M)\n", sizeof(uint) * lookupWordSize, (float) sizeof(uint) * lookupWordSize/1024.0/1024.0);
      
      printf("[Memory usage in Host] occ for all cpu threads: %i bytes (%9.4f M)\n", sizeof(OCC)*ini_params.Ini_NumOfCpuThreads, (float) sizeof(OCC)*ini_params.Ini_NumOfCpuThreads/1024.0/1024.0);
      
#endif
      

}


// set the multi-threading arguments
void setHostKernelArguments(HostKernelArguements* hostKernelArguments, pthread_t* threads, IniParams ini_params, BWT *bwt, HSP *hsp,
                            uint *lkt, uint *revLkt, int* dummyQuality, uint maxReadLength, uint word_per_ans,
                            InputOptions * input_options) {

                            
      char readType = input_options->readType;
      int insert_high = input_options->insert_high;
      int insert_low = input_options->insert_low;
      int alignmentType = input_options->alignmentType; // for CPU Alignment
      int reportType = input_options->alignmentType; // for reporting the alignments
      int outputFormat = input_options->outputFormat;
      
      // Convert to CPU Kernel data
      // This shouldn't have been needed...
      if (readType==SINGLE_READ) {
            // for single-end alignment
            if (alignmentType == OUTPUT_ALL_BEST) {
              alignmentType = REPORT_ALL_BEST;
            } else if (alignmentType == OUTPUT_UNIQUE_BEST) {
              alignmentType = REPORT_UNIQUE_BEST;
            } else if (alignmentType == OUTPUT_RANDOM_BEST) {
              alignmentType = REPORT_RANDOM_BEST;
            } else {
              alignmentType = REPORT_ALL;
            }
      } else {
            // for paired-end alignment, the alignment type always is REPORT_ALL
            alignmentType = REPORT_ALL;
      }
      
      // MULTI-THREADING arguments
      for (uint threadId=0;threadId<ini_params.Ini_NumOfCpuThreads;++threadId) {
            threads[threadId]=0;
            
            hostKernelArguments[threadId].occ = OCCConstruct();
            for (uint i = 0 ; i <= MAX_NUM_OF_MISMATCH ; i++){
                  hostKernelArguments[threadId].SRAAccumulatedResultCount.WithError[i]=0;
            }
            hostKernelArguments[threadId].SRAQuerySet.bwt = bwt;
            hostKernelArguments[threadId].SRAQuerySet.hsp = hsp;
            hostKernelArguments[threadId].SRAQuerySet.highOcc = NULL;
            hostKernelArguments[threadId].SRAQuerySet.occ = hostKernelArguments[threadId].occ;
            hostKernelArguments[threadId].SRAQuerySet.lookupTable = lkt;
            hostKernelArguments[threadId].SRAQuerySet.rev_lookupTable = revLkt;
            hostKernelArguments[threadId].SRAQuerySet.OutFilePtr = NULL;
            hostKernelArguments[threadId].SRAQuerySet.OutFileName = NULL;
            hostKernelArguments[threadId].SRAQuerySet.OutFilePart = 0;
            hostKernelArguments[threadId].SRAQuerySet.OutFileFormat = outputFormat;
            hostKernelArguments[threadId].SRAQuerySet.MaxOutputPerRead = ini_params.Ini_MaxOutputPerRead;
            hostKernelArguments[threadId].SRAQuerySet.writtenCacheCount = 0;
            hostKernelArguments[threadId].SRAQuerySet.ReadStrand=QUERY_BOTH_STRAND;
            hostKernelArguments[threadId].SRAQuerySet.ErrorType=SRA_STEP_ERROR_TYPE_MISMATCH_ONLY;
            hostKernelArguments[threadId].SRAQuerySet.OutputType=alignmentType;
            hostKernelArguments[threadId].SRAQuerySet.SAMOutFilePtr = NULL;
            
            hostKernelArguments[threadId].SRAAccumulatedResultCount.TotalOccurrences = 0;
            hostKernelArguments[threadId].SRAAccumulatedResultCount.RetrievedBySa = 0;
            hostKernelArguments[threadId].SRAAccumulatedResultCount.RetrievedByCe = 0;
            hostKernelArguments[threadId].SRAAccumulatedResultCount.RetrievedByHocc = 0;
            
            hostKernelArguments[threadId].batchFirstReadId = 0;
            hostKernelArguments[threadId].skipFirst = 0;
            hostKernelArguments[threadId].numQueries = 0;
            
            hostKernelArguments[threadId].SRAQueryInputPos.QuerySetting=&(hostKernelArguments[threadId].SRAQuerySet);
            hostKernelArguments[threadId].SRAQueryInputPos.QueryOutput=&(hostKernelArguments[threadId].SRAAccumulatedResultCount);
            hostKernelArguments[threadId].SRAQueryInputPos.ReadQuality=dummyQuality;
            hostKernelArguments[threadId].SRAQueryInputPos.ReadStrand = QUERY_POS_STRAND;
            
            hostKernelArguments[threadId].SRAQueryInputNeg.QuerySetting=&(hostKernelArguments[threadId].SRAQuerySet);
            hostKernelArguments[threadId].SRAQueryInputNeg.QueryOutput=&(hostKernelArguments[threadId].SRAAccumulatedResultCount);
            hostKernelArguments[threadId].SRAQueryInputNeg.ReadQuality=dummyQuality;
            hostKernelArguments[threadId].SRAQueryInputNeg.ReadStrand = QUERY_NEG_STRAND;
            
            hostKernelArguments[threadId].answers = NULL;
            hostKernelArguments[threadId].badReadIndices = NULL;
            hostKernelArguments[threadId].badAnswers = NULL;
            hostKernelArguments[threadId].badStartOffset = NULL;
            hostKernelArguments[threadId].badCountOffset = NULL;
            hostKernelArguments[threadId].alignedOcc = 0;
            hostKernelArguments[threadId].alignedReads = 0;
            hostKernelArguments[threadId].threadId = threadId;
            hostKernelArguments[threadId].maxReadLength =maxReadLength;
            hostKernelArguments[threadId].word_per_ans = word_per_ans;
            
            hostKernelArguments[threadId].readType = readType;
            hostKernelArguments[threadId].reportType = reportType;
            hostKernelArguments[threadId].insert_low = insert_low;
            hostKernelArguments[threadId].insert_high = insert_high;
            hostKernelArguments[threadId].peStrandLeftLeg = ini_params.Ini_PEStrandLeftLeg;
            hostKernelArguments[threadId].peStrandRightLeg = ini_params.Ini_PEStrandRightLeg;
            
            hostKernelArguments[threadId].peMaxOutputPerPair = ini_params.Ini_PEMaxOutputPerPair;
      }
}


// obtain the number of cases for this number of mismatch
// and obtain the number of SA ranges allowed
void getParametersForThisMismatch(uint numMismatch, uint& numCases, uint& sa_range_allowed_1,
                                  uint& sa_range_allowed_2, char& skip_round_2, 
                                  uint& word_per_ans, uint& word_per_ans_2) {
      switch(numMismatch) {
            case 0:
                  numCases = NUM_CASES_0M;
                  sa_range_allowed_1 = MAX_SA_RANGES_ALLOWED1_0;
                  sa_range_allowed_2 = MAX_SA_RANGES_ALLOWED2_0;
                  skip_round_2 = SKIP_ROUND2_0;
                  break;
            case 1:
                  numCases = NUM_CASES_1M;
                  sa_range_allowed_1 = MAX_SA_RANGES_ALLOWED1_1;
                  sa_range_allowed_2 = MAX_SA_RANGES_ALLOWED2_1;
                  skip_round_2 = SKIP_ROUND2_1;
                  break;
            case 2:
                  numCases = NUM_CASES_2M;
                  sa_range_allowed_1 = MAX_SA_RANGES_ALLOWED1_2;
                  sa_range_allowed_2 = MAX_SA_RANGES_ALLOWED2_2;
                  skip_round_2 = SKIP_ROUND2_2;
                  break;
            case 3:
                  numCases = NUM_CASES_3M;
                  sa_range_allowed_1 = MAX_SA_RANGES_ALLOWED1_3;
                  sa_range_allowed_2 = MAX_SA_RANGES_ALLOWED2_3;
                  skip_round_2 = SKIP_ROUND2_3;
                  break;
            case 4:
                  numCases = NUM_CASES_4M;
                  sa_range_allowed_1 = MAX_SA_RANGES_ALLOWED1_4;
                  sa_range_allowed_2 = MAX_SA_RANGES_ALLOWED2_4;
                  skip_round_2 = SKIP_ROUND2_4;
                  break;
            default:
                  numCases = MAX_NUM_CASES;
                  sa_range_allowed_1 = 4;
                  sa_range_allowed_2 = 1024;
                  skip_round_2 = 0;
                  break;
      }
      word_per_ans = sa_range_allowed_1*2;
      word_per_ans_2 = sa_range_allowed_2*2;
}


// show the merging file command at the end of the program
void show_merge_file_command(InputOptions input_options, char* queryFileName, char* queryFileName2) {
      printf("\n");
      printf("Please type the following command to merge the alignment results into one file:\n");
      if (input_options.outputFormat==SRA_OUTPUT_FORMAT_DEFAULT) {
            // binary output format
            if (input_options.readType == SINGLE_READ) {
                  // single reads
                  printf("./make_view_binary.sh single %s\n",queryFileName);
            } else {
                  // paired-end reads 
                  printf("./make_view_binary.sh pair %s %s\n",queryFileName,queryFileName2);
            }
      } else if (input_options.outputFormat==SRA_OUTPUT_FORMAT_PLAIN) {
            // plain output format
            printf("./make_view_simple.sh %s\n", queryFileName);
      } else {
            // SAM output format
            printf("./make_view_sam.sh %s\n", queryFileName);
      }

}


//Function hostKernel : core function of output report and host alignment.
//
//batchFirstReadId: is the readId of the first read in the batch 
//                  w.r.t to the read input file. (same among all threads)
//skipFirst       : (Multi-threading) define the number of reads in the batch to skip for 1st batch-run.
//answers         : is the answer arrays used by GPU to store alignment result of the 1st batch-run.
//badAnswers      : is the answer arrays used by GPU to store alignment result of the 2st batch-run.
//badReadIndices  : is the array of the read id w.r.t to the batch.

inline uint hostKernel(unsigned char * upkdQueries, char * upkdQueryNames, unsigned int batchFirstReadId, unsigned int skipFirst, unsigned int numQueries,
                       SRAQueryInput * SRAQueryInputPos, SRAQueryInput * SRAQueryInputNeg, SRAModel ** SRAMismatchModel,
                       char * outputFileName, 
                       unsigned int ** answers,
                       unsigned int * alignedOcc, unsigned int * alignedReads,
                       unsigned int ** badReadIndices, unsigned int ** badAnswers,
                       unsigned int * badStartOffset, unsigned int * badCountOffset,
                       unsigned int maxReadLength, unsigned int * readLengths, unsigned int numCases, 
                       unsigned int sa_range_allowed_1, unsigned int sa_range_allowed_2,
                       unsigned int * readIDs, char skip_round_2, unsigned int accumReadNum,
                       unsigned int word_per_ans, char readType, char maxNumMismatch,
                       int insert_low, int insert_high, 
                       int peStrandLeftLeg, int peStrandRightLeg, 
                       int threadID, unsigned int * unAlignedIDs, unsigned int * unAlignedOcc,
                       char reportType, int outputUnpairReads, int peMaxOutputPerPair, uint8_t isTerminalCase) {
                       
      uint numOfAnswer = 0;
      uint pre_numOfAnswer = 0;
      uint sumList = 0;
      uint numOfAlignedRead = 0;
      uint numOfUnAligned = 0;
      unsigned char oStrandQuery[MAX_READ_LENGTH];
      uint badCount;
      
      uint *ans;
      unsigned int l,r;
      int strand;
      char num_mis;
      
      unsigned char * thisQuery = NULL;
      char * thisQueryName = NULL;
      unsigned char * preQuery = NULL;
      int i,j,k;
      ullint readId = 0;
      ullint preReadId = 0;
      ullint batchReadId;
      int readLength = MAX_READ_LENGTH;
      int preReadLength = MAX_READ_LENGTH;
      char * preQueryName = NULL;
      
      // for collecting the SA ranges from GPU alignment
      SAList* sa_list1 = SAListConstruct();
      SAList* sa_list2 = SAListConstruct();
      SAList* curr_sa_list;
      // for collecting the SA ranges from CPU alignment
      SAList* sa_list_cpu1 = SAListConstruct();
      SAList* sa_list_cpu2 = SAListConstruct();
      SAList* curr_sa_list_cpu;
      
      OCCList* occ_list1 = OCCListConstruct();
      OCCList* occ_list2 = OCCListConstruct();
      OCCList* curr_occ_list;
      PEOutput* pe_out = PEOutputConstruct();
      SRAQuerySetting * qSetting = SRAQueryInputPos->QuerySetting;
    SRAQueryResultCount * rOutput = SRAQueryInputPos->QueryOutput;
      BWT * bwt = qSetting->bwt;
      HSP * hsp = qSetting->hsp;
      OCC * occ = qSetting->occ;
      PEInput* pe_in = PEInputConstruct(bwt, hsp);
      
      #ifdef BGS_ROUND_BREAKDOWN_TIME
      double start_time = setStartTime();
      #endif
      
      int word_per_ans_2 = sa_range_allowed_2*2;
      
      
      int outputFormat = qSetting->OutFileFormat;
      FILE * outputFile = NULL;
      
      //Output file for SAM output is handled by SAM API
      //=> OutFilePtr is not used for SAM API output format.
      switch (outputFormat) {
        case SRA_OUTPUT_FORMAT_SAM_API:
            break;
        default:
            outputFile = (FILE*)fopen(outputFileName, "a");
      }
      
      qSetting->OutFilePtr = outputFile;
      qSetting->OutFileName = outputFileName;
      
      pe_in->insertLbound = insert_low;
      pe_in->insertUbound = insert_high;
      pe_in->strandLeftLeg = peStrandLeftLeg;
      pe_in->strandRightLeg = peStrandRightLeg;
      unsigned int numOfPairEndAlignment = 0;

      // printf("[hostkernel] numCases = %u; numQueries = %u; accumReadNum = %u; skip_round_2 = %i\n", numCases, numQueries, accumReadNum, (int) skip_round_2);
      // printf("[hostkernel] sa_range_allowed_1 = %u; sa_range_allowed_2 = %u; word_per_ans = %u; word_per_ans_2 = %i\n", sa_range_allowed_1, sa_range_allowed_2, word_per_ans, word_per_ans_2);
      // printf("[hostkernel] batchFirstReadId = %u; skipFirst = %u; numQueries = %u\n", batchFirstReadId, skipFirst, numQueries);
      
      uint* badReadPos = (uint *) malloc(numCases * sizeof(uint));
      for (j=0; j<numCases; j++)
            badReadPos[j]=0;
      
      for (j=0;j<numQueries;++j) {
            rOutput->TotalOccurrences = 0;
            numOfPairEndAlignment = 0;
            // OBTAIN THE ALIGNMENT RESULT
            batchReadId = skipFirst+j;
            preReadId = readId;
            readId = readIDs[batchFirstReadId+skipFirst+j];
            preQuery = thisQuery;
            thisQuery = upkdQueries + maxReadLength * (readId-1);
            preQueryName = thisQueryName;
            thisQueryName = upkdQueryNames + MAX_READ_NAME_LENGTH * (readId-1);
            preReadLength = readLength;
            readLength = readLengths[batchFirstReadId + batchReadId];
            pre_numOfAnswer = numOfAnswer;
            
            SRAQueryInputPos->ReadName=thisQueryName;
            SRAQueryInputNeg->ReadName=thisQueryName;
            if ((readType == PAIR_END_READ) && (outputFormat == SRA_OUTPUT_FORMAT_PLAIN)) {
                  SRAQueryInputPos->ReadId=(unsigned long long)(readId+accumReadNum+1)/2;
                  SRAQueryInputNeg->ReadId=(unsigned long long)(readId+accumReadNum+1)/2;
            } else {
                  SRAQueryInputPos->ReadId=(unsigned long long)readId+accumReadNum;
                  SRAQueryInputNeg->ReadId=(unsigned long long)readId+accumReadNum;
            }

            
            SRAQueryInputPos->ReadCode=thisQuery;
            SRAQueryInputPos->ReadLength=readLength;
            SRAQueryInputNeg->ReadLength=readLength;
            
            if (readType == SINGLE_READ) {
                  SRAQueryInputPos->ReadStrand = QUERY_POS_STRAND;
                  OCCReportDelimitor(SRAQueryInputPos);
            }
            
            if (readType == PAIR_END_READ) {
                  if (j%2==0) {
                        SAListReset(sa_list1);
                        SAListReset(sa_list2);
                        curr_sa_list = sa_list1;
                        SAListReset(sa_list_cpu1);
                        SAListReset(sa_list_cpu2);
                        curr_sa_list_cpu = sa_list_cpu1;
                        OCCListReset(occ_list1);
                        OCCListReset(occ_list2);
                        curr_occ_list = occ_list1;
                  } else {
                        curr_sa_list = sa_list2;
                        curr_sa_list_cpu = sa_list_cpu2;
                        curr_occ_list = occ_list2;
                  }
            }
            

            int bestHitCount = 0;
            unsigned int bestHitSAIndex = 0;
            unsigned int bestHitStrand  = 0;
            unsigned int bestHitNumOfMismatch  = 0;
            for (int whichCase=0;whichCase<numCases;whichCase++) {
                  ans = answers[whichCase] + ((batchReadId) / 32 * 32 * word_per_ans + (batchReadId) % 32);
                  if (qSetting->OutputType == REPORT_UNIQUE_BEST) {
                      if (*(ans) < 0xFFFFFFFD) {
                          l = *(ans);
                          r = l + (*(ans+32)&BGS_GPU_ANSWER_OFFSET_MASK);
                          if (l<=r && r<=bwt->textLength) {
                              if (bestHitCount==0) {
                                    bestHitSAIndex = r;
                                    bestHitStrand = ((*(ans+32)>>(BGS_GPU_ANSWER_OFFSET_LENGTH+3)) & 1 )+ 1;
                                    bestHitNumOfMismatch = ((*(ans+32)>>(BGS_GPU_ANSWER_OFFSET_LENGTH)) & 7 );
                              }
                              bestHitCount+=r-l+1;
                          }
                      } else if (*(ans) > 0xFFFFFFFD) {
                            bestHitCount+=2; // TO make sure the buffer exploded.
                      }
                  } else if (qSetting->OutputType == REPORT_RANDOM_BEST) {
                      if (*(ans) < 0xFFFFFFFD) {
                          //If there are only 1 or 2 results,
                          // both the first and second reported SA ranges
                          // must be valid.
                          l = *(ans);
                          r = l + (*(ans+32)&BGS_GPU_ANSWER_OFFSET_MASK);
                          if ( r<=bwt->textLength) {
                              if (bestHitCount==0) {
                                    bestHitSAIndex = r;
                                    bestHitStrand = ((*(ans+32)>>(BGS_GPU_ANSWER_OFFSET_LENGTH+3)) & 1 )+ 1;
                                    bestHitNumOfMismatch = ((*(ans+32)>>(BGS_GPU_ANSWER_OFFSET_LENGTH)) & 7 );
                              }
                              bestHitCount++;
                          }
                      } else if (*(ans) == 0xFFFFFFFE) {
                          //If there are too many results,
                          // the second reported SA range must be valid.
                          ans+=32;ans+=32;
                          
                          l = *(ans);
                          if ( l<=bwt->textLength) {
                              if (bestHitCount==0) {
                                    bestHitSAIndex = l;
                                    bestHitStrand = ((*(ans+32)>>(BGS_GPU_ANSWER_OFFSET_LENGTH+3)) & 1 )+ 1;
                                    bestHitNumOfMismatch = ((*(ans+32)>>(BGS_GPU_ANSWER_OFFSET_LENGTH)) & 7 );
                              }
                              bestHitCount++;
                          }
                      }
                  } else {
                      if (*(ans) < 0xFFFFFFFD) {
                            //Number of SA ranges reported within MAX_SA_RANGES_ALLOWED
                            for (i=0;i<sa_range_allowed_1;i++) {
                                  if ((*(ans+(i*2)*32)) >= 0xFFFFFFFD || (*(ans+(i*2+1)*32)) >= 0xFFFFFFFD) {
                                        break;
                                  }
                                  l = *(ans+(i*2)*32);
                                  r = l + (*(ans+(i*2+1)*32)&BGS_GPU_ANSWER_OFFSET_MASK);
                                  strand = (((*(ans+(i*2+1)*32)>>(BGS_GPU_ANSWER_OFFSET_LENGTH+3)) & 1)) + 1;
                                  num_mis = (((*(ans+(i*2+1)*32)>>(BGS_GPU_ANSWER_OFFSET_LENGTH)) & 7));
                                  // printf("[inside] ReadID: %u; SA range: [%u - %u]; \n", readId, l, r);
                                  if ( l <= r && r<=bwt->textLength) { // assume unique hits
                                        if (rOutput->TotalOccurrences<qSetting->MaxOutputPerRead) {
                                            if ((rOutput->TotalOccurrences+(r-l+1))>qSetting->MaxOutputPerRead) {
                                                r = l + qSetting->MaxOutputPerRead - rOutput->TotalOccurrences - 1;
                                            }
                                        }
                                        if (readType == SINGLE_READ) {
                                            SRAQueryInputPos->ReadStrand = strand;
                                            OCCReportSARange(SRAQueryInputPos,l,r,(int) num_mis);
                                            numOfAnswer += r - l + 1;
                                        } else {
                                              // [attention]: needs to update the corresponding strand and number of mismatches
                                              addSAToSAList(curr_sa_list, l, r, strand, num_mis);
                                        }
                                        
                                        if (rOutput->TotalOccurrences>=qSetting->MaxOutputPerRead) { break; }
                                  } else {
                                        break;
                                  }
                            }
                      } else if ((skip_round_2==1) && (*(ans) > 0xFFFFFFFD)) {
                            // for the case when (1) round 2 is skipped and (2) it is a bad read
                            // this read will be handled by CPU
                            
                            #ifndef BGS_DISABLE_NEGATIVE_STRAND
                                  for (i=0;i<readLength;i++) {
                                        oStrandQuery[i] = 3-thisQuery[readLength-i-1];
                                  }
                                  SRAQueryInputNeg->ReadCode=oStrandQuery;
                                  SRAQueryInputPos->ReadStrand = QUERY_POS_STRAND;
                                  SRAQueryInputNeg->ReadStrand = QUERY_NEG_STRAND;
                                  //Reversing the read from positive strand to negative strand.
                                  if (readType == SINGLE_READ) {
                                        numOfAnswer += ProcessReadDoubleStrand(SRAQueryInputPos, SRAQueryInputNeg, SRAMismatchModel[readLength], whichCase);
                                  } else {
                                        ProcessReadDoubleStrand2(SRAQueryInputPos, SRAQueryInputNeg, SRAMismatchModel[readLength], whichCase, curr_sa_list_cpu, curr_occ_list);
                                  }
                            #endif
                            
                            #ifdef BGS_DISABLE_NEGATIVE_STRAND
                                  if (readType == SINGLE_READ) {
                                        numOfAnswer += ProcessReadSingleStrand(SRAQueryInputPos, SRAMismatchModel[readLength], whichCase);
                                  } else {
                                        ProcessReadSingleStrand2(SRAQueryInputPos, SRAMismatchModel[readLength], whichCase, curr_sa_list_cpu, curr_occ_list);
                                  }
                            #endif
                            
                            // numReadsHandledByCPU1++;
                      } else if ((skip_round_2==0) && (*(ans) > 0xFFFFFFFD)) {
                            // check the second round answer
                            while  (badReadPos[whichCase] < badCountOffset[whichCase] && 
                                    (badReadIndices[whichCase][badReadPos[whichCase]+badStartOffset[whichCase]] == batchReadId)) {

                                    ullint pos = badReadPos[whichCase];
                                    
                                    ans = badAnswers[whichCase] + ((pos+badStartOffset[whichCase]) / 32 * 32 * word_per_ans_2 + (pos+badStartOffset[whichCase]) % 32);
                                    if (*(ans) > 0xFFFFFFFD) {
                                        // super bad read

                                        #ifndef BGS_DISABLE_NEGATIVE_STRAND
                                              //Reversing the read from positive strand to negative strand.
                                              for (i=0;i<readLength;i++) {
                                                    oStrandQuery[i] = 3-thisQuery[readLength-i-1];
                                              }
                                              SRAQueryInputNeg->ReadCode=oStrandQuery;
                                              SRAQueryInputPos->ReadStrand = QUERY_POS_STRAND;
                                              SRAQueryInputNeg->ReadStrand = QUERY_NEG_STRAND;

                                                if (readType == SINGLE_READ) {
                                                      numOfAnswer += ProcessReadDoubleStrand(SRAQueryInputPos, SRAQueryInputNeg, SRAMismatchModel[readLength], whichCase);
                                                } else {
                                                      ProcessReadDoubleStrand2(SRAQueryInputPos, SRAQueryInputNeg, SRAMismatchModel[readLength], whichCase, curr_sa_list_cpu, curr_occ_list);
                                                }
                                        #endif

                                        #ifdef BGS_DISABLE_NEGATIVE_STRAND
                                              if (readType == SINGLE_READ)
                                                    numOfAnswer += ProcessReadSingleStrand(SRAQueryInputPos, SRAMismatchModel[readLength], whichCase);
                                              else
                                                    ProcessReadSingleStrand2(SRAQueryInputPos, SRAMismatchModel[readLength], whichCase, curr_sa_list_cpu, curr_occ_list);
                                        #endif
                                          
                                          // numReadsHandledByCPU2++;
                                    } else {
                                          //Number of SA ranges reported within sa_range_allowed_2
                                          for (i=0;i<sa_range_allowed_2;i++) {
                                                if ((*(ans+(i*2)*32)) >= 0xFFFFFFFD || (*(ans+(i*2+1)*32)) >= 0xFFFFFFFD) {
                                                      break;
                                                }
                                                l = *(ans+(i*2)*32);
                                                r = l + (*(ans+(i*2+1)*32)&BGS_GPU_ANSWER_OFFSET_MASK);
                                                strand = ((*(ans+(i*2+1)*32)>>(BGS_GPU_ANSWER_OFFSET_LENGTH+3)) & 1) + 1;
                                                num_mis = ((*(ans+(i*2+1)*32)>>(BGS_GPU_ANSWER_OFFSET_LENGTH)) & 7);
                                                if ( l <= r && r<=bwt->textLength) { // assume unique hits
                                                
                                                    #ifdef BGS_OCC_RESULT_BREAKDOWN
                                                          secondRoundAnswer += r - l + 1;
                                                    #endif
                                                    
                                                    if (rOutput->TotalOccurrences<qSetting->MaxOutputPerRead) {
                                                        if ((rOutput->TotalOccurrences+(r-l+1))>qSetting->MaxOutputPerRead) {
                                                            r = l + qSetting->MaxOutputPerRead - rOutput->TotalOccurrences - 1;
                                                        }
                                                    }
                                                    if (readType == SINGLE_READ) {
                                                        SRAQueryInputPos->ReadStrand = strand;
                                                        OCCReportSARange(SRAQueryInputPos,l,r,(int) num_mis);
                                                        numOfAnswer += r - l + 1;
                                                    } else {
                                                          // [attention]: needs to update the corresponding strand and number of mismatches
                                                          addSAToSAList(curr_sa_list, l, r, strand, num_mis);
                                                    }
                                                    
                                                    if (rOutput->TotalOccurrences>=qSetting->MaxOutputPerRead) { break; }
                                                } else {
                                                      break;
                                                }
                                          }
                                    }
                                    
                                    badReadPos[whichCase]++;
                              }
                      }
                  }
            }
            
            if (qSetting->OutputType == REPORT_UNIQUE_BEST) {
                if (bestHitCount==1) {                    
                    if (readType == SINGLE_READ) {
                        SRAQueryInputPos->ReadStrand = bestHitStrand;
                        OCCReportSARange(SRAQueryInputPos,bestHitSAIndex,bestHitSAIndex,(int) bestHitNumOfMismatch);     
                        numOfAnswer++;
                    } else {
                        // [attention]: needs to update the corresponding strand and number of mismatches
                        addSAToSAList(curr_sa_list, bestHitSAIndex, bestHitSAIndex, bestHitStrand, bestHitNumOfMismatch);
                    }
                }
            } else if (qSetting->OutputType == REPORT_RANDOM_BEST) {
                if (bestHitCount>0) {
                    if (readType == SINGLE_READ) {
                        SRAQueryInputPos->ReadStrand = bestHitStrand;
                        OCCReportSARange(SRAQueryInputPos,bestHitSAIndex,bestHitSAIndex,(int) bestHitNumOfMismatch);
                        numOfAnswer++;
                    } else {
                        // [attention]: needs to update the corresponding strand and number of mismatches
                        addSAToSAList(curr_sa_list, bestHitSAIndex, bestHitSAIndex, bestHitStrand, bestHitNumOfMismatch);
                    }
                }
            }
            
            if (readType==SINGLE_READ) {
                  // SINGLE READ
                  if (numOfAnswer > pre_numOfAnswer) {
                        numOfAlignedRead++;
                  } else if (isTerminalCase) {
                        OCCReportNoAlignment(SRAQueryInputPos);
                  } else if (qSetting->OutputType == REPORT_UNIQUE_BEST && bestHitCount>1) {
                        OCCReportNoAlignment(SRAQueryInputPos);
                  }
                    
                    if ( outputFormat == SRA_OUTPUT_FORMAT_SAM ||
                         outputFormat == SRA_OUTPUT_FORMAT_SAM_API ) {
                        OCCFlushCache(SRAQueryInputPos);
                    }
                        
            } else if (j%2==1) {
                  // FOR PAIR-END READ
                  
                  // transfer all the SA ranges in sa_list to occ_list
                  if ((sa_list1->curr_size > 0) || (sa_list2->curr_size > 0) || (sa_list_cpu1->curr_size > 0) || (sa_list_cpu2->curr_size > 0)) {
                        unsigned int l; unsigned int r;
                        // for sa_list1
                        for (i=0; i<sa_list1->curr_size; i++) {
                              l = sa_list1->sa[i].saIndexLeft;
                              r = sa_list1->sa[i].saIndexRight;
                              strand = sa_list1->sa[i].strand;
                              num_mis = sa_list1->sa[i].mismatchCount;
                              
                              for (k=l; k<=r; k++) {
                                    addToOCCList(occ_list1, BWTSaValue(bwt,k), strand, num_mis);
                              }
                        }
                        // for sa_list2
                        for (i=0; i<sa_list2->curr_size; i++) {
                              l = sa_list2->sa[i].saIndexLeft;
                              r = sa_list2->sa[i].saIndexRight;
                              strand = sa_list2->sa[i].strand;
                              num_mis = sa_list2->sa[i].mismatchCount;
                              
                              for (k=l; k<=r; k++) {
                                    addToOCCList(occ_list2, BWTSaValue(bwt,k), strand, num_mis);
                              }
                        }
                        
                        // for sa_list_cpu1
                        for (i=0; i<sa_list_cpu1->curr_size; i++) {
                              l = sa_list_cpu1->sa[i].saIndexLeft;
                              r = sa_list_cpu1->sa[i].saIndexRight;
                              strand = sa_list_cpu1->sa[i].strand;
                              num_mis = sa_list_cpu1->sa[i].mismatchCount;
                              for (k=l; k<=r; k++) {
                                    addToOCCList(occ_list1, BWTSaValue(bwt,k), strand, num_mis);
                              }
                        }
                        // for sa_list_cpu2
                        for (i=0; i<sa_list_cpu2->curr_size; i++) {
                              l = sa_list_cpu2->sa[i].saIndexLeft;
                              r = sa_list_cpu2->sa[i].saIndexRight;
                              strand = sa_list_cpu2->sa[i].strand;
                              num_mis = sa_list_cpu2->sa[i].mismatchCount;
                              for (k=l; k<=r; k++) {
                                    addToOCCList(occ_list2, BWTSaValue(bwt,k), strand, num_mis);
                              }
                        }
                  }
                  
                  char pair_alignment_exist = 0;
                  PEPairList* pairList;
                  
                  if ((occ_list1->curr_size>0) && (occ_list2->curr_size > 0)) {

                        // FIND THE VALID ALIGNMENT PAIRS
                        pe_in->patternLength = readLength;
                        
                        PEMappingOccurrences(pe_in, pe_out,
                                                    occ_list1->occ, occ_list1->curr_size,
                                                    occ_list2->occ, occ_list2->curr_size);
                        
                        

                        if (reportType == OUTPUT_RANDOM_BEST) {
                              // FOR REPORT RANDOM BEST
                              // find the minimum total number of mismatches for the resulting paired-end alignments.
                              char min_totalMismatchCount = 127;
                              unsigned int num_minMismatch = 0;
                              PEPairs* pePair_minMismatch;
                              pairList = pe_out->root;
                              while (pairList!=NULL && pairList->pairsCount>0) {
                                    for (i=0;i<pairList->pairsCount;i++) {
                                          PEPairs * pePair = &(pairList->pairs[i]);
                                          // fprintf(stderr, "pePair->totalMismatchCount = %i\n", (int) pePair->totalMismatchCount);
                                          if (pePair->totalMismatchCount < min_totalMismatchCount) {
                                                min_totalMismatchCount = pePair->totalMismatchCount;
                                                num_minMismatch=1;
                                                pePair_minMismatch = pePair;
                                          } else if (pePair->totalMismatchCount == min_totalMismatchCount) {
                                                num_minMismatch++;
                                          }
                                    }
                                    pairList = pairList->next;
                              }
                              // OUTPUT THE RESULT
                              if (num_minMismatch > 0) {
                                    if ( outputFormat == SRA_OUTPUT_FORMAT_PLAIN || 
                                         outputFormat == SRA_OUTPUT_FORMAT_DEFAULT ) 
                                        OCCReportDelimitor(SRAQueryInputPos);
                                        
                                    if ( outputFormat == SRA_OUTPUT_FORMAT_SAM ) {
                                          SRAQueryInputPos->ReadName=preQueryName;
                                          SRAQueryInputPos->ReadCode=preQuery;
                                          SRAQueryInputPos->ReadLength=preReadLength;
                                          OCCDirectWritePairOccSAM(SRAQueryInputPos,pePair_minMismatch);
                                    } else if ( outputFormat == SRA_OUTPUT_FORMAT_SAM_API ) {
                                          SRAQueryInputPos->ReadName=preQueryName;
                                          SRAQueryInputPos->ReadCode=preQuery;
                                          SRAQueryInputPos->ReadLength=preReadLength;
                                          OCCDirectWritePairOccSAMAPI(SRAQueryInputPos,pePair_minMismatch,0,0,readLength);
                                          SRAQueryInputPos->ReadName=thisQueryName;
                                          SRAQueryInputPos->ReadCode=thisQuery;
                                          SRAQueryInputPos->ReadLength=readLength;
                                          OCCDirectWritePairOccSAMAPI2(SRAQueryInputPos,pePair_minMismatch,0,0,preReadLength);
                                    } else {
                                          // output the alignment of the first read
                                          SRAQueryInputPos->ReadStrand = strand;
                                          if (occ->occPositionCacheCount >= OCC_CACHE_SIZE) {OCCFlushCache(SRAQueryInputPos);}
                                          occ->occPositionCache[occ->occPositionCacheCount].tp=pePair_minMismatch->algnmt_1;
                                          occ->occPositionCache[occ->occPositionCacheCount].ReadStrand=pePair_minMismatch->strand_1;
                                          occ->occPositionCache[occ->occPositionCacheCount].ChromId=0;
                                          occ->occPositionCache[occ->occPositionCacheCount].occMismatch=pePair_minMismatch->mismatch_1;
                                          occ->occPositionCacheCount++;
                                          
                                          // output the alignment of the second read
                                          // OCCReportDelimitor(occ,hsp,outputFile,(unsigned long long)readId+accumReadNum);
                                          if (occ->occPositionCacheCount >= OCC_CACHE_SIZE) {OCCFlushCache(SRAQueryInputPos);}
                                          occ->occPositionCache[occ->occPositionCacheCount].tp=pePair_minMismatch->algnmt_2;
                                          occ->occPositionCache[occ->occPositionCacheCount].ReadStrand=pePair_minMismatch->strand_2;
                                          occ->occPositionCache[occ->occPositionCacheCount].ChromId=0;
                                          occ->occPositionCache[occ->occPositionCacheCount].occMismatch=pePair_minMismatch->mismatch_2;
                                          occ->occPositionCacheCount++;
                                    }
                                    numOfAnswer++;
                                    numOfAlignedRead+=2;
                                    pair_alignment_exist=1;
                              }

                        } else {
                              // OUTPUT ALL VALID THE RESULT
                              pairList = pe_out->root;
                              if (pairList != NULL && pairList->pairsCount > 0) {
                                    if ( outputFormat == SRA_OUTPUT_FORMAT_PLAIN || 
                                         outputFormat == SRA_OUTPUT_FORMAT_DEFAULT ) 
                                        OCCReportDelimitor(SRAQueryInputPos);
                                    while (pairList!=NULL && pairList->pairsCount>0) {
                                          unsigned int pairsCount = pairList->pairsCount;
                                          if ((numOfPairEndAlignment+pairsCount)>peMaxOutputPerPair) {
                                              pairsCount = peMaxOutputPerPair - numOfPairEndAlignment;
                                          }
                                          numOfAnswer += pairsCount;
                                          numOfPairEndAlignment += pairsCount;
                                          for (i=0;i<pairsCount;i++) {
                                                PEPairs * pePair = &(pairList->pairs[i]);
                                                if ( outputFormat == SRA_OUTPUT_FORMAT_SAM ) {
                                                  SRAQueryInputPos->ReadName=preQueryName;
                                                  SRAQueryInputPos->ReadCode=preQuery;
                                                  SRAQueryInputPos->ReadLength=preReadLength;
                                                  OCCDirectWritePairOccSAM(SRAQueryInputPos,pePair);
                                                } else if ( outputFormat == SRA_OUTPUT_FORMAT_SAM_API ) {
                                                  SRAQueryInputPos->ReadName=preQueryName;
                                                  SRAQueryInputPos->ReadCode=preQuery;
                                                  SRAQueryInputPos->ReadLength=preReadLength;
                                                  OCCDirectWritePairOccSAMAPI(SRAQueryInputPos,pePair,0,0,readLength);
                                                  SRAQueryInputPos->ReadName=thisQueryName;
                                                  SRAQueryInputPos->ReadCode=thisQuery;
                                                  SRAQueryInputPos->ReadLength=readLength;
                                                  OCCDirectWritePairOccSAMAPI(SRAQueryInputPos,pePair,0,0,preReadLength);
                                                } else {
                                                    // output the alignment of the first read
                                                    if (occ->occPositionCacheCount >= OCC_CACHE_SIZE) {OCCFlushCache(SRAQueryInputPos);}
                                                    occ->occPositionCache[occ->occPositionCacheCount].tp=pePair->algnmt_1;
                                                    occ->occPositionCache[occ->occPositionCacheCount].ReadStrand=pePair->strand_1;
                                                    occ->occPositionCache[occ->occPositionCacheCount].ChromId=0;
                                                    occ->occPositionCache[occ->occPositionCacheCount].occMismatch=pePair->mismatch_1;
                                                    occ->occPositionCacheCount++;

                                                    // output the alignment of the second read
                                                    // OCCReportDelimitor(occ,hsp,outputFile,(unsigned long long)readId+accumReadNum);
                                                    if (occ->occPositionCacheCount >= OCC_CACHE_SIZE) {OCCFlushCache(SRAQueryInputPos);}
                                                    occ->occPositionCache[occ->occPositionCacheCount].tp=pePair->algnmt_2;
                                                    occ->occPositionCache[occ->occPositionCacheCount].ReadStrand=pePair->strand_2;
                                                    occ->occPositionCache[occ->occPositionCacheCount].ChromId=0;
                                                    occ->occPositionCache[occ->occPositionCacheCount].occMismatch=pePair->mismatch_2;
                                                    occ->occPositionCacheCount++;
                                                }
                                          }
                                          if (numOfPairEndAlignment >= peMaxOutputPerPair) {
                                                break;
                                          }
                                          pairList = pairList->next;
                                    }
                                    numOfAlignedRead+=2;
                                    pair_alignment_exist=1;
                              }
                        }
                  }
                  
                  if ( reportType == OUTPUT_RANDOM_BEST && pair_alignment_exist==0 ) {
                        unAlignedIDs[numOfUnAligned++]=batchFirstReadId+skipFirst+j-1;
                        unAlignedIDs[numOfUnAligned++]=batchFirstReadId+skipFirst+j;
                  }
                  
                  if ((outputUnpairReads==1) && (pair_alignment_exist==0)) {
                        if ( reportType == OUTPUT_ALL_VALID) {
                              if ( outputFormat == SRA_OUTPUT_FORMAT_SAM ) {
                                    for (i=0;i<occ_list2->curr_size;i++) {
                                          SRAQueryInputPos->ReadName=thisQueryName;
                                          SRAQueryInputPos->ReadCode=thisQuery;
                                          SRAQueryInputPos->ReadLength=readLength;
                                          SRAOccurrence * sraOcc = &(occ_list2->occ[i]);
                                          OCCDirectWritePairUnmapSAM(SRAQueryInputPos,sraOcc,0);
                                    }
                                    for (i=0;i<occ_list1->curr_size;i++) {
                                        SRAQueryInputPos->ReadName=preQueryName;
                                        SRAQueryInputPos->ReadCode=preQuery;
                                        SRAQueryInputPos->ReadLength=preReadLength;
                                        SRAOccurrence * sraOcc = &(occ_list1->occ[i]);
                                        OCCDirectWritePairUnmapSAM(SRAQueryInputPos,sraOcc,1);
                                    }
                              } else if ( outputFormat == SRA_OUTPUT_FORMAT_SAM_API ) {
                                    for (i=0;i<occ_list2->curr_size;i++) {
                                          SRAQueryInputPos->ReadName=thisQueryName;
                                          SRAQueryInputPos->ReadCode=thisQuery;
                                          SRAQueryInputPos->ReadLength=readLength;
                                          SRAOccurrence * sraOcc = &(occ_list2->occ[i]);
                                          OCCDirectWritePairUnmapSAMAPI(SRAQueryInputPos,sraOcc,0);
                                    }
                                    for (i=0;i<occ_list1->curr_size;i++) {
                                          SRAQueryInputPos->ReadName=preQueryName;
                                          SRAQueryInputPos->ReadCode=preQuery;
                                          SRAQueryInputPos->ReadLength=preReadLength;
                                          SRAOccurrence * sraOcc = &(occ_list1->occ[i]);
                                          OCCDirectWritePairUnmapSAMAPI(SRAQueryInputPos,sraOcc,1);
                                    }
                              }
                        } else if (reportType == OUTPUT_RANDOM_BEST) {
                              if ( outputFormat == SRA_OUTPUT_FORMAT_SAM ) {
                                    SRAOccurrence * bestOcc;
                                    if (occ_list2->curr_size > 0) {
                                          bestOcc = &(occ_list2->occ[0]);
                                    }
                                    for (i=1;i<occ_list2->curr_size;i++) {
                                          if (occ_list2->occ[i].mismatchCount < bestOcc->mismatchCount)
                                                bestOcc = &(occ_list2->occ[i]);
                                    }
                                    if (occ_list2->curr_size > 0) {
                                          SRAQueryInputPos->ReadName=thisQueryName;
                                          SRAQueryInputPos->ReadCode=thisQuery;
                                          SRAQueryInputPos->ReadLength=readLength;
                                          OCCDirectWritePairUnmapSAM(SRAQueryInputPos,bestOcc,0);
                                    }
                                    if (occ_list1->curr_size > 0) {
                                          bestOcc = &(occ_list1->occ[0]);
                                    }
                                    for (i=1;i<occ_list1->curr_size;i++) {
                                          if (occ_list1->occ[i].mismatchCount < bestOcc->mismatchCount)
                                                bestOcc = &(occ_list1->occ[i]);
                                    }
                                    if (occ_list1->curr_size > 0) {
                                          SRAQueryInputPos->ReadName=preQueryName;
                                          SRAQueryInputPos->ReadCode=preQuery;
                                          SRAQueryInputPos->ReadLength=preReadLength;
                                          OCCDirectWritePairUnmapSAM(SRAQueryInputPos,bestOcc,1);
                                    }
                              } else if ( outputFormat == SRA_OUTPUT_FORMAT_SAM_API ) {
                                    SRAOccurrence * bestOcc1;
                                    if (occ_list1->curr_size > 0) {
                                          bestOcc1 = &(occ_list1->occ[0]);
                                    }
                                    for (i=1;i<occ_list1->curr_size;i++) {
                                          if (occ_list1->occ[i].mismatchCount < bestOcc1->mismatchCount)
                                                bestOcc1 = &(occ_list1->occ[i]);
                                    }
                                    SRAOccurrence * bestOcc2;
                                    if (occ_list2->curr_size > 0) {
                                          bestOcc2 = &(occ_list2->occ[0]);
                                    }
                                    for (i=1;i<occ_list2->curr_size;i++) {
                                          if (occ_list2->occ[i].mismatchCount < bestOcc2->mismatchCount)
                                                bestOcc2 = &(occ_list2->occ[i]);
                                    }
                                    PEPairs bestPePair;
                                    if (occ_list1->curr_size > 0) {
                                          bestPePair.algnmt_1 = bestOcc1->ambPosition;
                                          bestPePair.strand_1 = bestOcc1->strand;
                                          bestPePair.mismatch_1 = bestOcc1->mismatchCount;
                                    }
                                    if (occ_list2->curr_size > 0) {
                                          bestPePair.algnmt_2 = bestOcc2->ambPosition;
                                          bestPePair.strand_2 = bestOcc2->strand;
                                          bestPePair.mismatch_2 = bestOcc2->mismatchCount;
                                    }
                                    SRAQueryInputPos->ReadName=preQueryName;
                                    SRAQueryInputPos->ReadCode=preQuery;
                                    SRAQueryInputPos->ReadLength=preReadLength;
                                    OCCDirectWritePairOccSAMAPI(SRAQueryInputPos,&bestPePair,occ_list1->curr_size==0,occ_list2->curr_size==0,readLength);
                                    SRAQueryInputPos->ReadName=thisQueryName;
                                    SRAQueryInputPos->ReadCode=thisQuery;
                                    SRAQueryInputPos->ReadLength=readLength;
                                    OCCDirectWritePairOccSAMAPI2(SRAQueryInputPos,&bestPePair,occ_list1->curr_size==0,occ_list2->curr_size==0,preReadLength);
                              }
                        }
                   }
            }
      }
      
      
      // printf("[hostKernel] # of bad reads handled by CPU : %i\n", numReadsHandledByCPU1);
      // printf("[hostKernel] # of super bad reads handled by CPU : %i\n", numReadsHandledByCPU2);
      free(badReadPos);
      
      #ifdef BGS_OCC_RESULT_BREAKDOWN
            hostRoundAnswer = numOfAnswer - firstRoundAnswer - secondRoundAnswer;
            printf("[hostKernel] GPU-Round-1 reported %u occurrences.\n",firstRoundAnswer);
            printf("[hostKernel] GPU-Round-2 reported %u occurrences.\n",secondRoundAnswer);
            printf("[hostKernel] CPU         reported %u occurrences.\n",hostRoundAnswer);
      #endif
      // printf("Child reported %u occurrences.\n",numOfAnswer);
      
      switch (outputFormat) {
        case SRA_OUTPUT_FORMAT_SAM_API:
            break;
        case SRA_OUTPUT_FORMAT_DEFAULT:
        case SRA_OUTPUT_FORMAT_PLAIN:
            OCCFlushCache(SRAQueryInputPos);
        default:
            fclose(outputFile);
      }
      
      // printf("numOfAnswer = %u; numOfAlignedRead = %u \n", numOfAnswer, numOfAlignedRead);
      (*alignedOcc) = numOfAnswer;
      (*alignedReads) = numOfAlignedRead;
      (*unAlignedOcc) = numOfUnAligned;
      SAListFree(sa_list1);
      SAListFree(sa_list2);
      SAListFree(sa_list_cpu1);
      SAListFree(sa_list_cpu2);
      OCCListFree(occ_list1);
      OCCListFree(occ_list2);
      PEInputFree(pe_in);
      PEOutputFree(pe_out);

      #ifdef BGS_ROUND_BREAKDOWN_TIME
            printf("[threadID: %i] total time elapsed: %9.4f seconds\n",threadID, getElapsedTime(start_time));
      #endif
      
      return numOfAnswer;
}


// A Thread Wrapper to perform the CPU task
void *hostKernelThreadWrapper(void *arg) {
      HostKernelArguements * myArgs = (HostKernelArguements*) arg;
#ifdef BGS_CPU_CASE_BREAKDOWN_TIME
      double startTime = setStartTime();
#endif
      hostKernel(myArgs->upkdQueries,myArgs->upkdQueryNames, myArgs->batchFirstReadId,myArgs->skipFirst,myArgs->numQueries,
                 &(myArgs->SRAQueryInputPos),&(myArgs->SRAQueryInputNeg),
                 myArgs->SRAMismatchModel,myArgs->outputFileName,
                 myArgs->answers,
                 &(myArgs->alignedOcc), &(myArgs->alignedReads),
                 myArgs->badReadIndices, myArgs->badAnswers, myArgs->badStartOffset, myArgs->badCountOffset,
                 myArgs->maxReadLength, myArgs->readLengths, myArgs->numCases, 
                 myArgs->sa_range_allowed_1,myArgs->sa_range_allowed_2,
                 myArgs->readIDs, myArgs->skip_round_2, myArgs->accumReadNum, 
                 myArgs->word_per_ans, myArgs->readType, myArgs->maxNumMismatch, 
                 myArgs->insert_low, myArgs->insert_high, 
                 myArgs->peStrandLeftLeg, myArgs->peStrandRightLeg,
                 myArgs->threadId, myArgs->unAlignedIDs, &(myArgs->unAlignedOcc), 
                 myArgs->reportType, myArgs->outputUnpairReads, myArgs->peMaxOutputPerPair, myArgs->isTerminalCase);
#ifdef BGS_CPU_CASE_BREAKDOWN_TIME
      double alignmentTime = getElapsedTime(startTime);
      printf("[Main:Thread_%d] Elapsed time on host : %9.4f seconds\n", myArgs->threadId, alignmentTime);
#endif
      pthread_exit(0);
      return 0;
}
