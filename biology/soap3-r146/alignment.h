/*
 *
 *    alignment.h
 *    Soap3(gpu)
 *
 *    Copyright (C) 2011, HKU
 *
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU General Public License
 *    as published by the Free Software Foundation; either version 2
 *    of the License, or (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#ifndef _ALIGNMENT_H_
#define _ALIGNMENT_H_

#include <pthread.h>
#include "definitions.h"
#include "GPUfunctions.h"
#include "CPUfunctions.h"
#include "BWT.h"
#include "HSP.h"

// COPY INDEX TO DEVICE MEMORY
void copyIndexToGPU(uint **_bwt, uint **_occ, uint **_lookupTable,
                    uint **_revBwt, uint **_revOcc, uint **_revLookupTable,
                    BWT *bwt, BWT *revBwt, uint *occValue, uint *revOccValue,
                    uint *lkt, uint *revLkt, uint numOfOccValue, uint lookupWordSize);


// perform round1 alignment in GPU
void perform_round1_alignment(uint* nextQuery, uint* nextReadLength, uint* answers[][MAX_NUM_CASES], 
                              uint numMismatch, uint numCases, uint sa_range_allowed, uint wordPerQuery, uint word_per_ans,
                              bool isExactNumMismatch, int doubleBufferIdx, uint blocksNeeded, ullint batchSize, 
                              BWT* bwt, BWT* revBwt, uint* _bwt, uint* _revBwt, uint* _occ, uint* _revOcc, uint* _lookupTable,
                              uint* _revLookupTable);

// perform round2 alignment in GPU
void perform_round2_alignment(uint* queries, uint* readLengths, uint* answers[][MAX_NUM_CASES], 
                              uint numMismatch, uint numCases, uint sa_range_allowed_2, uint wordPerQuery, uint word_per_ans, uint word_per_ans_2,
                              bool isExactNumMismatch, int doubleBufferIdx, uint blocksNeeded, ullint batchSize, 
                              BWT* bwt, BWT* revBwt, uint* _bwt, uint* _revBwt, uint* _occ, uint* _revOcc, uint* _lookupTable,
                              uint* _revLookupTable,  uint processedQuery, uint* badReadIndices[][MAX_NUM_CASES],
                              uint* badAnswers[][MAX_NUM_CASES]);

// paired-end alignment: for all-valid, all-best, unique-best 
// single-end alignment: for all-valid 
void all_valid_alignment(uint* queries, uint* readLengths, uint numMismatch, uint wordPerQuery,
                         ullint maxBatchSize, uint numQueries, uint accumReadNum,
                         BWT* bwt, BWT* revBwt, uint* _bwt, uint* _revBwt, uint* _occ, uint* _revOcc,
                         HSP * hsp, uint *lkt, uint *revLkt,
                         uint* _lookupTable, uint* _revLookupTable, IniParams ini_params, InputOptions input_options,
                         unsigned char *upkdQueries, 
                         unsigned char *noAlignment, 
                         uint *unAlignedPair, uint& numOfUnPaired,
                         uint *readIDs, char *upkdQueryNames,
                         char** currOutputFileName, samfile_t ** currSamOutputFilePtr,
                         uint& numOfAnswer, uint& numOfAlignedRead,
                         bool outNoAlign, int outputUnpairReadsForSAM, uint8_t isTerminalCase);

// pair-end alignment: for random-best
// 4-phases [0,1,2,4]
void random_best_pair_alignment(uint* queries, uint* readLengths, uint numMismatch, uint wordPerQuery,
                                 ullint maxBatchSize, uint numQueries, uint accumReadNum,
                                 BWT* bwt, BWT* revBwt, uint* _bwt, uint* _revBwt, uint* _occ, uint* _revOcc,
                                 HSP * hsp, uint *lkt, uint *revLkt,
                                 uint* _lookupTable, uint* _revLookupTable, IniParams ini_params, InputOptions input_options,
                                 unsigned char *upkdQueries, 
                                 unsigned char *noAlignment, 
                                 uint *unAlignedPair, uint& numOfUnPaired,
                                 uint *readIDs, char *upkdQueryNames,
                                 char** currOutputFileName, samfile_t ** currSamOutputFilePtr,
                                 uint& numOfAnswer, uint& numOfAlignedRead);

// single-end alignment: for all-best, unique-best and random-best
void best_single_alignment(uint* queries, uint* readLengths, uint numMismatch, uint wordPerQuery,
                           ullint maxBatchSize, uint numQueries, uint accumReadNum,
                           BWT* bwt, BWT* revBwt, uint* _bwt, uint* _revBwt, uint* _occ, uint* _revOcc,
                           HSP * hsp, uint *lkt, uint *revLkt,
                           uint* _lookupTable, uint* _revLookupTable, IniParams ini_params, InputOptions input_options,
                           unsigned char *upkdQueries, 
                           unsigned char *noAlignment, 
                           uint *unAlignedPair, uint& numOfUnPaired,
                           uint *readIDs, char *upkdQueryNames,
                           char** currOutputFileName, samfile_t ** currSamOutputFilePtr,
                           uint& numOfAnswer, uint& numOfAlignedRead);
#endif
