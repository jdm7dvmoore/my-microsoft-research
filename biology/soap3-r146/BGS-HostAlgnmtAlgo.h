/*
 *
 *    BGS-HostAlgnmtAlgo.h
 *    Soap3(gpu)
 *
 *    Copyright (C) 2011, HKU
 *
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU General Public License
 *    as published by the Free Software Foundation; either version 2
 *    of the License, or (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#ifndef __BGS_HOSTALIGNMENT_ALGO_H__
#define __BGS_HOSTALIGNMENT_ALGO_H__

#include <stdio.h>
#include <stdlib.h>
#include "BWT.h"
#include "HSP.h"

#include "BGS-IO.h"
#include "BGS-HostAlgnmtMdl.h"
#include "BGS-HostAlgnmtOps.h"

#define LOOKUP_SIZE 13



//====================PRIMITIVE (SHOULD NOT BE USED)======================
// BWTExactMatching matches pattern on text without using any other aux, e.g. lookup table.
// Also, the direction of the BWT search is defined by the parameter start and end.
// If end>start then forward, otherwise backward.
// To initiate backward search, input a start index > then end index, and normal bwt inversed
// To initiate forward search, input a start index < then end index, and reversed bwt as normal.
unsigned long long BWTExactMatchingAnyDirection(const unsigned char * convertedKey, int start, int end, 
									BWT * bwt, HSP* hsp, 
									unsigned long long * l, unsigned long long * r,
									unsigned long long * rev_l, unsigned long long * rev_r);

// BWTExactMatchingBackward matches pattern on text without using any other aux, e.g. lookup table.
unsigned long long BWTExactMatchingBackward(const unsigned char * convertedKey, int start, int end, 
									BWT * bwt, unsigned long long * saRanges);
                                    
// BWTMismatchMatchingAnyDirection matches pattern on text without using any other aux, e.g. lookup table.
// Also, the direction of the BWT search is defined by the parameter start and end.
// If end>start then forward, otherwise backward.
// To initiate backward search, input a start index > then end index, and normal bwt inversed
// To initiate forward search, input a start index < then end index, and reversed bwt as normal.
unsigned long long BWTMismatchMatchingAnyDirection(const unsigned char * convertedKey, int start, int end, 
								    int minMismatch, int maxMismatch, int mismatchInserted,
									BWT * bwt, HSP* hsp, 
									unsigned long long * l, unsigned long long * r,
									unsigned long long * rev_l, unsigned long long * rev_r);










//====================MODELIZED BELOW======================
// BWTxxxModelxxx functions are fully generalised BWT search algorithm that searchs for reads contain any number of edit/mismatch
// However, calling these functions requires user to define themselves a 'searching model'.
// The searching model requires each BWT step to be defined. The search algorithm will then follow the defined model.

// BWTMismatchModelAnyDirection_CE matches steps with check and extend mechanism.
// It allows starting off CE in the middle of a step and recursive CE until SRACase completes.
unsigned long long BWTMismatchModelAnyDirection_CE(SRAQueryInput * qInput, int i, int mismatchInserted, 
									SRACase * alignmentCase, int stepInCase, 
									unsigned long long * saRanges,
									int occMismatch, int occQuality);


// BWTExactModelForward_Lookup lookup your pattern in lookup table, bi-directional and assuming forward
unsigned long long BWTExactModelForward_Lookup(SRAQueryInput * qInput,
									SRACase * alignmentCase, int stepInCase,
									unsigned long long * saRanges);

// BWTExactModelBackward_Lookup lookup your pattern in lookup table, single direction and assuming backward
unsigned long long BWTExactModelBackward_Lookup(SRAQueryInput * qInput,
									SRACase * alignmentCase, int stepInCase,
									unsigned long long * saRanges);

// BWTExactModelBackward matches pattern on text without using any other aux, e.g. lookup table.
unsigned long long BWTExactModelBackward(SRAQueryInput * qInput, int i, int errorInserted,
									SRACase * alignmentCase, int stepInCase,
									unsigned long long * saRanges,
									int occError, int occQuality);
                                    
// BWTMismatchModelAnyDirection matches pattern on text without using any other aux, e.g. lookup table, with mismatches.
unsigned long long BWTExactModelAnyDirection(SRAQueryInput * qInput, int i, int errorInserted,
									SRACase * alignmentCase, int stepInCase, 
									unsigned long long * saRanges,
									int occError, int occQuality);
                                    
// BWTMismatchModelAnyDirection matches pattern on text without using any other aux, e.g. lookup table, with mismatches.
unsigned long long BWTMismatchModelAnyDirection(SRAQueryInput * qInput, int i, int mismatchInserted,
									SRACase * alignmentCase, int stepInCase, 
									unsigned long long * saRanges,
									int occMismatch, int occQuality);

// BWTMismatchModelBackward matches pattern on text without using any other aux, e.g. lookup table, with mismatches.
unsigned long long BWTMismatchModelBackward(SRAQueryInput * qInput, int i, int mismatchInserted,
									SRACase * alignmentCase, int stepInCase, 
									unsigned long long * saRanges,
									int occMismatch, int occQuality);


// BWTEditModelAnyDirection matches pattern on text without using any other aux, e.g. lookup table, with mismatches.
unsigned long long BWTEditModelAnyDirection(SRAQueryInput * qInput, int i, int editInserted,
									SRACase * alignmentCase, int stepInCase, 
									unsigned long long * saRanges,
									int occEdit, int occQuality);
                                    
// BWTEditModelBackward matches pattern on text without using any other aux, e.g. lookup table, with mismatches.
unsigned long long BWTEditModelBackward(SRAQueryInput * qInput, int i, int editInserted,
									SRACase * alignmentCase, int stepInCase, 
									unsigned long long * saRanges,
									int occEdit, int occQuality);

unsigned long long BWTModelSwitchAnyDirection(SRAQueryInput * qInput, int i, int errorInserted,
									SRACase * alignmentCase, int stepInCase, 
									unsigned long long * saRanges,
									int occError, int occQuality);

unsigned long long BWTModelSwitchBackward(SRAQueryInput * qInput,  int i, int errorInserted,
									SRACase * alignmentCase, int stepInCase, 
									unsigned long long * saRanges,
									int occError, int occQuality);

#endif
