/*
 *
 *    BGS-IO.h
 *    Soap3(gpu)
 *
 *    Copyright (C) 2011, HKU
 *
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU General Public License
 *    as published by the Free Software Foundation; either version 2
 *    of the License, or (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#ifndef __BGS_IO__
#define __BGS_IO__

#include <stdio.h>
#include <stdlib.h>
#include "BWT.h"
#include "HSP.h"
#include "OCC.h"
#include "HOCC.h"
#include "PEAlgnmt.h"
#include "BGS-HostAlgnmtMdl.h"
#include "SAM.h"

//Define the below parameter to output the alignment result(text position)
// on screen instead of writing into output file
//#define DEBUG_2BWT_OUTPUT_TO_SCREEN

//Define the below parameter to stop cache reported SA range and text position.
// This is turned on when we are trying to obtain alignment timing only.
//#define DEBUG_2BWT_NO_OUTPUT

//Define the below parameter to skip writing the alignment result(text position)
// into disk. This is turned on when we are trying to obtain alignment timing only.
//#define DEBUG_2BWT_NO_WRITE_FILE

//Define the below parameter to skip translating the alignment result(text position).
// This is turned on when we are trying to obtain alignment timing only.
//#define DEBUG_2BWT_NO_TRANSLATION

//Define below parameter to skip outputing the header for plain output
#define SKIP_PLAIN_HEADER

OCC * OCCConstruct();
void OCCFree(OCC * occ);

unsigned int OCCWriteOutputHeader(HSP * hsp, FILE * outFilePtr, 
									unsigned int maxReadLength,
									unsigned int numOfReads,
                                    int outputFormat);

void OCCFlushCache(SRAQueryInput * qInput);

void OCCFlushCacheDefault(OCC * occ, HSP * hsp, FILE * outFilePtr);

void OCCFlushCacheSAM(SRAQueryInput * qInput);
void OCCDirectWritePairOccSAM(SRAQueryInput * qInput, PEPairs * pePair);
void OCCDirectWritePairUnmapSAM(SRAQueryInput * qInput, SRAOccurrence * sraOcc, int isFirst);
void OCCDirectWritePairOccSAMAPI(SRAQueryInput * qInput, PEPairs * pePair, int isFirstUnaligned, int isSecondUnaligned, int mateLength);
void OCCDirectWritePairOccSAMAPI2(SRAQueryInput * qInput, PEPairs * pePair, int isFirstUnaligned, int isSecondUnaligned, int mateLength);
void OCCDirectWritePairUnmapSAMAPI(SRAQueryInput * qInput, SRAOccurrence * sraOcc, int isFirst);

void OCCReportDelimitor(SRAQueryInput * qInput);
void OCCReportNoAlignment(SRAQueryInput * qInput);

//OCCReportSARange : asumming l<=r

unsigned long long OCCReportSARange(SRAQueryInput * qInput,
									unsigned long long l, unsigned long long r, 
                                    int occMismatch);

unsigned long long OCCReportTextPositions(SRAQueryInput * qInput, int posCount);

#endif
