/*
 *
 *    CPUfunctions.h
 *    Soap3(gpu)
 *
 *    Copyright (C) 2011, HKU
 *
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU General Public License
 *    as published by the Free Software Foundation; either version 2
 *    of the License, or (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#ifndef _CPUFUNCTIONS_H_
#define _CPUFUNCTIONS_H_ 

#include <pthread.h>
#include "dictionary.h"
#include "BGS-HostAlgnmtAlgo.h"
#include "BGS-HostAlgnmtAlgo2.h"
#include "iniparser.h"
#include "definitions.h"
#include "SAList.h"
#include "PE.h"
#include "PEAlgnmt.h"
#include "Timing.h"

// The offset mask to retrieve the least significant 24-bit from the 32-bit word.
#define  BGS_GPU_ANSWER_OFFSET_LENGTH   24
#define  BGS_GPU_ANSWER_OFFSET_MASK     ((1<<BGS_GPU_ANSWER_OFFSET_LENGTH)-1)

// The function is to print out the help on the usage of the program
void printUsage(char* program_name);


// This function is to parse the ini file and collect the paramaters of
// 1. The file extension of SA file
// 2. The number of CPU threads
// 3. The alignment model
// 4. The memory size in the GPU card
int ParseIniFile(char *iniFileName, IniParams &ini_params);


// This function is to load the pair-end reads for at most "maxNumQueries/2" # of pairs of reads
int loadPairReads(FILE* queryFile, char* queryFileBuffer, FILE* queryFile2, char* queryFileBuffer2,
                  uint* queries, uint* readLengths, uint* readIDs, unsigned char* upkdQueries,  char* upkdQueryNames, uint maxReadLength, uint maxNumQueries,
                  size_t &bufferSize, char &queryChar, uint &bufferIndex, size_t &bufferSize2, char &queryChar2, uint &bufferIndex2, 
                  uint accumReadNum, uint wordPerQuery);      


// This function is to load the single reads for at most "maxNumQueries" # of reads
int loadSingleReads(FILE* queryFile, char* queryFileBuffer, uint* queries, uint* readLengths, uint* readIDs, 
                    unsigned char* upkdQueries, char* upkdQueryNames, uint maxReadLength, uint maxNumQueries,
                    size_t& bufferSize, char& queryChar, uint& bufferIndex, uint accumReadNum, uint wordPerQuery);

// This function is to check and parse the input arguments
// arguments: <program> single bwtCodeIndex queryFileName numQueries maxReadLength [options]
// OR         <program> pair bwtCodeIndex queryFileName1 queryFileName2 numQueries maxReadLength [options]
// If the argument is not correct, then it returns FALSE. Else it returns TRUE


int get_default_mismatch_num(uint* readLengths, uint numQueries);
// if user does not specify # of mismatches
// then scan the first ten reads
// if the read length < 50, then return DEFAULT_NUM_MISMATCH_FOR_SHORT_READ
// else return DEFAULT_NUM_MISMATCH_FOR_NORMAL_READ
      

bool parseInputArgs(int argc, char** argv, InputOptions& input_options);

// A Thread Wrapper to perform the CPU task
void *hostKernelThreadWrapper(void *arg);

// print out the parameters
void printParameters(InputOptions input_options, IniParams ini_params);

// process index file name
void processIndexFileName(IndexFileNames& index, char* indexName, IniParams ini_params);

void loadIndex(IndexFileNames index, BWT **bwt, BWT **revBwt, HSP **hsp, uint **lkt, uint **revLkt,
               uint **revOccValue, uint **occValue, uint &numOfOccValue, uint &lookupWordSize);

// set the multi-threading arguments
void setHostKernelArguments(HostKernelArguements* hostKernelArguments, pthread_t* threads, IniParams ini_params, BWT *bwt, HSP *hsp,
                            uint *lkt, uint *revLkt, int* dummyQuality, uint maxReadLength, uint word_per_ans,
                            InputOptions * input_options);
                            //char readType, uint insert_high, uint insert_low);      

// obtain the number of cases for this number of mismatch
// and obtain the number of SA ranges allowed
void getParametersForThisMismatch(uint numMismatch, uint& numCases, uint& sa_range_allowed_1,
                                  uint& sa_range_allowed_2, char& skip_round_2, 
                                  uint& word_per_ans, uint& word_per_ans_2);


// pack the reads which are unpaired
void packUnPairedReads(uint* queries, uint* readIDs, uint* readLengths, uint* unAlignedPair,
                       uint wordPerQuery, uint numOfUnPaired, ullint maxBatchSize);
      

// pack the reads with no alignment together
// return number of reads with no alignment
uint packReads(uint* queries, uint* readIDs, uint* readLengths, unsigned char* noAlignment,
               uint wordPerQuery, uint numQueries);


// show the merging file command at the end of the program
void show_merge_file_command(InputOptions input_options, char* queryFileName, char* queryFileName2);

#endif
