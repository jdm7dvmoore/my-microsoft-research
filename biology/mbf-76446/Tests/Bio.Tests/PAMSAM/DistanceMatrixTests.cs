﻿// *********************************************************
// 
//     Copyright (c) Microsoft. All rights reserved.
//     This code is licensed under the Apache License, Version 2.0.
//     THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
//     ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
//     IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
//     PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
// 
// *********************************************************
using Bio.Algorithms.Alignment.MultipleSequenceAlignment;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bio.Tests
{
    /// <summary>
    /// Test for DistanceMatrix Class
    /// </summary>
    [TestClass]
    public class DistanceMatrixTests
    {
        /// <summary>
        /// Test DistanceMatrix Class
        /// </summary>
        [TestMethod]
        [Priority(0)]
        [TestCategory("Priority0")]
        public void TestDistanceMatrix()
        {
            int dimension = 4;
            IDistanceMatrix distanceMatrix = new SymmetricDistanceMatrix(dimension);
            for (int i = 0; i < distanceMatrix.Dimension - 1; ++i)
            {
                for (int j = i + 1; j < distanceMatrix.Dimension; ++j)
                {
                    distanceMatrix[i, j] = i + j;
                    distanceMatrix[j, i] = i + j;
                }
            }

            Assert.AreEqual(dimension, distanceMatrix.Dimension);
            Assert.AreEqual(dimension, distanceMatrix.NearestNeighbors.Length);
            Assert.AreEqual(dimension, distanceMatrix.NearestDistances.Length);

            // Test elements
            for (int i = 0; i < distanceMatrix.Dimension - 1; ++i)
            {
                for (int j = i + 1; j < distanceMatrix.Dimension; ++j)
                {
                    Assert.AreEqual(i + j, distanceMatrix[i, j]);
                    Assert.AreEqual(i + j, distanceMatrix[j, i]);
                }
            }

            // Test NearestNeighbors
            Assert.AreEqual(1, distanceMatrix.NearestNeighbors[0]);
            Assert.AreEqual(0, distanceMatrix.NearestNeighbors[1]);
            Assert.AreEqual(0, distanceMatrix.NearestNeighbors[2]);
            Assert.AreEqual(0, distanceMatrix.NearestNeighbors[3]);

            Assert.AreEqual(1, distanceMatrix.NearestDistances[0]);
            Assert.AreEqual(1, distanceMatrix.NearestDistances[1]);
            Assert.AreEqual(2, distanceMatrix.NearestDistances[2]);
            Assert.AreEqual(3, distanceMatrix.NearestDistances[3]);
        }
    }
}