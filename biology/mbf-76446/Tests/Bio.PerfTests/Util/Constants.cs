﻿//*********************************************************
//
// Copyright (c) Microsoft. All rights reserved.
// This code is licensed under the Apache License, Version 2.0.
// THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
// IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
// PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

namespace Bio.PerfTests.Util
{
    internal static class Constants
    {
        internal const string ObjectModelPerfUsingLargeSizeTestDataNodeName =
            "ObjectModelPerfUsingLargeSizeTestData";
        internal const string ObjectModelPerfUsingSmallSizeTestDataNodeName =
            "ObjectModelPerfUsingSmallSizeTestData";
        internal const string FilePathNode = "FilePath";
        internal const string AlphabetNode = "Alphabet";
        internal const string SequenceIDNode = "SequenceID";
        internal const string StartIndexNode = "StartIndex";
        internal const string EndIndexNode = "EndIndex";
        internal const string StartLineNode = "StartLine";
        internal const string SequenceRangeToRead = "SequenceRangeToRead";

        internal const string NcumerSmallSizeTestDataNodeName = "NcumerSmallSizeTestData";
        internal const string MUMmerSmallSizeTestDataNodeName = "MUMmerSmallSizeTestData";
        internal const string MUMmerLargeSizeTestDataNodeName = "MUMmerLargeSizeTestData";
        internal const string MUMmerQUTTestDataNodeName = "MUMmerQUTTestData";
        internal const string NUCmerMediumSizeTestDataNodeName = "NUCmerMediumSizeTestData";
        internal const string RefFilePathNode = "RefFilePath";
        internal const string QueryFilePathNode = "QueryFilePath";
        internal const string SMFilePathNode = "SMFilePath";
        internal const string MUMLengthNode = "MUMLength";
        internal const string PamsamSmallSizeTestDataNode = "PamsamSmallSizeTestData";
        internal const string PamsamLargeSizeTestDataNode = "PamsamLargeSizeTestData";

        internal const string PaDeNAEulerTestDataName = "PaDeNAEulerTestData";
        internal const string PaDeNASmallSizeEulerTestDataNode =
            "PaDeNASmallSizeEulerTestData";
        internal const string KmerLengthNode = "KmerLength";
        internal const string DepthNode = "Depth";
        internal const string StdDeviationNode = "StdDeviation";
        internal const string MeanNode = "Mean";
        internal const string AlignmentAlgorithmSmallSizeTestDataNode = "AlignmentAlgorithmSmallSizeTestData";
        internal const string AlignmentAlgorithmMediumSizeTestDataNode = "AlignmentAlgorithmMediumSizeTestData";
        internal const string BAMParserLargeSizeTestDataNode = "BAMParserLargeSizeTestData";
        internal const string BAMParserVeryLargeSizeTestDataNode = "BAMParserVeryLargeSizeTestData";
        internal const string SAMParserLargeSizeTestDataNode = "SAMParserLargeSizeTestData";
        internal const string SAMParserVeryLargeSizeTestDataNode = "SAMParserVeryLargeSizeTestData";
        internal const int VistaMinorVersion = 0;
        internal const int Win7MinorVersion = 1;
        internal const int WinXPMinorVersion = 1;
        internal const int WinNTMajorVersion = 6;
        internal const int WinMajorVersion = 5;
        internal const int Win2K3MinorVersion = 2;
        internal const int Win2KMinorVersion = 0;
        internal const int Win2K8MinorVersion = 3;
    }
}
