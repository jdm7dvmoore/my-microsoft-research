﻿// *****************************************************************
//    Copyright (c) Microsoft. All rights reserved.
//    This code is licensed under the Apache License, Version 2.0.
//    THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
//    ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
//    IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
//    PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
// *****************************************************************

/****************************************************************************
 * FastaP1TestCases.cs
 * 
 *   This file contains the Fasta - Parsers and Formatters Priority One test cases.
 * 
***************************************************************************/

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using Bio.IO.FastA;
using Bio.TestAutomation.Util;
using Bio.Util.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bio;

namespace Bio.TestAutomation.IO.Fasta
{
    /// <summary>
    /// FASTA Priority One parser and formatter test cases implementation.
    /// </summary>
    [TestClass]
    public class FastaP1TestCases
    {

        #region Global Variables

        Utility utilityObj = new Utility(@"TestUtils\TestsConfig.xml");
        ASCIIEncoding encodingObj = new ASCIIEncoding();

        #endregion Global Variables

        #region Constructor

        /// <summary>
        /// Static constructor to open log and make other settings needed for test
        /// </summary>
        static FastaP1TestCases()
        {
            Trace.Set(Trace.SeqWarnings);
            if (!ApplicationLog.Ready)
            {
                ApplicationLog.Open("bio.automation.log");
            }
        }

        #endregion Constructor

        #region Fasta Parser P1 Test cases

        /// <summary>
        /// Parse a valid FastA file (DNA) and using Parse(file-name) method and 
        /// validate the expected sequence
        /// Input : DNA FastA File
        /// Validation : Read the FastA file to which the sequence was formatted and 
        /// validate Sequence, Sequence Count
        /// </summary>
        [TestMethod]
        [Priority(1)]
        [TestCategory("Priority1")]
        public void FastaParserValidateParseWithDnaSequence()
        {
            ValidateParseGeneralTestCases(Constants.SimpleFastaDnaNodeName);
        }

        /// <summary>
        /// Parse a valid FastA file (Protein) and using Parse(file-name) method and 
        /// validate the expected sequence
        /// Input : Protein FastA File
        /// Validation : Read the FastA file to which the sequence was formatted and 
        /// validate Sequence, Sequence Count
        /// </summary>
        [TestMethod]
        [Priority(1)]
        [TestCategory("Priority1")]
        public void FastaParserValidateParseWithProteinSequence()
        {
            ValidateParseGeneralTestCases(Constants.SimpleFastaProteinNodeName);
        }

        /// <summary>
        /// Parse a valid FastA file (RNA) and using Parse(file-name) method and 
        /// validate the expected sequence
        /// Input : RNA FastA File
        /// Validation : Read the FastA file to which the sequence was formatted and 
        /// validate Sequence, Sequence Count
        /// </summary>
        [TestMethod]
        [Priority(1)]
        [TestCategory("Priority1")]
        public void FastaParserValidateParseWithRnaSequence()
        {
            ValidateParseGeneralTestCases(Constants.SimpleFastaRnaNodeName);
        }

        /// <summary>
        /// Parse a valid FastA file (DNA) which is of less than 100KB
        /// and using Parse(file-name) method and validate the expected 
        /// sequence
        /// Input : Medium Size FastA File
        /// Validation : Read the FastA file to which the sequence was formatted and 
        /// validate Sequence, Sequence Count
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "System.Console.WriteLine(System.String)"), TestMethod]
        [Priority(1)]
        [TestCategory("Priority1")]
        public void FastaParserValidateParseWithMediumSizeSequence()
        {
            string filePath = utilityObj.xmlUtil.GetTextValue(
                Constants.MediumSizeFastaNodeName, Constants.FilePathNode);
            string alphabet = utilityObj.xmlUtil.GetTextValue(Constants.MediumSizeFastaNodeName,
                Constants.AlphabetNameNode);
            Assert.IsTrue(File.Exists(filePath));
            // Logs information to the log file
            ApplicationLog.WriteLine(string.Format((IFormatProvider)null,
                "FastA Parser : File Exists in the Path '{0}'.", filePath));

            IEnumerable<ISequence> seqs = null;
            using (FastAParser parserObj = new FastAParser(filePath))
            {
                parserObj.Alphabet = Utility.GetAlphabet(alphabet);
                seqs = parserObj.Parse();

                Assert.IsNotNull(seqs);
                Assert.AreEqual(1, seqs.Count());
                ApplicationLog.WriteLine(string.Format((IFormatProvider)null,
                    "FastA Parser: Number of Sequences found are '{0}'.",
                    seqs.Count()));

                // Gets the expected sequence from the Xml
                string expectedSequence = utilityObj.xmlUtil.GetTextValue(
                    Constants.MediumSizeFastaNodeName, Constants.ExpectedSequenceNode);

                Sequence seq = (Sequence)seqs.ElementAt(0);
                char[] seqString = seqs.ElementAt(0).Select(a => (char)a).ToArray();
                string newSequence = new string(seqString);
                Assert.IsNotNull(seq);

                // Replace all the empty spaces, paragraphs and new line for validation
                string updatedExpSequence =
                    expectedSequence.Replace("\r", "").Replace("\n", "").Replace(" ", "");
                string updatedActualSequence =
                    newSequence.Replace("\r", "").Replace("\n", "").Replace(" ", "");
                Assert.AreEqual(updatedExpSequence, updatedActualSequence);
                ApplicationLog.WriteLine(
                    string.Format((IFormatProvider)null, "FastA Parser: Sequence is '{0}' and is as expected.",
                    updatedActualSequence));
                // Logs to the NUnit GUI (Console.Out) window
                Console.WriteLine(
                    string.Format((IFormatProvider)null, "FastA Parser: Sequence is '{0}' and is as expected.",
                    updatedActualSequence));

                Assert.AreEqual(updatedExpSequence.Length, updatedActualSequence.Length);
                ApplicationLog.WriteLine(
                    string.Format((IFormatProvider)null, "FastA Parser: Sequence Length is '{0}' and is as expected.",
                    updatedActualSequence.Length));

                Assert.IsNotNull(seq.Alphabet);
                Assert.AreEqual(seq.Alphabet.Name.ToLower(CultureInfo.CurrentCulture),
                    utilityObj.xmlUtil.GetTextValue(Constants.MediumSizeFastaNodeName,
                    Constants.AlphabetNameNode).ToLower(CultureInfo.CurrentCulture));
                ApplicationLog.WriteLine(string.Format((IFormatProvider)null,
                    "FastA Parser: The Sequence Alphabet is '{0}' and is as expected.",
                    seq.Alphabet.Name));

                Assert.AreEqual(utilityObj.xmlUtil.GetTextValue(
                    Constants.MediumSizeFastaNodeName, Constants.SequenceIdNode), seq.ID);
                ApplicationLog.WriteLine(string.Format((IFormatProvider)null,
                    "FastA Parser: Sequence ID is '{0}' and is as expected.", seq.ID));
                // Logs to the NUnit GUI (Console.Out) window
                Console.WriteLine(string.Format((IFormatProvider)null,
                    "FastA Parser: Sequence ID is '{0}' and is as expected.", seq.ID));
            }
        }

        /// <summary>
        /// Parse a valid FastA file with one line sequence and using 
        /// Parse(file-name) method and validate the expected sequence
        /// Input : One line sequence FastA File
        /// Validation : Read the FastA file to which the sequence was formatted and 
        /// validate Sequence, Sequence Count
        /// </summary>
        [TestMethod]
        [Priority(1)]
        [TestCategory("Priority1")]
        public void FastaParserValidateParseWithOneLineSequence()
        {
            ValidateParseGeneralTestCases(Constants.OneLineSequenceFastaNodeName);
        }

        /// <summary>
        /// Parse a valid FastA file with Alphabet passed as property 
        /// and using Parse(file-name) method and 
        /// validate the expected sequence
        /// Input : DNA FastA File with Encoding specified
        /// Validation : Read the FastA file to which the sequence was formatted and 
        /// validate Sequence, Sequence Count
        /// </summary>
        [TestMethod]
        [Priority(1)]
        [TestCategory("Priority1")]
        public void FastaParserValidateParseWithAlphabetAsProperty()
        {
            string filePath = utilityObj.xmlUtil.GetTextValue(
                Constants.SimpleFastaDnaNodeName, Constants.FilePathNode);

            using (FastAParser parserObj = new FastAParser(filePath))
            {
                Assert.IsTrue(File.Exists(filePath));

                // Logs information to the log file
                ApplicationLog.WriteLine(string.Format((IFormatProvider)null,
                    "FastA Parser : File Exists in the Path '{0}'.", filePath));
                parserObj.Alphabet = Utility.GetAlphabet(
                    utilityObj.xmlUtil.GetTextValue(Constants.SimpleFastaDnaNodeName,
                    Constants.AlphabetNameNode));

                ValidateParserGeneralTestCases(parserObj);
            }
        }

        #endregion Fasta Parser P1 Test cases

        #region Fasta Formatter P1 Test cases

        /// <summary>
        /// Format a valid DNA Sequence to a 
        /// FastA file Format() method and validate the same.
        /// Input : FastA DNA Sequence
        /// Validation : Read the FastA file to which the sequence was formatted and 
        /// validate Sequence, Sequence Count
        /// </summary>
        [TestMethod]
        [Priority(1)]
        [TestCategory("Priority1")]
        public void FastaFormatterValidateFormatWithDnaSequence()
        {
            ValidateFormatterGeneralTestCases(Constants.SimpleFastaDnaNodeName);
        }

        /// <summary>
        /// Format a valid RNA Sequence to a 
        /// FastA file Format() method and validate the same.
        /// Input : FastA RNA Sequence
        /// Validation : Read the FastA file to which the sequence was formatted and 
        /// validate Sequence, Sequence Count
        /// </summary>
        [TestMethod]
        [Priority(1)]
        [TestCategory("Priority1")]
        public void FastaFormatterValidateFormatWithRnaSequence()
        {
            ValidateFormatterGeneralTestCases(Constants.SimpleFastaRnaNodeName);
        }

        /// <summary>
        /// Format a valid Protein Sequence to a 
        /// FastA file Format() method and validate the same.
        /// Input : FastA Protein Sequence
        /// Validation : Read the FastA file to which the sequence was formatted and 
        /// validate Sequence, Sequence Count
        /// </summary>
        [TestMethod]
        [Priority(1)]
        [TestCategory("Priority1")]
        public void FastaFormatterValidateFormatWithProteinSequence()
        {
            ValidateFormatterGeneralTestCases(Constants.SimpleFastaProteinNodeName);
        }

        /// <summary>
        /// Parse a FastA DNA File using Parse() method and Format the 
        /// same to a FastA file using Format() method and validate the same.
        /// Input : FastA DNA File which would be parsed
        /// Validation : Read the New FastA file to which the sequence was formatted and 
        /// validate Sequence, Sequence Count
        /// </summary>
        [TestMethod]
        [Priority(1)]
        [TestCategory("Priority1")]
        public void FastaFormatterValidateWithParseDnaSequence()
        {
            ValidateParseFormatGeneralTestCases(Constants.SimpleFastaDnaNodeName);
        }

        /// <summary>
        /// Parse a FastA RNA File using Parse() method and Format the 
        /// same to a FastA file using Format() method and validate the same.
        /// Input : FastA RNA File which would be parsed
        /// Validation : Read the New FastA file to which the sequence was formatted and 
        /// validate Sequence, Sequence Count
        /// </summary>
        [TestMethod]
        [Priority(1)]
        [TestCategory("Priority1")]
        public void FastaFormatterValidateWithParseRnaSequence()
        {
            ValidateParseFormatGeneralTestCases(Constants.SimpleFastaRnaNodeName);
        }

        /// <summary>
        /// Parse a FastA Protein File using Parse() method and Format the 
        /// same to a FastA file using Format() method and validate the same.
        /// Input : FastA Protein File which would be parsed
        /// Validation : Read the New FastA file to which the sequence was formatted and 
        /// validate Sequence, Sequence Count
        /// </summary>
        [TestMethod]
        [Priority(1)]
        [TestCategory("Priority1")]
        public void FastaFormatterValidateWithParseProteinSequence()
        {
            ValidateParseFormatGeneralTestCases(Constants.SimpleFastaProteinNodeName);
        }

        /// <summary>
        /// Format a valid medium size i.e., less than 100KB Fasta File 
        /// using Format() method and validate the same.
        /// Input : Medium size FastA file
        /// Validation : Read the FastA file to which the sequence was formatted and 
        /// validate Sequence, Sequence Count
        /// </summary>
        [TestMethod]
        [Priority(1)]
        [TestCategory("Priority1")]
        public void FastaFormatterValidateFormatWithMediumSizeSequence()
        {
            ValidateFormatterGeneralTestCases(Constants.MediumSizeFastaNodeName);
        }

        /// <summary>
        /// Format a valid large size i.e., greater than 100 KB and less tha 350 KB
        /// using Format() method and validate the same.
        /// Input : Large size FastA file
        /// Validation : Read the FastA file to which the sequence was formatted and 
        /// validate Sequence, Sequence Count
        /// </summary>
        [TestMethod]
        [Priority(1)]
        [TestCategory("Priority1")]
        public void FastaFormatterValidateFormatWithLargeSizeSequence()
        {
            ValidateFormatterGeneralTestCases(Constants.LargeSizeFasta);
        }


        /// <summary>
        /// Parse a medium size FastA File i.e., less than 100 KB 
        /// using Parse() method and Format the 
        /// same to a FastA file using Format() method and validate the same.
        /// Input : Medium size FastA File which would be parsed
        /// Validation : Read the New FastA file to which the sequence was formatted and 
        /// validate Sequence, Sequence Count
        /// </summary>
        [TestMethod]
        [Priority(1)]
        [TestCategory("Priority1")]
        public void FastaFormatterValidateWithParseMediumSizeSequence()
        {
            ValidateParseFormatGeneralTestCases(Constants.MediumSizeFastaNodeName);
        }

        /// <summary>
        /// Parse a large size FastA File i.e., greater than 100 KB and less than 350 KB
        /// using Parse() method and Format the 
        /// same to a FastA file using Format() method and validate the same.
        /// Input : Large size FastA File which would be parsed
        /// Validation : Read the New FastA file to which the sequence was formatted and 
        /// validate Sequence, Sequence Count
        /// </summary>
        [TestMethod]
        [Priority(1)]
        [TestCategory("Priority1")]
        public void FastaFormatterValidateWithParseLargeSizeSequence()
        {
            ValidateParseFormatGeneralTestCases(Constants.LargeSizeFasta);
        }

        /// <summary>
        /// Format a valid Sequence to a FastA file using Format() method and 
        /// validate the same by Parsing it back.
        /// Input : FastA Sequence
        /// Validation : Read the FastA file using Parse() and 
        /// validate Sequence, Sequence Count
        /// </summary>
        [TestMethod]
        [Priority(1)]
        [TestCategory("Priority1")]
        public void FastaFormatterValidateWithFormatAndParse()
        {
            ValidateFormatterGeneralTestCases(Constants.SimpleFastaNodeName);
        }

        #endregion Fasta Formatter P1 Test cases

        #region Supporting Methods

        /// <summary>
        /// Validates general Parse test cases with the xml node name specified.
        /// </summary>
        /// <param name="nodeName">xml node name.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "System.Console.WriteLine(System.String)")]
        void ValidateParseGeneralTestCases(string nodeName)
        {
            string filePath = utilityObj.xmlUtil.GetTextValue(
                nodeName, Constants.FilePathNode);
            string alphabet = utilityObj.xmlUtil.GetTextValue(nodeName,
                Constants.AlphabetNameNode);

            Assert.IsTrue(File.Exists(filePath));
            // Logs information to the log file
            ApplicationLog.WriteLine(string.Format((IFormatProvider)null,
                "FastA Parser : File Exists in the Path '{0}'.", filePath));

            IEnumerable<ISequence> seqs = null;
            using (FastAParser parserObj = new FastAParser(filePath))
            {
                parserObj.Alphabet = Utility.GetAlphabet(alphabet);
                seqs = parserObj.Parse();

                Assert.IsNotNull(seqs);
                Assert.AreEqual(1, seqs.Count());
                ApplicationLog.WriteLine(string.Format((IFormatProvider)null,
                    "FastA Parser: Number of Sequences found are '{0}'.",
                    seqs.Count()));

                // Gets the expected sequence from the Xml
                string expectedSequence = utilityObj.xmlUtil.GetTextValue(
                    nodeName, Constants.ExpectedSequenceNode);

                Sequence seq = (Sequence)seqs.ElementAt(0);
                Assert.IsNotNull(seq);
                char[] seqString = seqs.ElementAt(0).Select(a => (char)a).ToArray();
                string newSequence = new string(seqString);
                Assert.AreEqual(expectedSequence, newSequence);
                ApplicationLog.WriteLine(string.Format((IFormatProvider)null,
                    "FastA Parser: Sequence is '{0}' and is as expected.",
                    newSequence));
                // Logs to the NUnit GUI (Console.Out) window
                Console.WriteLine(string.Format((IFormatProvider)null,
                    "FastA Parser: Sequence is '{0}' and is as expected.", newSequence));

                byte[] tmpEncodedSeq = new byte[seq.Count];
                (seq as IEnumerable<byte>).ToArray().CopyTo(tmpEncodedSeq, 0);
                Assert.AreEqual(expectedSequence.Length, tmpEncodedSeq.Length);
                ApplicationLog.WriteLine(string.Format((IFormatProvider)null,
                    "FastA Parser: Sequence Length is '{0}' and is as expected.",
                    expectedSequence.Length));

                Assert.IsNotNull(seq.Alphabet);
                Assert.AreEqual(seq.Alphabet.Name.ToLower(CultureInfo.CurrentCulture),
                    utilityObj.xmlUtil.GetTextValue(nodeName,
                    Constants.AlphabetNameNode).ToLower(CultureInfo.CurrentCulture));
                ApplicationLog.WriteLine(string.Format((IFormatProvider)null,
                    "FastA Parser: The Sequence Alphabet is '{0}' and is as expected.",
                    seq.Alphabet.Name));

                Assert.AreEqual(utilityObj.xmlUtil.GetTextValue(
                    nodeName, Constants.SequenceIdNode), seq.ID);
                ApplicationLog.WriteLine(string.Format((IFormatProvider)null,
                    "FastA Parser: Sequence ID is '{0}' and is as expected.",
                    seq.ID));
                // Logs to the NUnit GUI (Console.Out) window
                Console.WriteLine(string.Format((IFormatProvider)null,
                    "FastA Parser: Sequence ID is '{0}' and is as expected.",
                    seq.ID));
            }
        }

        /// <summary>
        /// Validates general Parse test cases with Fasta parser object name specified.
        /// </summary>
        /// <param name="parserObj">fasta parser object.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "System.Console.WriteLine(System.String)")]
        void ValidateParserGeneralTestCases(FastAParser parserObj)
        {
            IEnumerable<ISequence> seqs = null;
            seqs = parserObj.Parse();

            Assert.IsNotNull(seqs);
            Assert.AreEqual(1, seqs.Count());

            ApplicationLog.WriteLine(string.Format((IFormatProvider)null,
                "FastA Parser with Alphabet: Number of Sequences found are '{0}'.",
                seqs.Count()));

            // Gets the expected sequence from the Xml
            string expectedSequence = utilityObj.xmlUtil.GetTextValue(
                Constants.SimpleFastaDnaNodeName, Constants.ExpectedSequenceNode);

            Sequence seq = (Sequence)seqs.ElementAt(0);
            Assert.IsNotNull(seq);
            char[] seqString = seqs.ElementAt(0).Select(a => (char)a).ToArray();
            string newSequence = new string(seqString);
            Assert.AreEqual(expectedSequence, newSequence);

            ApplicationLog.WriteLine(string.Format((IFormatProvider)null,
                "FastA Parser with Alphabet: DNA Sequence is '{0}' and is as expected.",
                newSequence));
            // Logs to the NUnit GUI (Console.Out) window
            Console.WriteLine(string.Format((IFormatProvider)null,
                "FastA Parser with Alphabet: DNA Sequence is '{0}' and is as expected.",
                newSequence));

            byte[] tmpEncodedSeq = new byte[seq.Count];
            (seq as IEnumerable<byte>).ToArray().CopyTo(tmpEncodedSeq, 0);
            Assert.AreEqual(expectedSequence.Length, tmpEncodedSeq.Length);
            ApplicationLog.WriteLine(string.Format((IFormatProvider)null,
                "FastA Parser with Alphabet: DNA Sequence Length is '{0}' and is as expected.",
                expectedSequence.Length));

            Assert.IsNotNull(seq.Alphabet);
            Assert.AreEqual(seq.Alphabet.Name.ToLower(CultureInfo.CurrentCulture),
                utilityObj.xmlUtil.GetTextValue(Constants.SimpleFastaDnaNodeName,
                Constants.AlphabetNameNode).ToLower(CultureInfo.CurrentCulture));
            ApplicationLog.WriteLine(string.Format((IFormatProvider)null,
                "FastA Parser with Alphabet: The Sequence Alphabet is '{0}' and is as expected.",
                seq.Alphabet.Name));

            Assert.AreEqual(utilityObj.xmlUtil.GetTextValue(
                Constants.SimpleFastaDnaNodeName, Constants.SequenceIdNode), seq.ID);
            ApplicationLog.WriteLine(string.Format((IFormatProvider)null,
                "FastA Parser with Alphabet: DNA Sequence ID is '{0}' and is as expected.",
                seq.ID));
            // Logs to the NUnit GUI (Console.Out) window
            Console.WriteLine(string.Format((IFormatProvider)null,
                "FastA Parser with Alphabet: DNA Sequence ID is '{0}' and is as expected.",
                seq.ID));
        }

        /// <summary>
        /// Validates general FastA Formatter test cases with the xml node name specified.
        /// </summary>
        /// <param name="nodeName">xml node name.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        void ValidateFormatterGeneralTestCases(string nodeName)
        {
            using (FastAFormatter formatter = new FastAFormatter(Constants.FastaTempFileName))
            {

                // Gets the actual sequence and the alphabet from the Xml
                string actualSequence = utilityObj.xmlUtil.GetTextValue(nodeName,
                    Constants.ExpectedSequenceNode);
                string formattedActualSequence = actualSequence.Replace("\r", "").Replace("\n", "").Replace(" ", "");

                string alphabet = utilityObj.xmlUtil.GetTextValue(nodeName,
                    Constants.AlphabetNameNode);

                // Logs information to the log file
                ApplicationLog.WriteLine(string.Format((IFormatProvider)null,
                    "FastA Formatter : Validating with Sequence '{0}' and Alphabet '{1}'.",
                    actualSequence, alphabet));

                // Replacing all the empty characters, Paragraphs and null entries added 
                // while formatting the xml.
                Sequence seqOriginal = new Sequence(Utility.GetAlphabet(alphabet),
                    encodingObj.GetBytes(actualSequence.Replace("\r", "").Replace("\n", "").Replace(" ", "")));
                seqOriginal.ID = "";
                Assert.IsNotNull(seqOriginal);

                // Use the formatter to write the original sequences to a temp file
                ApplicationLog.WriteLine(string.Format((IFormatProvider)null,
                    "FastA Formatter : Creating the Temp file '{0}'.",
                    Constants.FastaTempFileName));
                formatter.Write(seqOriginal);
                formatter.Close();

                // Read the new file, then compare the sequences
                IEnumerable<ISequence> seqsNew = null;
                using (FastAParser parserObj = new FastAParser(Constants.FastaTempFileName))
                {
                    parserObj.Alphabet = Utility.GetAlphabet(alphabet);
                    seqsNew = parserObj.Parse();

                    char[] seqString = seqsNew.ElementAt(0).Select(a => (char)a).ToArray();
                    string newSequence = new string(seqString);

                    Assert.IsNotNull(seqsNew);
                    ApplicationLog.WriteLine(string.Format((IFormatProvider)null,
                        "FastA Formatter : New Sequence is '{0}'.",
                        newSequence));

                    // Now compare the sequences.
                    int countNew = seqsNew.Count();
                    Assert.AreEqual(1, countNew);

                    ApplicationLog.WriteLine("The Number of sequences are matching.");
                    Assert.AreEqual(seqOriginal.ID, seqsNew.ElementAt(0).ID);
                    Assert.AreEqual(formattedActualSequence, newSequence);

                    Console.WriteLine(string.Format((IFormatProvider)null,
                        "FastA Formatter : The FASTA sequences '{0}' are matching with Format() method and is as expected.",
                        newSequence));
                    ApplicationLog.WriteLine(string.Format((IFormatProvider)null,
                        "FastA Formatter : The FASTA sequences '{0}' are matching with Format() method.",
                        newSequence));
                }

                // Passed all the tests, delete the tmp file. If we failed an Assert,
                // the tmp file will still be there in case we need it for debugging.
                File.Delete(Constants.FastaTempFileName);
                ApplicationLog.WriteLine("Deleted the temp file created.");
            }
        }

        /// <summary>
        /// Validates general FastA Parser test cases which are further Formatted
        /// with the xml node name specified.
        /// </summary>
        /// <param name="nodeName">xml node name.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        void ValidateParseFormatGeneralTestCases(string nodeName)
        {
            // Gets the expected sequence from the Xml
            string filePath = utilityObj.xmlUtil.GetTextValue(nodeName,
                Constants.FilePathNode);
            string alphabet = utilityObj.xmlUtil.GetTextValue(nodeName,
                Constants.AlphabetNameNode);
            Assert.IsTrue(File.Exists(filePath));

            // Logs information to the log file
            ApplicationLog.WriteLine(string.Format((IFormatProvider)null,
                "FastA Formatter : File Exists in the Path '{0}'.",
                filePath));
            string filepathTmp = "tmp.ffn";

            using (FastAParser parserObj = new FastAParser(filePath))
            {

                using (FastAFormatter formatter = new FastAFormatter(filepathTmp))
                {

                    // Read the original file
                    IEnumerable<ISequence> seqsOriginal = null;
                    parserObj.Alphabet = Utility.GetAlphabet(alphabet);
                    seqsOriginal = parserObj.Parse();
                    Assert.IsNotNull(seqsOriginal);

                    // Use the formatter to write the original sequences to a temp file                
                    ApplicationLog.WriteLine(string.Format((IFormatProvider)null,
                        "FastA Formatter : Creating the Temp file '{0}'.",
                        filepathTmp));
                    foreach (Sequence s in seqsOriginal)
                    {
                        formatter.Write(s);
                    }
                    formatter.Close();

                    using (FastAParser parserObjNew = new FastAParser(filepathTmp))
                    {

                        // Read the new file, then compare the sequences
                        IEnumerable<ISequence> seqsNew = null;
                        parserObjNew.Alphabet = Utility.GetAlphabet(alphabet);
                        seqsNew = parserObjNew.Parse();
                        Assert.IsNotNull(seqsNew);

                        char[] seqString = seqsNew.ElementAt(0).Select(a => (char)a).ToArray();
                        string newSequence = new string(seqString);

                        ApplicationLog.WriteLine(string.Format((IFormatProvider)null,
                            "FastA Formatter : New Sequence is '{0}'.",
                            newSequence));

                        // Now compare the sequences.
                        int countOriginal = seqsOriginal.Count();
                        int countNew = seqsNew.Count();
                        Assert.AreEqual(countOriginal, countNew);
                        ApplicationLog.WriteLine("FastA Formatter :The Number of sequences are matching.");

                        int i;
                        for (i = 0; i < countOriginal; i++)
                        {
                            Assert.AreEqual(seqsOriginal.ElementAt(i).ID, seqsNew.ElementAt(i).ID);
                            string orgSeq = new string(seqsOriginal.ElementAt(i).Select(a => (char)a).ToArray());
                            string newSeq = new string(seqsNew.ElementAt(i).Select(a => (char)a).ToArray());
                            Assert.AreEqual(orgSeq, newSeq);
                            Console.WriteLine(
                                string.Format((IFormatProvider)null,
                                "FastA Formatter : The FASTA sequences '{0}' are matching with Format() method and is as expected.",
                                seqsNew.ElementAt(i).ID));
                            ApplicationLog.WriteLine(
                                string.Format((IFormatProvider)null, "FastA Formatter : The FASTA sequences '{0}' are matching with Format() method.",
                                seqsNew.ElementAt(i).ID));
                        }

                        // Passed all the tests, delete the tmp file. If we failed an Assert,
                        // the tmp file will still be there in case we need it for debugging.
                        parserObjNew.Close();
                    }
                    File.Delete(filepathTmp);
                    ApplicationLog.WriteLine("Deleted the temp file created.");
                }
            }
        }

        #endregion Supporting Methods
    }
}
