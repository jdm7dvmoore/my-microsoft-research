﻿// *****************************************************************
//    Copyright (c) Microsoft. All rights reserved.
//    This code is licensed under the Apache License, Version 2.0.
//    THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
//    ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
//    IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
//    PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
// *****************************************************************

/****************************************************************************
 * PhylipBvtTestCases.cs
 * 
 *   This file contains the Phylip - Parsers and Formatters Bvt test cases.
 * 
***************************************************************************/

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Xml;
using System.Linq;

using Bio.Algorithms.Alignment;
using Bio.IO;
using Bio.IO.Phylip;
using Bio.TestAutomation.Util;
using Bio.Util.Logging;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bio;

namespace Bio.TestAutomation.IO.Phylip
{
    /// <summary>
    /// Phylip Bvt parser and formatter Test case implementation.
    /// </summary>
    [TestClass]
    public class PhylipBvtTestCases
    {

        #region Enums

        /// <summary>
        /// Additional parameters to validate different scenarios.
        /// </summary>
        enum ParserTestAttributes
        {
            Parse,
            ParseOne,
            ParseTextReader,
            ParseOneTextReader
        };

        /// <summary>
        /// Additional parameters to validate Common Sequence Parser scenarios.
        /// </summary>
        enum CommonSequenceParserAttributes
        {
            ParseRNA,
            ParseProtein
        };

        #endregion Enums

        #region Global Variables

        Utility utilityObj = new Utility(@"TestUtils\PhylipTestsConfig.xml");

        #endregion Global Variables

        #region Constructor

        /// <summary>
        /// Static constructor to open log and make other settings needed for test
        /// </summary>
        static PhylipBvtTestCases()
        {
            Trace.Set(Trace.SeqWarnings);
            if (!ApplicationLog.Ready)
            {
                ApplicationLog.Open("bio.automation.log");
            }
        }

        #endregion Constructor

        #region Phylip Parser BVT Test cases

        /// <summary>
        /// Parse a valid Phylip file (Small size sequence less than 35 kb) and 
        /// convert the same to one sequence using Parse(file-name) method and 
        /// validate with the expected sequence.
        /// Input : Phylip File
        /// Validation: Sequence Alignment list
        /// </summary>
        [TestMethod]
        [Priority(0)]
        [TestCategory("Priority0")]
        public void PhylipParserValidateParseFileName()
        {
            ParserGeneralTestCases(Constants.SmallSizePhylipNodeName,
                ParserTestAttributes.Parse);
        }

        /// <summary>
        /// Parse a valid Phylip file (Small size sequence less than 35 kb) and 
        /// convert the same to one sequence using ParseOne(file-name) method and 
        /// validate with the expected sequence.
        /// Input : Phylip File
        /// Validation: Sequence Alignment list
        /// </summary>
        [TestMethod]
        [Priority(0)]
        [TestCategory("Priority0")]
        public void PhylipParserValidateParseOneFileName()
        {
            ParserGeneralTestCases(Constants.SmallSizePhylipNodeName,
                ParserTestAttributes.ParseOne);
        }

        /// <summary>
        /// Parse a valid Phylip file (Small size sequence less than 35 kb) and 
        /// convert the same to one sequence using ParseOne(text-reader) method and 
        /// validate with the expected sequence.
        /// Input : Phylip File
        /// Validation: Sequence Alignment list
        /// </summary>
        [TestMethod]
        [Priority(0)]
        [TestCategory("Priority0")]
        public void PhylipParserValidateParseOneTextReader()
        {
            ParserGeneralTestCases(Constants.SmallSizePhylipNodeName,
                ParserTestAttributes.ParseOneTextReader);
        }

        /// <summary>
        /// Parse a valid Phylip file (Small size sequence less than 35 kb) and 
        /// convert the same to one sequence using Parse(text-reader) method and 
        /// validate with the expected sequence.
        /// Input : Phylip File
        /// Validation: Sequence Alignment list
        /// </summary>
        [TestMethod]
        [Priority(0)]
        [TestCategory("Priority0")]
        public void PhylipParserValidateParseTextReader()
        {
            ParserGeneralTestCases(Constants.SmallSizePhylipNodeName,
                ParserTestAttributes.ParseTextReader);
        }

        /// <summary>
        /// Parse a valida Phylip Parser object and validate its properties
        /// Input : Valide Object
        /// Output : Validatation of properties
        /// </summary>
        [TestMethod]
        [Priority(0)]
        [TestCategory("Priority0")]
        public void ValidatePhylipParserProperties()
        {
            PhylipParser parser = new PhylipParser();
            Assert.AreEqual(
                utilityObj.xmlUtil.GetTextValue(Constants.PhylipPropertyNode,
                Constants.PhylipDescriptionNode),
                parser.Description);
            Assert.AreEqual(
                utilityObj.xmlUtil.GetTextValue(Constants.PhylipPropertyNode,
                Constants.PhylipNameNode),
                parser.Name);
            Assert.AreEqual(null, parser.Alphabet);
        }

        #endregion Phylip Parser BVT Test cases

        #region Supported Methods

        /// <summary>
        /// Parsers the Phylip file for different test cases based
        /// on Additional parameter
        /// </summary>
        /// <param name="nodeName">Xml Node name</param>
        /// <param name="addParam">Additional parameter</param>
        void ParserGeneralTestCases(string nodeName,
            ParserTestAttributes addParam)
        {
            // Gets the Filename
            string filePath = utilityObj.xmlUtil.GetTextValue(
                nodeName, Constants.FilePathNode);

            Assert.IsFalse(string.IsNullOrEmpty(filePath));
            ApplicationLog.WriteLine(string.Format((IFormatProvider)null,
                "Phylip Parser BVT: Reading the File from location '{0}'", filePath));
            Console.WriteLine(string.Format((IFormatProvider)null,
                "Phylip Parser BVT: Reading the File from location '{0}'", filePath));

            // Get the rangelist after parsing.
            PhylipParser parserObj = new PhylipParser();

            IList<ISequenceAlignment> sequenceAlignmentList = null;
            ISequenceAlignment sequenceAlignment = null;

            // Gets the SequenceAlignment list based on the parameters.
            switch (addParam)
            {
                case ParserTestAttributes.Parse:
                    sequenceAlignmentList = parserObj.Parse(filePath);
                    break;
                case ParserTestAttributes.ParseOne:
                    sequenceAlignment = parserObj.ParseOne(filePath);
                    break;
                case ParserTestAttributes.ParseTextReader:
                    using (StreamReader rdrObj = new StreamReader(filePath))
                    {
                        sequenceAlignmentList = parserObj.Parse(rdrObj);
                    }
                    break;
                case ParserTestAttributes.ParseOneTextReader:
                    using (StreamReader rdrObj = new StreamReader(filePath))
                    {
                        sequenceAlignment = parserObj.ParseOne(rdrObj);
                    }
                    break;
                default:
                    break;
            }

            // Gets all the expected values from xml.
            IList<Dictionary<string, string>> expectedAlignmentList =
                new List<Dictionary<string, string>>();
            Dictionary<string, string> expectedAlignmentObj =
                new Dictionary<string, string>();

            XmlNode expectedAlignmentNodes = utilityObj.xmlUtil.GetNode(
                nodeName, Constants.ExpectedAlignmentNode);
            XmlNodeList alignNodes = expectedAlignmentNodes.ChildNodes;

            // Create a ISequenceAlignment List
            switch (addParam)
            {
                case ParserTestAttributes.ParseOne:
                case ParserTestAttributes.ParseOneTextReader:
                    sequenceAlignmentList = new List<ISequenceAlignment>();
                    sequenceAlignmentList.Add(sequenceAlignment);
                    break;
                default:
                    break;
            }

            foreach (XmlNode expectedAlignment in alignNodes)
            {
                expectedAlignmentObj[expectedAlignment.Name] =
                    expectedAlignment.InnerText;
            }

            expectedAlignmentList.Add(expectedAlignmentObj);

            Assert.IsTrue(CompareOutput(sequenceAlignmentList,
                expectedAlignmentList));
            ApplicationLog.WriteLine(
                "Phylip Parser BVT: Successfully validated all the Alignment Sequences");
            Console.WriteLine(
                "Phylip Parser BVT: Successfully validated all the Alignment Sequences");
        }

        /// <summary>
        /// Compare the actual output with expected output
        /// </summary>
        /// <param name="actualOutput">Actual output</param>
        /// <param name="expectedOutput">Expected output</param>
        /// <returns>True, if comparison is successful</returns>
        private static bool CompareOutput(
                 IList<ISequenceAlignment> actualOutput,
                 IList<Dictionary<string, string>> expectedOutput)
        {
            if (expectedOutput.Count != actualOutput.Count)
            {
                return false;
            }

            int alignmentIndex = 0;

            // Validate each output alignment
            foreach (ISequenceAlignment alignment in actualOutput)
            {
                Dictionary<string, string> expectedAlignment =
                    expectedOutput[alignmentIndex];

                foreach (Sequence actualSequence in alignment.AlignedSequences[0].Sequences)
                {
                    if (0 != string.Compare(new String(actualSequence.Select(a => (char)a).ToArray()),
                            expectedAlignment[actualSequence.ID], true,
                            CultureInfo.CurrentCulture))
                    {
                        Console.WriteLine("Failed for " + actualSequence.ID);
                        return false;
                    }
                }

                alignmentIndex++;
            }

            return true;
        }

        #endregion Supported Methods
    }
}