This directory is the root Attribution point for all the Open Source 
tools we use in our build process.  Each sub-directory has an URL 
linking to the original location where we downloaded the tool 
binaries and/or source from.  If a binary was not available from the 
main distribution point, the tool used to build MBI can be built 
from the sources in the appropriate directory.
	
	HtmlHelpCompiler: http://msdn.microsoft.com/en-us/library/ms669985
		Microsoft HTML Help 1.4 SDK: is the standarad help system for the windows platform. 
	IronPython : http://ironpython.codeplex.com/
		IronPython is an Open Source implementation of the Python programming language running under .NET and Silverlight.
	Sandcastle Help file Builder: http://shfb.codeplex.com/
		Sandcastle, created by Microsoft, is a tool used for creating MSDN-style documentation from .NET assemblies and their associated XML comments files. 
	Wix3: http://wix.sourceforge.net/
		The Windows Installer XML (WiX) is a toolset that builds Windows installation packages from XML source code. 