﻿// *********************************************************
// 
//     Copyright (c) Microsoft. All rights reserved.
//     This code is licensed under the Apache License, Version 2.0.
//     THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
//     ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
//     IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
//     PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
// 
// *********************************************************
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "Bio.Algorithms.Assembly.Padena.Utility")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA2210:AssembliesShouldHaveValidStrongNames")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1014:MarkAssembliesWithClsCompliant")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1824:MarkAssembliesWithNeutralResourcesLanguage")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed", Scope = "member", Target = "Bio.Algorithms.Assembly.Padena.DanglingLinksPurger.#.ctor(System.Int32,System.Int32)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed", Scope = "member", Target = "Bio.Algorithms.Assembly.Padena.DanglingLinksPurger.#ErodeGraphEnds(Bio.Algorithms.Assembly.Graph.DeBruijnGraph,System.Int32)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "context", Scope = "member", Target = "Bio.Algorithms.Assembly.Padena.PadenaAssembly.#.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed", Scope = "member", Target = "Bio.Algorithms.Assembly.Padena.Scaffold.GraphScaffoldBuilder.#BuildScaffold(System.Collections.Generic.IEnumerable`1<Bio.ISequence>,System.Collections.Generic.IList`1<Bio.ISequence>,System.Int32,System.Int32,System.Int32)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Scope = "member", Target = "Bio.Algorithms.Assembly.Padena.Scaffold.GraphScaffoldBuilder.#GenerateContigOverlapGraph(System.Collections.Generic.IList`1<Bio.ISequence>)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed", Scope = "member", Target = "Bio.Algorithms.Assembly.Padena.Scaffold.OrientationBasedMatePairFilter.#FilterPairedReads(Bio.Algorithms.Assembly.Padena.Scaffold.ContigMatePairs,System.Int32)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Body", Scope = "member", Target = "Bio.Algorithms.Assembly.Padena.Scaffold.PathPurger.#RemoveOverlappingPaths(Bio.Algorithms.Assembly.Padena.Scaffold.ScaffoldPath,Bio.Algorithms.Assembly.Padena.Scaffold.ScaffoldPath)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed", Scope = "member", Target = "Bio.Algorithms.Assembly.Padena.Scaffold.TracePath.#FindPaths(Bio.Algorithms.Assembly.Padena.Scaffold.ContigOverlapGraph.ContigGraph,Bio.Algorithms.Assembly.Padena.Scaffold.ContigMatePairs,System.Int32,System.Int32)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2208:InstantiateArgumentExceptionsCorrectly", Scope = "member", Target = "Bio.Algorithms.Assembly.Padena.Scaffold.TracePath.#FindPaths(Bio.Algorithms.Assembly.Padena.Scaffold.ContigOverlapGraph.ContigGraph,Bio.Algorithms.Assembly.Padena.Scaffold.ContigMatePairs,System.Int32,System.Int32)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Scope = "member", Target = "Bio.Algorithms.Assembly.Padena.ParallelDeNovoAssembler.#SequenceReads")]
