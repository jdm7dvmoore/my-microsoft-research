﻿// *********************************************************
// 
//     Copyright (c) Microsoft. All rights reserved.
//     This code is licensed under the Apache License, Version 2.0.
//     THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
//     ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
//     IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
//     PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
// 
// *********************************************************
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "Bio.Algorithms.Assembly.Comparative")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA2210:AssembliesShouldHaveValidStrongNames")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1014:MarkAssembliesWithClsCompliant")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1824:MarkAssembliesWithNeutralResourcesLanguage")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "Bio.Algorithms.Assembly.Comparative.ComparativeGenomeAssembler.#BreakLength")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "Bio.Algorithms.Assembly.Comparative.ComparativeGenomeAssembler.#ConsensusGenerator(System.Collections.Generic.IEnumerable`1<Bio.Algorithms.Alignment.DeltaAlignment>)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "Bio.Algorithms.Assembly.Comparative.ComparativeGenomeAssembler.#LayoutRefinment(System.Collections.Generic.IList`1<Bio.Algorithms.Alignment.DeltaAlignment>)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "Bio.Algorithms.Assembly.Comparative.ComparativeGenomeAssembler.#Name")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "Bio.Algorithms.Assembly.Comparative.ComparativeGenomeAssembler.#RepeatResolution(System.Collections.Generic.IList`1<System.Collections.Generic.IEnumerable`1<Bio.Algorithms.Alignment.DeltaAlignment>>)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists", Scope = "member", Target = "Bio.Algorithms.Assembly.Comparative.RepeatResolver.#ResolveAmbiguity(System.Collections.Generic.IList`1<System.Collections.Generic.IEnumerable`1<Bio.Algorithms.Alignment.DeltaAlignment>>)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Scope = "member", Target = "Bio.Algorithms.Assembly.Comparative.RepeatResolver.#ResolveAmbiguity(System.Collections.Generic.IList`1<System.Collections.Generic.IEnumerable`1<Bio.Algorithms.Alignment.DeltaAlignment>>)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "Bio.Algorithms.Assembly.Comparative.ComparativeGenomeAssembler.#Description")]
