Microsoft Biology Foundation: Readme.txt
Version 2.0 Beta1 , April 2011

The Microsoft Biology Foundation (MBF) is an open source, reusable .NET library and application programming interface (API) for bioinformatics research.

The Microsoft Biology Foundation is available at http://mbf.codeplex.com.  It is licensed under the OSI approved Apache License , found here:  http://mbf.codeplex.com/license.

KNOWN ISSUES
============

- It is highly recommended to install the MBF library before installing the tools if development work is going to be performed using the library.  
It is also highly recommended to uninstall the tools prior to uninstalling the library.  Failure to do so could cause components to end up on multiple locations.


