﻿// *********************************************************
// 
//     Copyright (c) Microsoft. All rights reserved.
//     This code is licensed under the Apache License, Version 2.0.
//     THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
//     ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
//     IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
//     PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
// 
// *********************************************************
namespace SequenceAssembler
{
    /// <summary>
    /// ScaleMarkerLarge is a class which represents a intermediate spike on the scale.
    /// Multiple ScaleMarkerSmall classes will be placed next to ScaleMarkerLarge 
    /// will simualte a scale.
    /// </summary>
    public partial class ScaleMarkerLarge
    {
        /// <summary>
        /// Initializes a new instance of the ScaleMarkerLarge class.
        /// </summary>
        public ScaleMarkerLarge()
        {
            this.InitializeComponent();
        }
    }
}