﻿// *********************************************************
// 
//     Copyright (c) Microsoft. All rights reserved.
//     This code is licensed under the Apache License, Version 2.0.
//     THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
//     ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
//     IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
//     PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
// 
// *********************************************************
namespace SequenceAssembler
{
    #region -- Using Directives --

    using System;
    using System.Windows;
    using System.Windows.Input;

    #endregion -- Using Directives --

    /// <summary>
    /// Specifies the buttons that are displayed on a MessageDialog box. 
    /// </summary>
    public enum MessageDialogButton
    {
        /// <summary>
        ///  The MessageDialog displays Yes and No buttons.
        /// </summary>
        YesNo,

        /// <summary>
        /// The MessageDialog displays an OK button.
        /// </summary>
        OK
    }
}
