﻿// *********************************************************
// 
//     Copyright (c) Microsoft. All rights reserved.
//     This code is licensed under the Apache License, Version 2.0.
//     THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
//     ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
//     IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
//     PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
// 
// *********************************************************
using System.Resources;
using System.Runtime.InteropServices;
using System.Reflection;

[assembly: AssemblyTitle("Microsoft Biology Foundation Sequence Assembler")]
[assembly: AssemblyDescription("Microsoft Biology Foundation Sequence Assembler")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Microsoft Corporation")]
[assembly: AssemblyProduct("Microsoft Biology Foundation")]
[assembly: AssemblyCopyright("Copyright © 2010 Microsoft Corporation")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: NeutralResourcesLanguageAttribute("en-US", UltimateResourceFallbackLocation.MainAssembly)]
[assembly: ComVisibleAttribute(false)]

[assembly: AssemblyVersion("2.0.*")]
