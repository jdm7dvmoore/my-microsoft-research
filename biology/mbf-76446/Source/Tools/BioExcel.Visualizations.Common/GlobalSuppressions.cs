﻿// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.
//
// To add a suppression to this file, right-click the message in the 
// Error List, point to "Suppress Message(s)", and click 
// "In Project Suppression File".
// You do not need to add suppressions to this file manually.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists", Scope = "member", Target = "BioExcel.Visualizations.Common.AlignerInputEventArgs.#Sequences")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists", Scope = "member", Target = "BioExcel.Visualizations.Common.AssemblyInputEventArgs.#.ctor(System.Collections.Generic.List`1<Bio.ISequence>,Bio.Algorithms.Alignment.ISequenceAligner)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists", Scope = "member", Target = "BioExcel.Visualizations.Common.AssemblyInputEventArgs.#Sequences")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists", Scope = "member", Target = "BioExcel.Visualizations.Common.SequenceSelectionComplete.#Invoke(System.Collections.Generic.List`1<Bio.ISequence>,System.Object[])")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists", Scope = "member", Target = "BioExcel.Visualizations.Common.InputSequenceRangeSelectionEventArg.#.ctor(System.Collections.Generic.Dictionary`2<Bio.SequenceRangeGrouping,BioExcel.Visualizations.Common.GroupData>,System.Collections.Generic.List`1<Bio.SequenceRangeGrouping>,System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Object[])")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Scope = "member", Target = "BioExcel.Visualizations.Common.GroupData.#.ctor(Bio.SequenceRangeGrouping,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<Bio.ISequenceRange,System.String>>)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Scope = "member", Target = "BioExcel.Visualizations.Common.GroupData.#Metadata")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1014:MarkAssembliesWithClsCompliant")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1819:PropertiesShouldNotReturnArrays", Scope = "member", Target = "BioExcel.Visualizations.Common.InputSequenceRangeSelectionEventArg.#ArgsForCallback")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1824:MarkAssembliesWithNeutralResourcesLanguage")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA2210:AssembliesShouldHaveValidStrongNames")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Scope = "member", Target = "BioExcel.Visualizations.Common.AlignerInputEventArgs.#Sequences")]
