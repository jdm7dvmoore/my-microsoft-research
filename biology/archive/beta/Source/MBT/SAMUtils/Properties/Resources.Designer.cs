﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.1
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SAMUtils.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("SAMUtils.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Usage:   SAMUtils.exe Chimera [options] in.bam MeanValue StdDeviationValue.
        /// </summary>
        internal static string ChimericRegionsHelp {
            get {
                return ResourceManager.GetString("ChimericRegionsHelp", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Usage:    SAMUtils.exe CoverageProfile [options] in.bam.
        /// </summary>
        internal static string DNACoverageHelp {
            get {
                return ResourceManager.GetString("DNACoverageHelp", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Usage:    SAMUtils.exe NucleotideDistribution[options] in.bam .
        /// </summary>
        internal static string DNAPossibleOccurenceHelp {
            get {
                return ResourceManager.GetString("DNAPossibleOccurenceHelp", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Input File is empty..
        /// </summary>
        internal static string EmptyFile {
            get {
                return ResourceManager.GetString("EmptyFile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Header Absent, Please reference list file.
        /// </summary>
        internal static string HeaderAbsent {
            get {
                return ResourceManager.GetString("HeaderAbsent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Header Missing.
        /// </summary>
        internal static string HeaderMissing {
            get {
                return ResourceManager.GetString("HeaderMissing", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Usage:  SAMUtils.exe import [options] [out.bam|out.sam] in.sam|in.bam
        ///        Import converts SAM &lt;=&gt; BAM file formats.
        ///
        ///Options:
        ///
        ///-r:FILE  File path of TAB delimited file.(in.ref_list).
        /// </summary>
        internal static string ImportHelp {
            get {
                return ResourceManager.GetString("ImportHelp", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Incorrect Header file format.
        /// </summary>
        internal static string IncorrectHeaderFile {
            get {
                return ResourceManager.GetString("IncorrectHeaderFile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Usage:	SAMUtils.exe index  in.bam [out.index]
        ///	Generates Index file for given BAM file..
        /// </summary>
        internal static string IndexHelp {
            get {
                return ResourceManager.GetString("IndexHelp", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid BAM file..
        /// </summary>
        internal static string InvalidBAMFile {
            get {
                return ResourceManager.GetString("InvalidBAMFile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid SAM File..
        /// </summary>
        internal static string InvalidSAMFile {
            get {
                return ResourceManager.GetString("InvalidSAMFile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Usage:    SAMUtils.exe LengthAnomaly [options] in.bam MeanValue StdDeviationValue.
        /// </summary>
        internal static string LengthAnomalyHelp {
            get {
                return ResourceManager.GetString("LengthAnomalyHelp", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Usage:      SAMUtils.exe [options] out.bam in1.bam in2.bam [..]
        ///
        ///Options:
        ///-n          sort by read names
        ///-h:FILE     copy the header in FILE(SAM/BAM format) to out.bam [in1.bam]
        ///
        ///Note: Samtool merge does not reconstruct the @RG dictionary in the header.
        ///      Users must provide the correct header with -h.
        /// </summary>
        internal static string MergeHelp {
            get {
                return ResourceManager.GetString("MergeHelp", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Usage: SAMUtils.exe orphan [options] in.bam MeanValue StdDeviationValue.
        /// </summary>
        internal static string OrphanRegionsHelp {
            get {
                return ResourceManager.GetString("OrphanRegionsHelp", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reference File not in correct format.
        /// </summary>
        internal static string ReferenceFile {
            get {
                return ResourceManager.GetString("ReferenceFile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Program:   SAMUtils
        ///Usage:	   SAMUtils.exe command [options]
        ///
        ///Command:   Import  SAM &lt;=&gt; BAM conversion
        ///           Sort    Sort alignment file
        ///           Index   Index alignment
        ///           Merge   Merge sorted alignments
        ///           View    View output on console.
        /// </summary>
        internal static string SAMUtilHelp {
            get {
                return ResourceManager.GetString("SAMUtilHelp", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Usage:     SAMUtils.exe sort [options] in.bam out.bam
        ///
        ///Options:
        ///-n         sort by read names.
        /// </summary>
        internal static string SortHelp {
            get {
                return ResourceManager.GetString("SortHelp", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Usage:    SAMUtils.exe view [options] in.bam|in.sam
        ///
        ///Default:  This command assumes the file on the command line is
        ///          in the BAM format and it prints the alignments in SAM.
        ///
        ///Options:
        ///
        ///-b       Output BAM
        ///-h       Print header for the SAM output
        ///-H       Print header only (no alignments)
        ///-S       Input is SAM format
        ///-u       Uncompressed BAM output
        ///-x       Output flag in HEX
        ///-X       Output flag in string
        ///-o:FILE  Output file name
        ///-f:INT   Required flag
        ///-F:INT   Filtering flag
        ///-q:IN [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string ViewHelp {
            get {
                return ResourceManager.GetString("ViewHelp", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot write BAM output.
        /// </summary>
        internal static string WriteBAM {
            get {
                return ResourceManager.GetString("WriteBAM", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot write SAM output.
        /// </summary>
        internal static string WriteSAM {
            get {
                return ResourceManager.GetString("WriteSAM", resourceCulture);
            }
        }
    }
}
