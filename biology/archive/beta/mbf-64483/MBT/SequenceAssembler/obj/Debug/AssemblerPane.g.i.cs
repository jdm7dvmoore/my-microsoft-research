﻿#pragma checksum "..\..\AssemblerPane.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "BADA9580CE1676258F886021EF620469"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.1
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using SequenceAssembler;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace SequenceAssembler {
    
    
    /// <summary>
    /// AssemblerPane
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
    public partial class AssemblerPane : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 225 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Expander sequenceViewExpander;
        
        #line default
        #line hidden
        
        
        #line 233 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid AssemblyGrid;
        
        #line default
        #line hidden
        
        
        #line 259 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid sequenceViewer;
        
        #line default
        #line hidden
        
        
        #line 260 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal SequenceAssembler.ConsensusCustomView customSequenceView;
        
        #line default
        #line hidden
        
        
        #line 264 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DockPanel assemblePane;
        
        #line default
        #line hidden
        
        
        #line 265 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnAssemble;
        
        #line default
        #line hidden
        
        
        #line 266 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnAlign;
        
        #line default
        #line hidden
        
        
        #line 268 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel sequenceViewZoomPanel;
        
        #line default
        #line hidden
        
        
        #line 270 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Slider sequenceViewZoomSlider;
        
        #line default
        #line hidden
        
        
        #line 275 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel assembleProgress;
        
        #line default
        #line hidden
        
        
        #line 278 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock assemblyInProgressText;
        
        #line default
        #line hidden
        
        
        #line 279 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCancelAssembly;
        
        #line default
        #line hidden
        
        
        #line 300 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TreeView sequenceTree;
        
        #line default
        #line hidden
        
        
        #line 308 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Expander consensusViewExpander;
        
        #line default
        #line hidden
        
        
        #line 342 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabControl assemblyResultTab;
        
        #line default
        #line hidden
        
        
        #line 346 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stkAssemblyReport;
        
        #line default
        #line hidden
        
        
        #line 353 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtInputSequence;
        
        #line default
        #line hidden
        
        
        #line 361 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtAlignmentAlgorithm;
        
        #line default
        #line hidden
        
        
        #line 373 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtTotalTime;
        
        #line default
        #line hidden
        
        
        #line 381 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtStartTime;
        
        #line default
        #line hidden
        
        
        #line 389 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtEndTime;
        
        #line default
        #line hidden
        
        
        #line 396 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock numberOfContigsLabel;
        
        #line default
        #line hidden
        
        
        #line 397 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtContigs;
        
        #line default
        #line hidden
        
        
        #line 399 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid txtUnAssembledCountGrid;
        
        #line default
        #line hidden
        
        
        #line 405 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtUnAssembled;
        
        #line default
        #line hidden
        
        
        #line 407 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid txtLengthGrid;
        
        #line default
        #line hidden
        
        
        #line 413 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtLength;
        
        #line default
        #line hidden
        
        
        #line 419 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabItem customViewTab;
        
        #line default
        #line hidden
        
        
        #line 420 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal SequenceAssembler.ConsensusCustomView consensusCustomView;
        
        #line default
        #line hidden
        
        
        #line 434 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stkWebProgressBar;
        
        #line default
        #line hidden
        
        
        #line 438 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button cancelServiceBtn;
        
        #line default
        #line hidden
        
        
        #line 445 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DockPanel stkWebService;
        
        #line default
        #line hidden
        
        
        #line 447 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cmbWebServices;
        
        #line default
        #line hidden
        
        
        #line 448 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button executeServiceBtn;
        
        #line default
        #line hidden
        
        
        #line 450 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel consensusViewZoomPanel;
        
        #line default
        #line hidden
        
        
        #line 452 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Slider consensusViewZoomSlider;
        
        #line default
        #line hidden
        
        
        #line 471 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock consensusTreeViewCaption;
        
        #line default
        #line hidden
        
        
        #line 473 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TreeView consensusTree;
        
        #line default
        #line hidden
        
        
        #line 474 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TreeViewItem treeViewConsensus;
        
        #line default
        #line hidden
        
        
        #line 483 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Expander blastResultsExpander;
        
        #line default
        #line hidden
        
        
        #line 484 "..\..\AssemblerPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal SequenceAssembler.BlastPane webServicePresenter;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/SequenceAssembler;component/assemblerpane.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\AssemblerPane.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.sequenceViewExpander = ((System.Windows.Controls.Expander)(target));
            return;
            case 2:
            this.AssemblyGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.sequenceViewer = ((System.Windows.Controls.Grid)(target));
            return;
            case 4:
            this.customSequenceView = ((SequenceAssembler.ConsensusCustomView)(target));
            return;
            case 5:
            this.assemblePane = ((System.Windows.Controls.DockPanel)(target));
            return;
            case 6:
            this.btnAssemble = ((System.Windows.Controls.Button)(target));
            return;
            case 7:
            this.btnAlign = ((System.Windows.Controls.Button)(target));
            return;
            case 8:
            this.sequenceViewZoomPanel = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 9:
            this.sequenceViewZoomSlider = ((System.Windows.Controls.Slider)(target));
            
            #line 270 "..\..\AssemblerPane.xaml"
            this.sequenceViewZoomSlider.ValueChanged += new System.Windows.RoutedPropertyChangedEventHandler<double>(this.OnSequenceViewZoomSliderValueChanged);
            
            #line default
            #line hidden
            return;
            case 10:
            this.assembleProgress = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 11:
            this.assemblyInProgressText = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 12:
            this.btnCancelAssembly = ((System.Windows.Controls.Button)(target));
            return;
            case 13:
            this.sequenceTree = ((System.Windows.Controls.TreeView)(target));
            return;
            case 14:
            this.consensusViewExpander = ((System.Windows.Controls.Expander)(target));
            return;
            case 15:
            this.assemblyResultTab = ((System.Windows.Controls.TabControl)(target));
            return;
            case 16:
            this.stkAssemblyReport = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 17:
            this.txtInputSequence = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 18:
            this.txtAlignmentAlgorithm = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 19:
            this.txtTotalTime = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 20:
            this.txtStartTime = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 21:
            this.txtEndTime = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 22:
            this.numberOfContigsLabel = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 23:
            this.txtContigs = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 24:
            this.txtUnAssembledCountGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 25:
            this.txtUnAssembled = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 26:
            this.txtLengthGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 27:
            this.txtLength = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 28:
            this.customViewTab = ((System.Windows.Controls.TabItem)(target));
            return;
            case 29:
            this.consensusCustomView = ((SequenceAssembler.ConsensusCustomView)(target));
            return;
            case 30:
            this.stkWebProgressBar = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 31:
            this.cancelServiceBtn = ((System.Windows.Controls.Button)(target));
            return;
            case 32:
            this.stkWebService = ((System.Windows.Controls.DockPanel)(target));
            return;
            case 33:
            this.cmbWebServices = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 34:
            this.executeServiceBtn = ((System.Windows.Controls.Button)(target));
            return;
            case 35:
            this.consensusViewZoomPanel = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 36:
            this.consensusViewZoomSlider = ((System.Windows.Controls.Slider)(target));
            
            #line 452 "..\..\AssemblerPane.xaml"
            this.consensusViewZoomSlider.ValueChanged += new System.Windows.RoutedPropertyChangedEventHandler<double>(this.OnConsensusViewZoomSliderValueChanged);
            
            #line default
            #line hidden
            return;
            case 37:
            this.consensusTreeViewCaption = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 38:
            this.consensusTree = ((System.Windows.Controls.TreeView)(target));
            return;
            case 39:
            this.treeViewConsensus = ((System.Windows.Controls.TreeViewItem)(target));
            return;
            case 40:
            this.blastResultsExpander = ((System.Windows.Controls.Expander)(target));
            return;
            case 41:
            this.webServicePresenter = ((SequenceAssembler.BlastPane)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

