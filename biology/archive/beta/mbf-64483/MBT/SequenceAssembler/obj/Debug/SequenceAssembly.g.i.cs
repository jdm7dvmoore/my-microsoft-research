﻿#pragma checksum "..\..\SequenceAssembly.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "FC61E8520DE7EA1C8F8DB1FCB0656DBC"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.1
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using SequenceAssembler;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace SequenceAssembler {
    
    
    /// <summary>
    /// SequenceAssembly
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
    public partial class SequenceAssembly : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 62 "..\..\SequenceAssembly.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal SequenceAssembler.FileMenu fileMenu;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\SequenceAssembly.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem optionsMenu;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\SequenceAssembly.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem optionsMenuChangeColors;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\SequenceAssembly.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem optionsMenuAssociateFileTypes;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\SequenceAssembly.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem helpMenu;
        
        #line default
        #line hidden
        
        
        #line 70 "..\..\SequenceAssembly.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem menuUserGuide;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\SequenceAssembly.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem menuAbout;
        
        #line default
        #line hidden
        
        
        #line 82 "..\..\SequenceAssembly.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal SequenceAssembler.AssemblerPane assembler;
        
        #line default
        #line hidden
        
        
        #line 84 "..\..\SequenceAssembly.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Rectangle overlayrect;
        
        #line default
        #line hidden
        
        
        #line 92 "..\..\SequenceAssembly.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock applicationHelpText;
        
        #line default
        #line hidden
        
        
        #line 95 "..\..\SequenceAssembly.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border panelDialog;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/SequenceAssembler;component/sequenceassembly.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\SequenceAssembly.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.fileMenu = ((SequenceAssembler.FileMenu)(target));
            return;
            case 2:
            this.optionsMenu = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 3:
            this.optionsMenuChangeColors = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 4:
            this.optionsMenuAssociateFileTypes = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 5:
            this.helpMenu = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 6:
            this.menuUserGuide = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 7:
            this.menuAbout = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 8:
            this.assembler = ((SequenceAssembler.AssemblerPane)(target));
            return;
            case 9:
            this.overlayrect = ((System.Windows.Shapes.Rectangle)(target));
            return;
            case 10:
            this.applicationHelpText = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 11:
            this.panelDialog = ((System.Windows.Controls.Border)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

