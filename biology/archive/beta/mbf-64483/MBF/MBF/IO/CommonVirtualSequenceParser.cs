﻿// *****************************************************************
//    Copyright (c) Microsoft. All rights reserved.
//    This code is licensed under the Microsoft Public License.
//    THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
//    ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
//    IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
//    PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
// *****************************************************************

namespace MBF.IO
{
    /// <summary>
    /// This is a class that provides some basic operations common to virtual sequence
    /// parsers. It is meant to be used as private member inside the parser implementations
    /// if the implementer wants to make use of some common behavior across parsers.
    /// </summary>
    public class CommonVirtualSequenceParser
    {
        /// <summary>
        /// Initializes a new instance of the CommonVirtualSequenceParser class.
        /// </summary>
        public CommonVirtualSequenceParser()
        {
        }
    }
}
