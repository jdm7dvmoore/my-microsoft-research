This directory contains the data for the BLOSUM series of similarity matrices.

BLOSUM matrices reference:
Available at http://www.ncbi.nlm.nih.gov/IEB/ToolBox/C_DOC/lxr/source/data/BLOSUM50.
Available at ftp://ftp.ncbi.nih.gov/blast/matrices/IDENTITY.
 
TestIupacNA.txt and TestNcbiStdAA.txt were manually created and has no references

