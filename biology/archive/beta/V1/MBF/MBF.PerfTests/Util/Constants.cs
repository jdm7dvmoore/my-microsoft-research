﻿// *****************************************************************
//    Copyright (c) Microsoft. All rights reserved.
//    This code is licensed under the Microsoft Public License.
//    THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
//    ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
//    IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
//    PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
// *****************************************************************

namespace MBF.PerfTests.Util
{
    internal static class Constants
    {
        internal const string ObjectModelNodeName = "ObjectModel";
        internal const string FilePathNode = "FilePath";
        internal const string AlphabetNode = "Alphabet";
        internal const string SequenceIDNode = "SequenceID";
        internal const string StartIndexNode = "StartIndex";
        internal const string EndIndexNode = "EndIndex";
        internal const string StartLineNode = "StartLine";
        internal const string SequenceRangeToRead = "SequenceRangeToRead";

        internal const string MUMmerNodeName = "MUMmer";
        internal const string RefFilePathNode = "RefFilePath";
        internal const string QueryFilePathNode = "QueryFilePath";
        internal const string SMFilePathNode = "SMFilePath";
        internal const string MUMLengthNode = "MUMLength";
        internal const string PamsamNode = "Pamsam";

        internal const string PaDeNANodeName = "PaDeNA";
        internal const string AlignmentAlgorithmNodeName = "AlignmentAlgorithm";
        internal const string BAMParserNode = "BAMParser";
        internal const int VistaMinorVersion = 0;
        internal const int Win7MinorVersion = 1;
        internal const int WinXPMinorVersion = 1;
        internal const int WinNTMajorVersion = 6;
        internal const int WinMajorVersion = 5;
        internal const int Win2K3MinorVersion = 2;
        internal const int Win2KMinorVersion = 0;
        internal const int Win2K8MinorVersion = 3;
    }
}
