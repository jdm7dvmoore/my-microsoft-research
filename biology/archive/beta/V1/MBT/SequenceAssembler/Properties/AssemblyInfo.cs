﻿// ------------------------------------------------------------------------------
// <copyright file="AssemblyInfo.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
// <summary>
// Assembly information of this dll.
// </summary>
// ------------------------------------------------------------------------------
using System.Resources;
using System.Runtime.InteropServices;
using System.Reflection;

[assembly: AssemblyTitle("Microsoft Biology Foundation Sequence Assembler")]
[assembly: AssemblyDescription("Microsoft Biology Foundation Sequence Assembler")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Microsoft Corporation")]
[assembly: AssemblyProduct("Microsoft Biology Foundation")]
[assembly: AssemblyCopyright("Copyright © 2010 Microsoft Corporation")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: NeutralResourcesLanguageAttribute("en-US", UltimateResourceFallbackLocation.MainAssembly)]
[assembly: ComVisibleAttribute(false)]
