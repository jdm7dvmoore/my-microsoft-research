.NET Bio: Readme.txt
Version 1.0 June 2011

The .NET Bio is an open source, reusable .NET library and application programming interface (API) 
for bioinformatics research.

The .NET Bio is available at http://bio.codeplex.com.  
It is licensed under the OSI approved Apache License , located at:  http://bio.codeplex.com/license.

After downloading and installing Bidoex Framework, the SDK �Tools� folder 
containing the the tools and their source code by default is at:
	$\Program Files (x86)\.NET Bio\1.0\SDK\Tools\

The source code for each tool is in the specific tool's folder,
	for example, $\Program Files (x86)\.NET Bio\1.0\SDK\Tools\PadenaUtil

How to use the tool's source code 
=================================
1. Double click the specific tool's source code folder:
	for example, for Padena click on:
	$\Program Files (x86)\.NET Bio\1.0\SDK\Tools\PadenaUtil
2. Create a Visual C# Console Application project using Visual Studio 2010.
3. Add the dll(s) you require from the source code folder, as References to the project created in step 2.
	Bio.dll at $\Program Files (x86)\.NET Bio\1.0\Tools\Bin	 
4. Add the source code files (.cs files) from the source folder (step 1) to your project in step 2.
5. Make any edits you require.
6. Compile the project.

