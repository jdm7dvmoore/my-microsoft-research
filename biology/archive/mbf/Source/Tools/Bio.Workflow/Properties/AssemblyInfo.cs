﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Bio.Workflow")]
[assembly: AssemblyDescription("Bio Workflow")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Bio.Workflow")]
[assembly: AssemblyCopyright("Copyright (c) 2011, The Outercurve Foundation.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using '*'.
[assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: ComVisible(false)]


//NOTE: When updating the namespaces in the project please add new or update existing the XmlnsDefinitionAttribute
//You can add additional attributes in order to map any additional namespaces you have in the project
//[assembly: System.Workflow.ComponentModel.Serialization.XmlnsDefinition("http://schemas.com/Bio.Workflow", "Bio.Workflow")]
