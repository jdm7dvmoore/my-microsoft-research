.NET Bio: Readme.txt
Version 1.0, June 2011

.NET Bio is an open source, reusable .NET library and application programming interface (API) for bioinformatics research.

.NET Bio is available at http://bio.codeplex.com.  It is licensed under the OSI approved Apache 2.0 License , found here:  http://bio.codeplex.com/license.


KNOWN ISSUES
============
- Current issues are listed: http://bio.codeplex.com/workitem/list/basic
- We currently have a limit of a sequence can be of at most 2GB in length. 